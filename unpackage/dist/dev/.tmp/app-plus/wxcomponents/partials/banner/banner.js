const app = getApp();
Component({
  properties: {
    data: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {},
    sysWidth:"",
    setting:{},
    imgs:null,
  },
  ready:function(){
    let that=this;
    that.setData({
      sysWidth: app.globalData.sysWidth,
      setting:app.globalData.setting
    });
    console.log("=====banner--data=====", that.data.data)
  },
  methods: {
    imageLoad: function (e) {
      let that=this;
      console.log("=====imageLoad====",e,that.data.data.jsonData.height,that.data.sysWidth)
      let index = e.currentTarget.dataset.index;
      let width = e.detail.width;
      let height = e.detail.height;
      let imgData = that.data.data.jsonData.images;
      let fixedHeght = Number(that.data.data.jsonData.height) * that.data.sysWidth;
      imgData[index].height = fixedHeght;
      imgData[index].width = (width / height ) * fixedHeght;
      that.setData({ imgs: imgData })
      console.log("=====imageLoad====", that.data.imgs)
    },
    tolinkUrl: function (event) {
      console.log(event.currentTarget.dataset.link)
      app.globalData.linkEvent(event.currentTarget.dataset.link);
    }
  },
})