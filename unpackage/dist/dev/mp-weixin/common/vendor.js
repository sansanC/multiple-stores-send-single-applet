(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["common/vendor"],{

/***/ 1:
/*!************************************************************!*\
  !*** ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.createApp = createApp;exports.createComponent = createComponent;exports.createPage = createPage;exports.createSubpackageApp = createSubpackageApp;exports.default = void 0;var _vue = _interopRequireDefault(__webpack_require__(/*! vue */ 2));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}function ownKeys(object, enumerableOnly) {var keys = Object.keys(object);if (Object.getOwnPropertySymbols) {var symbols = Object.getOwnPropertySymbols(object);if (enumerableOnly) symbols = symbols.filter(function (sym) {return Object.getOwnPropertyDescriptor(object, sym).enumerable;});keys.push.apply(keys, symbols);}return keys;}function _objectSpread(target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i] != null ? arguments[i] : {};if (i % 2) {ownKeys(Object(source), true).forEach(function (key) {_defineProperty(target, key, source[key]);});} else if (Object.getOwnPropertyDescriptors) {Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));} else {ownKeys(Object(source)).forEach(function (key) {Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));});}}return target;}function _slicedToArray(arr, i) {return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();}function _nonIterableRest() {throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _iterableToArrayLimit(arr, i) {if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;var _arr = [];var _n = true;var _d = false;var _e = undefined;try {for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {_arr.push(_s.value);if (i && _arr.length === i) break;}} catch (err) {_d = true;_e = err;} finally {try {if (!_n && _i["return"] != null) _i["return"]();} finally {if (_d) throw _e;}}return _arr;}function _arrayWithHoles(arr) {if (Array.isArray(arr)) return arr;}function _defineProperty(obj, key, value) {if (key in obj) {Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });} else {obj[key] = value;}return obj;}function _toConsumableArray(arr) {return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();}function _nonIterableSpread() {throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _unsupportedIterableToArray(o, minLen) {if (!o) return;if (typeof o === "string") return _arrayLikeToArray(o, minLen);var n = Object.prototype.toString.call(o).slice(8, -1);if (n === "Object" && o.constructor) n = o.constructor.name;if (n === "Map" || n === "Set") return Array.from(o);if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);}function _iterableToArray(iter) {if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter);}function _arrayWithoutHoles(arr) {if (Array.isArray(arr)) return _arrayLikeToArray(arr);}function _arrayLikeToArray(arr, len) {if (len == null || len > arr.length) len = arr.length;for (var i = 0, arr2 = new Array(len); i < len; i++) {arr2[i] = arr[i];}return arr2;}

var _toString = Object.prototype.toString;
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isFn(fn) {
  return typeof fn === 'function';
}

function isStr(str) {
  return typeof str === 'string';
}

function isPlainObject(obj) {
  return _toString.call(obj) === '[object Object]';
}

function hasOwn(obj, key) {
  return hasOwnProperty.call(obj, key);
}

function noop() {}

/**
                    * Create a cached version of a pure function.
                    */
function cached(fn) {
  var cache = Object.create(null);
  return function cachedFn(str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str));
  };
}

/**
   * Camelize a hyphen-delimited string.
   */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) {return c ? c.toUpperCase() : '';});
});

var HOOKS = [
'invoke',
'success',
'fail',
'complete',
'returnValue'];


var globalInterceptors = {};
var scopedInterceptors = {};

function mergeHook(parentVal, childVal) {
  var res = childVal ?
  parentVal ?
  parentVal.concat(childVal) :
  Array.isArray(childVal) ?
  childVal : [childVal] :
  parentVal;
  return res ?
  dedupeHooks(res) :
  res;
}

function dedupeHooks(hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res;
}

function removeHook(hooks, hook) {
  var index = hooks.indexOf(hook);
  if (index !== -1) {
    hooks.splice(index, 1);
  }
}

function mergeInterceptorHook(interceptor, option) {
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      interceptor[hook] = mergeHook(interceptor[hook], option[hook]);
    }
  });
}

function removeInterceptorHook(interceptor, option) {
  if (!interceptor || !option) {
    return;
  }
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      removeHook(interceptor[hook], option[hook]);
    }
  });
}

function addInterceptor(method, option) {
  if (typeof method === 'string' && isPlainObject(option)) {
    mergeInterceptorHook(scopedInterceptors[method] || (scopedInterceptors[method] = {}), option);
  } else if (isPlainObject(method)) {
    mergeInterceptorHook(globalInterceptors, method);
  }
}

function removeInterceptor(method, option) {
  if (typeof method === 'string') {
    if (isPlainObject(option)) {
      removeInterceptorHook(scopedInterceptors[method], option);
    } else {
      delete scopedInterceptors[method];
    }
  } else if (isPlainObject(method)) {
    removeInterceptorHook(globalInterceptors, method);
  }
}

function wrapperHook(hook) {
  return function (data) {
    return hook(data) || data;
  };
}

function isPromise(obj) {
  return !!obj && (typeof obj === 'object' || typeof obj === 'function') && typeof obj.then === 'function';
}

function queue(hooks, data) {
  var promise = false;
  for (var i = 0; i < hooks.length; i++) {
    var hook = hooks[i];
    if (promise) {
      promise = Promise.resolve(wrapperHook(hook));
    } else {
      var res = hook(data);
      if (isPromise(res)) {
        promise = Promise.resolve(res);
      }
      if (res === false) {
        return {
          then: function then() {} };

      }
    }
  }
  return promise || {
    then: function then(callback) {
      return callback(data);
    } };

}

function wrapperOptions(interceptor) {var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  ['success', 'fail', 'complete'].forEach(function (name) {
    if (Array.isArray(interceptor[name])) {
      var oldCallback = options[name];
      options[name] = function callbackInterceptor(res) {
        queue(interceptor[name], res).then(function (res) {
          /* eslint-disable no-mixed-operators */
          return isFn(oldCallback) && oldCallback(res) || res;
        });
      };
    }
  });
  return options;
}

function wrapperReturnValue(method, returnValue) {
  var returnValueHooks = [];
  if (Array.isArray(globalInterceptors.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, _toConsumableArray(globalInterceptors.returnValue));
  }
  var interceptor = scopedInterceptors[method];
  if (interceptor && Array.isArray(interceptor.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, _toConsumableArray(interceptor.returnValue));
  }
  returnValueHooks.forEach(function (hook) {
    returnValue = hook(returnValue) || returnValue;
  });
  return returnValue;
}

function getApiInterceptorHooks(method) {
  var interceptor = Object.create(null);
  Object.keys(globalInterceptors).forEach(function (hook) {
    if (hook !== 'returnValue') {
      interceptor[hook] = globalInterceptors[hook].slice();
    }
  });
  var scopedInterceptor = scopedInterceptors[method];
  if (scopedInterceptor) {
    Object.keys(scopedInterceptor).forEach(function (hook) {
      if (hook !== 'returnValue') {
        interceptor[hook] = (interceptor[hook] || []).concat(scopedInterceptor[hook]);
      }
    });
  }
  return interceptor;
}

function invokeApi(method, api, options) {for (var _len = arguments.length, params = new Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {params[_key - 3] = arguments[_key];}
  var interceptor = getApiInterceptorHooks(method);
  if (interceptor && Object.keys(interceptor).length) {
    if (Array.isArray(interceptor.invoke)) {
      var res = queue(interceptor.invoke, options);
      return res.then(function (options) {
        return api.apply(void 0, [wrapperOptions(interceptor, options)].concat(params));
      });
    } else {
      return api.apply(void 0, [wrapperOptions(interceptor, options)].concat(params));
    }
  }
  return api.apply(void 0, [options].concat(params));
}

var promiseInterceptor = {
  returnValue: function returnValue(res) {
    if (!isPromise(res)) {
      return res;
    }
    return res.then(function (res) {
      return res[1];
    }).catch(function (res) {
      return res[0];
    });
  } };


var SYNC_API_RE =
/^\$|Window$|WindowStyle$|sendNativeEvent|restoreGlobal|getCurrentSubNVue|getMenuButtonBoundingClientRect|^report|interceptors|Interceptor$|getSubNVueById|requireNativePlugin|upx2px|hideKeyboard|canIUse|^create|Sync$|Manager$|base64ToArrayBuffer|arrayBufferToBase64/;

var CONTEXT_API_RE = /^create|Manager$/;

// Context例外情况
var CONTEXT_API_RE_EXC = ['createBLEConnection'];

// 同步例外情况
var ASYNC_API = ['createBLEConnection'];

var CALLBACK_API_RE = /^on|^off/;

function isContextApi(name) {
  return CONTEXT_API_RE.test(name) && CONTEXT_API_RE_EXC.indexOf(name) === -1;
}
function isSyncApi(name) {
  return SYNC_API_RE.test(name) && ASYNC_API.indexOf(name) === -1;
}

function isCallbackApi(name) {
  return CALLBACK_API_RE.test(name) && name !== 'onPush';
}

function handlePromise(promise) {
  return promise.then(function (data) {
    return [null, data];
  }).
  catch(function (err) {return [err];});
}

function shouldPromise(name) {
  if (
  isContextApi(name) ||
  isSyncApi(name) ||
  isCallbackApi(name))
  {
    return false;
  }
  return true;
}

/* eslint-disable no-extend-native */
if (!Promise.prototype.finally) {
  Promise.prototype.finally = function (callback) {
    var promise = this.constructor;
    return this.then(
    function (value) {return promise.resolve(callback()).then(function () {return value;});},
    function (reason) {return promise.resolve(callback()).then(function () {
        throw reason;
      });});

  };
}

function promisify(name, api) {
  if (!shouldPromise(name)) {
    return api;
  }
  return function promiseApi() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};for (var _len2 = arguments.length, params = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {params[_key2 - 1] = arguments[_key2];}
    if (isFn(options.success) || isFn(options.fail) || isFn(options.complete)) {
      return wrapperReturnValue(name, invokeApi.apply(void 0, [name, api, options].concat(params)));
    }
    return wrapperReturnValue(name, handlePromise(new Promise(function (resolve, reject) {
      invokeApi.apply(void 0, [name, api, Object.assign({}, options, {
        success: resolve,
        fail: reject })].concat(
      params));
    })));
  };
}

var EPS = 1e-4;
var BASE_DEVICE_WIDTH = 750;
var isIOS = false;
var deviceWidth = 0;
var deviceDPR = 0;

function checkDeviceWidth() {var _wx$getSystemInfoSync =




  wx.getSystemInfoSync(),platform = _wx$getSystemInfoSync.platform,pixelRatio = _wx$getSystemInfoSync.pixelRatio,windowWidth = _wx$getSystemInfoSync.windowWidth; // uni=>wx runtime 编译目标是 uni 对象，内部不允许直接使用 uni

  deviceWidth = windowWidth;
  deviceDPR = pixelRatio;
  isIOS = platform === 'ios';
}

function upx2px(number, newDeviceWidth) {
  if (deviceWidth === 0) {
    checkDeviceWidth();
  }

  number = Number(number);
  if (number === 0) {
    return 0;
  }
  var result = number / BASE_DEVICE_WIDTH * (newDeviceWidth || deviceWidth);
  if (result < 0) {
    result = -result;
  }
  result = Math.floor(result + EPS);
  if (result === 0) {
    if (deviceDPR === 1 || !isIOS) {
      result = 1;
    } else {
      result = 0.5;
    }
  }
  return number < 0 ? -result : result;
}

var interceptors = {
  promiseInterceptor: promiseInterceptor };


var baseApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  upx2px: upx2px,
  addInterceptor: addInterceptor,
  removeInterceptor: removeInterceptor,
  interceptors: interceptors });


function findExistsPageIndex(url) {
  var pages = getCurrentPages();
  var len = pages.length;
  while (len--) {
    var page = pages[len];
    if (page.$page && page.$page.fullPath === url) {
      return len;
    }
  }
  return -1;
}

var redirectTo = {
  name: function name(fromArgs) {
    if (fromArgs.exists === 'back' && fromArgs.delta) {
      return 'navigateBack';
    }
    return 'redirectTo';
  },
  args: function args(fromArgs) {
    if (fromArgs.exists === 'back' && fromArgs.url) {
      var existsPageIndex = findExistsPageIndex(fromArgs.url);
      if (existsPageIndex !== -1) {
        var delta = getCurrentPages().length - 1 - existsPageIndex;
        if (delta > 0) {
          fromArgs.delta = delta;
        }
      }
    }
  } };


var previewImage = {
  args: function args(fromArgs) {
    var currentIndex = parseInt(fromArgs.current);
    if (isNaN(currentIndex)) {
      return;
    }
    var urls = fromArgs.urls;
    if (!Array.isArray(urls)) {
      return;
    }
    var len = urls.length;
    if (!len) {
      return;
    }
    if (currentIndex < 0) {
      currentIndex = 0;
    } else if (currentIndex >= len) {
      currentIndex = len - 1;
    }
    if (currentIndex > 0) {
      fromArgs.current = urls[currentIndex];
      fromArgs.urls = urls.filter(
      function (item, index) {return index < currentIndex ? item !== urls[currentIndex] : true;});

    } else {
      fromArgs.current = urls[0];
    }
    return {
      indicator: false,
      loop: false };

  } };


var UUID_KEY = '__DC_STAT_UUID';
var deviceId;
function addUuid(result) {
  deviceId = deviceId || wx.getStorageSync(UUID_KEY);
  if (!deviceId) {
    deviceId = Date.now() + '' + Math.floor(Math.random() * 1e7);
    wx.setStorage({
      key: UUID_KEY,
      data: deviceId });

  }
  result.deviceId = deviceId;
}

function addSafeAreaInsets(result) {
  if (result.safeArea) {
    var safeArea = result.safeArea;
    result.safeAreaInsets = {
      top: safeArea.top,
      left: safeArea.left,
      right: result.windowWidth - safeArea.right,
      bottom: result.windowHeight - safeArea.bottom };

  }
}

var getSystemInfo = {
  returnValue: function returnValue(result) {
    addUuid(result);
    addSafeAreaInsets(result);
  } };


// import navigateTo from 'uni-helpers/navigate-to'

var protocols = {
  redirectTo: redirectTo,
  // navigateTo,  // 由于在微信开发者工具的页面参数，会显示__id__参数，因此暂时关闭mp-weixin对于navigateTo的AOP
  previewImage: previewImage,
  getSystemInfo: getSystemInfo,
  getSystemInfoSync: getSystemInfo };

var todos = [
'vibrate',
'preloadPage',
'unPreloadPage',
'loadSubPackage'];

var canIUses = [];

var CALLBACKS = ['success', 'fail', 'cancel', 'complete'];

function processCallback(methodName, method, returnValue) {
  return function (res) {
    return method(processReturnValue(methodName, res, returnValue));
  };
}

function processArgs(methodName, fromArgs) {var argsOption = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};var returnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};var keepFromArgs = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
  if (isPlainObject(fromArgs)) {// 一般 api 的参数解析
    var toArgs = keepFromArgs === true ? fromArgs : {}; // returnValue 为 false 时，说明是格式化返回值，直接在返回值对象上修改赋值
    if (isFn(argsOption)) {
      argsOption = argsOption(fromArgs, toArgs) || {};
    }
    for (var key in fromArgs) {
      if (hasOwn(argsOption, key)) {
        var keyOption = argsOption[key];
        if (isFn(keyOption)) {
          keyOption = keyOption(fromArgs[key], fromArgs, toArgs);
        }
        if (!keyOption) {// 不支持的参数
          console.warn("\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F ".concat(methodName, "\u6682\u4E0D\u652F\u6301").concat(key));
        } else if (isStr(keyOption)) {// 重写参数 key
          toArgs[keyOption] = fromArgs[key];
        } else if (isPlainObject(keyOption)) {// {name:newName,value:value}可重新指定参数 key:value
          toArgs[keyOption.name ? keyOption.name : key] = keyOption.value;
        }
      } else if (CALLBACKS.indexOf(key) !== -1) {
        if (isFn(fromArgs[key])) {
          toArgs[key] = processCallback(methodName, fromArgs[key], returnValue);
        }
      } else {
        if (!keepFromArgs) {
          toArgs[key] = fromArgs[key];
        }
      }
    }
    return toArgs;
  } else if (isFn(fromArgs)) {
    fromArgs = processCallback(methodName, fromArgs, returnValue);
  }
  return fromArgs;
}

function processReturnValue(methodName, res, returnValue) {var keepReturnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  if (isFn(protocols.returnValue)) {// 处理通用 returnValue
    res = protocols.returnValue(methodName, res);
  }
  return processArgs(methodName, res, returnValue, {}, keepReturnValue);
}

function wrapper(methodName, method) {
  if (hasOwn(protocols, methodName)) {
    var protocol = protocols[methodName];
    if (!protocol) {// 暂不支持的 api
      return function () {
        console.error("\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F \u6682\u4E0D\u652F\u6301".concat(methodName));
      };
    }
    return function (arg1, arg2) {// 目前 api 最多两个参数
      var options = protocol;
      if (isFn(protocol)) {
        options = protocol(arg1);
      }

      arg1 = processArgs(methodName, arg1, options.args, options.returnValue);

      var args = [arg1];
      if (typeof arg2 !== 'undefined') {
        args.push(arg2);
      }
      if (isFn(options.name)) {
        methodName = options.name(arg1);
      } else if (isStr(options.name)) {
        methodName = options.name;
      }
      var returnValue = wx[methodName].apply(wx, args);
      if (isSyncApi(methodName)) {// 同步 api
        return processReturnValue(methodName, returnValue, options.returnValue, isContextApi(methodName));
      }
      return returnValue;
    };
  }
  return method;
}

var todoApis = Object.create(null);

var TODOS = [
'onTabBarMidButtonTap',
'subscribePush',
'unsubscribePush',
'onPush',
'offPush',
'share'];


function createTodoApi(name) {
  return function todoApi(_ref)


  {var fail = _ref.fail,complete = _ref.complete;
    var res = {
      errMsg: "".concat(name, ":fail:\u6682\u4E0D\u652F\u6301 ").concat(name, " \u65B9\u6CD5") };

    isFn(fail) && fail(res);
    isFn(complete) && complete(res);
  };
}

TODOS.forEach(function (name) {
  todoApis[name] = createTodoApi(name);
});

var providers = {
  oauth: ['weixin'],
  share: ['weixin'],
  payment: ['wxpay'],
  push: ['weixin'] };


function getProvider(_ref2)




{var service = _ref2.service,success = _ref2.success,fail = _ref2.fail,complete = _ref2.complete;
  var res = false;
  if (providers[service]) {
    res = {
      errMsg: 'getProvider:ok',
      service: service,
      provider: providers[service] };

    isFn(success) && success(res);
  } else {
    res = {
      errMsg: 'getProvider:fail:服务[' + service + ']不存在' };

    isFn(fail) && fail(res);
  }
  isFn(complete) && complete(res);
}

var extraApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  getProvider: getProvider });


var getEmitter = function () {
  var Emitter;
  return function getUniEmitter() {
    if (!Emitter) {
      Emitter = new _vue.default();
    }
    return Emitter;
  };
}();

function apply(ctx, method, args) {
  return ctx[method].apply(ctx, args);
}

function $on() {
  return apply(getEmitter(), '$on', Array.prototype.slice.call(arguments));
}
function $off() {
  return apply(getEmitter(), '$off', Array.prototype.slice.call(arguments));
}
function $once() {
  return apply(getEmitter(), '$once', Array.prototype.slice.call(arguments));
}
function $emit() {
  return apply(getEmitter(), '$emit', Array.prototype.slice.call(arguments));
}

var eventApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  $on: $on,
  $off: $off,
  $once: $once,
  $emit: $emit });


var api = /*#__PURE__*/Object.freeze({
  __proto__: null });


var MPPage = Page;
var MPComponent = Component;

var customizeRE = /:/g;

var customize = cached(function (str) {
  return camelize(str.replace(customizeRE, '-'));
});

function initTriggerEvent(mpInstance) {
  {
    if (!wx.canIUse('nextTick')) {
      return;
    }
  }
  var oldTriggerEvent = mpInstance.triggerEvent;
  mpInstance.triggerEvent = function (event) {for (var _len3 = arguments.length, args = new Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {args[_key3 - 1] = arguments[_key3];}
    return oldTriggerEvent.apply(mpInstance, [customize(event)].concat(args));
  };
}

function initHook(name, options) {
  var oldHook = options[name];
  if (!oldHook) {
    options[name] = function () {
      initTriggerEvent(this);
    };
  } else {
    options[name] = function () {
      initTriggerEvent(this);for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {args[_key4] = arguments[_key4];}
      return oldHook.apply(this, args);
    };
  }
}
if (!MPPage.__$wrappered) {
  MPPage.__$wrappered = true;
  Page = function Page() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    initHook('onLoad', options);
    return MPPage(options);
  };
  Page.after = MPPage.after;

  Component = function Component() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    initHook('created', options);
    return MPComponent(options);
  };
}

var PAGE_EVENT_HOOKS = [
'onPullDownRefresh',
'onReachBottom',
'onAddToFavorites',
'onShareTimeline',
'onShareAppMessage',
'onPageScroll',
'onResize',
'onTabItemTap'];


function initMocks(vm, mocks) {
  var mpInstance = vm.$mp[vm.mpType];
  mocks.forEach(function (mock) {
    if (hasOwn(mpInstance, mock)) {
      vm[mock] = mpInstance[mock];
    }
  });
}

function hasHook(hook, vueOptions) {
  if (!vueOptions) {
    return true;
  }

  if (_vue.default.options && Array.isArray(_vue.default.options[hook])) {
    return true;
  }

  vueOptions = vueOptions.default || vueOptions;

  if (isFn(vueOptions)) {
    if (isFn(vueOptions.extendOptions[hook])) {
      return true;
    }
    if (vueOptions.super &&
    vueOptions.super.options &&
    Array.isArray(vueOptions.super.options[hook])) {
      return true;
    }
    return false;
  }

  if (isFn(vueOptions[hook])) {
    return true;
  }
  var mixins = vueOptions.mixins;
  if (Array.isArray(mixins)) {
    return !!mixins.find(function (mixin) {return hasHook(hook, mixin);});
  }
}

function initHooks(mpOptions, hooks, vueOptions) {
  hooks.forEach(function (hook) {
    if (hasHook(hook, vueOptions)) {
      mpOptions[hook] = function (args) {
        return this.$vm && this.$vm.__call_hook(hook, args);
      };
    }
  });
}

function initVueComponent(Vue, vueOptions) {
  vueOptions = vueOptions.default || vueOptions;
  var VueComponent;
  if (isFn(vueOptions)) {
    VueComponent = vueOptions;
  } else {
    VueComponent = Vue.extend(vueOptions);
  }
  vueOptions = VueComponent.options;
  return [VueComponent, vueOptions];
}

function initSlots(vm, vueSlots) {
  if (Array.isArray(vueSlots) && vueSlots.length) {
    var $slots = Object.create(null);
    vueSlots.forEach(function (slotName) {
      $slots[slotName] = true;
    });
    vm.$scopedSlots = vm.$slots = $slots;
  }
}

function initVueIds(vueIds, mpInstance) {
  vueIds = (vueIds || '').split(',');
  var len = vueIds.length;

  if (len === 1) {
    mpInstance._$vueId = vueIds[0];
  } else if (len === 2) {
    mpInstance._$vueId = vueIds[0];
    mpInstance._$vuePid = vueIds[1];
  }
}

function initData(vueOptions, context) {
  var data = vueOptions.data || {};
  var methods = vueOptions.methods || {};

  if (typeof data === 'function') {
    try {
      data = data.call(context); // 支持 Vue.prototype 上挂的数据
    } catch (e) {
      if (Object({"NODE_ENV":"development","VUE_APP_NAME":"分享保","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.warn('根据 Vue 的 data 函数初始化小程序 data 失败，请尽量确保 data 函数中不访问 vm 对象，否则可能影响首次数据渲染速度。', data);
      }
    }
  } else {
    try {
      // 对 data 格式化
      data = JSON.parse(JSON.stringify(data));
    } catch (e) {}
  }

  if (!isPlainObject(data)) {
    data = {};
  }

  Object.keys(methods).forEach(function (methodName) {
    if (context.__lifecycle_hooks__.indexOf(methodName) === -1 && !hasOwn(data, methodName)) {
      data[methodName] = methods[methodName];
    }
  });

  return data;
}

var PROP_TYPES = [String, Number, Boolean, Object, Array, null];

function createObserver(name) {
  return function observer(newVal, oldVal) {
    if (this.$vm) {
      this.$vm[name] = newVal; // 为了触发其他非 render watcher
    }
  };
}

function initBehaviors(vueOptions, initBehavior) {
  var vueBehaviors = vueOptions.behaviors;
  var vueExtends = vueOptions.extends;
  var vueMixins = vueOptions.mixins;

  var vueProps = vueOptions.props;

  if (!vueProps) {
    vueOptions.props = vueProps = [];
  }

  var behaviors = [];
  if (Array.isArray(vueBehaviors)) {
    vueBehaviors.forEach(function (behavior) {
      behaviors.push(behavior.replace('uni://', "wx".concat("://")));
      if (behavior === 'uni://form-field') {
        if (Array.isArray(vueProps)) {
          vueProps.push('name');
          vueProps.push('value');
        } else {
          vueProps.name = {
            type: String,
            default: '' };

          vueProps.value = {
            type: [String, Number, Boolean, Array, Object, Date],
            default: '' };

        }
      }
    });
  }
  if (isPlainObject(vueExtends) && vueExtends.props) {
    behaviors.push(
    initBehavior({
      properties: initProperties(vueExtends.props, true) }));


  }
  if (Array.isArray(vueMixins)) {
    vueMixins.forEach(function (vueMixin) {
      if (isPlainObject(vueMixin) && vueMixin.props) {
        behaviors.push(
        initBehavior({
          properties: initProperties(vueMixin.props, true) }));


      }
    });
  }
  return behaviors;
}

function parsePropType(key, type, defaultValue, file) {
  // [String]=>String
  if (Array.isArray(type) && type.length === 1) {
    return type[0];
  }
  return type;
}

function initProperties(props) {var isBehavior = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;var file = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
  var properties = {};
  if (!isBehavior) {
    properties.vueId = {
      type: String,
      value: '' };

    // 用于字节跳动小程序模拟抽象节点
    properties.generic = {
      type: Object,
      value: null };

    properties.vueSlots = { // 小程序不能直接定义 $slots 的 props，所以通过 vueSlots 转换到 $slots
      type: null,
      value: [],
      observer: function observer(newVal, oldVal) {
        var $slots = Object.create(null);
        newVal.forEach(function (slotName) {
          $slots[slotName] = true;
        });
        this.setData({
          $slots: $slots });

      } };

  }
  if (Array.isArray(props)) {// ['title']
    props.forEach(function (key) {
      properties[key] = {
        type: null,
        observer: createObserver(key) };

    });
  } else if (isPlainObject(props)) {// {title:{type:String,default:''},content:String}
    Object.keys(props).forEach(function (key) {
      var opts = props[key];
      if (isPlainObject(opts)) {// title:{type:String,default:''}
        var value = opts.default;
        if (isFn(value)) {
          value = value();
        }

        opts.type = parsePropType(key, opts.type);

        properties[key] = {
          type: PROP_TYPES.indexOf(opts.type) !== -1 ? opts.type : null,
          value: value,
          observer: createObserver(key) };

      } else {// content:String
        var type = parsePropType(key, opts);
        properties[key] = {
          type: PROP_TYPES.indexOf(type) !== -1 ? type : null,
          observer: createObserver(key) };

      }
    });
  }
  return properties;
}

function wrapper$1(event) {
  // TODO 又得兼容 mpvue 的 mp 对象
  try {
    event.mp = JSON.parse(JSON.stringify(event));
  } catch (e) {}

  event.stopPropagation = noop;
  event.preventDefault = noop;

  event.target = event.target || {};

  if (!hasOwn(event, 'detail')) {
    event.detail = {};
  }

  if (hasOwn(event, 'markerId')) {
    event.detail = typeof event.detail === 'object' ? event.detail : {};
    event.detail.markerId = event.markerId;
  }

  if (isPlainObject(event.detail)) {
    event.target = Object.assign({}, event.target, event.detail);
  }

  return event;
}

function getExtraValue(vm, dataPathsArray) {
  var context = vm;
  dataPathsArray.forEach(function (dataPathArray) {
    var dataPath = dataPathArray[0];
    var value = dataPathArray[2];
    if (dataPath || typeof value !== 'undefined') {// ['','',index,'disable']
      var propPath = dataPathArray[1];
      var valuePath = dataPathArray[3];

      var vFor;
      if (Number.isInteger(dataPath)) {
        vFor = dataPath;
      } else if (!dataPath) {
        vFor = context;
      } else if (typeof dataPath === 'string' && dataPath) {
        if (dataPath.indexOf('#s#') === 0) {
          vFor = dataPath.substr(3);
        } else {
          vFor = vm.__get_value(dataPath, context);
        }
      }

      if (Number.isInteger(vFor)) {
        context = value;
      } else if (!propPath) {
        context = vFor[value];
      } else {
        if (Array.isArray(vFor)) {
          context = vFor.find(function (vForItem) {
            return vm.__get_value(propPath, vForItem) === value;
          });
        } else if (isPlainObject(vFor)) {
          context = Object.keys(vFor).find(function (vForKey) {
            return vm.__get_value(propPath, vFor[vForKey]) === value;
          });
        } else {
          console.error('v-for 暂不支持循环数据：', vFor);
        }
      }

      if (valuePath) {
        context = vm.__get_value(valuePath, context);
      }
    }
  });
  return context;
}

function processEventExtra(vm, extra, event) {
  var extraObj = {};

  if (Array.isArray(extra) && extra.length) {
    /**
                                              *[
                                              *    ['data.items', 'data.id', item.data.id],
                                              *    ['metas', 'id', meta.id]
                                              *],
                                              *[
                                              *    ['data.items', 'data.id', item.data.id],
                                              *    ['metas', 'id', meta.id]
                                              *],
                                              *'test'
                                              */
    extra.forEach(function (dataPath, index) {
      if (typeof dataPath === 'string') {
        if (!dataPath) {// model,prop.sync
          extraObj['$' + index] = vm;
        } else {
          if (dataPath === '$event') {// $event
            extraObj['$' + index] = event;
          } else if (dataPath === 'arguments') {
            if (event.detail && event.detail.__args__) {
              extraObj['$' + index] = event.detail.__args__;
            } else {
              extraObj['$' + index] = [event];
            }
          } else if (dataPath.indexOf('$event.') === 0) {// $event.target.value
            extraObj['$' + index] = vm.__get_value(dataPath.replace('$event.', ''), event);
          } else {
            extraObj['$' + index] = vm.__get_value(dataPath);
          }
        }
      } else {
        extraObj['$' + index] = getExtraValue(vm, dataPath);
      }
    });
  }

  return extraObj;
}

function getObjByArray(arr) {
  var obj = {};
  for (var i = 1; i < arr.length; i++) {
    var element = arr[i];
    obj[element[0]] = element[1];
  }
  return obj;
}

function processEventArgs(vm, event) {var args = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];var extra = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];var isCustom = arguments.length > 4 ? arguments[4] : undefined;var methodName = arguments.length > 5 ? arguments[5] : undefined;
  var isCustomMPEvent = false; // wxcomponent 组件，传递原始 event 对象
  if (isCustom) {// 自定义事件
    isCustomMPEvent = event.currentTarget &&
    event.currentTarget.dataset &&
    event.currentTarget.dataset.comType === 'wx';
    if (!args.length) {// 无参数，直接传入 event 或 detail 数组
      if (isCustomMPEvent) {
        return [event];
      }
      return event.detail.__args__ || event.detail;
    }
  }

  var extraObj = processEventExtra(vm, extra, event);

  var ret = [];
  args.forEach(function (arg) {
    if (arg === '$event') {
      if (methodName === '__set_model' && !isCustom) {// input v-model value
        ret.push(event.target.value);
      } else {
        if (isCustom && !isCustomMPEvent) {
          ret.push(event.detail.__args__[0]);
        } else {// wxcomponent 组件或内置组件
          ret.push(event);
        }
      }
    } else {
      if (Array.isArray(arg) && arg[0] === 'o') {
        ret.push(getObjByArray(arg));
      } else if (typeof arg === 'string' && hasOwn(extraObj, arg)) {
        ret.push(extraObj[arg]);
      } else {
        ret.push(arg);
      }
    }
  });

  return ret;
}

var ONCE = '~';
var CUSTOM = '^';

function isMatchEventType(eventType, optType) {
  return eventType === optType ||

  optType === 'regionchange' && (

  eventType === 'begin' ||
  eventType === 'end');


}

function getContextVm(vm) {
  var $parent = vm.$parent;
  // 父组件是 scoped slots 或者其他自定义组件时继续查找
  while ($parent && $parent.$parent && ($parent.$options.generic || $parent.$parent.$options.generic || $parent.$scope._$vuePid)) {
    $parent = $parent.$parent;
  }
  return $parent && $parent.$parent;
}

function handleEvent(event) {var _this = this;
  event = wrapper$1(event);

  // [['tap',[['handle',[1,2,a]],['handle1',[1,2,a]]]]]
  var dataset = (event.currentTarget || event.target).dataset;
  if (!dataset) {
    return console.warn('事件信息不存在');
  }
  var eventOpts = dataset.eventOpts || dataset['event-opts']; // 支付宝 web-view 组件 dataset 非驼峰
  if (!eventOpts) {
    return console.warn('事件信息不存在');
  }

  // [['handle',[1,2,a]],['handle1',[1,2,a]]]
  var eventType = event.type;

  var ret = [];

  eventOpts.forEach(function (eventOpt) {
    var type = eventOpt[0];
    var eventsArray = eventOpt[1];

    var isCustom = type.charAt(0) === CUSTOM;
    type = isCustom ? type.slice(1) : type;
    var isOnce = type.charAt(0) === ONCE;
    type = isOnce ? type.slice(1) : type;

    if (eventsArray && isMatchEventType(eventType, type)) {
      eventsArray.forEach(function (eventArray) {
        var methodName = eventArray[0];
        if (methodName) {
          var handlerCtx = _this.$vm;
          if (handlerCtx.$options.generic) {// mp-weixin,mp-toutiao 抽象节点模拟 scoped slots
            handlerCtx = getContextVm(handlerCtx) || handlerCtx;
          }
          if (methodName === '$emit') {
            handlerCtx.$emit.apply(handlerCtx,
            processEventArgs(
            _this.$vm,
            event,
            eventArray[1],
            eventArray[2],
            isCustom,
            methodName));

            return;
          }
          var handler = handlerCtx[methodName];
          if (!isFn(handler)) {
            throw new Error(" _vm.".concat(methodName, " is not a function"));
          }
          if (isOnce) {
            if (handler.once) {
              return;
            }
            handler.once = true;
          }
          var params = processEventArgs(
          _this.$vm,
          event,
          eventArray[1],
          eventArray[2],
          isCustom,
          methodName);

          params = Array.isArray(params) ? params : [];
          // 参数尾部增加原始事件对象用于复杂表达式内获取额外数据
          if (/=\s*\S+\.eventParams\s*\|\|\s*\S+\[['"]event-params['"]\]/.test(handler.toString())) {
            // eslint-disable-next-line no-sparse-arrays
            params = params.concat([,,,,,,,,,, event]);
          }
          ret.push(handler.apply(handlerCtx, params));
        }
      });
    }
  });

  if (
  eventType === 'input' &&
  ret.length === 1 &&
  typeof ret[0] !== 'undefined')
  {
    return ret[0];
  }
}

var eventChannels = {};

var eventChannelStack = [];

function getEventChannel(id) {
  if (id) {
    var eventChannel = eventChannels[id];
    delete eventChannels[id];
    return eventChannel;
  }
  return eventChannelStack.shift();
}

var hooks = [
'onShow',
'onHide',
'onError',
'onPageNotFound',
'onThemeChange',
'onUnhandledRejection'];


function initEventChannel() {
  _vue.default.prototype.getOpenerEventChannel = function () {
    // 微信小程序使用自身getOpenerEventChannel
    {
      return this.$scope.getOpenerEventChannel();
    }
  };
  var callHook = _vue.default.prototype.__call_hook;
  _vue.default.prototype.__call_hook = function (hook, args) {
    if (hook === 'onLoad' && args && args.__id__) {
      this.__eventChannel__ = getEventChannel(args.__id__);
      delete args.__id__;
    }
    return callHook.call(this, hook, args);
  };
}

function parseBaseApp(vm, _ref3)


{var mocks = _ref3.mocks,initRefs = _ref3.initRefs;
  initEventChannel();
  if (vm.$options.store) {
    _vue.default.prototype.$store = vm.$options.store;
  }

  _vue.default.prototype.mpHost = "mp-weixin";

  _vue.default.mixin({
    beforeCreate: function beforeCreate() {
      if (!this.$options.mpType) {
        return;
      }

      this.mpType = this.$options.mpType;

      this.$mp = _defineProperty({
        data: {} },
      this.mpType, this.$options.mpInstance);


      this.$scope = this.$options.mpInstance;

      delete this.$options.mpType;
      delete this.$options.mpInstance;

      if (this.mpType !== 'app') {
        initRefs(this);
        initMocks(this, mocks);
      }
    } });


  var appOptions = {
    onLaunch: function onLaunch(args) {
      if (this.$vm) {// 已经初始化过了，主要是为了百度，百度 onShow 在 onLaunch 之前
        return;
      }
      {
        if (!wx.canIUse('nextTick')) {// 事实 上2.2.3 即可，简单使用 2.3.0 的 nextTick 判断
          console.error('当前微信基础库版本过低，请将 微信开发者工具-详情-项目设置-调试基础库版本 更换为`2.3.0`以上');
        }
      }

      this.$vm = vm;

      this.$vm.$mp = {
        app: this };


      this.$vm.$scope = this;
      // vm 上也挂载 globalData
      this.$vm.globalData = this.globalData;

      this.$vm._isMounted = true;
      this.$vm.__call_hook('mounted', args);

      this.$vm.__call_hook('onLaunch', args);
    } };


  // 兼容旧版本 globalData
  appOptions.globalData = vm.$options.globalData || {};
  // 将 methods 中的方法挂在 getApp() 中
  var methods = vm.$options.methods;
  if (methods) {
    Object.keys(methods).forEach(function (name) {
      appOptions[name] = methods[name];
    });
  }

  initHooks(appOptions, hooks);

  return appOptions;
}

var mocks = ['__route__', '__wxExparserNodeId__', '__wxWebviewId__'];

function findVmByVueId(vm, vuePid) {
  var $children = vm.$children;
  // 优先查找直属(反向查找:https://github.com/dcloudio/uni-app/issues/1200)
  for (var i = $children.length - 1; i >= 0; i--) {
    var childVm = $children[i];
    if (childVm.$scope._$vueId === vuePid) {
      return childVm;
    }
  }
  // 反向递归查找
  var parentVm;
  for (var _i = $children.length - 1; _i >= 0; _i--) {
    parentVm = findVmByVueId($children[_i], vuePid);
    if (parentVm) {
      return parentVm;
    }
  }
}

function initBehavior(options) {
  return Behavior(options);
}

function isPage() {
  return !!this.route;
}

function initRelation(detail) {
  this.triggerEvent('__l', detail);
}

function selectAllComponents(mpInstance, selector, $refs) {
  var components = mpInstance.selectAllComponents(selector);
  components.forEach(function (component) {
    var ref = component.dataset.ref;
    $refs[ref] = component.$vm || component;
    {
      if (component.dataset.vueGeneric === 'scoped') {
        component.selectAllComponents('.scoped-ref').forEach(function (scopedComponent) {
          selectAllComponents(scopedComponent, selector, $refs);
        });
      }
    }
  });
}

function initRefs(vm) {
  var mpInstance = vm.$scope;
  Object.defineProperty(vm, '$refs', {
    get: function get() {
      var $refs = {};
      selectAllComponents(mpInstance, '.vue-ref', $refs);
      // TODO 暂不考虑 for 中的 scoped
      var forComponents = mpInstance.selectAllComponents('.vue-ref-in-for');
      forComponents.forEach(function (component) {
        var ref = component.dataset.ref;
        if (!$refs[ref]) {
          $refs[ref] = [];
        }
        $refs[ref].push(component.$vm || component);
      });
      return $refs;
    } });

}

function handleLink(event) {var _ref4 =



  event.detail || event.value,vuePid = _ref4.vuePid,vueOptions = _ref4.vueOptions; // detail 是微信,value 是百度(dipatch)

  var parentVm;

  if (vuePid) {
    parentVm = findVmByVueId(this.$vm, vuePid);
  }

  if (!parentVm) {
    parentVm = this.$vm;
  }

  vueOptions.parent = parentVm;
}

function parseApp(vm) {
  return parseBaseApp(vm, {
    mocks: mocks,
    initRefs: initRefs });

}

function createApp(vm) {
  App(parseApp(vm));
  return vm;
}

var encodeReserveRE = /[!'()*]/g;
var encodeReserveReplacer = function encodeReserveReplacer(c) {return '%' + c.charCodeAt(0).toString(16);};
var commaRE = /%2C/g;

// fixed encodeURIComponent which is more conformant to RFC3986:
// - escapes [!'()*]
// - preserve commas
var encode = function encode(str) {return encodeURIComponent(str).
  replace(encodeReserveRE, encodeReserveReplacer).
  replace(commaRE, ',');};

function stringifyQuery(obj) {var encodeStr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : encode;
  var res = obj ? Object.keys(obj).map(function (key) {
    var val = obj[key];

    if (val === undefined) {
      return '';
    }

    if (val === null) {
      return encodeStr(key);
    }

    if (Array.isArray(val)) {
      var result = [];
      val.forEach(function (val2) {
        if (val2 === undefined) {
          return;
        }
        if (val2 === null) {
          result.push(encodeStr(key));
        } else {
          result.push(encodeStr(key) + '=' + encodeStr(val2));
        }
      });
      return result.join('&');
    }

    return encodeStr(key) + '=' + encodeStr(val);
  }).filter(function (x) {return x.length > 0;}).join('&') : null;
  return res ? "?".concat(res) : '';
}

function parseBaseComponent(vueComponentOptions)


{var _ref5 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},isPage = _ref5.isPage,initRelation = _ref5.initRelation;var _initVueComponent =
  initVueComponent(_vue.default, vueComponentOptions),_initVueComponent2 = _slicedToArray(_initVueComponent, 2),VueComponent = _initVueComponent2[0],vueOptions = _initVueComponent2[1];

  var options = _objectSpread({
    multipleSlots: true,
    addGlobalClass: true },
  vueOptions.options || {});


  {
    // 微信 multipleSlots 部分情况有 bug，导致内容顺序错乱 如 u-list，提供覆盖选项
    if (vueOptions['mp-weixin'] && vueOptions['mp-weixin'].options) {
      Object.assign(options, vueOptions['mp-weixin'].options);
    }
  }

  var componentOptions = {
    options: options,
    data: initData(vueOptions, _vue.default.prototype),
    behaviors: initBehaviors(vueOptions, initBehavior),
    properties: initProperties(vueOptions.props, false, vueOptions.__file),
    lifetimes: {
      attached: function attached() {
        var properties = this.properties;

        var options = {
          mpType: isPage.call(this) ? 'page' : 'component',
          mpInstance: this,
          propsData: properties };


        initVueIds(properties.vueId, this);

        // 处理父子关系
        initRelation.call(this, {
          vuePid: this._$vuePid,
          vueOptions: options });


        // 初始化 vue 实例
        this.$vm = new VueComponent(options);

        // 处理$slots,$scopedSlots（暂不支持动态变化$slots）
        initSlots(this.$vm, properties.vueSlots);

        // 触发首次 setData
        this.$vm.$mount();
      },
      ready: function ready() {
        // 当组件 props 默认值为 true，初始化时传入 false 会导致 created,ready 触发, 但 attached 不触发
        // https://developers.weixin.qq.com/community/develop/doc/00066ae2844cc0f8eb883e2a557800
        if (this.$vm) {
          this.$vm._isMounted = true;
          this.$vm.__call_hook('mounted');
          this.$vm.__call_hook('onReady');
        }
      },
      detached: function detached() {
        this.$vm && this.$vm.$destroy();
      } },

    pageLifetimes: {
      show: function show(args) {
        this.$vm && this.$vm.__call_hook('onPageShow', args);
      },
      hide: function hide() {
        this.$vm && this.$vm.__call_hook('onPageHide');
      },
      resize: function resize(size) {
        this.$vm && this.$vm.__call_hook('onPageResize', size);
      } },

    methods: {
      __l: handleLink,
      __e: handleEvent } };


  // externalClasses
  if (vueOptions.externalClasses) {
    componentOptions.externalClasses = vueOptions.externalClasses;
  }

  if (Array.isArray(vueOptions.wxsCallMethods)) {
    vueOptions.wxsCallMethods.forEach(function (callMethod) {
      componentOptions.methods[callMethod] = function (args) {
        return this.$vm[callMethod](args);
      };
    });
  }

  if (isPage) {
    return componentOptions;
  }
  return [componentOptions, VueComponent];
}

function parseComponent(vueComponentOptions) {
  return parseBaseComponent(vueComponentOptions, {
    isPage: isPage,
    initRelation: initRelation });

}

var hooks$1 = [
'onShow',
'onHide',
'onUnload'];


hooks$1.push.apply(hooks$1, PAGE_EVENT_HOOKS);

function parseBasePage(vuePageOptions, _ref6)


{var isPage = _ref6.isPage,initRelation = _ref6.initRelation;
  var pageOptions = parseComponent(vuePageOptions);

  initHooks(pageOptions.methods, hooks$1, vuePageOptions);

  pageOptions.methods.onLoad = function (query) {
    this.options = query;
    var copyQuery = Object.assign({}, query);
    delete copyQuery.__id__;
    this.$page = {
      fullPath: '/' + (this.route || this.is) + stringifyQuery(copyQuery) };

    this.$vm.$mp.query = query; // 兼容 mpvue
    this.$vm.__call_hook('onLoad', query);
  };

  return pageOptions;
}

function parsePage(vuePageOptions) {
  return parseBasePage(vuePageOptions, {
    isPage: isPage,
    initRelation: initRelation });

}

function createPage(vuePageOptions) {
  {
    return Component(parsePage(vuePageOptions));
  }
}

function createComponent(vueOptions) {
  {
    return Component(parseComponent(vueOptions));
  }
}

function createSubpackageApp(vm) {
  var appOptions = parseApp(vm);
  var app = getApp({
    allowDefault: true });

  var globalData = app.globalData;
  if (globalData) {
    Object.keys(appOptions.globalData).forEach(function (name) {
      if (!hasOwn(globalData, name)) {
        globalData[name] = appOptions.globalData[name];
      }
    });
  }
  Object.keys(appOptions).forEach(function (name) {
    if (!hasOwn(app, name)) {
      app[name] = appOptions[name];
    }
  });
  if (isFn(appOptions.onShow) && wx.onAppShow) {
    wx.onAppShow(function () {for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {args[_key5] = arguments[_key5];}
      appOptions.onShow.apply(app, args);
    });
  }
  if (isFn(appOptions.onHide) && wx.onAppHide) {
    wx.onAppHide(function () {for (var _len6 = arguments.length, args = new Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {args[_key6] = arguments[_key6];}
      appOptions.onHide.apply(app, args);
    });
  }
  if (isFn(appOptions.onLaunch)) {
    var args = wx.getLaunchOptionsSync && wx.getLaunchOptionsSync();
    appOptions.onLaunch.call(app, args);
  }
  return vm;
}

todos.forEach(function (todoApi) {
  protocols[todoApi] = false;
});

canIUses.forEach(function (canIUseApi) {
  var apiName = protocols[canIUseApi] && protocols[canIUseApi].name ? protocols[canIUseApi].name :
  canIUseApi;
  if (!wx.canIUse(apiName)) {
    protocols[canIUseApi] = false;
  }
});

var uni = {};

if (typeof Proxy !== 'undefined' && "mp-weixin" !== 'app-plus') {
  uni = new Proxy({}, {
    get: function get(target, name) {
      if (hasOwn(target, name)) {
        return target[name];
      }
      if (baseApi[name]) {
        return baseApi[name];
      }
      if (api[name]) {
        return promisify(name, api[name]);
      }
      {
        if (extraApi[name]) {
          return promisify(name, extraApi[name]);
        }
        if (todoApis[name]) {
          return promisify(name, todoApis[name]);
        }
      }
      if (eventApi[name]) {
        return eventApi[name];
      }
      if (!hasOwn(wx, name) && !hasOwn(protocols, name)) {
        return;
      }
      return promisify(name, wrapper(name, wx[name]));
    },
    set: function set(target, name, value) {
      target[name] = value;
      return true;
    } });

} else {
  Object.keys(baseApi).forEach(function (name) {
    uni[name] = baseApi[name];
  });

  {
    Object.keys(todoApis).forEach(function (name) {
      uni[name] = promisify(name, todoApis[name]);
    });
    Object.keys(extraApi).forEach(function (name) {
      uni[name] = promisify(name, todoApis[name]);
    });
  }

  Object.keys(eventApi).forEach(function (name) {
    uni[name] = eventApi[name];
  });

  Object.keys(api).forEach(function (name) {
    uni[name] = promisify(name, api[name]);
  });

  Object.keys(wx).forEach(function (name) {
    if (hasOwn(wx, name) || hasOwn(protocols, name)) {
      uni[name] = promisify(name, wrapper(name, wx[name]));
    }
  });
}

wx.createApp = createApp;
wx.createPage = createPage;
wx.createComponent = createComponent;
wx.createSubpackageApp = createSubpackageApp;

var uni$1 = uni;var _default =

uni$1;exports.default = _default;

/***/ }),

/***/ 10:
/*!******************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/wxcomponents/public/json2Form.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.json2Form = json2Form;exports.socketFun = void 0;
var app = getApp();
function json2Form(json) {

  var str = [];
  for (var p in json) {
    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(json[p]));
  }
  console.log(str.join("&"));
  return str.join("&");

}



var socketFun = {
  uploadImage: function uploadImage(tempFilePaths, type, params) {
    var app = getApp();
    if (!app.globalData.loginUser) {
      app.globalData.echoErr('用户未登录');
      return;
    }
    var param = {
      userId: app.globalData.loginUser.id };

    var customIndex = app.globalData.AddClientUrl("/file_uploading.html", param, 'POST');
    wx.uploadFile({
      url: customIndex.url, //接口地址
      filePath: tempFilePaths,
      name: 'file',
      formData: customIndex.params,
      header: { 'content-type': 'multipart/form-data' },
      success: function success(res) {
        console.log("===success===", res);
        //do something
        var data = res.data;
        if (typeof data == 'string') {
          data = JSON.parse(data);
          console.log("====string====", data);
          if (data.errcode == 0) {
            socketFun.sendMsgFun(data.relateObj.imageUrl, type, params);
          }
        }
      }, fail: function fail(e) {
        console.log(e);
      }, complete: function complete(e) {

      } });

  },
  sendImgFun: function sendImgFun(type, params) {
    var app = getApp();
    console.log("socket sendImg", type);
    wx.chooseImage({
      count: 9, // 默认9
      sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function success(res) {
        console.log("===chooseImage===", res);
        var tempFilePaths = res.tempFilePaths;
        for (var i = 0; i < tempFilePaths.length; i++) {
          socketFun.uploadImage(tempFilePaths[i], type, params);
        }
      } });

  },
  sendMsgFun: function sendMsgFun(msg, type, params) {
    var app = getApp();
    console.log("socket sendMsg", type, app.globalData.loginUser);
    var msgData = '{"viewHandler":"chat","content":"' + msg + '","toUserId":' + params.puid + ',"fromUserId":' + app.globalData.loginUser.platformUser.id + ',"msgType":' + (type || 0) + '}';
    console.log("====msgData==", msgData);
    if (app.globalData.socketTask.readyState == app.globalData.socketTask.OPEN) {
      app.globalData.socketTask.send({
        data: msgData,
        success: function success() {
          console.info('客户端发送成功');
        } });

    } else {
      console.error('连接已经关闭');
    }
  },
  removeListener: function removeListener(key) {
    var app = getApp();
    if (app.globalData.chatMessageListeners) {
      for (var i = 0; i < app.globalData.chatMessageListeners.length; i++) {
        if (app.globalData.chatMessageListeners[i].key == key) {
          app.globalData.chatMessageListeners.splice(i, 1);
        }
      }
    }
  },
  addListener: function addListener(viewHandler, key, listener, prior) {
    var app = getApp();
    if (!prior) {
      prior = 0;
    }
    if (app.globalData.chatMessageListeners) {
      var replace = false;
      for (var i = 0; i < app.globalData.chatMessageListeners.length; i++) {
        if (app.globalData.chatMessageListeners[i].key == key) {
          app.globalData.chatMessageListeners[i].listener = listener;
          app.globalData.chatMessageListeners[i].prior = prior;
          app.globalData.chatMessageListeners[i].viewHandler = viewHandler;
          replace = true;
        }
      }
      if (!replace) {
        app.globalData.chatMessageListeners.push({ "key": key, "viewHandler": viewHandler, "listener": listener, "prior": prior });
      }
    } else {
      app.globalData.chatMessageListeners = [];
      app.globalData.chatMessageListeners.push({ "key": key, "viewHandler": viewHandler, "listener": listener, "prior": prior });
    }

    app.globalData.chatMessageListeners.sort(function compare(a, b) {return b.prior - a.prior;});
  },
  socketConnect: function socketConnect() {
    var app = getApp();
    console.log('connect to server!');
    if (app.globalData.socketTask && app.globalData.socketTask.readyState == app.globalData.socketTask.OPEN) {
      console.log("正常连接中~");

    } else {
      var params = { puid: app.globalData.loginUser.platformUser.id };
      var customIndex = app.globalData.AddClientUrl("/connect_chat.socket", params, 'get');
      app.globalData.socketTask = wx.connectSocket({
        url: customIndex.url,
        header: {
          'content-type': app.globalData.header },

        success: function success(res) {
          console.log("====success====", res);
        },
        fail: function fail(res) {
          console.log("====fail====", res);
        },
        complete: function complete(res) {
          console.log("====complete====", res);
        } });

      app.globalData.socketTask.onOpen(function () {
        console.log('socket open');
      });
      app.globalData.socketTask.onClose(function () {
        console.log('socket close');
        setTimeout(function () {
          console.log("reconnect socket!!!");
          app.globalData.socketTask = socketFun.socketConnect();
        }, 3000);

      });
      app.globalData.socketTask.onError(function () {
        console.log('socket onError', app.globalData.socketTask);
        try {
          app.globalData.socketTask.close();
          app.globalData.socketTask = null;
        } catch (e) {}
        setTimeout(function () {
          console.log("reconnect socket!!!");
          app.globalData.socketTask = socketFun.socketConnect();
        }, 3000);
      });
      app.globalData.socketTask.onMessage(function (msg) {
        console.log('socket on message', msg, app.globalData.chatMessageListeners);
        if (typeof msg.data == 'string') {
          try {
            msg.data = JSON.parse(msg.data);
          } catch (e) {}
        }
        var msgData = msg;
        if (app.globalData.chatMessageListeners) {
          for (var i = 0; i < app.globalData.chatMessageListeners.length; i++) {
            if (app.globalData.chatMessageListeners[i].viewHandler == msgData.data.viewHandler) {
              var next = app.globalData.chatMessageListeners[i].listener(msgData);
              if (!next && msgData.isHandler) {
                break;
              }
            } else {

            }

          }
        }
      });
    }
    return app.globalData.socketTask;
  } };exports.socketFun = socketFun;

/***/ }),

/***/ 11:
/*!*****************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/wxcomponents/promise/promise.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process, global) {/* @preserve
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013-2017 Petka Antonov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
/**
     * bluebird build version 3.5.0
     * Features enabled: core
     * Features disabled: race, call_get, generators, map, nodeify, promisify, props, reduce, settle, some, using, timers, filter, any, each
    */
!function (t) {if (true) module.exports = t();else { var e; }}(function () {var t, e, n;return function r(t, e, n) {function i(a, s) {if (!e[a]) {if (!t[a]) {var c = "function" == typeof _dereq_ && _dereq_;if (!s && c) return c(a, !0);if (o) return o(a, !0);var l = new Error("Cannot find module '" + a + "'");throw l.code = "MODULE_NOT_FOUND", l;}var u = e[a] = { exports: {} };t[a][0].call(u.exports, function (e) {var n = t[a][1][e];return i(n ? n : e);}, u, u.exports, r, t, e, n);}return e[a].exports;}for (var o = "function" == typeof _dereq_ && _dereq_, a = 0; a < n.length; a++) {i(n[a]);}return i;}({ 1: [function (t, e, n) {"use strict";function r() {this._customScheduler = !1, this._isTickUsed = !1, this._lateQueue = new u(16), this._normalQueue = new u(16), this._haveDrainedQueues = !1, this._trampolineEnabled = !0;var t = this;this.drainQueues = function () {t._drainQueues();}, this._schedule = l;}function i(t, e, n) {this._lateQueue.push(t, e, n), this._queueTick();}function o(t, e, n) {this._normalQueue.push(t, e, n), this._queueTick();}function a(t) {this._normalQueue._pushOne(t), this._queueTick();}var s;try {throw new Error();} catch (c) {s = c;}var l = t("./schedule"),u = t("./queue"),p = t("./util");r.prototype.setScheduler = function (t) {var e = this._schedule;return this._schedule = t, this._customScheduler = !0, e;}, r.prototype.hasCustomScheduler = function () {return this._customScheduler;}, r.prototype.enableTrampoline = function () {this._trampolineEnabled = !0;}, r.prototype.disableTrampolineIfNecessary = function () {p.hasDevTools && (this._trampolineEnabled = !1);}, r.prototype.haveItemsQueued = function () {return this._isTickUsed || this._haveDrainedQueues;}, r.prototype.fatalError = function (t, e) {e ? (process.stderr.write("Fatal " + (t instanceof Error ? t.stack : t) + "\n"), process.exit(2)) : this.throwLater(t);}, r.prototype.throwLater = function (t, e) {if (1 === arguments.length && (e = t, t = function t() {throw e;}), "undefined" != typeof setTimeout) setTimeout(function () {t(e);}, 0);else try {this._schedule(function () {t(e);});} catch (n) {throw new Error("No async scheduler available\n\n    See http://goo.gl/MqrFmX\n");}}, p.hasDevTools ? (r.prototype.invokeLater = function (t, e, n) {this._trampolineEnabled ? i.call(this, t, e, n) : this._schedule(function () {setTimeout(function () {t.call(e, n);}, 100);});}, r.prototype.invoke = function (t, e, n) {this._trampolineEnabled ? o.call(this, t, e, n) : this._schedule(function () {t.call(e, n);});}, r.prototype.settlePromises = function (t) {this._trampolineEnabled ? a.call(this, t) : this._schedule(function () {t._settlePromises();});}) : (r.prototype.invokeLater = i, r.prototype.invoke = o, r.prototype.settlePromises = a), r.prototype._drainQueue = function (t) {for (; t.length() > 0;) {var e = t.shift();if ("function" == typeof e) {var n = t.shift(),r = t.shift();e.call(n, r);} else e._settlePromises();}}, r.prototype._drainQueues = function () {this._drainQueue(this._normalQueue), this._reset(), this._haveDrainedQueues = !0, this._drainQueue(this._lateQueue);}, r.prototype._queueTick = function () {this._isTickUsed || (this._isTickUsed = !0, this._schedule(this.drainQueues));}, r.prototype._reset = function () {this._isTickUsed = !1;}, e.exports = r, e.exports.firstLineError = s;}, { "./queue": 17, "./schedule": 18, "./util": 21 }], 2: [function (t, e, n) {"use strict";e.exports = function (t, e, n, r) {var i = !1,o = function o(t, e) {this._reject(e);},a = function a(t, e) {e.promiseRejectionQueued = !0, e.bindingPromise._then(o, o, null, this, t);},s = function s(t, e) {0 === (50397184 & this._bitField) && this._resolveCallback(e.target);},c = function c(t, e) {e.promiseRejectionQueued || this._reject(t);};t.prototype.bind = function (o) {i || (i = !0, t.prototype._propagateFrom = r.propagateFromFunction(), t.prototype._boundValue = r.boundValueFunction());var l = n(o),u = new t(e);u._propagateFrom(this, 1);var p = this._target();if (u._setBoundTo(l), l instanceof t) {var f = { promiseRejectionQueued: !1, promise: u, target: p, bindingPromise: l };p._then(e, a, void 0, u, f), l._then(s, c, void 0, u, f), u._setOnCancel(l);} else u._resolveCallback(p);return u;}, t.prototype._setBoundTo = function (t) {void 0 !== t ? (this._bitField = 2097152 | this._bitField, this._boundTo = t) : this._bitField = -2097153 & this._bitField;}, t.prototype._isBound = function () {return 2097152 === (2097152 & this._bitField);}, t.bind = function (e, n) {return t.resolve(n).bind(e);};};}, {}], 3: [function (t, e, n) {"use strict";function r() {try {Promise === o && (Promise = i);} catch (t) {}return o;}var i;"undefined" != typeof Promise && (i = Promise);var o = t("./promise")();o.noConflict = r, e.exports = o;}, { "./promise": 15 }], 4: [function (t, e, n) {"use strict";e.exports = function (e, n, r, i) {var o = t("./util"),a = o.tryCatch,s = o.errorObj,c = e._async;e.prototype["break"] = e.prototype.cancel = function () {if (!i.cancellation()) return this._warn("cancellation is disabled");for (var t = this, e = t; t._isCancellable();) {if (!t._cancelBy(e)) {e._isFollowing() ? e._followee().cancel() : e._cancelBranched();break;}var n = t._cancellationParent;if (null == n || !n._isCancellable()) {t._isFollowing() ? t._followee().cancel() : t._cancelBranched();break;}t._isFollowing() && t._followee().cancel(), t._setWillBeCancelled(), e = t, t = n;}}, e.prototype._branchHasCancelled = function () {this._branchesRemainingToCancel--;}, e.prototype._enoughBranchesHaveCancelled = function () {return void 0 === this._branchesRemainingToCancel || this._branchesRemainingToCancel <= 0;}, e.prototype._cancelBy = function (t) {return t === this ? (this._branchesRemainingToCancel = 0, this._invokeOnCancel(), !0) : (this._branchHasCancelled(), this._enoughBranchesHaveCancelled() ? (this._invokeOnCancel(), !0) : !1);}, e.prototype._cancelBranched = function () {this._enoughBranchesHaveCancelled() && this._cancel();}, e.prototype._cancel = function () {this._isCancellable() && (this._setCancelled(), c.invoke(this._cancelPromises, this, void 0));}, e.prototype._cancelPromises = function () {this._length() > 0 && this._settlePromises();}, e.prototype._unsetOnCancel = function () {this._onCancelField = void 0;}, e.prototype._isCancellable = function () {return this.isPending() && !this._isCancelled();}, e.prototype.isCancellable = function () {return this.isPending() && !this.isCancelled();}, e.prototype._doInvokeOnCancel = function (t, e) {if (o.isArray(t)) for (var n = 0; n < t.length; ++n) {this._doInvokeOnCancel(t[n], e);} else if (void 0 !== t) if ("function" == typeof t) {if (!e) {var r = a(t).call(this._boundValue());r === s && (this._attachExtraTrace(r.e), c.throwLater(r.e));}} else t._resultCancelled(this);}, e.prototype._invokeOnCancel = function () {var t = this._onCancel();this._unsetOnCancel(), c.invoke(this._doInvokeOnCancel, this, t);}, e.prototype._invokeInternalOnCancel = function () {this._isCancellable() && (this._doInvokeOnCancel(this._onCancel(), !0), this._unsetOnCancel());}, e.prototype._resultCancelled = function () {this.cancel();};};}, { "./util": 21 }], 5: [function (t, e, n) {"use strict";e.exports = function (e) {function n(t, n, s) {return function (c) {var l = s._boundValue();t: for (var u = 0; u < t.length; ++u) {var p = t[u];if (p === Error || null != p && p.prototype instanceof Error) {if (c instanceof p) return o(n).call(l, c);} else if ("function" == typeof p) {var f = o(p).call(l, c);if (f === a) return f;if (f) return o(n).call(l, c);} else if (r.isObject(c)) {for (var h = i(p), _ = 0; _ < h.length; ++_) {var d = h[_];if (p[d] != c[d]) continue t;}return o(n).call(l, c);}}return e;};}var r = t("./util"),i = t("./es5").keys,o = r.tryCatch,a = r.errorObj;return n;};}, { "./es5": 10, "./util": 21 }], 6: [function (t, e, n) {"use strict";e.exports = function (t) {function e() {this._trace = new e.CapturedTrace(r());}function n() {return i ? new e() : void 0;}function r() {var t = o.length - 1;return t >= 0 ? o[t] : void 0;}var i = !1,o = [];return t.prototype._promiseCreated = function () {}, t.prototype._pushContext = function () {}, t.prototype._popContext = function () {return null;}, t._peekContext = t.prototype._peekContext = function () {}, e.prototype._pushContext = function () {void 0 !== this._trace && (this._trace._promiseCreated = null, o.push(this._trace));}, e.prototype._popContext = function () {if (void 0 !== this._trace) {var t = o.pop(),e = t._promiseCreated;return t._promiseCreated = null, e;}return null;}, e.CapturedTrace = null, e.create = n, e.deactivateLongStackTraces = function () {}, e.activateLongStackTraces = function () {var n = t.prototype._pushContext,o = t.prototype._popContext,a = t._peekContext,s = t.prototype._peekContext,c = t.prototype._promiseCreated;e.deactivateLongStackTraces = function () {t.prototype._pushContext = n, t.prototype._popContext = o, t._peekContext = a, t.prototype._peekContext = s, t.prototype._promiseCreated = c, i = !1;}, i = !0, t.prototype._pushContext = e.prototype._pushContext, t.prototype._popContext = e.prototype._popContext, t._peekContext = t.prototype._peekContext = r, t.prototype._promiseCreated = function () {var t = this._peekContext();t && null == t._promiseCreated && (t._promiseCreated = this);};}, e;};}, {}], 7: [function (t, e, n) {"use strict";e.exports = function (e, n) {function r(t, e) {return { promise: e };}function i() {return !1;}function o(t, e, n) {var r = this;try {t(e, n, function (t) {if ("function" != typeof t) throw new TypeError("onCancel must be a function, got: " + I.toString(t));r._attachCancellationCallback(t);});} catch (i) {return i;}}function a(t) {if (!this._isCancellable()) return this;var e = this._onCancel();void 0 !== e ? I.isArray(e) ? e.push(t) : this._setOnCancel([e, t]) : this._setOnCancel(t);}function s() {return this._onCancelField;}function c(t) {this._onCancelField = t;}function l() {this._cancellationParent = void 0, this._onCancelField = void 0;}function u(t, e) {if (0 !== (1 & e)) {this._cancellationParent = t;var n = t._branchesRemainingToCancel;void 0 === n && (n = 0), t._branchesRemainingToCancel = n + 1;}0 !== (2 & e) && t._isBound() && this._setBoundTo(t._boundTo);}function p(t, e) {0 !== (2 & e) && t._isBound() && this._setBoundTo(t._boundTo);}function f() {var t = this._boundTo;return void 0 !== t && t instanceof e ? t.isFulfilled() ? t.value() : void 0 : t;}function h() {this._trace = new x(this._peekContext());}function _(t, e) {if (H(t)) {var n = this._trace;if (void 0 !== n && e && (n = n._parent), void 0 !== n) n.attachExtraTrace(t);else if (!t.__stackCleaned__) {var r = E(t);I.notEnumerableProp(t, "stack", r.message + "\n" + r.stack.join("\n")), I.notEnumerableProp(t, "__stackCleaned__", !0);}}}function d(t, e, n, r, i) {if (void 0 === t && null !== e && X) {if (void 0 !== i && i._returnedNonUndefined()) return;if (0 === (65535 & r._bitField)) return;n && (n += " ");var o = "",a = "";if (e._trace) {for (var s = e._trace.stack.split("\n"), c = C(s), l = c.length - 1; l >= 0; --l) {var u = c[l];if (!V.test(u)) {var p = u.match(Q);p && (o = "at " + p[1] + ":" + p[2] + ":" + p[3] + " ");break;}}if (c.length > 0) for (var f = c[0], l = 0; l < s.length; ++l) {if (s[l] === f) {l > 0 && (a = "\n" + s[l - 1]);break;}}}var h = "a promise was created in a " + n + "handler " + o + "but was not returned from it, see http://goo.gl/rRqMUw" + a;r._warn(h, !0, e);}}function v(t, e) {var n = t + " is deprecated and will be removed in a future version.";return e && (n += " Use " + e + " instead."), y(n);}function y(t, n, r) {if (ot.warnings) {var i,o = new U(t);if (n) r._attachExtraTrace(o);else if (ot.longStackTraces && (i = e._peekContext())) i.attachExtraTrace(o);else {var a = E(o);o.stack = a.message + "\n" + a.stack.join("\n");}tt("warning", o) || k(o, "", !0);}}function g(t, e) {for (var n = 0; n < e.length - 1; ++n) {e[n].push("From previous event:"), e[n] = e[n].join("\n");}return n < e.length && (e[n] = e[n].join("\n")), t + "\n" + e.join("\n");}function m(t) {for (var e = 0; e < t.length; ++e) {(0 === t[e].length || e + 1 < t.length && t[e][0] === t[e + 1][0]) && (t.splice(e, 1), e--);}}function b(t) {for (var e = t[0], n = 1; n < t.length; ++n) {for (var r = t[n], i = e.length - 1, o = e[i], a = -1, s = r.length - 1; s >= 0; --s) {if (r[s] === o) {a = s;break;}}for (var s = a; s >= 0; --s) {var c = r[s];if (e[i] !== c) break;e.pop(), i--;}e = r;}}function C(t) {for (var e = [], n = 0; n < t.length; ++n) {var r = t[n],i = "    (No stack trace)" === r || q.test(r),o = i && nt(r);i && !o && (M && " " !== r.charAt(0) && (r = "    " + r), e.push(r));}return e;}function w(t) {for (var e = t.stack.replace(/\s+$/g, "").split("\n"), n = 0; n < e.length; ++n) {var r = e[n];if ("    (No stack trace)" === r || q.test(r)) break;}return n > 0 && "SyntaxError" != t.name && (e = e.slice(n)), e;}function E(t) {var e = t.stack,n = t.toString();return e = "string" == typeof e && e.length > 0 ? w(t) : ["    (No stack trace)"], { message: n, stack: "SyntaxError" == t.name ? e : C(e) };}function k(t, e, n) {if ("undefined" != typeof console) {var r;if (I.isObject(t)) {var i = t.stack;r = e + G(i, t);} else r = e + String(t);"function" == typeof L ? L(r, n) : ("function" == typeof console.log || "object" == typeof console.log) && console.log(r);}}function j(t, e, n, r) {var i = !1;try {"function" == typeof e && (i = !0, "rejectionHandled" === t ? e(r) : e(n, r));} catch (o) {B.throwLater(o);}"unhandledRejection" === t ? tt(t, n, r) || i || k(n, "Unhandled rejection ") : tt(t, r);}function F(t) {var e;if ("function" == typeof t) e = "[function " + (t.name || "anonymous") + "]";else {e = t && "function" == typeof t.toString ? t.toString() : I.toString(t);var n = /\[object [a-zA-Z0-9$_]+\]/;if (n.test(e)) try {var r = JSON.stringify(t);e = r;} catch (i) {}0 === e.length && (e = "(empty array)");}return "(<" + T(e) + ">, no stack trace)";}function T(t) {var e = 41;return t.length < e ? t : t.substr(0, e - 3) + "...";}function P() {return "function" == typeof it;}function R(t) {var e = t.match(rt);return e ? { fileName: e[1], line: parseInt(e[2], 10) } : void 0;}function S(t, e) {if (P()) {for (var n, r, i = t.stack.split("\n"), o = e.stack.split("\n"), a = -1, s = -1, c = 0; c < i.length; ++c) {var l = R(i[c]);if (l) {n = l.fileName, a = l.line;break;}}for (var c = 0; c < o.length; ++c) {var l = R(o[c]);if (l) {r = l.fileName, s = l.line;break;}}0 > a || 0 > s || !n || !r || n !== r || a >= s || (nt = function nt(t) {if (D.test(t)) return !0;var e = R(t);return e && e.fileName === n && a <= e.line && e.line <= s ? !0 : !1;});}}function x(t) {this._parent = t, this._promisesCreated = 0;var e = this._length = 1 + (void 0 === t ? 0 : t._length);it(this, x), e > 32 && this.uncycle();}var O,A,L,N = e._getDomain,B = e._async,U = t("./errors").Warning,I = t("./util"),H = I.canAttachTrace,D = /[\\\/]bluebird[\\\/]js[\\\/](release|debug|instrumented)/,V = /\((?:timers\.js):\d+:\d+\)/,Q = /[\/<\(](.+?):(\d+):(\d+)\)?\s*$/,q = null,G = null,M = !1,W = !(0 == I.env("BLUEBIRD_DEBUG") || !I.env("BLUEBIRD_DEBUG") && "development" !== I.env("NODE_ENV")),$ = !(0 == I.env("BLUEBIRD_WARNINGS") || !W && !I.env("BLUEBIRD_WARNINGS")),z = !(0 == I.env("BLUEBIRD_LONG_STACK_TRACES") || !W && !I.env("BLUEBIRD_LONG_STACK_TRACES")),X = 0 != I.env("BLUEBIRD_W_FORGOTTEN_RETURN") && ($ || !!I.env("BLUEBIRD_W_FORGOTTEN_RETURN"));e.prototype.suppressUnhandledRejections = function () {var t = this._target();t._bitField = -1048577 & t._bitField | 524288;}, e.prototype._ensurePossibleRejectionHandled = function () {0 === (524288 & this._bitField) && (this._setRejectionIsUnhandled(), B.invokeLater(this._notifyUnhandledRejection, this, void 0));}, e.prototype._notifyUnhandledRejectionIsHandled = function () {j("rejectionHandled", O, void 0, this);}, e.prototype._setReturnedNonUndefined = function () {this._bitField = 268435456 | this._bitField;}, e.prototype._returnedNonUndefined = function () {return 0 !== (268435456 & this._bitField);}, e.prototype._notifyUnhandledRejection = function () {if (this._isRejectionUnhandled()) {var t = this._settledValue();this._setUnhandledRejectionIsNotified(), j("unhandledRejection", A, t, this);}}, e.prototype._setUnhandledRejectionIsNotified = function () {this._bitField = 262144 | this._bitField;}, e.prototype._unsetUnhandledRejectionIsNotified = function () {this._bitField = -262145 & this._bitField;}, e.prototype._isUnhandledRejectionNotified = function () {return (262144 & this._bitField) > 0;}, e.prototype._setRejectionIsUnhandled = function () {this._bitField = 1048576 | this._bitField;}, e.prototype._unsetRejectionIsUnhandled = function () {this._bitField = -1048577 & this._bitField, this._isUnhandledRejectionNotified() && (this._unsetUnhandledRejectionIsNotified(), this._notifyUnhandledRejectionIsHandled());}, e.prototype._isRejectionUnhandled = function () {return (1048576 & this._bitField) > 0;}, e.prototype._warn = function (t, e, n) {return y(t, e, n || this);}, e.onPossiblyUnhandledRejection = function (t) {var e = N();A = "function" == typeof t ? null === e ? t : I.domainBind(e, t) : void 0;}, e.onUnhandledRejectionHandled = function (t) {var e = N();O = "function" == typeof t ? null === e ? t : I.domainBind(e, t) : void 0;};var K = function K() {};e.longStackTraces = function () {if (B.haveItemsQueued() && !ot.longStackTraces) throw new Error("cannot enable long stack traces after promises have been created\n\n    See http://goo.gl/MqrFmX\n");if (!ot.longStackTraces && P()) {var t = e.prototype._captureStackTrace,r = e.prototype._attachExtraTrace;ot.longStackTraces = !0, K = function K() {if (B.haveItemsQueued() && !ot.longStackTraces) throw new Error("cannot enable long stack traces after promises have been created\n\n    See http://goo.gl/MqrFmX\n");e.prototype._captureStackTrace = t, e.prototype._attachExtraTrace = r, n.deactivateLongStackTraces(), B.enableTrampoline(), ot.longStackTraces = !1;}, e.prototype._captureStackTrace = h, e.prototype._attachExtraTrace = _, n.activateLongStackTraces(), B.disableTrampolineIfNecessary();}}, e.hasLongStackTraces = function () {return ot.longStackTraces && P();};var J = function () {try {if ("function" == typeof CustomEvent) {var t = new CustomEvent("CustomEvent");return I.global.dispatchEvent(t), function (t, e) {var n = new CustomEvent(t.toLowerCase(), { detail: e, cancelable: !0 });return !I.global.dispatchEvent(n);};}if ("function" == typeof Event) {var t = new Event("CustomEvent");return I.global.dispatchEvent(t), function (t, e) {var n = new Event(t.toLowerCase(), { cancelable: !0 });return n.detail = e, !I.global.dispatchEvent(n);};}var t = document.createEvent("CustomEvent");return t.initCustomEvent("testingtheevent", !1, !0, {}), I.global.dispatchEvent(t), function (t, e) {var n = document.createEvent("CustomEvent");return n.initCustomEvent(t.toLowerCase(), !1, !0, e), !I.global.dispatchEvent(n);};} catch (e) {}return function () {return !1;};}(),Y = function () {return I.isNode ? function () {return process.emit.apply(process, arguments);} : I.global ? function (t) {var e = "on" + t.toLowerCase(),n = I.global[e];return n ? (n.apply(I.global, [].slice.call(arguments, 1)), !0) : !1;} : function () {return !1;};}(),Z = { promiseCreated: r, promiseFulfilled: r, promiseRejected: r, promiseResolved: r, promiseCancelled: r, promiseChained: function promiseChained(t, e, n) {return { promise: e, child: n };}, warning: function warning(t, e) {return { warning: e };}, unhandledRejection: function unhandledRejection(t, e, n) {return { reason: e, promise: n };}, rejectionHandled: r },tt = function tt(t) {var e = !1;try {e = Y.apply(null, arguments);} catch (n) {B.throwLater(n), e = !0;}var r = !1;try {r = J(t, Z[t].apply(null, arguments));} catch (n) {B.throwLater(n), r = !0;}return r || e;};e.config = function (t) {if (t = Object(t), "longStackTraces" in t && (t.longStackTraces ? e.longStackTraces() : !t.longStackTraces && e.hasLongStackTraces() && K()), "warnings" in t) {var n = t.warnings;ot.warnings = !!n, X = ot.warnings, I.isObject(n) && "wForgottenReturn" in n && (X = !!n.wForgottenReturn);}if ("cancellation" in t && t.cancellation && !ot.cancellation) {if (B.haveItemsQueued()) throw new Error("cannot enable cancellation after promises are in use");e.prototype._clearCancellationData = l, e.prototype._propagateFrom = u, e.prototype._onCancel = s, e.prototype._setOnCancel = c, e.prototype._attachCancellationCallback = a, e.prototype._execute = o, et = u, ot.cancellation = !0;}return "monitoring" in t && (t.monitoring && !ot.monitoring ? (ot.monitoring = !0, e.prototype._fireEvent = tt) : !t.monitoring && ot.monitoring && (ot.monitoring = !1, e.prototype._fireEvent = i)), e;}, e.prototype._fireEvent = i, e.prototype._execute = function (t, e, n) {try {t(e, n);} catch (r) {return r;}}, e.prototype._onCancel = function () {}, e.prototype._setOnCancel = function (t) {}, e.prototype._attachCancellationCallback = function (t) {}, e.prototype._captureStackTrace = function () {}, e.prototype._attachExtraTrace = function () {}, e.prototype._clearCancellationData = function () {}, e.prototype._propagateFrom = function (t, e) {};var et = p,nt = function nt() {return !1;},rt = /[\/<\(]([^:\/]+):(\d+):(?:\d+)\)?\s*$/;I.inherits(x, Error), n.CapturedTrace = x, x.prototype.uncycle = function () {var t = this._length;if (!(2 > t)) {for (var e = [], n = {}, r = 0, i = this; void 0 !== i; ++r) {e.push(i), i = i._parent;}t = this._length = r;for (var r = t - 1; r >= 0; --r) {var o = e[r].stack;void 0 === n[o] && (n[o] = r);}for (var r = 0; t > r; ++r) {var a = e[r].stack,s = n[a];if (void 0 !== s && s !== r) {s > 0 && (e[s - 1]._parent = void 0, e[s - 1]._length = 1), e[r]._parent = void 0, e[r]._length = 1;var c = r > 0 ? e[r - 1] : this;t - 1 > s ? (c._parent = e[s + 1], c._parent.uncycle(), c._length = c._parent._length + 1) : (c._parent = void 0, c._length = 1);for (var l = c._length + 1, u = r - 2; u >= 0; --u) {e[u]._length = l, l++;}return;}}}}, x.prototype.attachExtraTrace = function (t) {if (!t.__stackCleaned__) {this.uncycle();for (var e = E(t), n = e.message, r = [e.stack], i = this; void 0 !== i;) {r.push(C(i.stack.split("\n"))), i = i._parent;}b(r), m(r), I.notEnumerableProp(t, "stack", g(n, r)), I.notEnumerableProp(t, "__stackCleaned__", !0);}};var it = function () {var t = /^\s*at\s*/,e = function e(t, _e) {return "string" == typeof t ? t : void 0 !== _e.name && void 0 !== _e.message ? _e.toString() : F(_e);};if ("number" == typeof Error.stackTraceLimit && "function" == typeof Error.captureStackTrace) {Error.stackTraceLimit += 6, q = t, G = e;var n = Error.captureStackTrace;return nt = function nt(t) {return D.test(t);}, function (t, e) {Error.stackTraceLimit += 6, n(t, e), Error.stackTraceLimit -= 6;};}var r = new Error();if ("string" == typeof r.stack && r.stack.split("\n")[0].indexOf("stackDetection@") >= 0) return q = /@/, G = e, M = !0, function (t) {t.stack = new Error().stack;};var i;try {throw new Error();} catch (o) {i = "stack" in o;}return "stack" in r || !i || "number" != typeof Error.stackTraceLimit ? (G = function G(t, e) {return "string" == typeof t ? t : "object" != typeof e && "function" != typeof e || void 0 === e.name || void 0 === e.message ? F(e) : e.toString();}, null) : (q = t, G = e, function (t) {Error.stackTraceLimit += 6;try {throw new Error();} catch (e) {t.stack = e.stack;}Error.stackTraceLimit -= 6;});}([]);"undefined" != typeof console && "undefined" != typeof console.warn && (L = function L(t) {console.warn(t);}, I.isNode && process.stderr.isTTY ? L = function L(t, e) {var n = e ? "[33m" : "[31m";console.warn(n + t + "[0m\n");} : I.isNode || "string" != typeof new Error().stack || (L = function L(t, e) {console.warn("%c" + t, e ? "color: darkorange" : "color: red");}));var ot = { warnings: $, longStackTraces: !1, cancellation: !1, monitoring: !1 };return z && e.longStackTraces(), { longStackTraces: function longStackTraces() {return ot.longStackTraces;}, warnings: function warnings() {return ot.warnings;}, cancellation: function cancellation() {return ot.cancellation;}, monitoring: function monitoring() {return ot.monitoring;}, propagateFromFunction: function propagateFromFunction() {return et;}, boundValueFunction: function boundValueFunction() {return f;}, checkForgottenReturns: d, setBounds: S, warn: y, deprecated: v, CapturedTrace: x, fireDomEvent: J, fireGlobalEvent: Y };};}, { "./errors": 9, "./util": 21 }], 8: [function (t, e, n) {"use strict";e.exports = function (t) {function e() {return this.value;}function n() {throw this.reason;}t.prototype["return"] = t.prototype.thenReturn = function (n) {return n instanceof t && n.suppressUnhandledRejections(), this._then(e, void 0, void 0, { value: n }, void 0);}, t.prototype["throw"] = t.prototype.thenThrow = function (t) {return this._then(n, void 0, void 0, { reason: t }, void 0);}, t.prototype.catchThrow = function (t) {if (arguments.length <= 1) return this._then(void 0, n, void 0, { reason: t }, void 0);var e = arguments[1],r = function r() {throw e;};return this.caught(t, r);}, t.prototype.catchReturn = function (n) {if (arguments.length <= 1) return n instanceof t && n.suppressUnhandledRejections(), this._then(void 0, e, void 0, { value: n }, void 0);var r = arguments[1];r instanceof t && r.suppressUnhandledRejections();var i = function i() {return r;};return this.caught(n, i);};};}, {}], 9: [function (t, e, n) {"use strict";function r(t, e) {function n(r) {return this instanceof n ? (p(this, "message", "string" == typeof r ? r : e), p(this, "name", t), void (Error.captureStackTrace ? Error.captureStackTrace(this, this.constructor) : Error.call(this))) : new n(r);}return u(n, Error), n;}function i(t) {return this instanceof i ? (p(this, "name", "OperationalError"), p(this, "message", t), this.cause = t, this.isOperational = !0, void (t instanceof Error ? (p(this, "message", t.message), p(this, "stack", t.stack)) : Error.captureStackTrace && Error.captureStackTrace(this, this.constructor))) : new i(t);}var o,a,s = t("./es5"),c = s.freeze,l = t("./util"),u = l.inherits,p = l.notEnumerableProp,f = r("Warning", "warning"),h = r("CancellationError", "cancellation error"),_ = r("TimeoutError", "timeout error"),d = r("AggregateError", "aggregate error");try {o = TypeError, a = RangeError;} catch (v) {o = r("TypeError", "type error"), a = r("RangeError", "range error");}for (var y = "join pop push shift unshift slice filter forEach some every map indexOf lastIndexOf reduce reduceRight sort reverse".split(" "), g = 0; g < y.length; ++g) {"function" == typeof Array.prototype[y[g]] && (d.prototype[y[g]] = Array.prototype[y[g]]);}s.defineProperty(d.prototype, "length", { value: 0, configurable: !1, writable: !0, enumerable: !0 }), d.prototype.isOperational = !0;var m = 0;d.prototype.toString = function () {var t = Array(4 * m + 1).join(" "),e = "\n" + t + "AggregateError of:\n";m++, t = Array(4 * m + 1).join(" ");for (var n = 0; n < this.length; ++n) {for (var r = this[n] === this ? "[Circular AggregateError]" : this[n] + "", i = r.split("\n"), o = 0; o < i.length; ++o) {i[o] = t + i[o];}r = i.join("\n"), e += r + "\n";}return m--, e;}, u(i, Error);var b = Error.__BluebirdErrorTypes__;b || (b = c({ CancellationError: h, TimeoutError: _, OperationalError: i, RejectionError: i, AggregateError: d }), s.defineProperty(Error, "__BluebirdErrorTypes__", { value: b, writable: !1, enumerable: !1, configurable: !1 })), e.exports = { Error: Error, TypeError: o, RangeError: a, CancellationError: b.CancellationError, OperationalError: b.OperationalError, TimeoutError: b.TimeoutError, AggregateError: b.AggregateError, Warning: f };}, { "./es5": 10, "./util": 21 }], 10: [function (t, e, n) {var r = function () {"use strict";return void 0 === this;}();if (r) e.exports = { freeze: Object.freeze, defineProperty: Object.defineProperty, getDescriptor: Object.getOwnPropertyDescriptor, keys: Object.keys, names: Object.getOwnPropertyNames, getPrototypeOf: Object.getPrototypeOf, isArray: Array.isArray, isES5: r, propertyIsWritable: function propertyIsWritable(t, e) {var n = Object.getOwnPropertyDescriptor(t, e);return !(n && !n.writable && !n.set);} };else {var i = {}.hasOwnProperty,o = {}.toString,a = {}.constructor.prototype,s = function s(t) {var e = [];for (var n in t) {i.call(t, n) && e.push(n);}return e;},c = function c(t, e) {return { value: t[e] };},l = function l(t, e, n) {return t[e] = n.value, t;},u = function u(t) {return t;},p = function p(t) {try {return Object(t).constructor.prototype;} catch (e) {return a;}},f = function f(t) {try {return "[object Array]" === o.call(t);} catch (e) {return !1;}};e.exports = { isArray: f, keys: s, names: s, defineProperty: l, getDescriptor: c, freeze: u, getPrototypeOf: p, isES5: r, propertyIsWritable: function propertyIsWritable() {return !0;} };}}, {}], 11: [function (t, e, n) {"use strict";e.exports = function (e, n, r) {function i(t, e, n) {this.promise = t, this.type = e, this.handler = n, this.called = !1, this.cancelPromise = null;}function o(t) {this.finallyHandler = t;}function a(t, e) {return null != t.cancelPromise ? (arguments.length > 1 ? t.cancelPromise._reject(e) : t.cancelPromise._cancel(), t.cancelPromise = null, !0) : !1;}function s() {return l.call(this, this.promise._target()._settledValue());}function c(t) {return a(this, t) ? void 0 : (f.e = t, f);}function l(t) {var i = this.promise,l = this.handler;if (!this.called) {this.called = !0;var u = this.isFinallyHandler() ? l.call(i._boundValue()) : l.call(i._boundValue(), t);if (u === r) return u;if (void 0 !== u) {i._setReturnedNonUndefined();var h = n(u, i);if (h instanceof e) {if (null != this.cancelPromise) {if (h._isCancelled()) {var _ = new p("late cancellation observer");return i._attachExtraTrace(_), f.e = _, f;}h.isPending() && h._attachCancellationCallback(new o(this));}return h._then(s, c, void 0, this, void 0);}}}return i.isRejected() ? (a(this), f.e = t, f) : (a(this), t);}var u = t("./util"),p = e.CancellationError,f = u.errorObj,h = t("./catch_filter")(r);return i.prototype.isFinallyHandler = function () {return 0 === this.type;}, o.prototype._resultCancelled = function () {a(this.finallyHandler);}, e.prototype._passThrough = function (t, e, n, r) {return "function" != typeof t ? this.then() : this._then(n, r, void 0, new i(this, e, t), void 0);}, e.prototype.lastly = e.prototype["finally"] = function (t) {return this._passThrough(t, 0, l, l);}, e.prototype.tap = function (t) {return this._passThrough(t, 1, l);}, e.prototype.tapCatch = function (t) {var n = arguments.length;if (1 === n) return this._passThrough(t, 1, void 0, l);var r,i = new Array(n - 1),o = 0;for (r = 0; n - 1 > r; ++r) {var a = arguments[r];if (!u.isObject(a)) return e.reject(new TypeError("tapCatch statement predicate: expecting an object but got " + u.classString(a)));i[o++] = a;}i.length = o;var s = arguments[r];return this._passThrough(h(i, s, this), 1, void 0, l);}, i;};}, { "./catch_filter": 5, "./util": 21 }], 12: [function (t, e, n) {"use strict";e.exports = function (e, n, r, i, o, a) {var s = t("./util");s.canEvaluate, s.tryCatch, s.errorObj;e.join = function () {var t,e = arguments.length - 1;if (e > 0 && "function" == typeof arguments[e]) {t = arguments[e];var r;}var i = [].slice.call(arguments);t && i.pop();var r = new n(i).promise();return void 0 !== t ? r.spread(t) : r;};};}, { "./util": 21 }], 13: [function (t, e, n) {"use strict";e.exports = function (e, n, r, i, o) {var a = t("./util"),s = a.tryCatch;e.method = function (t) {if ("function" != typeof t) throw new e.TypeError("expecting a function but got " + a.classString(t));return function () {var r = new e(n);r._captureStackTrace(), r._pushContext();var i = s(t).apply(this, arguments),a = r._popContext();return o.checkForgottenReturns(i, a, "Promise.method", r), r._resolveFromSyncValue(i), r;};}, e.attempt = e["try"] = function (t) {if ("function" != typeof t) return i("expecting a function but got " + a.classString(t));var r = new e(n);r._captureStackTrace(), r._pushContext();var c;if (arguments.length > 1) {o.deprecated("calling Promise.try with more than 1 argument");var l = arguments[1],u = arguments[2];c = a.isArray(l) ? s(t).apply(u, l) : s(t).call(u, l);} else c = s(t)();var p = r._popContext();return o.checkForgottenReturns(c, p, "Promise.try", r), r._resolveFromSyncValue(c), r;}, e.prototype._resolveFromSyncValue = function (t) {t === a.errorObj ? this._rejectCallback(t.e, !1) : this._resolveCallback(t, !0);};};}, { "./util": 21 }], 14: [function (t, e, n) {"use strict";function r(t) {return t instanceof Error && u.getPrototypeOf(t) === Error.prototype;}function i(t) {var e;if (r(t)) {e = new l(t), e.name = t.name, e.message = t.message, e.stack = t.stack;for (var n = u.keys(t), i = 0; i < n.length; ++i) {var o = n[i];p.test(o) || (e[o] = t[o]);}return e;}return a.markAsOriginatingFromRejection(t), t;}function o(t, e) {return function (n, r) {if (null !== t) {if (n) {var o = i(s(n));t._attachExtraTrace(o), t._reject(o);} else if (e) {var a = [].slice.call(arguments, 1);t._fulfill(a);} else t._fulfill(r);t = null;}};}var a = t("./util"),s = a.maybeWrapAsError,c = t("./errors"),l = c.OperationalError,u = t("./es5"),p = /^(?:name|message|stack|cause)$/;e.exports = o;}, { "./errors": 9, "./es5": 10, "./util": 21 }], 15: [function (t, e, n) {
      "use strict";e.exports = function () {
        function n() {}function r(t, e) {if (null == t || t.constructor !== i) throw new g("the promise constructor cannot be invoked directly\n\n    See http://goo.gl/MqrFmX\n");if ("function" != typeof e) throw new g("expecting a function but got " + h.classString(e));}function i(t) {t !== b && r(this, t), this._bitField = 0, this._fulfillmentHandler0 = void 0, this._rejectionHandler0 = void 0, this._promise0 = void 0, this._receiver0 = void 0, this._resolveFromExecutor(t), this._promiseCreated(), this._fireEvent("promiseCreated", this);}function o(t) {this.promise._resolveCallback(t);}function a(t) {this.promise._rejectCallback(t, !1);}function s(t) {var e = new i(b);e._fulfillmentHandler0 = t, e._rejectionHandler0 = t, e._promise0 = t, e._receiver0 = t;}var c,l = function l() {return new g("circular promise resolution chain\n\n    See http://goo.gl/MqrFmX\n");},u = function u() {return new i.PromiseInspection(this._target());},p = function p(t) {return i.reject(new g(t));},f = {},h = t("./util");c = h.isNode ? function () {var t = process.domain;return void 0 === t && (t = null), t;} : function () {return null;}, h.notEnumerableProp(i, "_getDomain", c);var _ = t("./es5"),d = t("./async"),v = new d();_.defineProperty(i, "_async", { value: v });var y = t("./errors"),g = i.TypeError = y.TypeError;i.RangeError = y.RangeError;var m = i.CancellationError = y.CancellationError;i.TimeoutError = y.TimeoutError, i.OperationalError = y.OperationalError, i.RejectionError = y.OperationalError, i.AggregateError = y.AggregateError;var b = function b() {},C = {},w = {},E = t("./thenables")(i, b),k = t("./promise_array")(i, b, E, p, n),j = t("./context")(i),F = (j.create, t("./debuggability")(i, j)),T = (F.CapturedTrace, t("./finally")(i, E, w)),P = t("./catch_filter")(w),R = t("./nodeback"),S = h.errorObj,x = h.tryCatch;return i.prototype.toString = function () {return "[object Promise]";}, i.prototype.caught = i.prototype["catch"] = function (t) {var e = arguments.length;if (e > 1) {var n,r = new Array(e - 1),i = 0;for (n = 0; e - 1 > n; ++n) {var o = arguments[n];if (!h.isObject(o)) return p("Catch statement predicate: expecting an object but got " + h.classString(o));r[i++] = o;}return r.length = i, t = arguments[n], this.then(void 0, P(r, t, this));}return this.then(void 0, t);}, i.prototype.reflect = function () {return this._then(u, u, void 0, this, void 0);}, i.prototype.then = function (t, e) {if (F.warnings() && arguments.length > 0 && "function" != typeof t && "function" != typeof e) {var n = ".then() only accepts functions but was passed: " + h.classString(t);arguments.length > 1 && (n += ", " + h.classString(e)), this._warn(n);}return this._then(t, e, void 0, void 0, void 0);}, i.prototype.done = function (t, e) {var n = this._then(t, e, void 0, void 0, void 0);n._setIsFinal();}, i.prototype.spread = function (t) {
          return "function" != typeof t ? p("expecting a function but got " + h.classString(t)) : this.all()._then(t, void 0, void 0, C, void 0);
        }, i.prototype.toJSON = function () {var t = { isFulfilled: !1, isRejected: !1, fulfillmentValue: void 0, rejectionReason: void 0 };return this.isFulfilled() ? (t.fulfillmentValue = this.value(), t.isFulfilled = !0) : this.isRejected() && (t.rejectionReason = this.reason(), t.isRejected = !0), t;}, i.prototype.all = function () {return arguments.length > 0 && this._warn(".all() was passed arguments but it does not take any"), new k(this).promise();}, i.prototype.error = function (t) {return this.caught(h.originatesFromRejection, t);}, i.getNewLibraryCopy = e.exports, i.is = function (t) {return t instanceof i;}, i.fromNode = i.fromCallback = function (t) {var e = new i(b);e._captureStackTrace();var n = arguments.length > 1 ? !!Object(arguments[1]).multiArgs : !1,r = x(t)(R(e, n));return r === S && e._rejectCallback(r.e, !0), e._isFateSealed() || e._setAsyncGuaranteed(), e;}, i.all = function (t) {return new k(t).promise();}, i.cast = function (t) {var e = E(t);return e instanceof i || (e = new i(b), e._captureStackTrace(), e._setFulfilled(), e._rejectionHandler0 = t), e;}, i.resolve = i.fulfilled = i.cast, i.reject = i.rejected = function (t) {var e = new i(b);return e._captureStackTrace(), e._rejectCallback(t, !0), e;}, i.setScheduler = function (t) {if ("function" != typeof t) throw new g("expecting a function but got " + h.classString(t));return v.setScheduler(t);}, i.prototype._then = function (t, e, n, r, o) {var a = void 0 !== o,s = a ? o : new i(b),l = this._target(),u = l._bitField;a || (s._propagateFrom(this, 3), s._captureStackTrace(), void 0 === r && 0 !== (2097152 & this._bitField) && (r = 0 !== (50397184 & u) ? this._boundValue() : l === this ? void 0 : this._boundTo), this._fireEvent("promiseChained", this, s));var p = c();if (0 !== (50397184 & u)) {var f,_,d = l._settlePromiseCtx;0 !== (33554432 & u) ? (_ = l._rejectionHandler0, f = t) : 0 !== (16777216 & u) ? (_ = l._fulfillmentHandler0, f = e, l._unsetRejectionIsUnhandled()) : (d = l._settlePromiseLateCancellationObserver, _ = new m("late cancellation observer"), l._attachExtraTrace(_), f = e), v.invoke(d, l, { handler: null === p ? f : "function" == typeof f && h.domainBind(p, f), promise: s, receiver: r, value: _ });} else l._addCallbacks(t, e, s, r, p);return s;}, i.prototype._length = function () {return 65535 & this._bitField;}, i.prototype._isFateSealed = function () {return 0 !== (117506048 & this._bitField);}, i.prototype._isFollowing = function () {return 67108864 === (67108864 & this._bitField);}, i.prototype._setLength = function (t) {this._bitField = -65536 & this._bitField | 65535 & t;}, i.prototype._setFulfilled = function () {this._bitField = 33554432 | this._bitField, this._fireEvent("promiseFulfilled", this);}, i.prototype._setRejected = function () {this._bitField = 16777216 | this._bitField, this._fireEvent("promiseRejected", this);}, i.prototype._setFollowing = function () {this._bitField = 67108864 | this._bitField, this._fireEvent("promiseResolved", this);}, i.prototype._setIsFinal = function () {this._bitField = 4194304 | this._bitField;}, i.prototype._isFinal = function () {return (4194304 & this._bitField) > 0;}, i.prototype._unsetCancelled = function () {this._bitField = -65537 & this._bitField;}, i.prototype._setCancelled = function () {this._bitField = 65536 | this._bitField, this._fireEvent("promiseCancelled", this);}, i.prototype._setWillBeCancelled = function () {this._bitField = 8388608 | this._bitField;}, i.prototype._setAsyncGuaranteed = function () {v.hasCustomScheduler() || (this._bitField = 134217728 | this._bitField);}, i.prototype._receiverAt = function (t) {var e = 0 === t ? this._receiver0 : this[4 * t - 4 + 3];return e === f ? void 0 : void 0 === e && this._isBound() ? this._boundValue() : e;}, i.prototype._promiseAt = function (t) {return this[4 * t - 4 + 2];}, i.prototype._fulfillmentHandlerAt = function (t) {return this[4 * t - 4 + 0];}, i.prototype._rejectionHandlerAt = function (t) {return this[4 * t - 4 + 1];}, i.prototype._boundValue = function () {}, i.prototype._migrateCallback0 = function (t) {var e = (t._bitField, t._fulfillmentHandler0),n = t._rejectionHandler0,r = t._promise0,i = t._receiverAt(0);void 0 === i && (i = f), this._addCallbacks(e, n, r, i, null);}, i.prototype._migrateCallbackAt = function (t, e) {var n = t._fulfillmentHandlerAt(e),r = t._rejectionHandlerAt(e),i = t._promiseAt(e),o = t._receiverAt(e);void 0 === o && (o = f), this._addCallbacks(n, r, i, o, null);}, i.prototype._addCallbacks = function (t, e, n, r, i) {var o = this._length();if (o >= 65531 && (o = 0, this._setLength(0)), 0 === o) this._promise0 = n, this._receiver0 = r, "function" == typeof t && (this._fulfillmentHandler0 = null === i ? t : h.domainBind(i, t)), "function" == typeof e && (this._rejectionHandler0 = null === i ? e : h.domainBind(i, e));else {var a = 4 * o - 4;this[a + 2] = n, this[a + 3] = r, "function" == typeof t && (this[a + 0] = null === i ? t : h.domainBind(i, t)), "function" == typeof e && (this[a + 1] = null === i ? e : h.domainBind(i, e));}return this._setLength(o + 1), o;}, i.prototype._proxy = function (t, e) {this._addCallbacks(void 0, void 0, e, t, null);}, i.prototype._resolveCallback = function (t, e) {if (0 === (117506048 & this._bitField)) {if (t === this) return this._rejectCallback(l(), !1);var n = E(t, this);if (!(n instanceof i)) return this._fulfill(t);e && this._propagateFrom(n, 2);var r = n._target();if (r === this) return void this._reject(l());var o = r._bitField;if (0 === (50397184 & o)) {var a = this._length();a > 0 && r._migrateCallback0(this);for (var s = 1; a > s; ++s) {r._migrateCallbackAt(this, s);}this._setFollowing(), this._setLength(0), this._setFollowee(r);} else if (0 !== (33554432 & o)) this._fulfill(r._value());else if (0 !== (16777216 & o)) this._reject(r._reason());else {var c = new m("late cancellation observer");r._attachExtraTrace(c), this._reject(c);}}}, i.prototype._rejectCallback = function (t, e, n) {var r = h.ensureErrorObject(t),i = r === t;if (!i && !n && F.warnings()) {var o = "a promise was rejected with a non-error: " + h.classString(t);this._warn(o, !0);}this._attachExtraTrace(r, e ? i : !1), this._reject(t);}, i.prototype._resolveFromExecutor = function (t) {if (t !== b) {var e = this;this._captureStackTrace(), this._pushContext();var n = !0,r = this._execute(t, function (t) {e._resolveCallback(t);}, function (t) {e._rejectCallback(t, n);});n = !1, this._popContext(), void 0 !== r && e._rejectCallback(r, !0);}}, i.prototype._settlePromiseFromHandler = function (t, e, n, r) {var i = r._bitField;if (0 === (65536 & i)) {r._pushContext();var o;e === C ? n && "number" == typeof n.length ? o = x(t).apply(this._boundValue(), n) : (o = S, o.e = new g("cannot .spread() a non-array: " + h.classString(n))) : o = x(t).call(e, n);var a = r._popContext();i = r._bitField, 0 === (65536 & i) && (o === w ? r._reject(n) : o === S ? r._rejectCallback(o.e, !1) : (F.checkForgottenReturns(o, a, "", r, this), r._resolveCallback(o)));}}, i.prototype._target = function () {for (var t = this; t._isFollowing();) {t = t._followee();}return t;}, i.prototype._followee = function () {return this._rejectionHandler0;}, i.prototype._setFollowee = function (t) {this._rejectionHandler0 = t;}, i.prototype._settlePromise = function (t, e, r, o) {var a = t instanceof i,s = this._bitField,c = 0 !== (134217728 & s);0 !== (65536 & s) ? (a && t._invokeInternalOnCancel(), r instanceof T && r.isFinallyHandler() ? (r.cancelPromise = t, x(e).call(r, o) === S && t._reject(S.e)) : e === u ? t._fulfill(u.call(r)) : r instanceof n ? r._promiseCancelled(t) : a || t instanceof k ? t._cancel() : r.cancel()) : "function" == typeof e ? a ? (c && t._setAsyncGuaranteed(), this._settlePromiseFromHandler(e, r, o, t)) : e.call(r, o, t) : r instanceof n ? r._isResolved() || (0 !== (33554432 & s) ? r._promiseFulfilled(o, t) : r._promiseRejected(o, t)) : a && (c && t._setAsyncGuaranteed(), 0 !== (33554432 & s) ? t._fulfill(o) : t._reject(o));}, i.prototype._settlePromiseLateCancellationObserver = function (t) {var e = t.handler,n = t.promise,r = t.receiver,o = t.value;"function" == typeof e ? n instanceof i ? this._settlePromiseFromHandler(e, r, o, n) : e.call(r, o, n) : n instanceof i && n._reject(o);}, i.prototype._settlePromiseCtx = function (t) {this._settlePromise(t.promise, t.handler, t.receiver, t.value);}, i.prototype._settlePromise0 = function (t, e, n) {var r = this._promise0,i = this._receiverAt(0);this._promise0 = void 0, this._receiver0 = void 0, this._settlePromise(r, t, i, e);}, i.prototype._clearCallbackDataAtIndex = function (t) {var e = 4 * t - 4;this[e + 2] = this[e + 3] = this[e + 0] = this[e + 1] = void 0;}, i.prototype._fulfill = function (t) {var e = this._bitField;if (!((117506048 & e) >>> 16)) {if (t === this) {var n = l();return this._attachExtraTrace(n), this._reject(n);}this._setFulfilled(), this._rejectionHandler0 = t, (65535 & e) > 0 && (0 !== (134217728 & e) ? this._settlePromises() : v.settlePromises(this));}}, i.prototype._reject = function (t) {var e = this._bitField;if (!((117506048 & e) >>> 16)) return this._setRejected(), this._fulfillmentHandler0 = t, this._isFinal() ? v.fatalError(t, h.isNode) : void ((65535 & e) > 0 ? v.settlePromises(this) : this._ensurePossibleRejectionHandled());}, i.prototype._fulfillPromises = function (t, e) {for (var n = 1; t > n; n++) {var r = this._fulfillmentHandlerAt(n),i = this._promiseAt(n),o = this._receiverAt(n);this._clearCallbackDataAtIndex(n), this._settlePromise(i, r, o, e);}}, i.prototype._rejectPromises = function (t, e) {for (var n = 1; t > n; n++) {var r = this._rejectionHandlerAt(n),i = this._promiseAt(n),o = this._receiverAt(n);this._clearCallbackDataAtIndex(n), this._settlePromise(i, r, o, e);}}, i.prototype._settlePromises = function () {var t = this._bitField,e = 65535 & t;if (e > 0) {if (0 !== (16842752 & t)) {var n = this._fulfillmentHandler0;this._settlePromise0(this._rejectionHandler0, n, t), this._rejectPromises(e, n);} else {var r = this._rejectionHandler0;this._settlePromise0(this._fulfillmentHandler0, r, t), this._fulfillPromises(e, r);}this._setLength(0);}this._clearCancellationData();}, i.prototype._settledValue = function () {var t = this._bitField;return 0 !== (33554432 & t) ? this._rejectionHandler0 : 0 !== (16777216 & t) ? this._fulfillmentHandler0 : void 0;}, i.defer = i.pending = function () {F.deprecated("Promise.defer", "new Promise");var t = new i(b);return { promise: t, resolve: o, reject: a };}, h.notEnumerableProp(i, "_makeSelfResolutionError", l), t("./method")(i, b, E, p, F), t("./bind")(i, b, E, F), t("./cancel")(i, k, p, F), t("./direct_resolve")(i), t("./synchronous_inspection")(i), t("./join")(i, k, E, b, v, c), i.Promise = i, i.version = "3.5.0", h.toFastProperties(i), h.toFastProperties(i.prototype), s({ a: 1 }), s({ b: 2 }), s({ c: 3 }), s(1), s(function () {}), s(void 0), s(!1), s(new i(b)), F.setBounds(d.firstLineError, h.lastLineError), i;
      };
    }, { "./async": 1, "./bind": 2, "./cancel": 4, "./catch_filter": 5, "./context": 6, "./debuggability": 7, "./direct_resolve": 8, "./errors": 9, "./es5": 10, "./finally": 11, "./join": 12, "./method": 13, "./nodeback": 14, "./promise_array": 16, "./synchronous_inspection": 19, "./thenables": 20, "./util": 21 }], 16: [function (t, e, n) {"use strict";e.exports = function (e, n, r, i, o) {function a(t) {switch (t) {case -2:return [];case -3:return {};case -6:return new Map();}}function s(t) {var r = this._promise = new e(n);t instanceof e && r._propagateFrom(t, 3), r._setOnCancel(this), this._values = t, this._length = 0, this._totalResolved = 0, this._init(void 0, -2);}var c = t("./util");c.isArray;return c.inherits(s, o), s.prototype.length = function () {return this._length;}, s.prototype.promise = function () {return this._promise;}, s.prototype._init = function l(t, n) {var o = r(this._values, this._promise);if (o instanceof e) {o = o._target();var s = o._bitField;if (this._values = o, 0 === (50397184 & s)) return this._promise._setAsyncGuaranteed(), o._then(l, this._reject, void 0, this, n);if (0 === (33554432 & s)) return 0 !== (16777216 & s) ? this._reject(o._reason()) : this._cancel();o = o._value();}if (o = c.asArray(o), null === o) {var u = i("expecting an array or an iterable object but got " + c.classString(o)).reason();return void this._promise._rejectCallback(u, !1);}return 0 === o.length ? void (-5 === n ? this._resolveEmptyArray() : this._resolve(a(n))) : void this._iterate(o);}, s.prototype._iterate = function (t) {var n = this.getActualLength(t.length);this._length = n, this._values = this.shouldCopyValues() ? new Array(n) : this._values;for (var i = this._promise, o = !1, a = null, s = 0; n > s; ++s) {var c = r(t[s], i);c instanceof e ? (c = c._target(), a = c._bitField) : a = null, o ? null !== a && c.suppressUnhandledRejections() : null !== a ? 0 === (50397184 & a) ? (c._proxy(this, s), this._values[s] = c) : o = 0 !== (33554432 & a) ? this._promiseFulfilled(c._value(), s) : 0 !== (16777216 & a) ? this._promiseRejected(c._reason(), s) : this._promiseCancelled(s) : o = this._promiseFulfilled(c, s);}o || i._setAsyncGuaranteed();}, s.prototype._isResolved = function () {return null === this._values;}, s.prototype._resolve = function (t) {this._values = null, this._promise._fulfill(t);}, s.prototype._cancel = function () {!this._isResolved() && this._promise._isCancellable() && (this._values = null, this._promise._cancel());}, s.prototype._reject = function (t) {this._values = null, this._promise._rejectCallback(t, !1);}, s.prototype._promiseFulfilled = function (t, e) {this._values[e] = t;var n = ++this._totalResolved;return n >= this._length ? (this._resolve(this._values), !0) : !1;}, s.prototype._promiseCancelled = function () {return this._cancel(), !0;}, s.prototype._promiseRejected = function (t) {return this._totalResolved++, this._reject(t), !0;}, s.prototype._resultCancelled = function () {if (!this._isResolved()) {var t = this._values;if (this._cancel(), t instanceof e) t.cancel();else for (var n = 0; n < t.length; ++n) {t[n] instanceof e && t[n].cancel();}}}, s.prototype.shouldCopyValues = function () {return !0;}, s.prototype.getActualLength = function (t) {return t;}, s;};}, { "./util": 21 }], 17: [function (t, e, n) {"use strict";function r(t, e, n, r, i) {for (var o = 0; i > o; ++o) {n[o + r] = t[o + e], t[o + e] = void 0;}}function i(t) {this._capacity = t, this._length = 0, this._front = 0;}i.prototype._willBeOverCapacity = function (t) {return this._capacity < t;}, i.prototype._pushOne = function (t) {var e = this.length();this._checkCapacity(e + 1);var n = this._front + e & this._capacity - 1;this[n] = t, this._length = e + 1;}, i.prototype.push = function (t, e, n) {var r = this.length() + 3;if (this._willBeOverCapacity(r)) return this._pushOne(t), this._pushOne(e), void this._pushOne(n);var i = this._front + r - 3;this._checkCapacity(r);var o = this._capacity - 1;this[i + 0 & o] = t, this[i + 1 & o] = e, this[i + 2 & o] = n, this._length = r;}, i.prototype.shift = function () {var t = this._front,e = this[t];return this[t] = void 0, this._front = t + 1 & this._capacity - 1, this._length--, e;}, i.prototype.length = function () {return this._length;}, i.prototype._checkCapacity = function (t) {this._capacity < t && this._resizeTo(this._capacity << 1);}, i.prototype._resizeTo = function (t) {var e = this._capacity;this._capacity = t;var n = this._front,i = this._length,o = n + i & e - 1;r(this, 0, this, e, o);}, e.exports = i;}, {}], 18: [function (t, e, n) {"use strict";var r,i = t("./util"),o = function o() {throw new Error("No async scheduler available\n\n    See http://goo.gl/MqrFmX\n");},a = i.getNativePromise();if (i.isNode && "undefined" == typeof MutationObserver) {var s = global.setImmediate,c = process.nextTick;r = i.isRecentNode ? function (t) {s.call(global, t);} : function (t) {c.call(process, t);};} else if ("function" == typeof a && "function" == typeof a.resolve) {var l = a.resolve();r = function r(t) {l.then(t);};} else r = "undefined" == typeof MutationObserver || "undefined" != typeof window && window.navigator && (window.navigator.standalone || window.cordova) ? "undefined" != typeof setImmediate ? function (t) {setImmediate(t);} : "undefined" != typeof setTimeout ? function (t) {setTimeout(t, 0);} : o : function () {var t = document.createElement("div"),e = { attributes: !0 },n = !1,r = document.createElement("div"),i = new MutationObserver(function () {t.classList.toggle("foo"), n = !1;});i.observe(r, e);var o = function o() {n || (n = !0, r.classList.toggle("foo"));};return function (n) {var r = new MutationObserver(function () {r.disconnect(), n();});r.observe(t, e), o();};}();e.exports = r;}, { "./util": 21 }], 19: [function (t, e, n) {"use strict";e.exports = function (t) {function e(t) {void 0 !== t ? (t = t._target(), this._bitField = t._bitField, this._settledValueField = t._isFateSealed() ? t._settledValue() : void 0) : (this._bitField = 0, this._settledValueField = void 0);}e.prototype._settledValue = function () {return this._settledValueField;};var n = e.prototype.value = function () {if (!this.isFulfilled()) throw new TypeError("cannot get fulfillment value of a non-fulfilled promise\n\n    See http://goo.gl/MqrFmX\n");return this._settledValue();},r = e.prototype.error = e.prototype.reason = function () {if (!this.isRejected()) throw new TypeError("cannot get rejection reason of a non-rejected promise\n\n    See http://goo.gl/MqrFmX\n");return this._settledValue();},i = e.prototype.isFulfilled = function () {return 0 !== (33554432 & this._bitField);},o = e.prototype.isRejected = function () {return 0 !== (16777216 & this._bitField);},a = e.prototype.isPending = function () {return 0 === (50397184 & this._bitField);},s = e.prototype.isResolved = function () {return 0 !== (50331648 & this._bitField);};e.prototype.isCancelled = function () {return 0 !== (8454144 & this._bitField);}, t.prototype.__isCancelled = function () {return 65536 === (65536 & this._bitField);}, t.prototype._isCancelled = function () {return this._target().__isCancelled();}, t.prototype.isCancelled = function () {return 0 !== (8454144 & this._target()._bitField);}, t.prototype.isPending = function () {return a.call(this._target());}, t.prototype.isRejected = function () {return o.call(this._target());}, t.prototype.isFulfilled = function () {return i.call(this._target());}, t.prototype.isResolved = function () {return s.call(this._target());}, t.prototype.value = function () {return n.call(this._target());}, t.prototype.reason = function () {var t = this._target();return t._unsetRejectionIsUnhandled(), r.call(t);}, t.prototype._value = function () {return this._settledValue();}, t.prototype._reason = function () {return this._unsetRejectionIsUnhandled(), this._settledValue();}, t.PromiseInspection = e;};}, {}], 20: [function (t, e, n) {"use strict";e.exports = function (e, n) {function r(t, r) {if (u(t)) {if (t instanceof e) return t;var i = o(t);if (i === l) {r && r._pushContext();var c = e.reject(i.e);return r && r._popContext(), c;}if ("function" == typeof i) {if (a(t)) {var c = new e(n);return t._then(c._fulfill, c._reject, void 0, c, null), c;}return s(t, i, r);}}return t;}function i(t) {return t.then;}function o(t) {try {return i(t);} catch (e) {return l.e = e, l;}}function a(t) {try {return p.call(t, "_promise0");} catch (e) {return !1;}}function s(t, r, i) {function o(t) {s && (s._resolveCallback(t), s = null);}function a(t) {s && (s._rejectCallback(t, p, !0), s = null);}var s = new e(n),u = s;i && i._pushContext(), s._captureStackTrace(), i && i._popContext();var p = !0,f = c.tryCatch(r).call(t, o, a);return p = !1, s && f === l && (s._rejectCallback(f.e, !0, !0), s = null), u;}var c = t("./util"),l = c.errorObj,u = c.isObject,p = {}.hasOwnProperty;return r;};}, { "./util": 21 }], 21: [function (t, e, n) {"use strict";function r() {try {var t = R;return R = null, t.apply(this, arguments);} catch (e) {return P.e = e, P;}}function i(t) {return R = t, r;}function o(t) {return null == t || t === !0 || t === !1 || "string" == typeof t || "number" == typeof t;}function a(t) {return "function" == typeof t || "object" == typeof t && null !== t;}function s(t) {return o(t) ? new Error(v(t)) : t;}function c(t, e) {var n,r = t.length,i = new Array(r + 1);for (n = 0; r > n; ++n) {i[n] = t[n];}return i[n] = e, i;}function l(t, e, n) {if (!F.isES5) return {}.hasOwnProperty.call(t, e) ? t[e] : void 0;var r = Object.getOwnPropertyDescriptor(t, e);return null != r ? null == r.get && null == r.set ? r.value : n : void 0;}function u(t, e, n) {if (o(t)) return t;var r = { value: n, configurable: !0, enumerable: !1, writable: !0 };return F.defineProperty(t, e, r), t;}function p(t) {throw t;}function f(t) {try {if ("function" == typeof t) {var e = F.names(t.prototype),n = F.isES5 && e.length > 1,r = e.length > 0 && !(1 === e.length && "constructor" === e[0]),i = A.test(t + "") && F.names(t).length > 0;if (n || r || i) return !0;}return !1;} catch (o) {return !1;}}function h(t) {function e() {}e.prototype = t;for (var n = 8; n--;) {new e();}return t;}function _(t) {return L.test(t);}function d(t, e, n) {for (var r = new Array(t), i = 0; t > i; ++i) {r[i] = e + i + n;}return r;}function v(t) {try {return t + "";} catch (e) {return "[no string representation]";}}function y(t) {return null !== t && "object" == typeof t && "string" == typeof t.message && "string" == typeof t.name;}function g(t) {try {u(t, "isOperational", !0);} catch (e) {}}function m(t) {return null == t ? !1 : t instanceof Error.__BluebirdErrorTypes__.OperationalError || t.isOperational === !0;}function b(t) {return y(t) && F.propertyIsWritable(t, "stack");}function C(t) {return {}.toString.call(t);}function w(t, e, n) {for (var r = F.names(t), i = 0; i < r.length; ++i) {var o = r[i];if (n(o)) try {F.defineProperty(e, o, F.getDescriptor(t, o));} catch (a) {}}}function E(t) {return H ? Object({"NODE_ENV":"development","VUE_APP_NAME":"分享保","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"})[t] : void 0;}function k() {if ("function" == typeof Promise) try {var t = new Promise(function () {});if ("[object Promise]" === {}.toString.call(t)) return Promise;} catch (e) {}}function j(t, e) {return t.bind(e);}var F = t("./es5"),T = "undefined" == typeof navigator,P = { e: {} },R,S = "undefined" != typeof self ? self : "undefined" != typeof window ? window : "undefined" != typeof global ? global : void 0 !== this ? this : null,x = function x(t, e) {function n() {this.constructor = t, this.constructor$ = e;for (var n in e.prototype) {r.call(e.prototype, n) && "$" !== n.charAt(n.length - 1) && (this[n + "$"] = e.prototype[n]);}}var r = {}.hasOwnProperty;return n.prototype = e.prototype, t.prototype = new n(), t.prototype;},O = function () {var t = [Array.prototype, Object.prototype, Function.prototype],e = function e(_e2) {for (var n = 0; n < t.length; ++n) {if (t[n] === _e2) return !0;}return !1;};if (F.isES5) {var n = Object.getOwnPropertyNames;return function (t) {for (var r = [], i = Object.create(null); null != t && !e(t);) {var o;try {o = n(t);} catch (a) {return r;}for (var s = 0; s < o.length; ++s) {var c = o[s];if (!i[c]) {i[c] = !0;var l = Object.getOwnPropertyDescriptor(t, c);null != l && null == l.get && null == l.set && r.push(c);}}t = F.getPrototypeOf(t);}return r;};}var r = {}.hasOwnProperty;return function (n) {if (e(n)) return [];var i = [];t: for (var o in n) {if (r.call(n, o)) i.push(o);else {for (var a = 0; a < t.length; ++a) {if (r.call(t[a], o)) continue t;}i.push(o);}}return i;};}(),A = /this\s*\.\s*\S+\s*=/,L = /^[a-z$_][a-z$_0-9]*$/i,N = function () {return "stack" in new Error() ? function (t) {return b(t) ? t : new Error(v(t));} : function (t) {if (b(t)) return t;try {throw new Error(v(t));} catch (e) {return e;}};}(),B = function B(t) {return F.isArray(t) ? t : null;};if ("undefined" != typeof Symbol && Symbol.iterator) {var U = "function" == typeof Array.from ? function (t) {return Array.from(t);} : function (t) {for (var e, n = [], r = t[Symbol.iterator](); !(e = r.next()).done;) {n.push(e.value);}return n;};B = function B(t) {return F.isArray(t) ? t : null != t && "function" == typeof t[Symbol.iterator] ? U(t) : null;};}var I = "undefined" != typeof process && "[object process]" === C(process).toLowerCase(),H = "undefined" != typeof process && "undefined" != "object",D = { isClass: f, isIdentifier: _, inheritedDataKeys: O, getDataPropertyOrDefault: l, thrower: p, isArray: F.isArray, asArray: B, notEnumerableProp: u, isPrimitive: o, isObject: a, isError: y, canEvaluate: T, errorObj: P, tryCatch: i, inherits: x, withAppended: c, maybeWrapAsError: s, toFastProperties: h, filledRange: d, toString: v, canAttachTrace: b, ensureErrorObject: N, originatesFromRejection: m, markAsOriginatingFromRejection: g, classString: C, copyDescriptors: w, hasDevTools: "undefined" != typeof chrome && chrome && "function" == typeof chrome.loadTimes, isNode: I, hasEnvVariables: H, env: E, global: S, getNativePromise: k, domainBind: j };D.isRecentNode = D.isNode && function () {var t = process.versions.node.split(".").map(Number);return 0 === t[0] && t[1] > 10 || t[0] > 0;}(), D.isNode && D.toFastProperties(process);try {throw new Error();} catch (V) {D.lastLineError = V;}e.exports = D;}, { "./es5": 10 }] }, {}, [3])(3);}), "undefined" != typeof window && null !== window ? window.P = window.Promise : "undefined" != typeof self && null !== self && (self.P = self.Promise);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/node-libs-browser/mock/process.js */ 12), __webpack_require__(/*! (webpack)/buildin/global.js */ 3)))

/***/ }),

/***/ 12:
/*!********************************************************!*\
  !*** ./node_modules/node-libs-browser/mock/process.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports.nextTick = function nextTick(fn) {
    var args = Array.prototype.slice.call(arguments);
    args.shift();
    setTimeout(function () {
        fn.apply(null, args);
    }, 0);
};

exports.platform = exports.arch = 
exports.execPath = exports.title = 'browser';
exports.pid = 1;
exports.browser = true;
exports.env = {};
exports.argv = [];

exports.binding = function (name) {
	throw new Error('No such module. (Possibly not yet loaded)')
};

(function () {
    var cwd = '/';
    var path;
    exports.cwd = function () { return cwd };
    exports.chdir = function (dir) {
        if (!path) path = __webpack_require__(/*! path */ 13);
        cwd = path.resolve(dir, cwd);
    };
})();

exports.exit = exports.kill = 
exports.umask = exports.dlopen = 
exports.uptime = exports.memoryUsage = 
exports.uvCounters = function() {};
exports.features = {};


/***/ }),

/***/ 13:
/*!***********************************************!*\
  !*** ./node_modules/path-browserify/index.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process) {// .dirname, .basename, and .extname methods are extracted from Node.js v8.11.1,
// backported and transplited with Babel, with backwards-compat fixes

// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

// resolves . and .. elements in a path array with directory names there
// must be no slashes, empty elements, or device names (c:\) in the array
// (so also no leading and trailing slashes - it does not distinguish
// relative and absolute paths)
function normalizeArray(parts, allowAboveRoot) {
  // if the path tries to go above the root, `up` ends up > 0
  var up = 0;
  for (var i = parts.length - 1; i >= 0; i--) {
    var last = parts[i];
    if (last === '.') {
      parts.splice(i, 1);
    } else if (last === '..') {
      parts.splice(i, 1);
      up++;
    } else if (up) {
      parts.splice(i, 1);
      up--;
    }
  }

  // if the path is allowed to go above the root, restore leading ..s
  if (allowAboveRoot) {
    for (; up--; up) {
      parts.unshift('..');
    }
  }

  return parts;
}

// path.resolve([from ...], to)
// posix version
exports.resolve = function() {
  var resolvedPath = '',
      resolvedAbsolute = false;

  for (var i = arguments.length - 1; i >= -1 && !resolvedAbsolute; i--) {
    var path = (i >= 0) ? arguments[i] : process.cwd();

    // Skip empty and invalid entries
    if (typeof path !== 'string') {
      throw new TypeError('Arguments to path.resolve must be strings');
    } else if (!path) {
      continue;
    }

    resolvedPath = path + '/' + resolvedPath;
    resolvedAbsolute = path.charAt(0) === '/';
  }

  // At this point the path should be resolved to a full absolute path, but
  // handle relative paths to be safe (might happen when process.cwd() fails)

  // Normalize the path
  resolvedPath = normalizeArray(filter(resolvedPath.split('/'), function(p) {
    return !!p;
  }), !resolvedAbsolute).join('/');

  return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
};

// path.normalize(path)
// posix version
exports.normalize = function(path) {
  var isAbsolute = exports.isAbsolute(path),
      trailingSlash = substr(path, -1) === '/';

  // Normalize the path
  path = normalizeArray(filter(path.split('/'), function(p) {
    return !!p;
  }), !isAbsolute).join('/');

  if (!path && !isAbsolute) {
    path = '.';
  }
  if (path && trailingSlash) {
    path += '/';
  }

  return (isAbsolute ? '/' : '') + path;
};

// posix version
exports.isAbsolute = function(path) {
  return path.charAt(0) === '/';
};

// posix version
exports.join = function() {
  var paths = Array.prototype.slice.call(arguments, 0);
  return exports.normalize(filter(paths, function(p, index) {
    if (typeof p !== 'string') {
      throw new TypeError('Arguments to path.join must be strings');
    }
    return p;
  }).join('/'));
};


// path.relative(from, to)
// posix version
exports.relative = function(from, to) {
  from = exports.resolve(from).substr(1);
  to = exports.resolve(to).substr(1);

  function trim(arr) {
    var start = 0;
    for (; start < arr.length; start++) {
      if (arr[start] !== '') break;
    }

    var end = arr.length - 1;
    for (; end >= 0; end--) {
      if (arr[end] !== '') break;
    }

    if (start > end) return [];
    return arr.slice(start, end - start + 1);
  }

  var fromParts = trim(from.split('/'));
  var toParts = trim(to.split('/'));

  var length = Math.min(fromParts.length, toParts.length);
  var samePartsLength = length;
  for (var i = 0; i < length; i++) {
    if (fromParts[i] !== toParts[i]) {
      samePartsLength = i;
      break;
    }
  }

  var outputParts = [];
  for (var i = samePartsLength; i < fromParts.length; i++) {
    outputParts.push('..');
  }

  outputParts = outputParts.concat(toParts.slice(samePartsLength));

  return outputParts.join('/');
};

exports.sep = '/';
exports.delimiter = ':';

exports.dirname = function (path) {
  if (typeof path !== 'string') path = path + '';
  if (path.length === 0) return '.';
  var code = path.charCodeAt(0);
  var hasRoot = code === 47 /*/*/;
  var end = -1;
  var matchedSlash = true;
  for (var i = path.length - 1; i >= 1; --i) {
    code = path.charCodeAt(i);
    if (code === 47 /*/*/) {
        if (!matchedSlash) {
          end = i;
          break;
        }
      } else {
      // We saw the first non-path separator
      matchedSlash = false;
    }
  }

  if (end === -1) return hasRoot ? '/' : '.';
  if (hasRoot && end === 1) {
    // return '//';
    // Backwards-compat fix:
    return '/';
  }
  return path.slice(0, end);
};

function basename(path) {
  if (typeof path !== 'string') path = path + '';

  var start = 0;
  var end = -1;
  var matchedSlash = true;
  var i;

  for (i = path.length - 1; i >= 0; --i) {
    if (path.charCodeAt(i) === 47 /*/*/) {
        // If we reached a path separator that was not part of a set of path
        // separators at the end of the string, stop now
        if (!matchedSlash) {
          start = i + 1;
          break;
        }
      } else if (end === -1) {
      // We saw the first non-path separator, mark this as the end of our
      // path component
      matchedSlash = false;
      end = i + 1;
    }
  }

  if (end === -1) return '';
  return path.slice(start, end);
}

// Uses a mixed approach for backwards-compatibility, as ext behavior changed
// in new Node.js versions, so only basename() above is backported here
exports.basename = function (path, ext) {
  var f = basename(path);
  if (ext && f.substr(-1 * ext.length) === ext) {
    f = f.substr(0, f.length - ext.length);
  }
  return f;
};

exports.extname = function (path) {
  if (typeof path !== 'string') path = path + '';
  var startDot = -1;
  var startPart = 0;
  var end = -1;
  var matchedSlash = true;
  // Track the state of characters (if any) we see before our first dot and
  // after any path separator we find
  var preDotState = 0;
  for (var i = path.length - 1; i >= 0; --i) {
    var code = path.charCodeAt(i);
    if (code === 47 /*/*/) {
        // If we reached a path separator that was not part of a set of path
        // separators at the end of the string, stop now
        if (!matchedSlash) {
          startPart = i + 1;
          break;
        }
        continue;
      }
    if (end === -1) {
      // We saw the first non-path separator, mark this as the end of our
      // extension
      matchedSlash = false;
      end = i + 1;
    }
    if (code === 46 /*.*/) {
        // If this is our first dot, mark it as the start of our extension
        if (startDot === -1)
          startDot = i;
        else if (preDotState !== 1)
          preDotState = 1;
    } else if (startDot !== -1) {
      // We saw a non-dot and non-path separator before our dot, so we should
      // have a good chance at having a non-empty extension
      preDotState = -1;
    }
  }

  if (startDot === -1 || end === -1 ||
      // We saw a non-dot character immediately before the dot
      preDotState === 0 ||
      // The (right-most) trimmed path component is exactly '..'
      preDotState === 1 && startDot === end - 1 && startDot === startPart + 1) {
    return '';
  }
  return path.slice(startDot, end);
};

function filter (xs, f) {
    if (xs.filter) return xs.filter(f);
    var res = [];
    for (var i = 0; i < xs.length; i++) {
        if (f(xs[i], i, xs)) res.push(xs[i]);
    }
    return res;
}

// String.prototype.substr - negative index don't work in IE8
var substr = 'ab'.substr(-1) === 'b'
    ? function (str, start, len) { return str.substr(start, len) }
    : function (str, start, len) {
        if (start < 0) start = str.length + start;
        return str.substr(start, len);
    }
;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node-libs-browser/mock/process.js */ 12)))

/***/ }),

/***/ 16:
/*!**********************************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/runtime/componentNormalizer.js ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode, /* vue-cli only */
  components, // fixed by xxxxxx auto components
  renderjs // fixed by xxxxxx renderjs
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // fixed by xxxxxx auto components
  if (components) {
    if (!options.components) {
      options.components = {}
    }
    var hasOwn = Object.prototype.hasOwnProperty
    for (var name in components) {
      if (hasOwn.call(components, name) && !hasOwn.call(options.components, name)) {
        options.components[name] = components[name]
      }
    }
  }
  // fixed by xxxxxx renderjs
  if (renderjs) {
    (renderjs.beforeCreate || (renderjs.beforeCreate = [])).unshift(function() {
      this[renderjs.__module] = this
    });
    (options.mixins || (options.mixins = [])).push(renderjs)
  }

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 199:
/*!*******************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/wxcomponents/public/publicFun.wxs ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var filters = {
  toFix: function (value){
    // console.log("===filters==", value)
    return value.toFixed(2) // 此处2为保留两位小数，保留几位小数，这里写几    
  },
  toAbs: function (value) {
    // console.log("===filters==", value)
    return Math.abs(value)
  },
  toCeil: function (value) {
    // console.log("===filters==", value)
    return Math.ceil(value)
  },
  typeOf: function (value) {
    // console.log("===filters==", typeof(value),value)
    return typeof(value)
  },
  toNum: function (value) {
    // console.log("===filters==",value)
    return Number(value)
  },
}
module.exports = { toFix: filters.toFix, toAbs: filters.toAbs, typeOf: filters.typeOf, toCeil: filters.toCeil, toNum: filters.toNum}
module.exports.msg = "some msg";

/***/ }),

/***/ 2:
/*!******************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/mp-vue/dist/mp.runtime.esm.js ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/*!
 * Vue.js v2.6.11
 * (c) 2014-2021 Evan You
 * Released under the MIT License.
 */
/*  */

var emptyObject = Object.freeze({});

// These helpers produce better VM code in JS engines due to their
// explicitness and function inlining.
function isUndef (v) {
  return v === undefined || v === null
}

function isDef (v) {
  return v !== undefined && v !== null
}

function isTrue (v) {
  return v === true
}

function isFalse (v) {
  return v === false
}

/**
 * Check if value is primitive.
 */
function isPrimitive (value) {
  return (
    typeof value === 'string' ||
    typeof value === 'number' ||
    // $flow-disable-line
    typeof value === 'symbol' ||
    typeof value === 'boolean'
  )
}

/**
 * Quick object check - this is primarily used to tell
 * Objects from primitive values when we know the value
 * is a JSON-compliant type.
 */
function isObject (obj) {
  return obj !== null && typeof obj === 'object'
}

/**
 * Get the raw type string of a value, e.g., [object Object].
 */
var _toString = Object.prototype.toString;

function toRawType (value) {
  return _toString.call(value).slice(8, -1)
}

/**
 * Strict object type check. Only returns true
 * for plain JavaScript objects.
 */
function isPlainObject (obj) {
  return _toString.call(obj) === '[object Object]'
}

function isRegExp (v) {
  return _toString.call(v) === '[object RegExp]'
}

/**
 * Check if val is a valid array index.
 */
function isValidArrayIndex (val) {
  var n = parseFloat(String(val));
  return n >= 0 && Math.floor(n) === n && isFinite(val)
}

function isPromise (val) {
  return (
    isDef(val) &&
    typeof val.then === 'function' &&
    typeof val.catch === 'function'
  )
}

/**
 * Convert a value to a string that is actually rendered.
 */
function toString (val) {
  return val == null
    ? ''
    : Array.isArray(val) || (isPlainObject(val) && val.toString === _toString)
      ? JSON.stringify(val, null, 2)
      : String(val)
}

/**
 * Convert an input value to a number for persistence.
 * If the conversion fails, return original string.
 */
function toNumber (val) {
  var n = parseFloat(val);
  return isNaN(n) ? val : n
}

/**
 * Make a map and return a function for checking if a key
 * is in that map.
 */
function makeMap (
  str,
  expectsLowerCase
) {
  var map = Object.create(null);
  var list = str.split(',');
  for (var i = 0; i < list.length; i++) {
    map[list[i]] = true;
  }
  return expectsLowerCase
    ? function (val) { return map[val.toLowerCase()]; }
    : function (val) { return map[val]; }
}

/**
 * Check if a tag is a built-in tag.
 */
var isBuiltInTag = makeMap('slot,component', true);

/**
 * Check if an attribute is a reserved attribute.
 */
var isReservedAttribute = makeMap('key,ref,slot,slot-scope,is');

/**
 * Remove an item from an array.
 */
function remove (arr, item) {
  if (arr.length) {
    var index = arr.indexOf(item);
    if (index > -1) {
      return arr.splice(index, 1)
    }
  }
}

/**
 * Check whether an object has the property.
 */
var hasOwnProperty = Object.prototype.hasOwnProperty;
function hasOwn (obj, key) {
  return hasOwnProperty.call(obj, key)
}

/**
 * Create a cached version of a pure function.
 */
function cached (fn) {
  var cache = Object.create(null);
  return (function cachedFn (str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str))
  })
}

/**
 * Camelize a hyphen-delimited string.
 */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) { return c ? c.toUpperCase() : ''; })
});

/**
 * Capitalize a string.
 */
var capitalize = cached(function (str) {
  return str.charAt(0).toUpperCase() + str.slice(1)
});

/**
 * Hyphenate a camelCase string.
 */
var hyphenateRE = /\B([A-Z])/g;
var hyphenate = cached(function (str) {
  return str.replace(hyphenateRE, '-$1').toLowerCase()
});

/**
 * Simple bind polyfill for environments that do not support it,
 * e.g., PhantomJS 1.x. Technically, we don't need this anymore
 * since native bind is now performant enough in most browsers.
 * But removing it would mean breaking code that was able to run in
 * PhantomJS 1.x, so this must be kept for backward compatibility.
 */

/* istanbul ignore next */
function polyfillBind (fn, ctx) {
  function boundFn (a) {
    var l = arguments.length;
    return l
      ? l > 1
        ? fn.apply(ctx, arguments)
        : fn.call(ctx, a)
      : fn.call(ctx)
  }

  boundFn._length = fn.length;
  return boundFn
}

function nativeBind (fn, ctx) {
  return fn.bind(ctx)
}

var bind = Function.prototype.bind
  ? nativeBind
  : polyfillBind;

/**
 * Convert an Array-like object to a real Array.
 */
function toArray (list, start) {
  start = start || 0;
  var i = list.length - start;
  var ret = new Array(i);
  while (i--) {
    ret[i] = list[i + start];
  }
  return ret
}

/**
 * Mix properties into target object.
 */
function extend (to, _from) {
  for (var key in _from) {
    to[key] = _from[key];
  }
  return to
}

/**
 * Merge an Array of Objects into a single Object.
 */
function toObject (arr) {
  var res = {};
  for (var i = 0; i < arr.length; i++) {
    if (arr[i]) {
      extend(res, arr[i]);
    }
  }
  return res
}

/* eslint-disable no-unused-vars */

/**
 * Perform no operation.
 * Stubbing args to make Flow happy without leaving useless transpiled code
 * with ...rest (https://flow.org/blog/2017/05/07/Strict-Function-Call-Arity/).
 */
function noop (a, b, c) {}

/**
 * Always return false.
 */
var no = function (a, b, c) { return false; };

/* eslint-enable no-unused-vars */

/**
 * Return the same value.
 */
var identity = function (_) { return _; };

/**
 * Check if two values are loosely equal - that is,
 * if they are plain objects, do they have the same shape?
 */
function looseEqual (a, b) {
  if (a === b) { return true }
  var isObjectA = isObject(a);
  var isObjectB = isObject(b);
  if (isObjectA && isObjectB) {
    try {
      var isArrayA = Array.isArray(a);
      var isArrayB = Array.isArray(b);
      if (isArrayA && isArrayB) {
        return a.length === b.length && a.every(function (e, i) {
          return looseEqual(e, b[i])
        })
      } else if (a instanceof Date && b instanceof Date) {
        return a.getTime() === b.getTime()
      } else if (!isArrayA && !isArrayB) {
        var keysA = Object.keys(a);
        var keysB = Object.keys(b);
        return keysA.length === keysB.length && keysA.every(function (key) {
          return looseEqual(a[key], b[key])
        })
      } else {
        /* istanbul ignore next */
        return false
      }
    } catch (e) {
      /* istanbul ignore next */
      return false
    }
  } else if (!isObjectA && !isObjectB) {
    return String(a) === String(b)
  } else {
    return false
  }
}

/**
 * Return the first index at which a loosely equal value can be
 * found in the array (if value is a plain object, the array must
 * contain an object of the same shape), or -1 if it is not present.
 */
function looseIndexOf (arr, val) {
  for (var i = 0; i < arr.length; i++) {
    if (looseEqual(arr[i], val)) { return i }
  }
  return -1
}

/**
 * Ensure a function is called only once.
 */
function once (fn) {
  var called = false;
  return function () {
    if (!called) {
      called = true;
      fn.apply(this, arguments);
    }
  }
}

var ASSET_TYPES = [
  'component',
  'directive',
  'filter'
];

var LIFECYCLE_HOOKS = [
  'beforeCreate',
  'created',
  'beforeMount',
  'mounted',
  'beforeUpdate',
  'updated',
  'beforeDestroy',
  'destroyed',
  'activated',
  'deactivated',
  'errorCaptured',
  'serverPrefetch'
];

/*  */



var config = ({
  /**
   * Option merge strategies (used in core/util/options)
   */
  // $flow-disable-line
  optionMergeStrategies: Object.create(null),

  /**
   * Whether to suppress warnings.
   */
  silent: false,

  /**
   * Show production mode tip message on boot?
   */
  productionTip: "development" !== 'production',

  /**
   * Whether to enable devtools
   */
  devtools: "development" !== 'production',

  /**
   * Whether to record perf
   */
  performance: false,

  /**
   * Error handler for watcher errors
   */
  errorHandler: null,

  /**
   * Warn handler for watcher warns
   */
  warnHandler: null,

  /**
   * Ignore certain custom elements
   */
  ignoredElements: [],

  /**
   * Custom user key aliases for v-on
   */
  // $flow-disable-line
  keyCodes: Object.create(null),

  /**
   * Check if a tag is reserved so that it cannot be registered as a
   * component. This is platform-dependent and may be overwritten.
   */
  isReservedTag: no,

  /**
   * Check if an attribute is reserved so that it cannot be used as a component
   * prop. This is platform-dependent and may be overwritten.
   */
  isReservedAttr: no,

  /**
   * Check if a tag is an unknown element.
   * Platform-dependent.
   */
  isUnknownElement: no,

  /**
   * Get the namespace of an element
   */
  getTagNamespace: noop,

  /**
   * Parse the real tag name for the specific platform.
   */
  parsePlatformTagName: identity,

  /**
   * Check if an attribute must be bound using property, e.g. value
   * Platform-dependent.
   */
  mustUseProp: no,

  /**
   * Perform updates asynchronously. Intended to be used by Vue Test Utils
   * This will significantly reduce performance if set to false.
   */
  async: true,

  /**
   * Exposed for legacy reasons
   */
  _lifecycleHooks: LIFECYCLE_HOOKS
});

/*  */

/**
 * unicode letters used for parsing html tags, component names and property paths.
 * using https://www.w3.org/TR/html53/semantics-scripting.html#potentialcustomelementname
 * skipping \u10000-\uEFFFF due to it freezing up PhantomJS
 */
var unicodeRegExp = /a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/;

/**
 * Check if a string starts with $ or _
 */
function isReserved (str) {
  var c = (str + '').charCodeAt(0);
  return c === 0x24 || c === 0x5F
}

/**
 * Define a property.
 */
function def (obj, key, val, enumerable) {
  Object.defineProperty(obj, key, {
    value: val,
    enumerable: !!enumerable,
    writable: true,
    configurable: true
  });
}

/**
 * Parse simple path.
 */
var bailRE = new RegExp(("[^" + (unicodeRegExp.source) + ".$_\\d]"));
function parsePath (path) {
  if (bailRE.test(path)) {
    return
  }
  var segments = path.split('.');
  return function (obj) {
    for (var i = 0; i < segments.length; i++) {
      if (!obj) { return }
      obj = obj[segments[i]];
    }
    return obj
  }
}

/*  */

// can we use __proto__?
var hasProto = '__proto__' in {};

// Browser environment sniffing
var inBrowser = typeof window !== 'undefined';
var inWeex = typeof WXEnvironment !== 'undefined' && !!WXEnvironment.platform;
var weexPlatform = inWeex && WXEnvironment.platform.toLowerCase();
var UA = inBrowser && window.navigator.userAgent.toLowerCase();
var isIE = UA && /msie|trident/.test(UA);
var isIE9 = UA && UA.indexOf('msie 9.0') > 0;
var isEdge = UA && UA.indexOf('edge/') > 0;
var isAndroid = (UA && UA.indexOf('android') > 0) || (weexPlatform === 'android');
var isIOS = (UA && /iphone|ipad|ipod|ios/.test(UA)) || (weexPlatform === 'ios');
var isChrome = UA && /chrome\/\d+/.test(UA) && !isEdge;
var isPhantomJS = UA && /phantomjs/.test(UA);
var isFF = UA && UA.match(/firefox\/(\d+)/);

// Firefox has a "watch" function on Object.prototype...
var nativeWatch = ({}).watch;
if (inBrowser) {
  try {
    var opts = {};
    Object.defineProperty(opts, 'passive', ({
      get: function get () {
      }
    })); // https://github.com/facebook/flow/issues/285
    window.addEventListener('test-passive', null, opts);
  } catch (e) {}
}

// this needs to be lazy-evaled because vue may be required before
// vue-server-renderer can set VUE_ENV
var _isServer;
var isServerRendering = function () {
  if (_isServer === undefined) {
    /* istanbul ignore if */
    if (!inBrowser && !inWeex && typeof global !== 'undefined') {
      // detect presence of vue-server-renderer and avoid
      // Webpack shimming the process
      _isServer = global['process'] && global['process'].env.VUE_ENV === 'server';
    } else {
      _isServer = false;
    }
  }
  return _isServer
};

// detect devtools
var devtools = inBrowser && window.__VUE_DEVTOOLS_GLOBAL_HOOK__;

/* istanbul ignore next */
function isNative (Ctor) {
  return typeof Ctor === 'function' && /native code/.test(Ctor.toString())
}

var hasSymbol =
  typeof Symbol !== 'undefined' && isNative(Symbol) &&
  typeof Reflect !== 'undefined' && isNative(Reflect.ownKeys);

var _Set;
/* istanbul ignore if */ // $flow-disable-line
if (typeof Set !== 'undefined' && isNative(Set)) {
  // use native Set when available.
  _Set = Set;
} else {
  // a non-standard Set polyfill that only works with primitive keys.
  _Set = /*@__PURE__*/(function () {
    function Set () {
      this.set = Object.create(null);
    }
    Set.prototype.has = function has (key) {
      return this.set[key] === true
    };
    Set.prototype.add = function add (key) {
      this.set[key] = true;
    };
    Set.prototype.clear = function clear () {
      this.set = Object.create(null);
    };

    return Set;
  }());
}

/*  */

var warn = noop;
var tip = noop;
var generateComponentTrace = (noop); // work around flow check
var formatComponentName = (noop);

if (true) {
  var hasConsole = typeof console !== 'undefined';
  var classifyRE = /(?:^|[-_])(\w)/g;
  var classify = function (str) { return str
    .replace(classifyRE, function (c) { return c.toUpperCase(); })
    .replace(/[-_]/g, ''); };

  warn = function (msg, vm) {
    var trace = vm ? generateComponentTrace(vm) : '';

    if (config.warnHandler) {
      config.warnHandler.call(null, msg, vm, trace);
    } else if (hasConsole && (!config.silent)) {
      console.error(("[Vue warn]: " + msg + trace));
    }
  };

  tip = function (msg, vm) {
    if (hasConsole && (!config.silent)) {
      console.warn("[Vue tip]: " + msg + (
        vm ? generateComponentTrace(vm) : ''
      ));
    }
  };

  formatComponentName = function (vm, includeFile) {
    if (vm.$root === vm) {
      if (vm.$options && vm.$options.__file) { // fixed by xxxxxx
        return ('') + vm.$options.__file
      }
      return '<Root>'
    }
    var options = typeof vm === 'function' && vm.cid != null
      ? vm.options
      : vm._isVue
        ? vm.$options || vm.constructor.options
        : vm;
    var name = options.name || options._componentTag;
    var file = options.__file;
    if (!name && file) {
      var match = file.match(/([^/\\]+)\.vue$/);
      name = match && match[1];
    }

    return (
      (name ? ("<" + (classify(name)) + ">") : "<Anonymous>") +
      (file && includeFile !== false ? (" at " + file) : '')
    )
  };

  var repeat = function (str, n) {
    var res = '';
    while (n) {
      if (n % 2 === 1) { res += str; }
      if (n > 1) { str += str; }
      n >>= 1;
    }
    return res
  };

  generateComponentTrace = function (vm) {
    if (vm._isVue && vm.$parent) {
      var tree = [];
      var currentRecursiveSequence = 0;
      while (vm && vm.$options.name !== 'PageBody') {
        if (tree.length > 0) {
          var last = tree[tree.length - 1];
          if (last.constructor === vm.constructor) {
            currentRecursiveSequence++;
            vm = vm.$parent;
            continue
          } else if (currentRecursiveSequence > 0) {
            tree[tree.length - 1] = [last, currentRecursiveSequence];
            currentRecursiveSequence = 0;
          }
        }
        !vm.$options.isReserved && tree.push(vm);
        vm = vm.$parent;
      }
      return '\n\nfound in\n\n' + tree
        .map(function (vm, i) { return ("" + (i === 0 ? '---> ' : repeat(' ', 5 + i * 2)) + (Array.isArray(vm)
            ? ((formatComponentName(vm[0])) + "... (" + (vm[1]) + " recursive calls)")
            : formatComponentName(vm))); })
        .join('\n')
    } else {
      return ("\n\n(found in " + (formatComponentName(vm)) + ")")
    }
  };
}

/*  */

var uid = 0;

/**
 * A dep is an observable that can have multiple
 * directives subscribing to it.
 */
var Dep = function Dep () {
  this.id = uid++;
  this.subs = [];
};

Dep.prototype.addSub = function addSub (sub) {
  this.subs.push(sub);
};

Dep.prototype.removeSub = function removeSub (sub) {
  remove(this.subs, sub);
};

Dep.prototype.depend = function depend () {
  if (Dep.SharedObject.target) {
    Dep.SharedObject.target.addDep(this);
  }
};

Dep.prototype.notify = function notify () {
  // stabilize the subscriber list first
  var subs = this.subs.slice();
  if ( true && !config.async) {
    // subs aren't sorted in scheduler if not running async
    // we need to sort them now to make sure they fire in correct
    // order
    subs.sort(function (a, b) { return a.id - b.id; });
  }
  for (var i = 0, l = subs.length; i < l; i++) {
    subs[i].update();
  }
};

// The current target watcher being evaluated.
// This is globally unique because only one watcher
// can be evaluated at a time.
// fixed by xxxxxx (nvue shared vuex)
/* eslint-disable no-undef */
Dep.SharedObject = {};
Dep.SharedObject.target = null;
Dep.SharedObject.targetStack = [];

function pushTarget (target) {
  Dep.SharedObject.targetStack.push(target);
  Dep.SharedObject.target = target;
  Dep.target = target;
}

function popTarget () {
  Dep.SharedObject.targetStack.pop();
  Dep.SharedObject.target = Dep.SharedObject.targetStack[Dep.SharedObject.targetStack.length - 1];
  Dep.target = Dep.SharedObject.target;
}

/*  */

var VNode = function VNode (
  tag,
  data,
  children,
  text,
  elm,
  context,
  componentOptions,
  asyncFactory
) {
  this.tag = tag;
  this.data = data;
  this.children = children;
  this.text = text;
  this.elm = elm;
  this.ns = undefined;
  this.context = context;
  this.fnContext = undefined;
  this.fnOptions = undefined;
  this.fnScopeId = undefined;
  this.key = data && data.key;
  this.componentOptions = componentOptions;
  this.componentInstance = undefined;
  this.parent = undefined;
  this.raw = false;
  this.isStatic = false;
  this.isRootInsert = true;
  this.isComment = false;
  this.isCloned = false;
  this.isOnce = false;
  this.asyncFactory = asyncFactory;
  this.asyncMeta = undefined;
  this.isAsyncPlaceholder = false;
};

var prototypeAccessors = { child: { configurable: true } };

// DEPRECATED: alias for componentInstance for backwards compat.
/* istanbul ignore next */
prototypeAccessors.child.get = function () {
  return this.componentInstance
};

Object.defineProperties( VNode.prototype, prototypeAccessors );

var createEmptyVNode = function (text) {
  if ( text === void 0 ) text = '';

  var node = new VNode();
  node.text = text;
  node.isComment = true;
  return node
};

function createTextVNode (val) {
  return new VNode(undefined, undefined, undefined, String(val))
}

// optimized shallow clone
// used for static nodes and slot nodes because they may be reused across
// multiple renders, cloning them avoids errors when DOM manipulations rely
// on their elm reference.
function cloneVNode (vnode) {
  var cloned = new VNode(
    vnode.tag,
    vnode.data,
    // #7975
    // clone children array to avoid mutating original in case of cloning
    // a child.
    vnode.children && vnode.children.slice(),
    vnode.text,
    vnode.elm,
    vnode.context,
    vnode.componentOptions,
    vnode.asyncFactory
  );
  cloned.ns = vnode.ns;
  cloned.isStatic = vnode.isStatic;
  cloned.key = vnode.key;
  cloned.isComment = vnode.isComment;
  cloned.fnContext = vnode.fnContext;
  cloned.fnOptions = vnode.fnOptions;
  cloned.fnScopeId = vnode.fnScopeId;
  cloned.asyncMeta = vnode.asyncMeta;
  cloned.isCloned = true;
  return cloned
}

/*
 * not type checking this file because flow doesn't play well with
 * dynamically accessing methods on Array prototype
 */

var arrayProto = Array.prototype;
var arrayMethods = Object.create(arrayProto);

var methodsToPatch = [
  'push',
  'pop',
  'shift',
  'unshift',
  'splice',
  'sort',
  'reverse'
];

/**
 * Intercept mutating methods and emit events
 */
methodsToPatch.forEach(function (method) {
  // cache original method
  var original = arrayProto[method];
  def(arrayMethods, method, function mutator () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    var result = original.apply(this, args);
    var ob = this.__ob__;
    var inserted;
    switch (method) {
      case 'push':
      case 'unshift':
        inserted = args;
        break
      case 'splice':
        inserted = args.slice(2);
        break
    }
    if (inserted) { ob.observeArray(inserted); }
    // notify change
    ob.dep.notify();
    return result
  });
});

/*  */

var arrayKeys = Object.getOwnPropertyNames(arrayMethods);

/**
 * In some cases we may want to disable observation inside a component's
 * update computation.
 */
var shouldObserve = true;

function toggleObserving (value) {
  shouldObserve = value;
}

/**
 * Observer class that is attached to each observed
 * object. Once attached, the observer converts the target
 * object's property keys into getter/setters that
 * collect dependencies and dispatch updates.
 */
var Observer = function Observer (value) {
  this.value = value;
  this.dep = new Dep();
  this.vmCount = 0;
  def(value, '__ob__', this);
  if (Array.isArray(value)) {
    if (hasProto) {
      {// fixed by xxxxxx 微信小程序使用 plugins 之后，数组方法被直接挂载到了数组对象上，需要执行 copyAugment 逻辑
        if(value.push !== value.__proto__.push){
          copyAugment(value, arrayMethods, arrayKeys);
        } else {
          protoAugment(value, arrayMethods);
        }
      }
    } else {
      copyAugment(value, arrayMethods, arrayKeys);
    }
    this.observeArray(value);
  } else {
    this.walk(value);
  }
};

/**
 * Walk through all properties and convert them into
 * getter/setters. This method should only be called when
 * value type is Object.
 */
Observer.prototype.walk = function walk (obj) {
  var keys = Object.keys(obj);
  for (var i = 0; i < keys.length; i++) {
    defineReactive$$1(obj, keys[i]);
  }
};

/**
 * Observe a list of Array items.
 */
Observer.prototype.observeArray = function observeArray (items) {
  for (var i = 0, l = items.length; i < l; i++) {
    observe(items[i]);
  }
};

// helpers

/**
 * Augment a target Object or Array by intercepting
 * the prototype chain using __proto__
 */
function protoAugment (target, src) {
  /* eslint-disable no-proto */
  target.__proto__ = src;
  /* eslint-enable no-proto */
}

/**
 * Augment a target Object or Array by defining
 * hidden properties.
 */
/* istanbul ignore next */
function copyAugment (target, src, keys) {
  for (var i = 0, l = keys.length; i < l; i++) {
    var key = keys[i];
    def(target, key, src[key]);
  }
}

/**
 * Attempt to create an observer instance for a value,
 * returns the new observer if successfully observed,
 * or the existing observer if the value already has one.
 */
function observe (value, asRootData) {
  if (!isObject(value) || value instanceof VNode) {
    return
  }
  var ob;
  if (hasOwn(value, '__ob__') && value.__ob__ instanceof Observer) {
    ob = value.__ob__;
  } else if (
    shouldObserve &&
    !isServerRendering() &&
    (Array.isArray(value) || isPlainObject(value)) &&
    Object.isExtensible(value) &&
    !value._isVue
  ) {
    ob = new Observer(value);
  }
  if (asRootData && ob) {
    ob.vmCount++;
  }
  return ob
}

/**
 * Define a reactive property on an Object.
 */
function defineReactive$$1 (
  obj,
  key,
  val,
  customSetter,
  shallow
) {
  var dep = new Dep();

  var property = Object.getOwnPropertyDescriptor(obj, key);
  if (property && property.configurable === false) {
    return
  }

  // cater for pre-defined getter/setters
  var getter = property && property.get;
  var setter = property && property.set;
  if ((!getter || setter) && arguments.length === 2) {
    val = obj[key];
  }

  var childOb = !shallow && observe(val);
  Object.defineProperty(obj, key, {
    enumerable: true,
    configurable: true,
    get: function reactiveGetter () {
      var value = getter ? getter.call(obj) : val;
      if (Dep.SharedObject.target) { // fixed by xxxxxx
        dep.depend();
        if (childOb) {
          childOb.dep.depend();
          if (Array.isArray(value)) {
            dependArray(value);
          }
        }
      }
      return value
    },
    set: function reactiveSetter (newVal) {
      var value = getter ? getter.call(obj) : val;
      /* eslint-disable no-self-compare */
      if (newVal === value || (newVal !== newVal && value !== value)) {
        return
      }
      /* eslint-enable no-self-compare */
      if ( true && customSetter) {
        customSetter();
      }
      // #7981: for accessor properties without setter
      if (getter && !setter) { return }
      if (setter) {
        setter.call(obj, newVal);
      } else {
        val = newVal;
      }
      childOb = !shallow && observe(newVal);
      dep.notify();
    }
  });
}

/**
 * Set a property on an object. Adds the new property and
 * triggers change notification if the property doesn't
 * already exist.
 */
function set (target, key, val) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot set reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.length = Math.max(target.length, key);
    target.splice(key, 1, val);
    return val
  }
  if (key in target && !(key in Object.prototype)) {
    target[key] = val;
    return val
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid adding reactive properties to a Vue instance or its root $data ' +
      'at runtime - declare it upfront in the data option.'
    );
    return val
  }
  if (!ob) {
    target[key] = val;
    return val
  }
  defineReactive$$1(ob.value, key, val);
  ob.dep.notify();
  return val
}

/**
 * Delete a property and trigger change if necessary.
 */
function del (target, key) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot delete reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.splice(key, 1);
    return
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid deleting properties on a Vue instance or its root $data ' +
      '- just set it to null.'
    );
    return
  }
  if (!hasOwn(target, key)) {
    return
  }
  delete target[key];
  if (!ob) {
    return
  }
  ob.dep.notify();
}

/**
 * Collect dependencies on array elements when the array is touched, since
 * we cannot intercept array element access like property getters.
 */
function dependArray (value) {
  for (var e = (void 0), i = 0, l = value.length; i < l; i++) {
    e = value[i];
    e && e.__ob__ && e.__ob__.dep.depend();
    if (Array.isArray(e)) {
      dependArray(e);
    }
  }
}

/*  */

/**
 * Option overwriting strategies are functions that handle
 * how to merge a parent option value and a child option
 * value into the final value.
 */
var strats = config.optionMergeStrategies;

/**
 * Options with restrictions
 */
if (true) {
  strats.el = strats.propsData = function (parent, child, vm, key) {
    if (!vm) {
      warn(
        "option \"" + key + "\" can only be used during instance " +
        'creation with the `new` keyword.'
      );
    }
    return defaultStrat(parent, child)
  };
}

/**
 * Helper that recursively merges two data objects together.
 */
function mergeData (to, from) {
  if (!from) { return to }
  var key, toVal, fromVal;

  var keys = hasSymbol
    ? Reflect.ownKeys(from)
    : Object.keys(from);

  for (var i = 0; i < keys.length; i++) {
    key = keys[i];
    // in case the object is already observed...
    if (key === '__ob__') { continue }
    toVal = to[key];
    fromVal = from[key];
    if (!hasOwn(to, key)) {
      set(to, key, fromVal);
    } else if (
      toVal !== fromVal &&
      isPlainObject(toVal) &&
      isPlainObject(fromVal)
    ) {
      mergeData(toVal, fromVal);
    }
  }
  return to
}

/**
 * Data
 */
function mergeDataOrFn (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    // in a Vue.extend merge, both should be functions
    if (!childVal) {
      return parentVal
    }
    if (!parentVal) {
      return childVal
    }
    // when parentVal & childVal are both present,
    // we need to return a function that returns the
    // merged result of both functions... no need to
    // check if parentVal is a function here because
    // it has to be a function to pass previous merges.
    return function mergedDataFn () {
      return mergeData(
        typeof childVal === 'function' ? childVal.call(this, this) : childVal,
        typeof parentVal === 'function' ? parentVal.call(this, this) : parentVal
      )
    }
  } else {
    return function mergedInstanceDataFn () {
      // instance merge
      var instanceData = typeof childVal === 'function'
        ? childVal.call(vm, vm)
        : childVal;
      var defaultData = typeof parentVal === 'function'
        ? parentVal.call(vm, vm)
        : parentVal;
      if (instanceData) {
        return mergeData(instanceData, defaultData)
      } else {
        return defaultData
      }
    }
  }
}

strats.data = function (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    if (childVal && typeof childVal !== 'function') {
       true && warn(
        'The "data" option should be a function ' +
        'that returns a per-instance value in component ' +
        'definitions.',
        vm
      );

      return parentVal
    }
    return mergeDataOrFn(parentVal, childVal)
  }

  return mergeDataOrFn(parentVal, childVal, vm)
};

/**
 * Hooks and props are merged as arrays.
 */
function mergeHook (
  parentVal,
  childVal
) {
  var res = childVal
    ? parentVal
      ? parentVal.concat(childVal)
      : Array.isArray(childVal)
        ? childVal
        : [childVal]
    : parentVal;
  return res
    ? dedupeHooks(res)
    : res
}

function dedupeHooks (hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res
}

LIFECYCLE_HOOKS.forEach(function (hook) {
  strats[hook] = mergeHook;
});

/**
 * Assets
 *
 * When a vm is present (instance creation), we need to do
 * a three-way merge between constructor options, instance
 * options and parent options.
 */
function mergeAssets (
  parentVal,
  childVal,
  vm,
  key
) {
  var res = Object.create(parentVal || null);
  if (childVal) {
     true && assertObjectType(key, childVal, vm);
    return extend(res, childVal)
  } else {
    return res
  }
}

ASSET_TYPES.forEach(function (type) {
  strats[type + 's'] = mergeAssets;
});

/**
 * Watchers.
 *
 * Watchers hashes should not overwrite one
 * another, so we merge them as arrays.
 */
strats.watch = function (
  parentVal,
  childVal,
  vm,
  key
) {
  // work around Firefox's Object.prototype.watch...
  if (parentVal === nativeWatch) { parentVal = undefined; }
  if (childVal === nativeWatch) { childVal = undefined; }
  /* istanbul ignore if */
  if (!childVal) { return Object.create(parentVal || null) }
  if (true) {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = {};
  extend(ret, parentVal);
  for (var key$1 in childVal) {
    var parent = ret[key$1];
    var child = childVal[key$1];
    if (parent && !Array.isArray(parent)) {
      parent = [parent];
    }
    ret[key$1] = parent
      ? parent.concat(child)
      : Array.isArray(child) ? child : [child];
  }
  return ret
};

/**
 * Other object hashes.
 */
strats.props =
strats.methods =
strats.inject =
strats.computed = function (
  parentVal,
  childVal,
  vm,
  key
) {
  if (childVal && "development" !== 'production') {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = Object.create(null);
  extend(ret, parentVal);
  if (childVal) { extend(ret, childVal); }
  return ret
};
strats.provide = mergeDataOrFn;

/**
 * Default strategy.
 */
var defaultStrat = function (parentVal, childVal) {
  return childVal === undefined
    ? parentVal
    : childVal
};

/**
 * Validate component names
 */
function checkComponents (options) {
  for (var key in options.components) {
    validateComponentName(key);
  }
}

function validateComponentName (name) {
  if (!new RegExp(("^[a-zA-Z][\\-\\.0-9_" + (unicodeRegExp.source) + "]*$")).test(name)) {
    warn(
      'Invalid component name: "' + name + '". Component names ' +
      'should conform to valid custom element name in html5 specification.'
    );
  }
  if (isBuiltInTag(name) || config.isReservedTag(name)) {
    warn(
      'Do not use built-in or reserved HTML elements as component ' +
      'id: ' + name
    );
  }
}

/**
 * Ensure all props option syntax are normalized into the
 * Object-based format.
 */
function normalizeProps (options, vm) {
  var props = options.props;
  if (!props) { return }
  var res = {};
  var i, val, name;
  if (Array.isArray(props)) {
    i = props.length;
    while (i--) {
      val = props[i];
      if (typeof val === 'string') {
        name = camelize(val);
        res[name] = { type: null };
      } else if (true) {
        warn('props must be strings when using array syntax.');
      }
    }
  } else if (isPlainObject(props)) {
    for (var key in props) {
      val = props[key];
      name = camelize(key);
      res[name] = isPlainObject(val)
        ? val
        : { type: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"props\": expected an Array or an Object, " +
      "but got " + (toRawType(props)) + ".",
      vm
    );
  }
  options.props = res;
}

/**
 * Normalize all injections into Object-based format
 */
function normalizeInject (options, vm) {
  var inject = options.inject;
  if (!inject) { return }
  var normalized = options.inject = {};
  if (Array.isArray(inject)) {
    for (var i = 0; i < inject.length; i++) {
      normalized[inject[i]] = { from: inject[i] };
    }
  } else if (isPlainObject(inject)) {
    for (var key in inject) {
      var val = inject[key];
      normalized[key] = isPlainObject(val)
        ? extend({ from: key }, val)
        : { from: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"inject\": expected an Array or an Object, " +
      "but got " + (toRawType(inject)) + ".",
      vm
    );
  }
}

/**
 * Normalize raw function directives into object format.
 */
function normalizeDirectives (options) {
  var dirs = options.directives;
  if (dirs) {
    for (var key in dirs) {
      var def$$1 = dirs[key];
      if (typeof def$$1 === 'function') {
        dirs[key] = { bind: def$$1, update: def$$1 };
      }
    }
  }
}

function assertObjectType (name, value, vm) {
  if (!isPlainObject(value)) {
    warn(
      "Invalid value for option \"" + name + "\": expected an Object, " +
      "but got " + (toRawType(value)) + ".",
      vm
    );
  }
}

/**
 * Merge two option objects into a new one.
 * Core utility used in both instantiation and inheritance.
 */
function mergeOptions (
  parent,
  child,
  vm
) {
  if (true) {
    checkComponents(child);
  }

  if (typeof child === 'function') {
    child = child.options;
  }

  normalizeProps(child, vm);
  normalizeInject(child, vm);
  normalizeDirectives(child);

  // Apply extends and mixins on the child options,
  // but only if it is a raw options object that isn't
  // the result of another mergeOptions call.
  // Only merged options has the _base property.
  if (!child._base) {
    if (child.extends) {
      parent = mergeOptions(parent, child.extends, vm);
    }
    if (child.mixins) {
      for (var i = 0, l = child.mixins.length; i < l; i++) {
        parent = mergeOptions(parent, child.mixins[i], vm);
      }
    }
  }

  var options = {};
  var key;
  for (key in parent) {
    mergeField(key);
  }
  for (key in child) {
    if (!hasOwn(parent, key)) {
      mergeField(key);
    }
  }
  function mergeField (key) {
    var strat = strats[key] || defaultStrat;
    options[key] = strat(parent[key], child[key], vm, key);
  }
  return options
}

/**
 * Resolve an asset.
 * This function is used because child instances need access
 * to assets defined in its ancestor chain.
 */
function resolveAsset (
  options,
  type,
  id,
  warnMissing
) {
  /* istanbul ignore if */
  if (typeof id !== 'string') {
    return
  }
  var assets = options[type];
  // check local registration variations first
  if (hasOwn(assets, id)) { return assets[id] }
  var camelizedId = camelize(id);
  if (hasOwn(assets, camelizedId)) { return assets[camelizedId] }
  var PascalCaseId = capitalize(camelizedId);
  if (hasOwn(assets, PascalCaseId)) { return assets[PascalCaseId] }
  // fallback to prototype chain
  var res = assets[id] || assets[camelizedId] || assets[PascalCaseId];
  if ( true && warnMissing && !res) {
    warn(
      'Failed to resolve ' + type.slice(0, -1) + ': ' + id,
      options
    );
  }
  return res
}

/*  */



function validateProp (
  key,
  propOptions,
  propsData,
  vm
) {
  var prop = propOptions[key];
  var absent = !hasOwn(propsData, key);
  var value = propsData[key];
  // boolean casting
  var booleanIndex = getTypeIndex(Boolean, prop.type);
  if (booleanIndex > -1) {
    if (absent && !hasOwn(prop, 'default')) {
      value = false;
    } else if (value === '' || value === hyphenate(key)) {
      // only cast empty string / same name to boolean if
      // boolean has higher priority
      var stringIndex = getTypeIndex(String, prop.type);
      if (stringIndex < 0 || booleanIndex < stringIndex) {
        value = true;
      }
    }
  }
  // check default value
  if (value === undefined) {
    value = getPropDefaultValue(vm, prop, key);
    // since the default value is a fresh copy,
    // make sure to observe it.
    var prevShouldObserve = shouldObserve;
    toggleObserving(true);
    observe(value);
    toggleObserving(prevShouldObserve);
  }
  if (
    true
  ) {
    assertProp(prop, key, value, vm, absent);
  }
  return value
}

/**
 * Get the default value of a prop.
 */
function getPropDefaultValue (vm, prop, key) {
  // no default, return undefined
  if (!hasOwn(prop, 'default')) {
    return undefined
  }
  var def = prop.default;
  // warn against non-factory defaults for Object & Array
  if ( true && isObject(def)) {
    warn(
      'Invalid default value for prop "' + key + '": ' +
      'Props with type Object/Array must use a factory function ' +
      'to return the default value.',
      vm
    );
  }
  // the raw prop value was also undefined from previous render,
  // return previous default value to avoid unnecessary watcher trigger
  if (vm && vm.$options.propsData &&
    vm.$options.propsData[key] === undefined &&
    vm._props[key] !== undefined
  ) {
    return vm._props[key]
  }
  // call factory function for non-Function types
  // a value is Function if its prototype is function even across different execution context
  return typeof def === 'function' && getType(prop.type) !== 'Function'
    ? def.call(vm)
    : def
}

/**
 * Assert whether a prop is valid.
 */
function assertProp (
  prop,
  name,
  value,
  vm,
  absent
) {
  if (prop.required && absent) {
    warn(
      'Missing required prop: "' + name + '"',
      vm
    );
    return
  }
  if (value == null && !prop.required) {
    return
  }
  var type = prop.type;
  var valid = !type || type === true;
  var expectedTypes = [];
  if (type) {
    if (!Array.isArray(type)) {
      type = [type];
    }
    for (var i = 0; i < type.length && !valid; i++) {
      var assertedType = assertType(value, type[i]);
      expectedTypes.push(assertedType.expectedType || '');
      valid = assertedType.valid;
    }
  }

  if (!valid) {
    warn(
      getInvalidTypeMessage(name, value, expectedTypes),
      vm
    );
    return
  }
  var validator = prop.validator;
  if (validator) {
    if (!validator(value)) {
      warn(
        'Invalid prop: custom validator check failed for prop "' + name + '".',
        vm
      );
    }
  }
}

var simpleCheckRE = /^(String|Number|Boolean|Function|Symbol)$/;

function assertType (value, type) {
  var valid;
  var expectedType = getType(type);
  if (simpleCheckRE.test(expectedType)) {
    var t = typeof value;
    valid = t === expectedType.toLowerCase();
    // for primitive wrapper objects
    if (!valid && t === 'object') {
      valid = value instanceof type;
    }
  } else if (expectedType === 'Object') {
    valid = isPlainObject(value);
  } else if (expectedType === 'Array') {
    valid = Array.isArray(value);
  } else {
    valid = value instanceof type;
  }
  return {
    valid: valid,
    expectedType: expectedType
  }
}

/**
 * Use function string name to check built-in types,
 * because a simple equality check will fail when running
 * across different vms / iframes.
 */
function getType (fn) {
  var match = fn && fn.toString().match(/^\s*function (\w+)/);
  return match ? match[1] : ''
}

function isSameType (a, b) {
  return getType(a) === getType(b)
}

function getTypeIndex (type, expectedTypes) {
  if (!Array.isArray(expectedTypes)) {
    return isSameType(expectedTypes, type) ? 0 : -1
  }
  for (var i = 0, len = expectedTypes.length; i < len; i++) {
    if (isSameType(expectedTypes[i], type)) {
      return i
    }
  }
  return -1
}

function getInvalidTypeMessage (name, value, expectedTypes) {
  var message = "Invalid prop: type check failed for prop \"" + name + "\"." +
    " Expected " + (expectedTypes.map(capitalize).join(', '));
  var expectedType = expectedTypes[0];
  var receivedType = toRawType(value);
  var expectedValue = styleValue(value, expectedType);
  var receivedValue = styleValue(value, receivedType);
  // check if we need to specify expected value
  if (expectedTypes.length === 1 &&
      isExplicable(expectedType) &&
      !isBoolean(expectedType, receivedType)) {
    message += " with value " + expectedValue;
  }
  message += ", got " + receivedType + " ";
  // check if we need to specify received value
  if (isExplicable(receivedType)) {
    message += "with value " + receivedValue + ".";
  }
  return message
}

function styleValue (value, type) {
  if (type === 'String') {
    return ("\"" + value + "\"")
  } else if (type === 'Number') {
    return ("" + (Number(value)))
  } else {
    return ("" + value)
  }
}

function isExplicable (value) {
  var explicitTypes = ['string', 'number', 'boolean'];
  return explicitTypes.some(function (elem) { return value.toLowerCase() === elem; })
}

function isBoolean () {
  var args = [], len = arguments.length;
  while ( len-- ) args[ len ] = arguments[ len ];

  return args.some(function (elem) { return elem.toLowerCase() === 'boolean'; })
}

/*  */

function handleError (err, vm, info) {
  // Deactivate deps tracking while processing error handler to avoid possible infinite rendering.
  // See: https://github.com/vuejs/vuex/issues/1505
  pushTarget();
  try {
    if (vm) {
      var cur = vm;
      while ((cur = cur.$parent)) {
        var hooks = cur.$options.errorCaptured;
        if (hooks) {
          for (var i = 0; i < hooks.length; i++) {
            try {
              var capture = hooks[i].call(cur, err, vm, info) === false;
              if (capture) { return }
            } catch (e) {
              globalHandleError(e, cur, 'errorCaptured hook');
            }
          }
        }
      }
    }
    globalHandleError(err, vm, info);
  } finally {
    popTarget();
  }
}

function invokeWithErrorHandling (
  handler,
  context,
  args,
  vm,
  info
) {
  var res;
  try {
    res = args ? handler.apply(context, args) : handler.call(context);
    if (res && !res._isVue && isPromise(res) && !res._handled) {
      res.catch(function (e) { return handleError(e, vm, info + " (Promise/async)"); });
      // issue #9511
      // avoid catch triggering multiple times when nested calls
      res._handled = true;
    }
  } catch (e) {
    handleError(e, vm, info);
  }
  return res
}

function globalHandleError (err, vm, info) {
  if (config.errorHandler) {
    try {
      return config.errorHandler.call(null, err, vm, info)
    } catch (e) {
      // if the user intentionally throws the original error in the handler,
      // do not log it twice
      if (e !== err) {
        logError(e, null, 'config.errorHandler');
      }
    }
  }
  logError(err, vm, info);
}

function logError (err, vm, info) {
  if (true) {
    warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
  }
  /* istanbul ignore else */
  if ((inBrowser || inWeex) && typeof console !== 'undefined') {
    console.error(err);
  } else {
    throw err
  }
}

/*  */

var callbacks = [];
var pending = false;

function flushCallbacks () {
  pending = false;
  var copies = callbacks.slice(0);
  callbacks.length = 0;
  for (var i = 0; i < copies.length; i++) {
    copies[i]();
  }
}

// Here we have async deferring wrappers using microtasks.
// In 2.5 we used (macro) tasks (in combination with microtasks).
// However, it has subtle problems when state is changed right before repaint
// (e.g. #6813, out-in transitions).
// Also, using (macro) tasks in event handler would cause some weird behaviors
// that cannot be circumvented (e.g. #7109, #7153, #7546, #7834, #8109).
// So we now use microtasks everywhere, again.
// A major drawback of this tradeoff is that there are some scenarios
// where microtasks have too high a priority and fire in between supposedly
// sequential events (e.g. #4521, #6690, which have workarounds)
// or even between bubbling of the same event (#6566).
var timerFunc;

// The nextTick behavior leverages the microtask queue, which can be accessed
// via either native Promise.then or MutationObserver.
// MutationObserver has wider support, however it is seriously bugged in
// UIWebView in iOS >= 9.3.3 when triggered in touch event handlers. It
// completely stops working after triggering a few times... so, if native
// Promise is available, we will use it:
/* istanbul ignore next, $flow-disable-line */
if (typeof Promise !== 'undefined' && isNative(Promise)) {
  var p = Promise.resolve();
  timerFunc = function () {
    p.then(flushCallbacks);
    // In problematic UIWebViews, Promise.then doesn't completely break, but
    // it can get stuck in a weird state where callbacks are pushed into the
    // microtask queue but the queue isn't being flushed, until the browser
    // needs to do some other work, e.g. handle a timer. Therefore we can
    // "force" the microtask queue to be flushed by adding an empty timer.
    if (isIOS) { setTimeout(noop); }
  };
} else if (!isIE && typeof MutationObserver !== 'undefined' && (
  isNative(MutationObserver) ||
  // PhantomJS and iOS 7.x
  MutationObserver.toString() === '[object MutationObserverConstructor]'
)) {
  // Use MutationObserver where native Promise is not available,
  // e.g. PhantomJS, iOS7, Android 4.4
  // (#6466 MutationObserver is unreliable in IE11)
  var counter = 1;
  var observer = new MutationObserver(flushCallbacks);
  var textNode = document.createTextNode(String(counter));
  observer.observe(textNode, {
    characterData: true
  });
  timerFunc = function () {
    counter = (counter + 1) % 2;
    textNode.data = String(counter);
  };
} else if (typeof setImmediate !== 'undefined' && isNative(setImmediate)) {
  // Fallback to setImmediate.
  // Technically it leverages the (macro) task queue,
  // but it is still a better choice than setTimeout.
  timerFunc = function () {
    setImmediate(flushCallbacks);
  };
} else {
  // Fallback to setTimeout.
  timerFunc = function () {
    setTimeout(flushCallbacks, 0);
  };
}

function nextTick (cb, ctx) {
  var _resolve;
  callbacks.push(function () {
    if (cb) {
      try {
        cb.call(ctx);
      } catch (e) {
        handleError(e, ctx, 'nextTick');
      }
    } else if (_resolve) {
      _resolve(ctx);
    }
  });
  if (!pending) {
    pending = true;
    timerFunc();
  }
  // $flow-disable-line
  if (!cb && typeof Promise !== 'undefined') {
    return new Promise(function (resolve) {
      _resolve = resolve;
    })
  }
}

/*  */

/* not type checking this file because flow doesn't play well with Proxy */

var initProxy;

if (true) {
  var allowedGlobals = makeMap(
    'Infinity,undefined,NaN,isFinite,isNaN,' +
    'parseFloat,parseInt,decodeURI,decodeURIComponent,encodeURI,encodeURIComponent,' +
    'Math,Number,Date,Array,Object,Boolean,String,RegExp,Map,Set,JSON,Intl,' +
    'require' // for Webpack/Browserify
  );

  var warnNonPresent = function (target, key) {
    warn(
      "Property or method \"" + key + "\" is not defined on the instance but " +
      'referenced during render. Make sure that this property is reactive, ' +
      'either in the data option, or for class-based components, by ' +
      'initializing the property. ' +
      'See: https://vuejs.org/v2/guide/reactivity.html#Declaring-Reactive-Properties.',
      target
    );
  };

  var warnReservedPrefix = function (target, key) {
    warn(
      "Property \"" + key + "\" must be accessed with \"$data." + key + "\" because " +
      'properties starting with "$" or "_" are not proxied in the Vue instance to ' +
      'prevent conflicts with Vue internals. ' +
      'See: https://vuejs.org/v2/api/#data',
      target
    );
  };

  var hasProxy =
    typeof Proxy !== 'undefined' && isNative(Proxy);

  if (hasProxy) {
    var isBuiltInModifier = makeMap('stop,prevent,self,ctrl,shift,alt,meta,exact');
    config.keyCodes = new Proxy(config.keyCodes, {
      set: function set (target, key, value) {
        if (isBuiltInModifier(key)) {
          warn(("Avoid overwriting built-in modifier in config.keyCodes: ." + key));
          return false
        } else {
          target[key] = value;
          return true
        }
      }
    });
  }

  var hasHandler = {
    has: function has (target, key) {
      var has = key in target;
      var isAllowed = allowedGlobals(key) ||
        (typeof key === 'string' && key.charAt(0) === '_' && !(key in target.$data));
      if (!has && !isAllowed) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return has || !isAllowed
    }
  };

  var getHandler = {
    get: function get (target, key) {
      if (typeof key === 'string' && !(key in target)) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return target[key]
    }
  };

  initProxy = function initProxy (vm) {
    if (hasProxy) {
      // determine which proxy handler to use
      var options = vm.$options;
      var handlers = options.render && options.render._withStripped
        ? getHandler
        : hasHandler;
      vm._renderProxy = new Proxy(vm, handlers);
    } else {
      vm._renderProxy = vm;
    }
  };
}

/*  */

var seenObjects = new _Set();

/**
 * Recursively traverse an object to evoke all converted
 * getters, so that every nested property inside the object
 * is collected as a "deep" dependency.
 */
function traverse (val) {
  _traverse(val, seenObjects);
  seenObjects.clear();
}

function _traverse (val, seen) {
  var i, keys;
  var isA = Array.isArray(val);
  if ((!isA && !isObject(val)) || Object.isFrozen(val) || val instanceof VNode) {
    return
  }
  if (val.__ob__) {
    var depId = val.__ob__.dep.id;
    if (seen.has(depId)) {
      return
    }
    seen.add(depId);
  }
  if (isA) {
    i = val.length;
    while (i--) { _traverse(val[i], seen); }
  } else {
    keys = Object.keys(val);
    i = keys.length;
    while (i--) { _traverse(val[keys[i]], seen); }
  }
}

var mark;
var measure;

if (true) {
  var perf = inBrowser && window.performance;
  /* istanbul ignore if */
  if (
    perf &&
    perf.mark &&
    perf.measure &&
    perf.clearMarks &&
    perf.clearMeasures
  ) {
    mark = function (tag) { return perf.mark(tag); };
    measure = function (name, startTag, endTag) {
      perf.measure(name, startTag, endTag);
      perf.clearMarks(startTag);
      perf.clearMarks(endTag);
      // perf.clearMeasures(name)
    };
  }
}

/*  */

var normalizeEvent = cached(function (name) {
  var passive = name.charAt(0) === '&';
  name = passive ? name.slice(1) : name;
  var once$$1 = name.charAt(0) === '~'; // Prefixed last, checked first
  name = once$$1 ? name.slice(1) : name;
  var capture = name.charAt(0) === '!';
  name = capture ? name.slice(1) : name;
  return {
    name: name,
    once: once$$1,
    capture: capture,
    passive: passive
  }
});

function createFnInvoker (fns, vm) {
  function invoker () {
    var arguments$1 = arguments;

    var fns = invoker.fns;
    if (Array.isArray(fns)) {
      var cloned = fns.slice();
      for (var i = 0; i < cloned.length; i++) {
        invokeWithErrorHandling(cloned[i], null, arguments$1, vm, "v-on handler");
      }
    } else {
      // return handler return value for single handlers
      return invokeWithErrorHandling(fns, null, arguments, vm, "v-on handler")
    }
  }
  invoker.fns = fns;
  return invoker
}

function updateListeners (
  on,
  oldOn,
  add,
  remove$$1,
  createOnceHandler,
  vm
) {
  var name, def$$1, cur, old, event;
  for (name in on) {
    def$$1 = cur = on[name];
    old = oldOn[name];
    event = normalizeEvent(name);
    if (isUndef(cur)) {
       true && warn(
        "Invalid handler for event \"" + (event.name) + "\": got " + String(cur),
        vm
      );
    } else if (isUndef(old)) {
      if (isUndef(cur.fns)) {
        cur = on[name] = createFnInvoker(cur, vm);
      }
      if (isTrue(event.once)) {
        cur = on[name] = createOnceHandler(event.name, cur, event.capture);
      }
      add(event.name, cur, event.capture, event.passive, event.params);
    } else if (cur !== old) {
      old.fns = cur;
      on[name] = old;
    }
  }
  for (name in oldOn) {
    if (isUndef(on[name])) {
      event = normalizeEvent(name);
      remove$$1(event.name, oldOn[name], event.capture);
    }
  }
}

/*  */

/*  */

// fixed by xxxxxx (mp properties)
function extractPropertiesFromVNodeData(data, Ctor, res, context) {
  var propOptions = Ctor.options.mpOptions && Ctor.options.mpOptions.properties;
  if (isUndef(propOptions)) {
    return res
  }
  var externalClasses = Ctor.options.mpOptions.externalClasses || [];
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      var result = checkProp(res, props, key, altKey, true) ||
          checkProp(res, attrs, key, altKey, false);
      // externalClass
      if (
        result &&
        res[key] &&
        externalClasses.indexOf(altKey) !== -1 &&
        context[camelize(res[key])]
      ) {
        // 赋值 externalClass 真正的值(模板里 externalClass 的值可能是字符串)
        res[key] = context[camelize(res[key])];
      }
    }
  }
  return res
}

function extractPropsFromVNodeData (
  data,
  Ctor,
  tag,
  context// fixed by xxxxxx
) {
  // we are only extracting raw values here.
  // validation and default values are handled in the child
  // component itself.
  var propOptions = Ctor.options.props;
  if (isUndef(propOptions)) {
    // fixed by xxxxxx
    return extractPropertiesFromVNodeData(data, Ctor, {}, context)
  }
  var res = {};
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      if (true) {
        var keyInLowerCase = key.toLowerCase();
        if (
          key !== keyInLowerCase &&
          attrs && hasOwn(attrs, keyInLowerCase)
        ) {
          tip(
            "Prop \"" + keyInLowerCase + "\" is passed to component " +
            (formatComponentName(tag || Ctor)) + ", but the declared prop name is" +
            " \"" + key + "\". " +
            "Note that HTML attributes are case-insensitive and camelCased " +
            "props need to use their kebab-case equivalents when using in-DOM " +
            "templates. You should probably use \"" + altKey + "\" instead of \"" + key + "\"."
          );
        }
      }
      checkProp(res, props, key, altKey, true) ||
      checkProp(res, attrs, key, altKey, false);
    }
  }
  // fixed by xxxxxx
  return extractPropertiesFromVNodeData(data, Ctor, res, context)
}

function checkProp (
  res,
  hash,
  key,
  altKey,
  preserve
) {
  if (isDef(hash)) {
    if (hasOwn(hash, key)) {
      res[key] = hash[key];
      if (!preserve) {
        delete hash[key];
      }
      return true
    } else if (hasOwn(hash, altKey)) {
      res[key] = hash[altKey];
      if (!preserve) {
        delete hash[altKey];
      }
      return true
    }
  }
  return false
}

/*  */

// The template compiler attempts to minimize the need for normalization by
// statically analyzing the template at compile time.
//
// For plain HTML markup, normalization can be completely skipped because the
// generated render function is guaranteed to return Array<VNode>. There are
// two cases where extra normalization is needed:

// 1. When the children contains components - because a functional component
// may return an Array instead of a single root. In this case, just a simple
// normalization is needed - if any child is an Array, we flatten the whole
// thing with Array.prototype.concat. It is guaranteed to be only 1-level deep
// because functional components already normalize their own children.
function simpleNormalizeChildren (children) {
  for (var i = 0; i < children.length; i++) {
    if (Array.isArray(children[i])) {
      return Array.prototype.concat.apply([], children)
    }
  }
  return children
}

// 2. When the children contains constructs that always generated nested Arrays,
// e.g. <template>, <slot>, v-for, or when the children is provided by user
// with hand-written render functions / JSX. In such cases a full normalization
// is needed to cater to all possible types of children values.
function normalizeChildren (children) {
  return isPrimitive(children)
    ? [createTextVNode(children)]
    : Array.isArray(children)
      ? normalizeArrayChildren(children)
      : undefined
}

function isTextNode (node) {
  return isDef(node) && isDef(node.text) && isFalse(node.isComment)
}

function normalizeArrayChildren (children, nestedIndex) {
  var res = [];
  var i, c, lastIndex, last;
  for (i = 0; i < children.length; i++) {
    c = children[i];
    if (isUndef(c) || typeof c === 'boolean') { continue }
    lastIndex = res.length - 1;
    last = res[lastIndex];
    //  nested
    if (Array.isArray(c)) {
      if (c.length > 0) {
        c = normalizeArrayChildren(c, ((nestedIndex || '') + "_" + i));
        // merge adjacent text nodes
        if (isTextNode(c[0]) && isTextNode(last)) {
          res[lastIndex] = createTextVNode(last.text + (c[0]).text);
          c.shift();
        }
        res.push.apply(res, c);
      }
    } else if (isPrimitive(c)) {
      if (isTextNode(last)) {
        // merge adjacent text nodes
        // this is necessary for SSR hydration because text nodes are
        // essentially merged when rendered to HTML strings
        res[lastIndex] = createTextVNode(last.text + c);
      } else if (c !== '') {
        // convert primitive to vnode
        res.push(createTextVNode(c));
      }
    } else {
      if (isTextNode(c) && isTextNode(last)) {
        // merge adjacent text nodes
        res[lastIndex] = createTextVNode(last.text + c.text);
      } else {
        // default key for nested array children (likely generated by v-for)
        if (isTrue(children._isVList) &&
          isDef(c.tag) &&
          isUndef(c.key) &&
          isDef(nestedIndex)) {
          c.key = "__vlist" + nestedIndex + "_" + i + "__";
        }
        res.push(c);
      }
    }
  }
  return res
}

/*  */

function initProvide (vm) {
  var provide = vm.$options.provide;
  if (provide) {
    vm._provided = typeof provide === 'function'
      ? provide.call(vm)
      : provide;
  }
}

function initInjections (vm) {
  var result = resolveInject(vm.$options.inject, vm);
  if (result) {
    toggleObserving(false);
    Object.keys(result).forEach(function (key) {
      /* istanbul ignore else */
      if (true) {
        defineReactive$$1(vm, key, result[key], function () {
          warn(
            "Avoid mutating an injected value directly since the changes will be " +
            "overwritten whenever the provided component re-renders. " +
            "injection being mutated: \"" + key + "\"",
            vm
          );
        });
      } else {}
    });
    toggleObserving(true);
  }
}

function resolveInject (inject, vm) {
  if (inject) {
    // inject is :any because flow is not smart enough to figure out cached
    var result = Object.create(null);
    var keys = hasSymbol
      ? Reflect.ownKeys(inject)
      : Object.keys(inject);

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      // #6574 in case the inject object is observed...
      if (key === '__ob__') { continue }
      var provideKey = inject[key].from;
      var source = vm;
      while (source) {
        if (source._provided && hasOwn(source._provided, provideKey)) {
          result[key] = source._provided[provideKey];
          break
        }
        source = source.$parent;
      }
      if (!source) {
        if ('default' in inject[key]) {
          var provideDefault = inject[key].default;
          result[key] = typeof provideDefault === 'function'
            ? provideDefault.call(vm)
            : provideDefault;
        } else if (true) {
          warn(("Injection \"" + key + "\" not found"), vm);
        }
      }
    }
    return result
  }
}

/*  */



/**
 * Runtime helper for resolving raw children VNodes into a slot object.
 */
function resolveSlots (
  children,
  context
) {
  if (!children || !children.length) {
    return {}
  }
  var slots = {};
  for (var i = 0, l = children.length; i < l; i++) {
    var child = children[i];
    var data = child.data;
    // remove slot attribute if the node is resolved as a Vue slot node
    if (data && data.attrs && data.attrs.slot) {
      delete data.attrs.slot;
    }
    // named slots should only be respected if the vnode was rendered in the
    // same context.
    if ((child.context === context || child.fnContext === context) &&
      data && data.slot != null
    ) {
      var name = data.slot;
      var slot = (slots[name] || (slots[name] = []));
      if (child.tag === 'template') {
        slot.push.apply(slot, child.children || []);
      } else {
        slot.push(child);
      }
    } else {
      // fixed by xxxxxx 临时 hack 掉 uni-app 中的异步 name slot page
      if(child.asyncMeta && child.asyncMeta.data && child.asyncMeta.data.slot === 'page'){
        (slots['page'] || (slots['page'] = [])).push(child);
      }else{
        (slots.default || (slots.default = [])).push(child);
      }
    }
  }
  // ignore slots that contains only whitespace
  for (var name$1 in slots) {
    if (slots[name$1].every(isWhitespace)) {
      delete slots[name$1];
    }
  }
  return slots
}

function isWhitespace (node) {
  return (node.isComment && !node.asyncFactory) || node.text === ' '
}

/*  */

function normalizeScopedSlots (
  slots,
  normalSlots,
  prevSlots
) {
  var res;
  var hasNormalSlots = Object.keys(normalSlots).length > 0;
  var isStable = slots ? !!slots.$stable : !hasNormalSlots;
  var key = slots && slots.$key;
  if (!slots) {
    res = {};
  } else if (slots._normalized) {
    // fast path 1: child component re-render only, parent did not change
    return slots._normalized
  } else if (
    isStable &&
    prevSlots &&
    prevSlots !== emptyObject &&
    key === prevSlots.$key &&
    !hasNormalSlots &&
    !prevSlots.$hasNormal
  ) {
    // fast path 2: stable scoped slots w/ no normal slots to proxy,
    // only need to normalize once
    return prevSlots
  } else {
    res = {};
    for (var key$1 in slots) {
      if (slots[key$1] && key$1[0] !== '$') {
        res[key$1] = normalizeScopedSlot(normalSlots, key$1, slots[key$1]);
      }
    }
  }
  // expose normal slots on scopedSlots
  for (var key$2 in normalSlots) {
    if (!(key$2 in res)) {
      res[key$2] = proxyNormalSlot(normalSlots, key$2);
    }
  }
  // avoriaz seems to mock a non-extensible $scopedSlots object
  // and when that is passed down this would cause an error
  if (slots && Object.isExtensible(slots)) {
    (slots)._normalized = res;
  }
  def(res, '$stable', isStable);
  def(res, '$key', key);
  def(res, '$hasNormal', hasNormalSlots);
  return res
}

function normalizeScopedSlot(normalSlots, key, fn) {
  var normalized = function () {
    var res = arguments.length ? fn.apply(null, arguments) : fn({});
    res = res && typeof res === 'object' && !Array.isArray(res)
      ? [res] // single vnode
      : normalizeChildren(res);
    return res && (
      res.length === 0 ||
      (res.length === 1 && res[0].isComment) // #9658
    ) ? undefined
      : res
  };
  // this is a slot using the new v-slot syntax without scope. although it is
  // compiled as a scoped slot, render fn users would expect it to be present
  // on this.$slots because the usage is semantically a normal slot.
  if (fn.proxy) {
    Object.defineProperty(normalSlots, key, {
      get: normalized,
      enumerable: true,
      configurable: true
    });
  }
  return normalized
}

function proxyNormalSlot(slots, key) {
  return function () { return slots[key]; }
}

/*  */

/**
 * Runtime helper for rendering v-for lists.
 */
function renderList (
  val,
  render
) {
  var ret, i, l, keys, key;
  if (Array.isArray(val) || typeof val === 'string') {
    ret = new Array(val.length);
    for (i = 0, l = val.length; i < l; i++) {
      ret[i] = render(val[i], i, i, i); // fixed by xxxxxx
    }
  } else if (typeof val === 'number') {
    ret = new Array(val);
    for (i = 0; i < val; i++) {
      ret[i] = render(i + 1, i, i, i); // fixed by xxxxxx
    }
  } else if (isObject(val)) {
    if (hasSymbol && val[Symbol.iterator]) {
      ret = [];
      var iterator = val[Symbol.iterator]();
      var result = iterator.next();
      while (!result.done) {
        ret.push(render(result.value, ret.length, i, i++)); // fixed by xxxxxx
        result = iterator.next();
      }
    } else {
      keys = Object.keys(val);
      ret = new Array(keys.length);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[i] = render(val[key], key, i, i); // fixed by xxxxxx
      }
    }
  }
  if (!isDef(ret)) {
    ret = [];
  }
  (ret)._isVList = true;
  return ret
}

/*  */

/**
 * Runtime helper for rendering <slot>
 */
function renderSlot (
  name,
  fallback,
  props,
  bindObject
) {
  var scopedSlotFn = this.$scopedSlots[name];
  var nodes;
  if (scopedSlotFn) { // scoped slot
    props = props || {};
    if (bindObject) {
      if ( true && !isObject(bindObject)) {
        warn(
          'slot v-bind without argument expects an Object',
          this
        );
      }
      props = extend(extend({}, bindObject), props);
    }
    // fixed by xxxxxx app-plus scopedSlot
    nodes = scopedSlotFn(props, this, props._i) || fallback;
  } else {
    nodes = this.$slots[name] || fallback;
  }

  var target = props && props.slot;
  if (target) {
    return this.$createElement('template', { slot: target }, nodes)
  } else {
    return nodes
  }
}

/*  */

/**
 * Runtime helper for resolving filters
 */
function resolveFilter (id) {
  return resolveAsset(this.$options, 'filters', id, true) || identity
}

/*  */

function isKeyNotMatch (expect, actual) {
  if (Array.isArray(expect)) {
    return expect.indexOf(actual) === -1
  } else {
    return expect !== actual
  }
}

/**
 * Runtime helper for checking keyCodes from config.
 * exposed as Vue.prototype._k
 * passing in eventKeyName as last argument separately for backwards compat
 */
function checkKeyCodes (
  eventKeyCode,
  key,
  builtInKeyCode,
  eventKeyName,
  builtInKeyName
) {
  var mappedKeyCode = config.keyCodes[key] || builtInKeyCode;
  if (builtInKeyName && eventKeyName && !config.keyCodes[key]) {
    return isKeyNotMatch(builtInKeyName, eventKeyName)
  } else if (mappedKeyCode) {
    return isKeyNotMatch(mappedKeyCode, eventKeyCode)
  } else if (eventKeyName) {
    return hyphenate(eventKeyName) !== key
  }
}

/*  */

/**
 * Runtime helper for merging v-bind="object" into a VNode's data.
 */
function bindObjectProps (
  data,
  tag,
  value,
  asProp,
  isSync
) {
  if (value) {
    if (!isObject(value)) {
       true && warn(
        'v-bind without argument expects an Object or Array value',
        this
      );
    } else {
      if (Array.isArray(value)) {
        value = toObject(value);
      }
      var hash;
      var loop = function ( key ) {
        if (
          key === 'class' ||
          key === 'style' ||
          isReservedAttribute(key)
        ) {
          hash = data;
        } else {
          var type = data.attrs && data.attrs.type;
          hash = asProp || config.mustUseProp(tag, type, key)
            ? data.domProps || (data.domProps = {})
            : data.attrs || (data.attrs = {});
        }
        var camelizedKey = camelize(key);
        var hyphenatedKey = hyphenate(key);
        if (!(camelizedKey in hash) && !(hyphenatedKey in hash)) {
          hash[key] = value[key];

          if (isSync) {
            var on = data.on || (data.on = {});
            on[("update:" + key)] = function ($event) {
              value[key] = $event;
            };
          }
        }
      };

      for (var key in value) loop( key );
    }
  }
  return data
}

/*  */

/**
 * Runtime helper for rendering static trees.
 */
function renderStatic (
  index,
  isInFor
) {
  var cached = this._staticTrees || (this._staticTrees = []);
  var tree = cached[index];
  // if has already-rendered static tree and not inside v-for,
  // we can reuse the same tree.
  if (tree && !isInFor) {
    return tree
  }
  // otherwise, render a fresh tree.
  tree = cached[index] = this.$options.staticRenderFns[index].call(
    this._renderProxy,
    null,
    this // for render fns generated for functional component templates
  );
  markStatic(tree, ("__static__" + index), false);
  return tree
}

/**
 * Runtime helper for v-once.
 * Effectively it means marking the node as static with a unique key.
 */
function markOnce (
  tree,
  index,
  key
) {
  markStatic(tree, ("__once__" + index + (key ? ("_" + key) : "")), true);
  return tree
}

function markStatic (
  tree,
  key,
  isOnce
) {
  if (Array.isArray(tree)) {
    for (var i = 0; i < tree.length; i++) {
      if (tree[i] && typeof tree[i] !== 'string') {
        markStaticNode(tree[i], (key + "_" + i), isOnce);
      }
    }
  } else {
    markStaticNode(tree, key, isOnce);
  }
}

function markStaticNode (node, key, isOnce) {
  node.isStatic = true;
  node.key = key;
  node.isOnce = isOnce;
}

/*  */

function bindObjectListeners (data, value) {
  if (value) {
    if (!isPlainObject(value)) {
       true && warn(
        'v-on without argument expects an Object value',
        this
      );
    } else {
      var on = data.on = data.on ? extend({}, data.on) : {};
      for (var key in value) {
        var existing = on[key];
        var ours = value[key];
        on[key] = existing ? [].concat(existing, ours) : ours;
      }
    }
  }
  return data
}

/*  */

function resolveScopedSlots (
  fns, // see flow/vnode
  res,
  // the following are added in 2.6
  hasDynamicKeys,
  contentHashKey
) {
  res = res || { $stable: !hasDynamicKeys };
  for (var i = 0; i < fns.length; i++) {
    var slot = fns[i];
    if (Array.isArray(slot)) {
      resolveScopedSlots(slot, res, hasDynamicKeys);
    } else if (slot) {
      // marker for reverse proxying v-slot without scope on this.$slots
      if (slot.proxy) {
        slot.fn.proxy = true;
      }
      res[slot.key] = slot.fn;
    }
  }
  if (contentHashKey) {
    (res).$key = contentHashKey;
  }
  return res
}

/*  */

function bindDynamicKeys (baseObj, values) {
  for (var i = 0; i < values.length; i += 2) {
    var key = values[i];
    if (typeof key === 'string' && key) {
      baseObj[values[i]] = values[i + 1];
    } else if ( true && key !== '' && key !== null) {
      // null is a special value for explicitly removing a binding
      warn(
        ("Invalid value for dynamic directive argument (expected string or null): " + key),
        this
      );
    }
  }
  return baseObj
}

// helper to dynamically append modifier runtime markers to event names.
// ensure only append when value is already string, otherwise it will be cast
// to string and cause the type check to miss.
function prependModifier (value, symbol) {
  return typeof value === 'string' ? symbol + value : value
}

/*  */

function installRenderHelpers (target) {
  target._o = markOnce;
  target._n = toNumber;
  target._s = toString;
  target._l = renderList;
  target._t = renderSlot;
  target._q = looseEqual;
  target._i = looseIndexOf;
  target._m = renderStatic;
  target._f = resolveFilter;
  target._k = checkKeyCodes;
  target._b = bindObjectProps;
  target._v = createTextVNode;
  target._e = createEmptyVNode;
  target._u = resolveScopedSlots;
  target._g = bindObjectListeners;
  target._d = bindDynamicKeys;
  target._p = prependModifier;
}

/*  */

function FunctionalRenderContext (
  data,
  props,
  children,
  parent,
  Ctor
) {
  var this$1 = this;

  var options = Ctor.options;
  // ensure the createElement function in functional components
  // gets a unique context - this is necessary for correct named slot check
  var contextVm;
  if (hasOwn(parent, '_uid')) {
    contextVm = Object.create(parent);
    // $flow-disable-line
    contextVm._original = parent;
  } else {
    // the context vm passed in is a functional context as well.
    // in this case we want to make sure we are able to get a hold to the
    // real context instance.
    contextVm = parent;
    // $flow-disable-line
    parent = parent._original;
  }
  var isCompiled = isTrue(options._compiled);
  var needNormalization = !isCompiled;

  this.data = data;
  this.props = props;
  this.children = children;
  this.parent = parent;
  this.listeners = data.on || emptyObject;
  this.injections = resolveInject(options.inject, parent);
  this.slots = function () {
    if (!this$1.$slots) {
      normalizeScopedSlots(
        data.scopedSlots,
        this$1.$slots = resolveSlots(children, parent)
      );
    }
    return this$1.$slots
  };

  Object.defineProperty(this, 'scopedSlots', ({
    enumerable: true,
    get: function get () {
      return normalizeScopedSlots(data.scopedSlots, this.slots())
    }
  }));

  // support for compiled functional template
  if (isCompiled) {
    // exposing $options for renderStatic()
    this.$options = options;
    // pre-resolve slots for renderSlot()
    this.$slots = this.slots();
    this.$scopedSlots = normalizeScopedSlots(data.scopedSlots, this.$slots);
  }

  if (options._scopeId) {
    this._c = function (a, b, c, d) {
      var vnode = createElement(contextVm, a, b, c, d, needNormalization);
      if (vnode && !Array.isArray(vnode)) {
        vnode.fnScopeId = options._scopeId;
        vnode.fnContext = parent;
      }
      return vnode
    };
  } else {
    this._c = function (a, b, c, d) { return createElement(contextVm, a, b, c, d, needNormalization); };
  }
}

installRenderHelpers(FunctionalRenderContext.prototype);

function createFunctionalComponent (
  Ctor,
  propsData,
  data,
  contextVm,
  children
) {
  var options = Ctor.options;
  var props = {};
  var propOptions = options.props;
  if (isDef(propOptions)) {
    for (var key in propOptions) {
      props[key] = validateProp(key, propOptions, propsData || emptyObject);
    }
  } else {
    if (isDef(data.attrs)) { mergeProps(props, data.attrs); }
    if (isDef(data.props)) { mergeProps(props, data.props); }
  }

  var renderContext = new FunctionalRenderContext(
    data,
    props,
    children,
    contextVm,
    Ctor
  );

  var vnode = options.render.call(null, renderContext._c, renderContext);

  if (vnode instanceof VNode) {
    return cloneAndMarkFunctionalResult(vnode, data, renderContext.parent, options, renderContext)
  } else if (Array.isArray(vnode)) {
    var vnodes = normalizeChildren(vnode) || [];
    var res = new Array(vnodes.length);
    for (var i = 0; i < vnodes.length; i++) {
      res[i] = cloneAndMarkFunctionalResult(vnodes[i], data, renderContext.parent, options, renderContext);
    }
    return res
  }
}

function cloneAndMarkFunctionalResult (vnode, data, contextVm, options, renderContext) {
  // #7817 clone node before setting fnContext, otherwise if the node is reused
  // (e.g. it was from a cached normal slot) the fnContext causes named slots
  // that should not be matched to match.
  var clone = cloneVNode(vnode);
  clone.fnContext = contextVm;
  clone.fnOptions = options;
  if (true) {
    (clone.devtoolsMeta = clone.devtoolsMeta || {}).renderContext = renderContext;
  }
  if (data.slot) {
    (clone.data || (clone.data = {})).slot = data.slot;
  }
  return clone
}

function mergeProps (to, from) {
  for (var key in from) {
    to[camelize(key)] = from[key];
  }
}

/*  */

/*  */

/*  */

/*  */

// inline hooks to be invoked on component VNodes during patch
var componentVNodeHooks = {
  init: function init (vnode, hydrating) {
    if (
      vnode.componentInstance &&
      !vnode.componentInstance._isDestroyed &&
      vnode.data.keepAlive
    ) {
      // kept-alive components, treat as a patch
      var mountedNode = vnode; // work around flow
      componentVNodeHooks.prepatch(mountedNode, mountedNode);
    } else {
      var child = vnode.componentInstance = createComponentInstanceForVnode(
        vnode,
        activeInstance
      );
      child.$mount(hydrating ? vnode.elm : undefined, hydrating);
    }
  },

  prepatch: function prepatch (oldVnode, vnode) {
    var options = vnode.componentOptions;
    var child = vnode.componentInstance = oldVnode.componentInstance;
    updateChildComponent(
      child,
      options.propsData, // updated props
      options.listeners, // updated listeners
      vnode, // new parent vnode
      options.children // new children
    );
  },

  insert: function insert (vnode) {
    var context = vnode.context;
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isMounted) {
      callHook(componentInstance, 'onServiceCreated');
      callHook(componentInstance, 'onServiceAttached');
      componentInstance._isMounted = true;
      callHook(componentInstance, 'mounted');
    }
    if (vnode.data.keepAlive) {
      if (context._isMounted) {
        // vue-router#1212
        // During updates, a kept-alive component's child components may
        // change, so directly walking the tree here may call activated hooks
        // on incorrect children. Instead we push them into a queue which will
        // be processed after the whole patch process ended.
        queueActivatedComponent(componentInstance);
      } else {
        activateChildComponent(componentInstance, true /* direct */);
      }
    }
  },

  destroy: function destroy (vnode) {
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isDestroyed) {
      if (!vnode.data.keepAlive) {
        componentInstance.$destroy();
      } else {
        deactivateChildComponent(componentInstance, true /* direct */);
      }
    }
  }
};

var hooksToMerge = Object.keys(componentVNodeHooks);

function createComponent (
  Ctor,
  data,
  context,
  children,
  tag
) {
  if (isUndef(Ctor)) {
    return
  }

  var baseCtor = context.$options._base;

  // plain options object: turn it into a constructor
  if (isObject(Ctor)) {
    Ctor = baseCtor.extend(Ctor);
  }

  // if at this stage it's not a constructor or an async component factory,
  // reject.
  if (typeof Ctor !== 'function') {
    if (true) {
      warn(("Invalid Component definition: " + (String(Ctor))), context);
    }
    return
  }

  // async component
  var asyncFactory;
  if (isUndef(Ctor.cid)) {
    asyncFactory = Ctor;
    Ctor = resolveAsyncComponent(asyncFactory, baseCtor);
    if (Ctor === undefined) {
      // return a placeholder node for async component, which is rendered
      // as a comment node but preserves all the raw information for the node.
      // the information will be used for async server-rendering and hydration.
      return createAsyncPlaceholder(
        asyncFactory,
        data,
        context,
        children,
        tag
      )
    }
  }

  data = data || {};

  // resolve constructor options in case global mixins are applied after
  // component constructor creation
  resolveConstructorOptions(Ctor);

  // transform component v-model data into props & events
  if (isDef(data.model)) {
    transformModel(Ctor.options, data);
  }

  // extract props
  var propsData = extractPropsFromVNodeData(data, Ctor, tag, context); // fixed by xxxxxx

  // functional component
  if (isTrue(Ctor.options.functional)) {
    return createFunctionalComponent(Ctor, propsData, data, context, children)
  }

  // extract listeners, since these needs to be treated as
  // child component listeners instead of DOM listeners
  var listeners = data.on;
  // replace with listeners with .native modifier
  // so it gets processed during parent component patch.
  data.on = data.nativeOn;

  if (isTrue(Ctor.options.abstract)) {
    // abstract components do not keep anything
    // other than props & listeners & slot

    // work around flow
    var slot = data.slot;
    data = {};
    if (slot) {
      data.slot = slot;
    }
  }

  // install component management hooks onto the placeholder node
  installComponentHooks(data);

  // return a placeholder vnode
  var name = Ctor.options.name || tag;
  var vnode = new VNode(
    ("vue-component-" + (Ctor.cid) + (name ? ("-" + name) : '')),
    data, undefined, undefined, undefined, context,
    { Ctor: Ctor, propsData: propsData, listeners: listeners, tag: tag, children: children },
    asyncFactory
  );

  return vnode
}

function createComponentInstanceForVnode (
  vnode, // we know it's MountedComponentVNode but flow doesn't
  parent // activeInstance in lifecycle state
) {
  var options = {
    _isComponent: true,
    _parentVnode: vnode,
    parent: parent
  };
  // check inline-template render functions
  var inlineTemplate = vnode.data.inlineTemplate;
  if (isDef(inlineTemplate)) {
    options.render = inlineTemplate.render;
    options.staticRenderFns = inlineTemplate.staticRenderFns;
  }
  return new vnode.componentOptions.Ctor(options)
}

function installComponentHooks (data) {
  var hooks = data.hook || (data.hook = {});
  for (var i = 0; i < hooksToMerge.length; i++) {
    var key = hooksToMerge[i];
    var existing = hooks[key];
    var toMerge = componentVNodeHooks[key];
    if (existing !== toMerge && !(existing && existing._merged)) {
      hooks[key] = existing ? mergeHook$1(toMerge, existing) : toMerge;
    }
  }
}

function mergeHook$1 (f1, f2) {
  var merged = function (a, b) {
    // flow complains about extra args which is why we use any
    f1(a, b);
    f2(a, b);
  };
  merged._merged = true;
  return merged
}

// transform component v-model info (value and callback) into
// prop and event handler respectively.
function transformModel (options, data) {
  var prop = (options.model && options.model.prop) || 'value';
  var event = (options.model && options.model.event) || 'input'
  ;(data.attrs || (data.attrs = {}))[prop] = data.model.value;
  var on = data.on || (data.on = {});
  var existing = on[event];
  var callback = data.model.callback;
  if (isDef(existing)) {
    if (
      Array.isArray(existing)
        ? existing.indexOf(callback) === -1
        : existing !== callback
    ) {
      on[event] = [callback].concat(existing);
    }
  } else {
    on[event] = callback;
  }
}

/*  */

var SIMPLE_NORMALIZE = 1;
var ALWAYS_NORMALIZE = 2;

// wrapper function for providing a more flexible interface
// without getting yelled at by flow
function createElement (
  context,
  tag,
  data,
  children,
  normalizationType,
  alwaysNormalize
) {
  if (Array.isArray(data) || isPrimitive(data)) {
    normalizationType = children;
    children = data;
    data = undefined;
  }
  if (isTrue(alwaysNormalize)) {
    normalizationType = ALWAYS_NORMALIZE;
  }
  return _createElement(context, tag, data, children, normalizationType)
}

function _createElement (
  context,
  tag,
  data,
  children,
  normalizationType
) {
  if (isDef(data) && isDef((data).__ob__)) {
     true && warn(
      "Avoid using observed data object as vnode data: " + (JSON.stringify(data)) + "\n" +
      'Always create fresh vnode data objects in each render!',
      context
    );
    return createEmptyVNode()
  }
  // object syntax in v-bind
  if (isDef(data) && isDef(data.is)) {
    tag = data.is;
  }
  if (!tag) {
    // in case of component :is set to falsy value
    return createEmptyVNode()
  }
  // warn against non-primitive key
  if ( true &&
    isDef(data) && isDef(data.key) && !isPrimitive(data.key)
  ) {
    {
      warn(
        'Avoid using non-primitive value as key, ' +
        'use string/number value instead.',
        context
      );
    }
  }
  // support single function children as default scoped slot
  if (Array.isArray(children) &&
    typeof children[0] === 'function'
  ) {
    data = data || {};
    data.scopedSlots = { default: children[0] };
    children.length = 0;
  }
  if (normalizationType === ALWAYS_NORMALIZE) {
    children = normalizeChildren(children);
  } else if (normalizationType === SIMPLE_NORMALIZE) {
    children = simpleNormalizeChildren(children);
  }
  var vnode, ns;
  if (typeof tag === 'string') {
    var Ctor;
    ns = (context.$vnode && context.$vnode.ns) || config.getTagNamespace(tag);
    if (config.isReservedTag(tag)) {
      // platform built-in elements
      if ( true && isDef(data) && isDef(data.nativeOn)) {
        warn(
          ("The .native modifier for v-on is only valid on components but it was used on <" + tag + ">."),
          context
        );
      }
      vnode = new VNode(
        config.parsePlatformTagName(tag), data, children,
        undefined, undefined, context
      );
    } else if ((!data || !data.pre) && isDef(Ctor = resolveAsset(context.$options, 'components', tag))) {
      // component
      vnode = createComponent(Ctor, data, context, children, tag);
    } else {
      // unknown or unlisted namespaced elements
      // check at runtime because it may get assigned a namespace when its
      // parent normalizes children
      vnode = new VNode(
        tag, data, children,
        undefined, undefined, context
      );
    }
  } else {
    // direct component options / constructor
    vnode = createComponent(tag, data, context, children);
  }
  if (Array.isArray(vnode)) {
    return vnode
  } else if (isDef(vnode)) {
    if (isDef(ns)) { applyNS(vnode, ns); }
    if (isDef(data)) { registerDeepBindings(data); }
    return vnode
  } else {
    return createEmptyVNode()
  }
}

function applyNS (vnode, ns, force) {
  vnode.ns = ns;
  if (vnode.tag === 'foreignObject') {
    // use default namespace inside foreignObject
    ns = undefined;
    force = true;
  }
  if (isDef(vnode.children)) {
    for (var i = 0, l = vnode.children.length; i < l; i++) {
      var child = vnode.children[i];
      if (isDef(child.tag) && (
        isUndef(child.ns) || (isTrue(force) && child.tag !== 'svg'))) {
        applyNS(child, ns, force);
      }
    }
  }
}

// ref #5318
// necessary to ensure parent re-render when deep bindings like :style and
// :class are used on slot nodes
function registerDeepBindings (data) {
  if (isObject(data.style)) {
    traverse(data.style);
  }
  if (isObject(data.class)) {
    traverse(data.class);
  }
}

/*  */

function initRender (vm) {
  vm._vnode = null; // the root of the child tree
  vm._staticTrees = null; // v-once cached trees
  var options = vm.$options;
  var parentVnode = vm.$vnode = options._parentVnode; // the placeholder node in parent tree
  var renderContext = parentVnode && parentVnode.context;
  vm.$slots = resolveSlots(options._renderChildren, renderContext);
  vm.$scopedSlots = emptyObject;
  // bind the createElement fn to this instance
  // so that we get proper render context inside it.
  // args order: tag, data, children, normalizationType, alwaysNormalize
  // internal version is used by render functions compiled from templates
  vm._c = function (a, b, c, d) { return createElement(vm, a, b, c, d, false); };
  // normalization is always applied for the public version, used in
  // user-written render functions.
  vm.$createElement = function (a, b, c, d) { return createElement(vm, a, b, c, d, true); };

  // $attrs & $listeners are exposed for easier HOC creation.
  // they need to be reactive so that HOCs using them are always updated
  var parentData = parentVnode && parentVnode.data;

  /* istanbul ignore else */
  if (true) {
    defineReactive$$1(vm, '$attrs', parentData && parentData.attrs || emptyObject, function () {
      !isUpdatingChildComponent && warn("$attrs is readonly.", vm);
    }, true);
    defineReactive$$1(vm, '$listeners', options._parentListeners || emptyObject, function () {
      !isUpdatingChildComponent && warn("$listeners is readonly.", vm);
    }, true);
  } else {}
}

var currentRenderingInstance = null;

function renderMixin (Vue) {
  // install runtime convenience helpers
  installRenderHelpers(Vue.prototype);

  Vue.prototype.$nextTick = function (fn) {
    return nextTick(fn, this)
  };

  Vue.prototype._render = function () {
    var vm = this;
    var ref = vm.$options;
    var render = ref.render;
    var _parentVnode = ref._parentVnode;

    if (_parentVnode) {
      vm.$scopedSlots = normalizeScopedSlots(
        _parentVnode.data.scopedSlots,
        vm.$slots,
        vm.$scopedSlots
      );
    }

    // set parent vnode. this allows render functions to have access
    // to the data on the placeholder node.
    vm.$vnode = _parentVnode;
    // render self
    var vnode;
    try {
      // There's no need to maintain a stack because all render fns are called
      // separately from one another. Nested component's render fns are called
      // when parent component is patched.
      currentRenderingInstance = vm;
      vnode = render.call(vm._renderProxy, vm.$createElement);
    } catch (e) {
      handleError(e, vm, "render");
      // return error render result,
      // or previous vnode to prevent render error causing blank component
      /* istanbul ignore else */
      if ( true && vm.$options.renderError) {
        try {
          vnode = vm.$options.renderError.call(vm._renderProxy, vm.$createElement, e);
        } catch (e) {
          handleError(e, vm, "renderError");
          vnode = vm._vnode;
        }
      } else {
        vnode = vm._vnode;
      }
    } finally {
      currentRenderingInstance = null;
    }
    // if the returned array contains only a single node, allow it
    if (Array.isArray(vnode) && vnode.length === 1) {
      vnode = vnode[0];
    }
    // return empty vnode in case the render function errored out
    if (!(vnode instanceof VNode)) {
      if ( true && Array.isArray(vnode)) {
        warn(
          'Multiple root nodes returned from render function. Render function ' +
          'should return a single root node.',
          vm
        );
      }
      vnode = createEmptyVNode();
    }
    // set parent
    vnode.parent = _parentVnode;
    return vnode
  };
}

/*  */

function ensureCtor (comp, base) {
  if (
    comp.__esModule ||
    (hasSymbol && comp[Symbol.toStringTag] === 'Module')
  ) {
    comp = comp.default;
  }
  return isObject(comp)
    ? base.extend(comp)
    : comp
}

function createAsyncPlaceholder (
  factory,
  data,
  context,
  children,
  tag
) {
  var node = createEmptyVNode();
  node.asyncFactory = factory;
  node.asyncMeta = { data: data, context: context, children: children, tag: tag };
  return node
}

function resolveAsyncComponent (
  factory,
  baseCtor
) {
  if (isTrue(factory.error) && isDef(factory.errorComp)) {
    return factory.errorComp
  }

  if (isDef(factory.resolved)) {
    return factory.resolved
  }

  var owner = currentRenderingInstance;
  if (owner && isDef(factory.owners) && factory.owners.indexOf(owner) === -1) {
    // already pending
    factory.owners.push(owner);
  }

  if (isTrue(factory.loading) && isDef(factory.loadingComp)) {
    return factory.loadingComp
  }

  if (owner && !isDef(factory.owners)) {
    var owners = factory.owners = [owner];
    var sync = true;
    var timerLoading = null;
    var timerTimeout = null

    ;(owner).$on('hook:destroyed', function () { return remove(owners, owner); });

    var forceRender = function (renderCompleted) {
      for (var i = 0, l = owners.length; i < l; i++) {
        (owners[i]).$forceUpdate();
      }

      if (renderCompleted) {
        owners.length = 0;
        if (timerLoading !== null) {
          clearTimeout(timerLoading);
          timerLoading = null;
        }
        if (timerTimeout !== null) {
          clearTimeout(timerTimeout);
          timerTimeout = null;
        }
      }
    };

    var resolve = once(function (res) {
      // cache resolved
      factory.resolved = ensureCtor(res, baseCtor);
      // invoke callbacks only if this is not a synchronous resolve
      // (async resolves are shimmed as synchronous during SSR)
      if (!sync) {
        forceRender(true);
      } else {
        owners.length = 0;
      }
    });

    var reject = once(function (reason) {
       true && warn(
        "Failed to resolve async component: " + (String(factory)) +
        (reason ? ("\nReason: " + reason) : '')
      );
      if (isDef(factory.errorComp)) {
        factory.error = true;
        forceRender(true);
      }
    });

    var res = factory(resolve, reject);

    if (isObject(res)) {
      if (isPromise(res)) {
        // () => Promise
        if (isUndef(factory.resolved)) {
          res.then(resolve, reject);
        }
      } else if (isPromise(res.component)) {
        res.component.then(resolve, reject);

        if (isDef(res.error)) {
          factory.errorComp = ensureCtor(res.error, baseCtor);
        }

        if (isDef(res.loading)) {
          factory.loadingComp = ensureCtor(res.loading, baseCtor);
          if (res.delay === 0) {
            factory.loading = true;
          } else {
            timerLoading = setTimeout(function () {
              timerLoading = null;
              if (isUndef(factory.resolved) && isUndef(factory.error)) {
                factory.loading = true;
                forceRender(false);
              }
            }, res.delay || 200);
          }
        }

        if (isDef(res.timeout)) {
          timerTimeout = setTimeout(function () {
            timerTimeout = null;
            if (isUndef(factory.resolved)) {
              reject(
                 true
                  ? ("timeout (" + (res.timeout) + "ms)")
                  : undefined
              );
            }
          }, res.timeout);
        }
      }
    }

    sync = false;
    // return in case resolved synchronously
    return factory.loading
      ? factory.loadingComp
      : factory.resolved
  }
}

/*  */

function isAsyncPlaceholder (node) {
  return node.isComment && node.asyncFactory
}

/*  */

function getFirstComponentChild (children) {
  if (Array.isArray(children)) {
    for (var i = 0; i < children.length; i++) {
      var c = children[i];
      if (isDef(c) && (isDef(c.componentOptions) || isAsyncPlaceholder(c))) {
        return c
      }
    }
  }
}

/*  */

/*  */

function initEvents (vm) {
  vm._events = Object.create(null);
  vm._hasHookEvent = false;
  // init parent attached events
  var listeners = vm.$options._parentListeners;
  if (listeners) {
    updateComponentListeners(vm, listeners);
  }
}

var target;

function add (event, fn) {
  target.$on(event, fn);
}

function remove$1 (event, fn) {
  target.$off(event, fn);
}

function createOnceHandler (event, fn) {
  var _target = target;
  return function onceHandler () {
    var res = fn.apply(null, arguments);
    if (res !== null) {
      _target.$off(event, onceHandler);
    }
  }
}

function updateComponentListeners (
  vm,
  listeners,
  oldListeners
) {
  target = vm;
  updateListeners(listeners, oldListeners || {}, add, remove$1, createOnceHandler, vm);
  target = undefined;
}

function eventsMixin (Vue) {
  var hookRE = /^hook:/;
  Vue.prototype.$on = function (event, fn) {
    var vm = this;
    if (Array.isArray(event)) {
      for (var i = 0, l = event.length; i < l; i++) {
        vm.$on(event[i], fn);
      }
    } else {
      (vm._events[event] || (vm._events[event] = [])).push(fn);
      // optimize hook:event cost by using a boolean flag marked at registration
      // instead of a hash lookup
      if (hookRE.test(event)) {
        vm._hasHookEvent = true;
      }
    }
    return vm
  };

  Vue.prototype.$once = function (event, fn) {
    var vm = this;
    function on () {
      vm.$off(event, on);
      fn.apply(vm, arguments);
    }
    on.fn = fn;
    vm.$on(event, on);
    return vm
  };

  Vue.prototype.$off = function (event, fn) {
    var vm = this;
    // all
    if (!arguments.length) {
      vm._events = Object.create(null);
      return vm
    }
    // array of events
    if (Array.isArray(event)) {
      for (var i$1 = 0, l = event.length; i$1 < l; i$1++) {
        vm.$off(event[i$1], fn);
      }
      return vm
    }
    // specific event
    var cbs = vm._events[event];
    if (!cbs) {
      return vm
    }
    if (!fn) {
      vm._events[event] = null;
      return vm
    }
    // specific handler
    var cb;
    var i = cbs.length;
    while (i--) {
      cb = cbs[i];
      if (cb === fn || cb.fn === fn) {
        cbs.splice(i, 1);
        break
      }
    }
    return vm
  };

  Vue.prototype.$emit = function (event) {
    var vm = this;
    if (true) {
      var lowerCaseEvent = event.toLowerCase();
      if (lowerCaseEvent !== event && vm._events[lowerCaseEvent]) {
        tip(
          "Event \"" + lowerCaseEvent + "\" is emitted in component " +
          (formatComponentName(vm)) + " but the handler is registered for \"" + event + "\". " +
          "Note that HTML attributes are case-insensitive and you cannot use " +
          "v-on to listen to camelCase events when using in-DOM templates. " +
          "You should probably use \"" + (hyphenate(event)) + "\" instead of \"" + event + "\"."
        );
      }
    }
    var cbs = vm._events[event];
    if (cbs) {
      cbs = cbs.length > 1 ? toArray(cbs) : cbs;
      var args = toArray(arguments, 1);
      var info = "event handler for \"" + event + "\"";
      for (var i = 0, l = cbs.length; i < l; i++) {
        invokeWithErrorHandling(cbs[i], vm, args, vm, info);
      }
    }
    return vm
  };
}

/*  */

var activeInstance = null;
var isUpdatingChildComponent = false;

function setActiveInstance(vm) {
  var prevActiveInstance = activeInstance;
  activeInstance = vm;
  return function () {
    activeInstance = prevActiveInstance;
  }
}

function initLifecycle (vm) {
  var options = vm.$options;

  // locate first non-abstract parent
  var parent = options.parent;
  if (parent && !options.abstract) {
    while (parent.$options.abstract && parent.$parent) {
      parent = parent.$parent;
    }
    parent.$children.push(vm);
  }

  vm.$parent = parent;
  vm.$root = parent ? parent.$root : vm;

  vm.$children = [];
  vm.$refs = {};

  vm._watcher = null;
  vm._inactive = null;
  vm._directInactive = false;
  vm._isMounted = false;
  vm._isDestroyed = false;
  vm._isBeingDestroyed = false;
}

function lifecycleMixin (Vue) {
  Vue.prototype._update = function (vnode, hydrating) {
    var vm = this;
    var prevEl = vm.$el;
    var prevVnode = vm._vnode;
    var restoreActiveInstance = setActiveInstance(vm);
    vm._vnode = vnode;
    // Vue.prototype.__patch__ is injected in entry points
    // based on the rendering backend used.
    if (!prevVnode) {
      // initial render
      vm.$el = vm.__patch__(vm.$el, vnode, hydrating, false /* removeOnly */);
    } else {
      // updates
      vm.$el = vm.__patch__(prevVnode, vnode);
    }
    restoreActiveInstance();
    // update __vue__ reference
    if (prevEl) {
      prevEl.__vue__ = null;
    }
    if (vm.$el) {
      vm.$el.__vue__ = vm;
    }
    // if parent is an HOC, update its $el as well
    if (vm.$vnode && vm.$parent && vm.$vnode === vm.$parent._vnode) {
      vm.$parent.$el = vm.$el;
    }
    // updated hook is called by the scheduler to ensure that children are
    // updated in a parent's updated hook.
  };

  Vue.prototype.$forceUpdate = function () {
    var vm = this;
    if (vm._watcher) {
      vm._watcher.update();
    }
  };

  Vue.prototype.$destroy = function () {
    var vm = this;
    if (vm._isBeingDestroyed) {
      return
    }
    callHook(vm, 'beforeDestroy');
    vm._isBeingDestroyed = true;
    // remove self from parent
    var parent = vm.$parent;
    if (parent && !parent._isBeingDestroyed && !vm.$options.abstract) {
      remove(parent.$children, vm);
    }
    // teardown watchers
    if (vm._watcher) {
      vm._watcher.teardown();
    }
    var i = vm._watchers.length;
    while (i--) {
      vm._watchers[i].teardown();
    }
    // remove reference from data ob
    // frozen object may not have observer.
    if (vm._data.__ob__) {
      vm._data.__ob__.vmCount--;
    }
    // call the last hook...
    vm._isDestroyed = true;
    // invoke destroy hooks on current rendered tree
    vm.__patch__(vm._vnode, null);
    // fire destroyed hook
    callHook(vm, 'destroyed');
    // turn off all instance listeners.
    vm.$off();
    // remove __vue__ reference
    if (vm.$el) {
      vm.$el.__vue__ = null;
    }
    // release circular reference (#6759)
    if (vm.$vnode) {
      vm.$vnode.parent = null;
    }
  };
}

function updateChildComponent (
  vm,
  propsData,
  listeners,
  parentVnode,
  renderChildren
) {
  if (true) {
    isUpdatingChildComponent = true;
  }

  // determine whether component has slot children
  // we need to do this before overwriting $options._renderChildren.

  // check if there are dynamic scopedSlots (hand-written or compiled but with
  // dynamic slot names). Static scoped slots compiled from template has the
  // "$stable" marker.
  var newScopedSlots = parentVnode.data.scopedSlots;
  var oldScopedSlots = vm.$scopedSlots;
  var hasDynamicScopedSlot = !!(
    (newScopedSlots && !newScopedSlots.$stable) ||
    (oldScopedSlots !== emptyObject && !oldScopedSlots.$stable) ||
    (newScopedSlots && vm.$scopedSlots.$key !== newScopedSlots.$key)
  );

  // Any static slot children from the parent may have changed during parent's
  // update. Dynamic scoped slots may also have changed. In such cases, a forced
  // update is necessary to ensure correctness.
  var needsForceUpdate = !!(
    renderChildren ||               // has new static slots
    vm.$options._renderChildren ||  // has old static slots
    hasDynamicScopedSlot
  );

  vm.$options._parentVnode = parentVnode;
  vm.$vnode = parentVnode; // update vm's placeholder node without re-render

  if (vm._vnode) { // update child tree's parent
    vm._vnode.parent = parentVnode;
  }
  vm.$options._renderChildren = renderChildren;

  // update $attrs and $listeners hash
  // these are also reactive so they may trigger child update if the child
  // used them during render
  vm.$attrs = parentVnode.data.attrs || emptyObject;
  vm.$listeners = listeners || emptyObject;

  // update props
  if (propsData && vm.$options.props) {
    toggleObserving(false);
    var props = vm._props;
    var propKeys = vm.$options._propKeys || [];
    for (var i = 0; i < propKeys.length; i++) {
      var key = propKeys[i];
      var propOptions = vm.$options.props; // wtf flow?
      props[key] = validateProp(key, propOptions, propsData, vm);
    }
    toggleObserving(true);
    // keep a copy of raw propsData
    vm.$options.propsData = propsData;
  }
  
  // fixed by xxxxxx update properties(mp runtime)
  vm._$updateProperties && vm._$updateProperties(vm);
  
  // update listeners
  listeners = listeners || emptyObject;
  var oldListeners = vm.$options._parentListeners;
  vm.$options._parentListeners = listeners;
  updateComponentListeners(vm, listeners, oldListeners);

  // resolve slots + force update if has children
  if (needsForceUpdate) {
    vm.$slots = resolveSlots(renderChildren, parentVnode.context);
    vm.$forceUpdate();
  }

  if (true) {
    isUpdatingChildComponent = false;
  }
}

function isInInactiveTree (vm) {
  while (vm && (vm = vm.$parent)) {
    if (vm._inactive) { return true }
  }
  return false
}

function activateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = false;
    if (isInInactiveTree(vm)) {
      return
    }
  } else if (vm._directInactive) {
    return
  }
  if (vm._inactive || vm._inactive === null) {
    vm._inactive = false;
    for (var i = 0; i < vm.$children.length; i++) {
      activateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'activated');
  }
}

function deactivateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = true;
    if (isInInactiveTree(vm)) {
      return
    }
  }
  if (!vm._inactive) {
    vm._inactive = true;
    for (var i = 0; i < vm.$children.length; i++) {
      deactivateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'deactivated');
  }
}

function callHook (vm, hook) {
  // #7573 disable dep collection when invoking lifecycle hooks
  pushTarget();
  var handlers = vm.$options[hook];
  var info = hook + " hook";
  if (handlers) {
    for (var i = 0, j = handlers.length; i < j; i++) {
      invokeWithErrorHandling(handlers[i], vm, null, vm, info);
    }
  }
  if (vm._hasHookEvent) {
    vm.$emit('hook:' + hook);
  }
  popTarget();
}

/*  */

var MAX_UPDATE_COUNT = 100;

var queue = [];
var activatedChildren = [];
var has = {};
var circular = {};
var waiting = false;
var flushing = false;
var index = 0;

/**
 * Reset the scheduler's state.
 */
function resetSchedulerState () {
  index = queue.length = activatedChildren.length = 0;
  has = {};
  if (true) {
    circular = {};
  }
  waiting = flushing = false;
}

// Async edge case #6566 requires saving the timestamp when event listeners are
// attached. However, calling performance.now() has a perf overhead especially
// if the page has thousands of event listeners. Instead, we take a timestamp
// every time the scheduler flushes and use that for all event listeners
// attached during that flush.
var currentFlushTimestamp = 0;

// Async edge case fix requires storing an event listener's attach timestamp.
var getNow = Date.now;

// Determine what event timestamp the browser is using. Annoyingly, the
// timestamp can either be hi-res (relative to page load) or low-res
// (relative to UNIX epoch), so in order to compare time we have to use the
// same timestamp type when saving the flush timestamp.
// All IE versions use low-res event timestamps, and have problematic clock
// implementations (#9632)
if (inBrowser && !isIE) {
  var performance = window.performance;
  if (
    performance &&
    typeof performance.now === 'function' &&
    getNow() > document.createEvent('Event').timeStamp
  ) {
    // if the event timestamp, although evaluated AFTER the Date.now(), is
    // smaller than it, it means the event is using a hi-res timestamp,
    // and we need to use the hi-res version for event listener timestamps as
    // well.
    getNow = function () { return performance.now(); };
  }
}

/**
 * Flush both queues and run the watchers.
 */
function flushSchedulerQueue () {
  currentFlushTimestamp = getNow();
  flushing = true;
  var watcher, id;

  // Sort queue before flush.
  // This ensures that:
  // 1. Components are updated from parent to child. (because parent is always
  //    created before the child)
  // 2. A component's user watchers are run before its render watcher (because
  //    user watchers are created before the render watcher)
  // 3. If a component is destroyed during a parent component's watcher run,
  //    its watchers can be skipped.
  queue.sort(function (a, b) { return a.id - b.id; });

  // do not cache length because more watchers might be pushed
  // as we run existing watchers
  for (index = 0; index < queue.length; index++) {
    watcher = queue[index];
    if (watcher.before) {
      watcher.before();
    }
    id = watcher.id;
    has[id] = null;
    watcher.run();
    // in dev build, check and stop circular updates.
    if ( true && has[id] != null) {
      circular[id] = (circular[id] || 0) + 1;
      if (circular[id] > MAX_UPDATE_COUNT) {
        warn(
          'You may have an infinite update loop ' + (
            watcher.user
              ? ("in watcher with expression \"" + (watcher.expression) + "\"")
              : "in a component render function."
          ),
          watcher.vm
        );
        break
      }
    }
  }

  // keep copies of post queues before resetting state
  var activatedQueue = activatedChildren.slice();
  var updatedQueue = queue.slice();

  resetSchedulerState();

  // call component updated and activated hooks
  callActivatedHooks(activatedQueue);
  callUpdatedHooks(updatedQueue);

  // devtool hook
  /* istanbul ignore if */
  if (devtools && config.devtools) {
    devtools.emit('flush');
  }
}

function callUpdatedHooks (queue) {
  var i = queue.length;
  while (i--) {
    var watcher = queue[i];
    var vm = watcher.vm;
    if (vm._watcher === watcher && vm._isMounted && !vm._isDestroyed) {
      callHook(vm, 'updated');
    }
  }
}

/**
 * Queue a kept-alive component that was activated during patch.
 * The queue will be processed after the entire tree has been patched.
 */
function queueActivatedComponent (vm) {
  // setting _inactive to false here so that a render function can
  // rely on checking whether it's in an inactive tree (e.g. router-view)
  vm._inactive = false;
  activatedChildren.push(vm);
}

function callActivatedHooks (queue) {
  for (var i = 0; i < queue.length; i++) {
    queue[i]._inactive = true;
    activateChildComponent(queue[i], true /* true */);
  }
}

/**
 * Push a watcher into the watcher queue.
 * Jobs with duplicate IDs will be skipped unless it's
 * pushed when the queue is being flushed.
 */
function queueWatcher (watcher) {
  var id = watcher.id;
  if (has[id] == null) {
    has[id] = true;
    if (!flushing) {
      queue.push(watcher);
    } else {
      // if already flushing, splice the watcher based on its id
      // if already past its id, it will be run next immediately.
      var i = queue.length - 1;
      while (i > index && queue[i].id > watcher.id) {
        i--;
      }
      queue.splice(i + 1, 0, watcher);
    }
    // queue the flush
    if (!waiting) {
      waiting = true;

      if ( true && !config.async) {
        flushSchedulerQueue();
        return
      }
      nextTick(flushSchedulerQueue);
    }
  }
}

/*  */



var uid$2 = 0;

/**
 * A watcher parses an expression, collects dependencies,
 * and fires callback when the expression value changes.
 * This is used for both the $watch() api and directives.
 */
var Watcher = function Watcher (
  vm,
  expOrFn,
  cb,
  options,
  isRenderWatcher
) {
  this.vm = vm;
  if (isRenderWatcher) {
    vm._watcher = this;
  }
  vm._watchers.push(this);
  // options
  if (options) {
    this.deep = !!options.deep;
    this.user = !!options.user;
    this.lazy = !!options.lazy;
    this.sync = !!options.sync;
    this.before = options.before;
  } else {
    this.deep = this.user = this.lazy = this.sync = false;
  }
  this.cb = cb;
  this.id = ++uid$2; // uid for batching
  this.active = true;
  this.dirty = this.lazy; // for lazy watchers
  this.deps = [];
  this.newDeps = [];
  this.depIds = new _Set();
  this.newDepIds = new _Set();
  this.expression =  true
    ? expOrFn.toString()
    : undefined;
  // parse expression for getter
  if (typeof expOrFn === 'function') {
    this.getter = expOrFn;
  } else {
    this.getter = parsePath(expOrFn);
    if (!this.getter) {
      this.getter = noop;
       true && warn(
        "Failed watching path: \"" + expOrFn + "\" " +
        'Watcher only accepts simple dot-delimited paths. ' +
        'For full control, use a function instead.',
        vm
      );
    }
  }
  this.value = this.lazy
    ? undefined
    : this.get();
};

/**
 * Evaluate the getter, and re-collect dependencies.
 */
Watcher.prototype.get = function get () {
  pushTarget(this);
  var value;
  var vm = this.vm;
  try {
    value = this.getter.call(vm, vm);
  } catch (e) {
    if (this.user) {
      handleError(e, vm, ("getter for watcher \"" + (this.expression) + "\""));
    } else {
      throw e
    }
  } finally {
    // "touch" every property so they are all tracked as
    // dependencies for deep watching
    if (this.deep) {
      traverse(value);
    }
    popTarget();
    this.cleanupDeps();
  }
  return value
};

/**
 * Add a dependency to this directive.
 */
Watcher.prototype.addDep = function addDep (dep) {
  var id = dep.id;
  if (!this.newDepIds.has(id)) {
    this.newDepIds.add(id);
    this.newDeps.push(dep);
    if (!this.depIds.has(id)) {
      dep.addSub(this);
    }
  }
};

/**
 * Clean up for dependency collection.
 */
Watcher.prototype.cleanupDeps = function cleanupDeps () {
  var i = this.deps.length;
  while (i--) {
    var dep = this.deps[i];
    if (!this.newDepIds.has(dep.id)) {
      dep.removeSub(this);
    }
  }
  var tmp = this.depIds;
  this.depIds = this.newDepIds;
  this.newDepIds = tmp;
  this.newDepIds.clear();
  tmp = this.deps;
  this.deps = this.newDeps;
  this.newDeps = tmp;
  this.newDeps.length = 0;
};

/**
 * Subscriber interface.
 * Will be called when a dependency changes.
 */
Watcher.prototype.update = function update () {
  /* istanbul ignore else */
  if (this.lazy) {
    this.dirty = true;
  } else if (this.sync) {
    this.run();
  } else {
    queueWatcher(this);
  }
};

/**
 * Scheduler job interface.
 * Will be called by the scheduler.
 */
Watcher.prototype.run = function run () {
  if (this.active) {
    var value = this.get();
    if (
      value !== this.value ||
      // Deep watchers and watchers on Object/Arrays should fire even
      // when the value is the same, because the value may
      // have mutated.
      isObject(value) ||
      this.deep
    ) {
      // set new value
      var oldValue = this.value;
      this.value = value;
      if (this.user) {
        try {
          this.cb.call(this.vm, value, oldValue);
        } catch (e) {
          handleError(e, this.vm, ("callback for watcher \"" + (this.expression) + "\""));
        }
      } else {
        this.cb.call(this.vm, value, oldValue);
      }
    }
  }
};

/**
 * Evaluate the value of the watcher.
 * This only gets called for lazy watchers.
 */
Watcher.prototype.evaluate = function evaluate () {
  this.value = this.get();
  this.dirty = false;
};

/**
 * Depend on all deps collected by this watcher.
 */
Watcher.prototype.depend = function depend () {
  var i = this.deps.length;
  while (i--) {
    this.deps[i].depend();
  }
};

/**
 * Remove self from all dependencies' subscriber list.
 */
Watcher.prototype.teardown = function teardown () {
  if (this.active) {
    // remove self from vm's watcher list
    // this is a somewhat expensive operation so we skip it
    // if the vm is being destroyed.
    if (!this.vm._isBeingDestroyed) {
      remove(this.vm._watchers, this);
    }
    var i = this.deps.length;
    while (i--) {
      this.deps[i].removeSub(this);
    }
    this.active = false;
  }
};

/*  */

var sharedPropertyDefinition = {
  enumerable: true,
  configurable: true,
  get: noop,
  set: noop
};

function proxy (target, sourceKey, key) {
  sharedPropertyDefinition.get = function proxyGetter () {
    return this[sourceKey][key]
  };
  sharedPropertyDefinition.set = function proxySetter (val) {
    this[sourceKey][key] = val;
  };
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function initState (vm) {
  vm._watchers = [];
  var opts = vm.$options;
  if (opts.props) { initProps(vm, opts.props); }
  if (opts.methods) { initMethods(vm, opts.methods); }
  if (opts.data) {
    initData(vm);
  } else {
    observe(vm._data = {}, true /* asRootData */);
  }
  if (opts.computed) { initComputed(vm, opts.computed); }
  if (opts.watch && opts.watch !== nativeWatch) {
    initWatch(vm, opts.watch);
  }
}

function initProps (vm, propsOptions) {
  var propsData = vm.$options.propsData || {};
  var props = vm._props = {};
  // cache prop keys so that future props updates can iterate using Array
  // instead of dynamic object key enumeration.
  var keys = vm.$options._propKeys = [];
  var isRoot = !vm.$parent;
  // root instance props should be converted
  if (!isRoot) {
    toggleObserving(false);
  }
  var loop = function ( key ) {
    keys.push(key);
    var value = validateProp(key, propsOptions, propsData, vm);
    /* istanbul ignore else */
    if (true) {
      var hyphenatedKey = hyphenate(key);
      if (isReservedAttribute(hyphenatedKey) ||
          config.isReservedAttr(hyphenatedKey)) {
        warn(
          ("\"" + hyphenatedKey + "\" is a reserved attribute and cannot be used as component prop."),
          vm
        );
      }
      defineReactive$$1(props, key, value, function () {
        if (!isRoot && !isUpdatingChildComponent) {
          {
            if(vm.mpHost === 'mp-baidu'){//百度 observer 在 setData callback 之后触发，直接忽略该 warn
                return
            }
            //fixed by xxxxxx __next_tick_pending,uni://form-field 时不告警
            if(
                key === 'value' && 
                Array.isArray(vm.$options.behaviors) &&
                vm.$options.behaviors.indexOf('uni://form-field') !== -1
              ){
              return
            }
            if(vm._getFormData){
              return
            }
            var $parent = vm.$parent;
            while($parent){
              if($parent.__next_tick_pending){
                return  
              }
              $parent = $parent.$parent;
            }
          }
          warn(
            "Avoid mutating a prop directly since the value will be " +
            "overwritten whenever the parent component re-renders. " +
            "Instead, use a data or computed property based on the prop's " +
            "value. Prop being mutated: \"" + key + "\"",
            vm
          );
        }
      });
    } else {}
    // static props are already proxied on the component's prototype
    // during Vue.extend(). We only need to proxy props defined at
    // instantiation here.
    if (!(key in vm)) {
      proxy(vm, "_props", key);
    }
  };

  for (var key in propsOptions) loop( key );
  toggleObserving(true);
}

function initData (vm) {
  var data = vm.$options.data;
  data = vm._data = typeof data === 'function'
    ? getData(data, vm)
    : data || {};
  if (!isPlainObject(data)) {
    data = {};
     true && warn(
      'data functions should return an object:\n' +
      'https://vuejs.org/v2/guide/components.html#data-Must-Be-a-Function',
      vm
    );
  }
  // proxy data on instance
  var keys = Object.keys(data);
  var props = vm.$options.props;
  var methods = vm.$options.methods;
  var i = keys.length;
  while (i--) {
    var key = keys[i];
    if (true) {
      if (methods && hasOwn(methods, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a data property."),
          vm
        );
      }
    }
    if (props && hasOwn(props, key)) {
       true && warn(
        "The data property \"" + key + "\" is already declared as a prop. " +
        "Use prop default value instead.",
        vm
      );
    } else if (!isReserved(key)) {
      proxy(vm, "_data", key);
    }
  }
  // observe data
  observe(data, true /* asRootData */);
}

function getData (data, vm) {
  // #7573 disable dep collection when invoking data getters
  pushTarget();
  try {
    return data.call(vm, vm)
  } catch (e) {
    handleError(e, vm, "data()");
    return {}
  } finally {
    popTarget();
  }
}

var computedWatcherOptions = { lazy: true };

function initComputed (vm, computed) {
  // $flow-disable-line
  var watchers = vm._computedWatchers = Object.create(null);
  // computed properties are just getters during SSR
  var isSSR = isServerRendering();

  for (var key in computed) {
    var userDef = computed[key];
    var getter = typeof userDef === 'function' ? userDef : userDef.get;
    if ( true && getter == null) {
      warn(
        ("Getter is missing for computed property \"" + key + "\"."),
        vm
      );
    }

    if (!isSSR) {
      // create internal watcher for the computed property.
      watchers[key] = new Watcher(
        vm,
        getter || noop,
        noop,
        computedWatcherOptions
      );
    }

    // component-defined computed properties are already defined on the
    // component prototype. We only need to define computed properties defined
    // at instantiation here.
    if (!(key in vm)) {
      defineComputed(vm, key, userDef);
    } else if (true) {
      if (key in vm.$data) {
        warn(("The computed property \"" + key + "\" is already defined in data."), vm);
      } else if (vm.$options.props && key in vm.$options.props) {
        warn(("The computed property \"" + key + "\" is already defined as a prop."), vm);
      }
    }
  }
}

function defineComputed (
  target,
  key,
  userDef
) {
  var shouldCache = !isServerRendering();
  if (typeof userDef === 'function') {
    sharedPropertyDefinition.get = shouldCache
      ? createComputedGetter(key)
      : createGetterInvoker(userDef);
    sharedPropertyDefinition.set = noop;
  } else {
    sharedPropertyDefinition.get = userDef.get
      ? shouldCache && userDef.cache !== false
        ? createComputedGetter(key)
        : createGetterInvoker(userDef.get)
      : noop;
    sharedPropertyDefinition.set = userDef.set || noop;
  }
  if ( true &&
      sharedPropertyDefinition.set === noop) {
    sharedPropertyDefinition.set = function () {
      warn(
        ("Computed property \"" + key + "\" was assigned to but it has no setter."),
        this
      );
    };
  }
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function createComputedGetter (key) {
  return function computedGetter () {
    var watcher = this._computedWatchers && this._computedWatchers[key];
    if (watcher) {
      if (watcher.dirty) {
        watcher.evaluate();
      }
      if (Dep.SharedObject.target) {// fixed by xxxxxx
        watcher.depend();
      }
      return watcher.value
    }
  }
}

function createGetterInvoker(fn) {
  return function computedGetter () {
    return fn.call(this, this)
  }
}

function initMethods (vm, methods) {
  var props = vm.$options.props;
  for (var key in methods) {
    if (true) {
      if (typeof methods[key] !== 'function') {
        warn(
          "Method \"" + key + "\" has type \"" + (typeof methods[key]) + "\" in the component definition. " +
          "Did you reference the function correctly?",
          vm
        );
      }
      if (props && hasOwn(props, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a prop."),
          vm
        );
      }
      if ((key in vm) && isReserved(key)) {
        warn(
          "Method \"" + key + "\" conflicts with an existing Vue instance method. " +
          "Avoid defining component methods that start with _ or $."
        );
      }
    }
    vm[key] = typeof methods[key] !== 'function' ? noop : bind(methods[key], vm);
  }
}

function initWatch (vm, watch) {
  for (var key in watch) {
    var handler = watch[key];
    if (Array.isArray(handler)) {
      for (var i = 0; i < handler.length; i++) {
        createWatcher(vm, key, handler[i]);
      }
    } else {
      createWatcher(vm, key, handler);
    }
  }
}

function createWatcher (
  vm,
  expOrFn,
  handler,
  options
) {
  if (isPlainObject(handler)) {
    options = handler;
    handler = handler.handler;
  }
  if (typeof handler === 'string') {
    handler = vm[handler];
  }
  return vm.$watch(expOrFn, handler, options)
}

function stateMixin (Vue) {
  // flow somehow has problems with directly declared definition object
  // when using Object.defineProperty, so we have to procedurally build up
  // the object here.
  var dataDef = {};
  dataDef.get = function () { return this._data };
  var propsDef = {};
  propsDef.get = function () { return this._props };
  if (true) {
    dataDef.set = function () {
      warn(
        'Avoid replacing instance root $data. ' +
        'Use nested data properties instead.',
        this
      );
    };
    propsDef.set = function () {
      warn("$props is readonly.", this);
    };
  }
  Object.defineProperty(Vue.prototype, '$data', dataDef);
  Object.defineProperty(Vue.prototype, '$props', propsDef);

  Vue.prototype.$set = set;
  Vue.prototype.$delete = del;

  Vue.prototype.$watch = function (
    expOrFn,
    cb,
    options
  ) {
    var vm = this;
    if (isPlainObject(cb)) {
      return createWatcher(vm, expOrFn, cb, options)
    }
    options = options || {};
    options.user = true;
    var watcher = new Watcher(vm, expOrFn, cb, options);
    if (options.immediate) {
      try {
        cb.call(vm, watcher.value);
      } catch (error) {
        handleError(error, vm, ("callback for immediate watcher \"" + (watcher.expression) + "\""));
      }
    }
    return function unwatchFn () {
      watcher.teardown();
    }
  };
}

/*  */

var uid$3 = 0;

function initMixin (Vue) {
  Vue.prototype._init = function (options) {
    var vm = this;
    // a uid
    vm._uid = uid$3++;

    var startTag, endTag;
    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      startTag = "vue-perf-start:" + (vm._uid);
      endTag = "vue-perf-end:" + (vm._uid);
      mark(startTag);
    }

    // a flag to avoid this being observed
    vm._isVue = true;
    // merge options
    if (options && options._isComponent) {
      // optimize internal component instantiation
      // since dynamic options merging is pretty slow, and none of the
      // internal component options needs special treatment.
      initInternalComponent(vm, options);
    } else {
      vm.$options = mergeOptions(
        resolveConstructorOptions(vm.constructor),
        options || {},
        vm
      );
    }
    /* istanbul ignore else */
    if (true) {
      initProxy(vm);
    } else {}
    // expose real self
    vm._self = vm;
    initLifecycle(vm);
    initEvents(vm);
    initRender(vm);
    callHook(vm, 'beforeCreate');
    !vm._$fallback && initInjections(vm); // resolve injections before data/props  
    initState(vm);
    !vm._$fallback && initProvide(vm); // resolve provide after data/props
    !vm._$fallback && callHook(vm, 'created');      

    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      vm._name = formatComponentName(vm, false);
      mark(endTag);
      measure(("vue " + (vm._name) + " init"), startTag, endTag);
    }

    if (vm.$options.el) {
      vm.$mount(vm.$options.el);
    }
  };
}

function initInternalComponent (vm, options) {
  var opts = vm.$options = Object.create(vm.constructor.options);
  // doing this because it's faster than dynamic enumeration.
  var parentVnode = options._parentVnode;
  opts.parent = options.parent;
  opts._parentVnode = parentVnode;

  var vnodeComponentOptions = parentVnode.componentOptions;
  opts.propsData = vnodeComponentOptions.propsData;
  opts._parentListeners = vnodeComponentOptions.listeners;
  opts._renderChildren = vnodeComponentOptions.children;
  opts._componentTag = vnodeComponentOptions.tag;

  if (options.render) {
    opts.render = options.render;
    opts.staticRenderFns = options.staticRenderFns;
  }
}

function resolveConstructorOptions (Ctor) {
  var options = Ctor.options;
  if (Ctor.super) {
    var superOptions = resolveConstructorOptions(Ctor.super);
    var cachedSuperOptions = Ctor.superOptions;
    if (superOptions !== cachedSuperOptions) {
      // super option changed,
      // need to resolve new options.
      Ctor.superOptions = superOptions;
      // check if there are any late-modified/attached options (#4976)
      var modifiedOptions = resolveModifiedOptions(Ctor);
      // update base extend options
      if (modifiedOptions) {
        extend(Ctor.extendOptions, modifiedOptions);
      }
      options = Ctor.options = mergeOptions(superOptions, Ctor.extendOptions);
      if (options.name) {
        options.components[options.name] = Ctor;
      }
    }
  }
  return options
}

function resolveModifiedOptions (Ctor) {
  var modified;
  var latest = Ctor.options;
  var sealed = Ctor.sealedOptions;
  for (var key in latest) {
    if (latest[key] !== sealed[key]) {
      if (!modified) { modified = {}; }
      modified[key] = latest[key];
    }
  }
  return modified
}

function Vue (options) {
  if ( true &&
    !(this instanceof Vue)
  ) {
    warn('Vue is a constructor and should be called with the `new` keyword');
  }
  this._init(options);
}

initMixin(Vue);
stateMixin(Vue);
eventsMixin(Vue);
lifecycleMixin(Vue);
renderMixin(Vue);

/*  */

function initUse (Vue) {
  Vue.use = function (plugin) {
    var installedPlugins = (this._installedPlugins || (this._installedPlugins = []));
    if (installedPlugins.indexOf(plugin) > -1) {
      return this
    }

    // additional parameters
    var args = toArray(arguments, 1);
    args.unshift(this);
    if (typeof plugin.install === 'function') {
      plugin.install.apply(plugin, args);
    } else if (typeof plugin === 'function') {
      plugin.apply(null, args);
    }
    installedPlugins.push(plugin);
    return this
  };
}

/*  */

function initMixin$1 (Vue) {
  Vue.mixin = function (mixin) {
    this.options = mergeOptions(this.options, mixin);
    return this
  };
}

/*  */

function initExtend (Vue) {
  /**
   * Each instance constructor, including Vue, has a unique
   * cid. This enables us to create wrapped "child
   * constructors" for prototypal inheritance and cache them.
   */
  Vue.cid = 0;
  var cid = 1;

  /**
   * Class inheritance
   */
  Vue.extend = function (extendOptions) {
    extendOptions = extendOptions || {};
    var Super = this;
    var SuperId = Super.cid;
    var cachedCtors = extendOptions._Ctor || (extendOptions._Ctor = {});
    if (cachedCtors[SuperId]) {
      return cachedCtors[SuperId]
    }

    var name = extendOptions.name || Super.options.name;
    if ( true && name) {
      validateComponentName(name);
    }

    var Sub = function VueComponent (options) {
      this._init(options);
    };
    Sub.prototype = Object.create(Super.prototype);
    Sub.prototype.constructor = Sub;
    Sub.cid = cid++;
    Sub.options = mergeOptions(
      Super.options,
      extendOptions
    );
    Sub['super'] = Super;

    // For props and computed properties, we define the proxy getters on
    // the Vue instances at extension time, on the extended prototype. This
    // avoids Object.defineProperty calls for each instance created.
    if (Sub.options.props) {
      initProps$1(Sub);
    }
    if (Sub.options.computed) {
      initComputed$1(Sub);
    }

    // allow further extension/mixin/plugin usage
    Sub.extend = Super.extend;
    Sub.mixin = Super.mixin;
    Sub.use = Super.use;

    // create asset registers, so extended classes
    // can have their private assets too.
    ASSET_TYPES.forEach(function (type) {
      Sub[type] = Super[type];
    });
    // enable recursive self-lookup
    if (name) {
      Sub.options.components[name] = Sub;
    }

    // keep a reference to the super options at extension time.
    // later at instantiation we can check if Super's options have
    // been updated.
    Sub.superOptions = Super.options;
    Sub.extendOptions = extendOptions;
    Sub.sealedOptions = extend({}, Sub.options);

    // cache constructor
    cachedCtors[SuperId] = Sub;
    return Sub
  };
}

function initProps$1 (Comp) {
  var props = Comp.options.props;
  for (var key in props) {
    proxy(Comp.prototype, "_props", key);
  }
}

function initComputed$1 (Comp) {
  var computed = Comp.options.computed;
  for (var key in computed) {
    defineComputed(Comp.prototype, key, computed[key]);
  }
}

/*  */

function initAssetRegisters (Vue) {
  /**
   * Create asset registration methods.
   */
  ASSET_TYPES.forEach(function (type) {
    Vue[type] = function (
      id,
      definition
    ) {
      if (!definition) {
        return this.options[type + 's'][id]
      } else {
        /* istanbul ignore if */
        if ( true && type === 'component') {
          validateComponentName(id);
        }
        if (type === 'component' && isPlainObject(definition)) {
          definition.name = definition.name || id;
          definition = this.options._base.extend(definition);
        }
        if (type === 'directive' && typeof definition === 'function') {
          definition = { bind: definition, update: definition };
        }
        this.options[type + 's'][id] = definition;
        return definition
      }
    };
  });
}

/*  */



function getComponentName (opts) {
  return opts && (opts.Ctor.options.name || opts.tag)
}

function matches (pattern, name) {
  if (Array.isArray(pattern)) {
    return pattern.indexOf(name) > -1
  } else if (typeof pattern === 'string') {
    return pattern.split(',').indexOf(name) > -1
  } else if (isRegExp(pattern)) {
    return pattern.test(name)
  }
  /* istanbul ignore next */
  return false
}

function pruneCache (keepAliveInstance, filter) {
  var cache = keepAliveInstance.cache;
  var keys = keepAliveInstance.keys;
  var _vnode = keepAliveInstance._vnode;
  for (var key in cache) {
    var cachedNode = cache[key];
    if (cachedNode) {
      var name = getComponentName(cachedNode.componentOptions);
      if (name && !filter(name)) {
        pruneCacheEntry(cache, key, keys, _vnode);
      }
    }
  }
}

function pruneCacheEntry (
  cache,
  key,
  keys,
  current
) {
  var cached$$1 = cache[key];
  if (cached$$1 && (!current || cached$$1.tag !== current.tag)) {
    cached$$1.componentInstance.$destroy();
  }
  cache[key] = null;
  remove(keys, key);
}

var patternTypes = [String, RegExp, Array];

var KeepAlive = {
  name: 'keep-alive',
  abstract: true,

  props: {
    include: patternTypes,
    exclude: patternTypes,
    max: [String, Number]
  },

  created: function created () {
    this.cache = Object.create(null);
    this.keys = [];
  },

  destroyed: function destroyed () {
    for (var key in this.cache) {
      pruneCacheEntry(this.cache, key, this.keys);
    }
  },

  mounted: function mounted () {
    var this$1 = this;

    this.$watch('include', function (val) {
      pruneCache(this$1, function (name) { return matches(val, name); });
    });
    this.$watch('exclude', function (val) {
      pruneCache(this$1, function (name) { return !matches(val, name); });
    });
  },

  render: function render () {
    var slot = this.$slots.default;
    var vnode = getFirstComponentChild(slot);
    var componentOptions = vnode && vnode.componentOptions;
    if (componentOptions) {
      // check pattern
      var name = getComponentName(componentOptions);
      var ref = this;
      var include = ref.include;
      var exclude = ref.exclude;
      if (
        // not included
        (include && (!name || !matches(include, name))) ||
        // excluded
        (exclude && name && matches(exclude, name))
      ) {
        return vnode
      }

      var ref$1 = this;
      var cache = ref$1.cache;
      var keys = ref$1.keys;
      var key = vnode.key == null
        // same constructor may get registered as different local components
        // so cid alone is not enough (#3269)
        ? componentOptions.Ctor.cid + (componentOptions.tag ? ("::" + (componentOptions.tag)) : '')
        : vnode.key;
      if (cache[key]) {
        vnode.componentInstance = cache[key].componentInstance;
        // make current key freshest
        remove(keys, key);
        keys.push(key);
      } else {
        cache[key] = vnode;
        keys.push(key);
        // prune oldest entry
        if (this.max && keys.length > parseInt(this.max)) {
          pruneCacheEntry(cache, keys[0], keys, this._vnode);
        }
      }

      vnode.data.keepAlive = true;
    }
    return vnode || (slot && slot[0])
  }
};

var builtInComponents = {
  KeepAlive: KeepAlive
};

/*  */

function initGlobalAPI (Vue) {
  // config
  var configDef = {};
  configDef.get = function () { return config; };
  if (true) {
    configDef.set = function () {
      warn(
        'Do not replace the Vue.config object, set individual fields instead.'
      );
    };
  }
  Object.defineProperty(Vue, 'config', configDef);

  // exposed util methods.
  // NOTE: these are not considered part of the public API - avoid relying on
  // them unless you are aware of the risk.
  Vue.util = {
    warn: warn,
    extend: extend,
    mergeOptions: mergeOptions,
    defineReactive: defineReactive$$1
  };

  Vue.set = set;
  Vue.delete = del;
  Vue.nextTick = nextTick;

  // 2.6 explicit observable API
  Vue.observable = function (obj) {
    observe(obj);
    return obj
  };

  Vue.options = Object.create(null);
  ASSET_TYPES.forEach(function (type) {
    Vue.options[type + 's'] = Object.create(null);
  });

  // this is used to identify the "base" constructor to extend all plain-object
  // components with in Weex's multi-instance scenarios.
  Vue.options._base = Vue;

  extend(Vue.options.components, builtInComponents);

  initUse(Vue);
  initMixin$1(Vue);
  initExtend(Vue);
  initAssetRegisters(Vue);
}

initGlobalAPI(Vue);

Object.defineProperty(Vue.prototype, '$isServer', {
  get: isServerRendering
});

Object.defineProperty(Vue.prototype, '$ssrContext', {
  get: function get () {
    /* istanbul ignore next */
    return this.$vnode && this.$vnode.ssrContext
  }
});

// expose FunctionalRenderContext for ssr runtime helper installation
Object.defineProperty(Vue, 'FunctionalRenderContext', {
  value: FunctionalRenderContext
});

Vue.version = '2.6.11';

/**
 * https://raw.githubusercontent.com/Tencent/westore/master/packages/westore/utils/diff.js
 */
var ARRAYTYPE = '[object Array]';
var OBJECTTYPE = '[object Object]';
// const FUNCTIONTYPE = '[object Function]'

function diff(current, pre) {
    var result = {};
    syncKeys(current, pre);
    _diff(current, pre, '', result);
    return result
}

function syncKeys(current, pre) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE && rootPreType == OBJECTTYPE) {
        if(Object.keys(current).length >= Object.keys(pre).length){
            for (var key in pre) {
                var currentValue = current[key];
                if (currentValue === undefined) {
                    current[key] = null;
                } else {
                    syncKeys(currentValue, pre[key]);
                }
            }
        }
    } else if (rootCurrentType == ARRAYTYPE && rootPreType == ARRAYTYPE) {
        if (current.length >= pre.length) {
            pre.forEach(function (item, index) {
                syncKeys(current[index], item);
            });
        }
    }
}

function _diff(current, pre, path, result) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE) {
        if (rootPreType != OBJECTTYPE || Object.keys(current).length < Object.keys(pre).length) {
            setResult(result, path, current);
        } else {
            var loop = function ( key ) {
                var currentValue = current[key];
                var preValue = pre[key];
                var currentType = type(currentValue);
                var preType = type(preValue);
                if (currentType != ARRAYTYPE && currentType != OBJECTTYPE) {
                    if (currentValue != pre[key]) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    }
                } else if (currentType == ARRAYTYPE) {
                    if (preType != ARRAYTYPE) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        if (currentValue.length < preValue.length) {
                            setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                        } else {
                            currentValue.forEach(function (item, index) {
                                _diff(item, preValue[index], (path == '' ? '' : path + ".") + key + '[' + index + ']', result);
                            });
                        }
                    }
                } else if (currentType == OBJECTTYPE) {
                    if (preType != OBJECTTYPE || Object.keys(currentValue).length < Object.keys(preValue).length) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        for (var subKey in currentValue) {
                            _diff(currentValue[subKey], preValue[subKey], (path == '' ? '' : path + ".") + key + '.' + subKey, result);
                        }
                    }
                }
            };

            for (var key in current) loop( key );
        }
    } else if (rootCurrentType == ARRAYTYPE) {
        if (rootPreType != ARRAYTYPE) {
            setResult(result, path, current);
        } else {
            if (current.length < pre.length) {
                setResult(result, path, current);
            } else {
                current.forEach(function (item, index) {
                    _diff(item, pre[index], path + '[' + index + ']', result);
                });
            }
        }
    } else {
        setResult(result, path, current);
    }
}

function setResult(result, k, v) {
    // if (type(v) != FUNCTIONTYPE) {
        result[k] = v;
    // }
}

function type(obj) {
    return Object.prototype.toString.call(obj)
}

/*  */

function flushCallbacks$1(vm) {
    if (vm.__next_tick_callbacks && vm.__next_tick_callbacks.length) {
        if (Object({"NODE_ENV":"development","VUE_APP_NAME":"分享保","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG) {
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:flushCallbacks[' + vm.__next_tick_callbacks.length + ']');
        }
        var copies = vm.__next_tick_callbacks.slice(0);
        vm.__next_tick_callbacks.length = 0;
        for (var i = 0; i < copies.length; i++) {
            copies[i]();
        }
    }
}

function hasRenderWatcher(vm) {
    return queue.find(function (watcher) { return vm._watcher === watcher; })
}

function nextTick$1(vm, cb) {
    //1.nextTick 之前 已 setData 且 setData 还未回调完成
    //2.nextTick 之前存在 render watcher
    if (!vm.__next_tick_pending && !hasRenderWatcher(vm)) {
        if(Object({"NODE_ENV":"development","VUE_APP_NAME":"分享保","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:nextVueTick');
        }
        return nextTick(cb, vm)
    }else{
        if(Object({"NODE_ENV":"development","VUE_APP_NAME":"分享保","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance$1 = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance$1.is || mpInstance$1.route) + '][' + vm._uid +
                ']:nextMPTick');
        }
    }
    var _resolve;
    if (!vm.__next_tick_callbacks) {
        vm.__next_tick_callbacks = [];
    }
    vm.__next_tick_callbacks.push(function () {
        if (cb) {
            try {
                cb.call(vm);
            } catch (e) {
                handleError(e, vm, 'nextTick');
            }
        } else if (_resolve) {
            _resolve(vm);
        }
    });
    // $flow-disable-line
    if (!cb && typeof Promise !== 'undefined') {
        return new Promise(function (resolve) {
            _resolve = resolve;
        })
    }
}

/*  */

function cloneWithData(vm) {
  // 确保当前 vm 所有数据被同步
  var ret = Object.create(null);
  var dataKeys = [].concat(
    Object.keys(vm._data || {}),
    Object.keys(vm._computedWatchers || {}));

  dataKeys.reduce(function(ret, key) {
    ret[key] = vm[key];
    return ret
  }, ret);

  // vue-composition-api
  var compositionApiState = vm.__composition_api_state__ || vm.__secret_vfa_state__;
  var rawBindings = compositionApiState && compositionApiState.rawBindings;
  if (rawBindings) {
    Object.keys(rawBindings).forEach(function (key) {
      ret[key] = vm[key];
    });
  }

  //TODO 需要把无用数据处理掉，比如 list=>l0 则 list 需要移除，否则多传输一份数据
  Object.assign(ret, vm.$mp.data || {});
  if (
    Array.isArray(vm.$options.behaviors) &&
    vm.$options.behaviors.indexOf('uni://form-field') !== -1
  ) { //form-field
    ret['name'] = vm.name;
    ret['value'] = vm.value;
  }

  return JSON.parse(JSON.stringify(ret))
}

var patch = function(oldVnode, vnode) {
  var this$1 = this;

  if (vnode === null) { //destroy
    return
  }
  if (this.mpType === 'page' || this.mpType === 'component') {
    var mpInstance = this.$scope;
    var data = Object.create(null);
    try {
      data = cloneWithData(this);
    } catch (err) {
      console.error(err);
    }
    data.__webviewId__ = mpInstance.data.__webviewId__;
    var mpData = Object.create(null);
    Object.keys(data).forEach(function (key) { //仅同步 data 中有的数据
      mpData[key] = mpInstance.data[key];
    });
    var diffData = this.$shouldDiffData === false ? data : diff(data, mpData);
    if (Object.keys(diffData).length) {
      if (Object({"NODE_ENV":"development","VUE_APP_NAME":"分享保","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + this._uid +
          ']差量更新',
          JSON.stringify(diffData));
      }
      this.__next_tick_pending = true;
      mpInstance.setData(diffData, function () {
        this$1.__next_tick_pending = false;
        flushCallbacks$1(this$1);
      });
    } else {
      flushCallbacks$1(this);
    }
  }
};

/*  */

function createEmptyRender() {

}

function mountComponent$1(
  vm,
  el,
  hydrating
) {
  if (!vm.mpType) {//main.js 中的 new Vue
    return vm
  }
  if (vm.mpType === 'app') {
    vm.$options.render = createEmptyRender;
  }
  if (!vm.$options.render) {
    vm.$options.render = createEmptyRender;
    if (true) {
      /* istanbul ignore if */
      if ((vm.$options.template && vm.$options.template.charAt(0) !== '#') ||
        vm.$options.el || el) {
        warn(
          'You are using the runtime-only build of Vue where the template ' +
          'compiler is not available. Either pre-compile the templates into ' +
          'render functions, or use the compiler-included build.',
          vm
        );
      } else {
        warn(
          'Failed to mount component: template or render function not defined.',
          vm
        );
      }
    }
  }
  
  !vm._$fallback && callHook(vm, 'beforeMount');

  var updateComponent = function () {
    vm._update(vm._render(), hydrating);
  };

  // we set this to vm._watcher inside the watcher's constructor
  // since the watcher's initial patch may call $forceUpdate (e.g. inside child
  // component's mounted hook), which relies on vm._watcher being already defined
  new Watcher(vm, updateComponent, noop, {
    before: function before() {
      if (vm._isMounted && !vm._isDestroyed) {
        callHook(vm, 'beforeUpdate');
      }
    }
  }, true /* isRenderWatcher */);
  hydrating = false;
  return vm
}

/*  */

function renderClass (
  staticClass,
  dynamicClass
) {
  if (isDef(staticClass) || isDef(dynamicClass)) {
    return concat(staticClass, stringifyClass(dynamicClass))
  }
  /* istanbul ignore next */
  return ''
}

function concat (a, b) {
  return a ? b ? (a + ' ' + b) : a : (b || '')
}

function stringifyClass (value) {
  if (Array.isArray(value)) {
    return stringifyArray(value)
  }
  if (isObject(value)) {
    return stringifyObject(value)
  }
  if (typeof value === 'string') {
    return value
  }
  /* istanbul ignore next */
  return ''
}

function stringifyArray (value) {
  var res = '';
  var stringified;
  for (var i = 0, l = value.length; i < l; i++) {
    if (isDef(stringified = stringifyClass(value[i])) && stringified !== '') {
      if (res) { res += ' '; }
      res += stringified;
    }
  }
  return res
}

function stringifyObject (value) {
  var res = '';
  for (var key in value) {
    if (value[key]) {
      if (res) { res += ' '; }
      res += key;
    }
  }
  return res
}

/*  */

var parseStyleText = cached(function (cssText) {
  var res = {};
  var listDelimiter = /;(?![^(]*\))/g;
  var propertyDelimiter = /:(.+)/;
  cssText.split(listDelimiter).forEach(function (item) {
    if (item) {
      var tmp = item.split(propertyDelimiter);
      tmp.length > 1 && (res[tmp[0].trim()] = tmp[1].trim());
    }
  });
  return res
});

// normalize possible array / string values into Object
function normalizeStyleBinding (bindingStyle) {
  if (Array.isArray(bindingStyle)) {
    return toObject(bindingStyle)
  }
  if (typeof bindingStyle === 'string') {
    return parseStyleText(bindingStyle)
  }
  return bindingStyle
}

/*  */

var MP_METHODS = ['createSelectorQuery', 'createIntersectionObserver', 'selectAllComponents', 'selectComponent'];

function getTarget(obj, path) {
  var parts = path.split('.');
  var key = parts[0];
  if (key.indexOf('__$n') === 0) { //number index
    key = parseInt(key.replace('__$n', ''));
  }
  if (parts.length === 1) {
    return obj[key]
  }
  return getTarget(obj[key], parts.slice(1).join('.'))
}

function internalMixin(Vue) {

  Vue.config.errorHandler = function(err, vm, info) {
    Vue.util.warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
    console.error(err);
    /* eslint-disable no-undef */
    var app = getApp();
    if (app && app.onError) {
      app.onError(err);
    }
  };

  var oldEmit = Vue.prototype.$emit;

  Vue.prototype.$emit = function(event) {
    if (this.$scope && event) {
      this.$scope['triggerEvent'](event, {
        __args__: toArray(arguments, 1)
      });
    }
    return oldEmit.apply(this, arguments)
  };

  Vue.prototype.$nextTick = function(fn) {
    return nextTick$1(this, fn)
  };

  MP_METHODS.forEach(function (method) {
    Vue.prototype[method] = function(args) {
      if (this.$scope && this.$scope[method]) {
        return this.$scope[method](args)
      }
      // mp-alipay
      if (typeof my === 'undefined') {
        return
      }
      if (method === 'createSelectorQuery') {
        /* eslint-disable no-undef */
        return my.createSelectorQuery(args)
      } else if (method === 'createIntersectionObserver') {
        /* eslint-disable no-undef */
        return my.createIntersectionObserver(args)
      }
      // TODO mp-alipay 暂不支持 selectAllComponents,selectComponent
    };
  });

  Vue.prototype.__init_provide = initProvide;

  Vue.prototype.__init_injections = initInjections;

  Vue.prototype.__call_hook = function(hook, args) {
    var vm = this;
    // #7573 disable dep collection when invoking lifecycle hooks
    pushTarget();
    var handlers = vm.$options[hook];
    var info = hook + " hook";
    var ret;
    if (handlers) {
      for (var i = 0, j = handlers.length; i < j; i++) {
        ret = invokeWithErrorHandling(handlers[i], vm, args ? [args] : null, vm, info);
      }
    }
    if (vm._hasHookEvent) {
      vm.$emit('hook:' + hook, args);
    }
    popTarget();
    return ret
  };

  Vue.prototype.__set_model = function(target, key, value, modifiers) {
    if (Array.isArray(modifiers)) {
      if (modifiers.indexOf('trim') !== -1) {
        value = value.trim();
      }
      if (modifiers.indexOf('number') !== -1) {
        value = this._n(value);
      }
    }
    if (!target) {
      target = this;
    }
    target[key] = value;
  };

  Vue.prototype.__set_sync = function(target, key, value) {
    if (!target) {
      target = this;
    }
    target[key] = value;
  };

  Vue.prototype.__get_orig = function(item) {
    if (isPlainObject(item)) {
      return item['$orig'] || item
    }
    return item
  };

  Vue.prototype.__get_value = function(dataPath, target) {
    return getTarget(target || this, dataPath)
  };


  Vue.prototype.__get_class = function(dynamicClass, staticClass) {
    return renderClass(staticClass, dynamicClass)
  };

  Vue.prototype.__get_style = function(dynamicStyle, staticStyle) {
    if (!dynamicStyle && !staticStyle) {
      return ''
    }
    var dynamicStyleObj = normalizeStyleBinding(dynamicStyle);
    var styleObj = staticStyle ? extend(staticStyle, dynamicStyleObj) : dynamicStyleObj;
    return Object.keys(styleObj).map(function (name) { return ((hyphenate(name)) + ":" + (styleObj[name])); }).join(';')
  };

  Vue.prototype.__map = function(val, iteratee) {
    //TODO 暂不考虑 string
    var ret, i, l, keys, key;
    if (Array.isArray(val)) {
      ret = new Array(val.length);
      for (i = 0, l = val.length; i < l; i++) {
        ret[i] = iteratee(val[i], i);
      }
      return ret
    } else if (isObject(val)) {
      keys = Object.keys(val);
      ret = Object.create(null);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[key] = iteratee(val[key], key, i);
      }
      return ret
    } else if (typeof val === 'number') {
      ret = new Array(val);
      for (i = 0, l = val; i < l; i++) {
        // 第一个参数暂时仍和小程序一致
        ret[i] = iteratee(i, i);
      }
      return ret
    }
    return []
  };

}

/*  */

var LIFECYCLE_HOOKS$1 = [
    //App
    'onLaunch',
    'onShow',
    'onHide',
    'onUniNViewMessage',
    'onPageNotFound',
    'onThemeChange',
    'onError',
    'onUnhandledRejection',
    //Page
    'onInit',
    'onLoad',
    // 'onShow',
    'onReady',
    // 'onHide',
    'onUnload',
    'onPullDownRefresh',
    'onReachBottom',
    'onTabItemTap',
    'onAddToFavorites',
    'onShareTimeline',
    'onShareAppMessage',
    'onResize',
    'onPageScroll',
    'onNavigationBarButtonTap',
    'onBackPress',
    'onNavigationBarSearchInputChanged',
    'onNavigationBarSearchInputConfirmed',
    'onNavigationBarSearchInputClicked',
    //Component
    // 'onReady', // 兼容旧版本，应该移除该事件
    'onPageShow',
    'onPageHide',
    'onPageResize'
];
function lifecycleMixin$1(Vue) {

    //fixed vue-class-component
    var oldExtend = Vue.extend;
    Vue.extend = function(extendOptions) {
        extendOptions = extendOptions || {};

        var methods = extendOptions.methods;
        if (methods) {
            Object.keys(methods).forEach(function (methodName) {
                if (LIFECYCLE_HOOKS$1.indexOf(methodName)!==-1) {
                    extendOptions[methodName] = methods[methodName];
                    delete methods[methodName];
                }
            });
        }

        return oldExtend.call(this, extendOptions)
    };

    var strategies = Vue.config.optionMergeStrategies;
    var mergeHook = strategies.created;
    LIFECYCLE_HOOKS$1.forEach(function (hook) {
        strategies[hook] = mergeHook;
    });

    Vue.prototype.__lifecycle_hooks__ = LIFECYCLE_HOOKS$1;
}

/*  */

// install platform patch function
Vue.prototype.__patch__ = patch;

// public mount method
Vue.prototype.$mount = function(
    el ,
    hydrating 
) {
    return mountComponent$1(this, el, hydrating)
};

lifecycleMixin$1(Vue);
internalMixin(Vue);

/*  */

/* harmony default export */ __webpack_exports__["default"] = (Vue);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../webpack/buildin/global.js */ 3)))

/***/ }),

/***/ 216:
/*!*****************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/wxcomponents/wxParse/wxParse.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";














var _showdown = _interopRequireDefault(__webpack_require__(/*! ./showdown.js */ 217));
var _html2json = _interopRequireDefault(__webpack_require__(/*! ./html2json.js */ 218));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };} /**
                                                                                                                                                                  * author: Di (微信小程序开发工程师)
                                                                                                                                                                  * organization: WeAppDev(微信小程序开发论坛)(http://weappdev.com)
                                                                                                                                                                  *               垂直微信小程序开发交流社区
                                                                                                                                                                  * 
                                                                                                                                                                  * github地址: https://github.com/icindy/wxParse
                                                                                                                                                                  * 
                                                                                                                                                                  * for: 微信小程序富文本解析
                                                                                                                                                                  * detail : http://weappdev.com/t/wxparse-alpha0-1-html-markdown/184
                                                                                                                                                                  */ /**
                                                                                                                                                                      * utils函数引入
                                                                                                                                                                      **/ /**
                                                                                                                                                                           * 配置及公有属性
                                                                                                                                                                           **/ /**
                                                                                                                                                                                * 主函数入口区
                                                                                                                                                                                **/function wxParse() {var bindName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'wxParseData';var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'html';var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '<div class="color:red;">数据不能为空</div>';var target = arguments.length > 3 ? arguments[3] : undefined;var imagePadding = arguments.length > 4 ? arguments[4] : undefined;var that = target;var transData = {}; //存放转化后的数据
  if (type == 'html') {transData = _html2json.default.html2json(data, bindName); // console.log(JSON.stringify(transData, ' ', ' '));
  } else if (type == 'md' || type == 'markdown') {var converter = new _showdown.default.Converter();var html = converter.makeHtml(data);transData = _html2json.default.html2json(html, bindName); //console.log(JSON.stringify(transData, ' ', ' '));
  }
  transData.view = {};
  transData.view.imagePadding = 0;
  if (typeof imagePadding != 'undefined') {
    transData.view.imagePadding = imagePadding;
  }
  var bindData = {};
  bindData[bindName] = transData;
  that.setData(bindData);
  that.wxParseImgLoad = wxParseImgLoad;
  that.wxParseImgTap = wxParseImgTap;
}
// 图片点击事件
function wxParseImgTap(e) {
  var that = this;
  var nowImgUrl = e.target.dataset.src;
  var tagFrom = e.target.dataset.from;
  if (typeof tagFrom != 'undefined' && tagFrom.length > 0) {
    wx.previewImage({
      current: nowImgUrl, // 当前显示图片的http链接
      urls: that.data[tagFrom].imageUrls // 需要预览的图片http链接列表
    });
  }
}

/**
   * 图片视觉宽高计算函数区 
   **/
function wxParseImgLoad(e) {
  var that = this;
  var tagFrom = e.target.dataset.from;
  var idx = e.target.dataset.idx;
  if (typeof tagFrom != 'undefined' && tagFrom.length > 0) {
    calMoreImageInfo(e, idx, that, tagFrom);
  }
}
// 假循环获取计算图片视觉最佳宽高
function calMoreImageInfo(e, idx, that, bindName) {
  var temData = that.data[bindName];
  if (temData.images.length == 0) {
    return;
  }
  var temImages = temData.images;
  //因为无法获取view宽度 需要自定义padding进行计算，稍后处理
  var recal = wxAutoImageCal(e.detail.width, e.detail.height, that, bindName);
  temImages[idx].width = recal.imageWidth;
  temImages[idx].height = recal.imageheight;
  temData.images = temImages;
  var bindData = {};
  bindData[bindName] = temData;
  that.setData(bindData);
}

// 计算视觉优先的图片宽高
function wxAutoImageCal(originalWidth, originalHeight, that, bindName) {
  //获取图片的原始长宽
  var windowWidth = 0,windowHeight = 0;
  var autoWidth = 0,autoHeight = 0;
  var results = {};
  wx.getSystemInfo({
    success: function success(res) {
      var padding = that.data[bindName].view.imagePadding;
      windowWidth = res.windowWidth - 2 * padding;
      windowHeight = res.windowHeight;
      //判断按照那种方式进行缩放
      //console.log("windowWidth" + windowWidth);
      if (originalWidth > windowWidth) {//在图片width大于手机屏幕width时候
        autoWidth = windowWidth;
        //console.log("autoWidth" + autoWidth);
        autoHeight = autoWidth * originalHeight / originalWidth;
        //console.log("autoHeight" + autoHeight);
        results.imageWidth = autoWidth;
        results.imageheight = autoHeight;
      } else {//否则展示原来的数据
        results.imageWidth = originalWidth;
        results.imageheight = originalHeight;
      }
    } });

  return results;
}

function wxParseTemArray(temArrayName, bindNameReg, total, that) {
  var array = [];
  var temData = that.data;
  var obj = null;
  for (var i = 0; i < total; i++) {
    var simArr = temData[bindNameReg + i].nodes;
    array.push(simArr);
  }

  temArrayName = temArrayName || 'wxParseTemArray';
  obj = JSON.parse('{"' + temArrayName + '":""}');
  obj[temArrayName] = array;
  that.setData(obj);
}

/**
   * 配置emojis
   * 
   */

function emojisInit() {var reg = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';var baseSrc = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "/wxParse/emojis/";var emojis = arguments.length > 2 ? arguments[2] : undefined;
  _html2json.default.emojisInit(reg, baseSrc, emojis);
}

module.exports = {
  wxParse: wxParse,
  wxParseTemArray: wxParseTemArray,
  emojisInit: emojisInit };

/***/ }),

/***/ 217:
/*!******************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/wxcomponents/wxParse/showdown.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * 
 * showdown: https://github.com/showdownjs/showdown
 * 
 * author: Di (微信小程序开发工程师)
 * organization: WeAppDev(微信小程序开发论坛)(http://weappdev.com)
 *               垂直微信小程序开发交流社区
 * 
 * github地址: https://github.com/icindy/wxParse
 * 
 * for: 微信小程序富文本解析
 * detail : http://weappdev.com/t/wxparse-alpha0-1-html-markdown/184
 */

function getDefaultOpts(simple) {
  'use strict';

  var defaultOptions = {
    omitExtraWLInCodeBlocks: {
      defaultValue: false,
      describe: 'Omit the default extra whiteline added to code blocks',
      type: 'boolean' },

    noHeaderId: {
      defaultValue: false,
      describe: 'Turn on/off generated header id',
      type: 'boolean' },

    prefixHeaderId: {
      defaultValue: false,
      describe: 'Specify a prefix to generated header ids',
      type: 'string' },

    headerLevelStart: {
      defaultValue: false,
      describe: 'The header blocks level start',
      type: 'integer' },

    parseImgDimensions: {
      defaultValue: false,
      describe: 'Turn on/off image dimension parsing',
      type: 'boolean' },

    simplifiedAutoLink: {
      defaultValue: false,
      describe: 'Turn on/off GFM autolink style',
      type: 'boolean' },

    literalMidWordUnderscores: {
      defaultValue: false,
      describe: 'Parse midword underscores as literal underscores',
      type: 'boolean' },

    strikethrough: {
      defaultValue: false,
      describe: 'Turn on/off strikethrough support',
      type: 'boolean' },

    tables: {
      defaultValue: false,
      describe: 'Turn on/off tables support',
      type: 'boolean' },

    tablesHeaderId: {
      defaultValue: false,
      describe: 'Add an id to table headers',
      type: 'boolean' },

    ghCodeBlocks: {
      defaultValue: true,
      describe: 'Turn on/off GFM fenced code blocks support',
      type: 'boolean' },

    tasklists: {
      defaultValue: false,
      describe: 'Turn on/off GFM tasklist support',
      type: 'boolean' },

    smoothLivePreview: {
      defaultValue: false,
      describe: 'Prevents weird effects in live previews due to incomplete input',
      type: 'boolean' },

    smartIndentationFix: {
      defaultValue: false,
      description: 'Tries to smartly fix identation in es6 strings',
      type: 'boolean' } };


  if (simple === false) {
    return JSON.parse(JSON.stringify(defaultOptions));
  }
  var ret = {};
  for (var opt in defaultOptions) {
    if (defaultOptions.hasOwnProperty(opt)) {
      ret[opt] = defaultOptions[opt].defaultValue;
    }
  }
  return ret;
}

/**
   * Created by Tivie on 06-01-2015.
   */

// Private properties
var showdown = {},
parsers = {},
extensions = {},
globalOptions = getDefaultOpts(true),
flavor = {
  github: {
    omitExtraWLInCodeBlocks: true,
    prefixHeaderId: 'user-content-',
    simplifiedAutoLink: true,
    literalMidWordUnderscores: true,
    strikethrough: true,
    tables: true,
    tablesHeaderId: true,
    ghCodeBlocks: true,
    tasklists: true },

  vanilla: getDefaultOpts(true) };


/**
                                    * helper namespace
                                    * @type {{}}
                                    */
showdown.helper = {};

/**
                       * TODO LEGACY SUPPORT CODE
                       * @type {{}}
                       */
showdown.extensions = {};

/**
                           * Set a global option
                           * @static
                           * @param {string} key
                           * @param {*} value
                           * @returns {showdown}
                           */
showdown.setOption = function (key, value) {
  'use strict';
  globalOptions[key] = value;
  return this;
};

/**
    * Get a global option
    * @static
    * @param {string} key
    * @returns {*}
    */
showdown.getOption = function (key) {
  'use strict';
  return globalOptions[key];
};

/**
    * Get the global options
    * @static
    * @returns {{}}
    */
showdown.getOptions = function () {
  'use strict';
  return globalOptions;
};

/**
    * Reset global options to the default values
    * @static
    */
showdown.resetOptions = function () {
  'use strict';
  globalOptions = getDefaultOpts(true);
};

/**
    * Set the flavor showdown should use as default
    * @param {string} name
    */
showdown.setFlavor = function (name) {
  'use strict';
  if (flavor.hasOwnProperty(name)) {
    var preset = flavor[name];
    for (var option in preset) {
      if (preset.hasOwnProperty(option)) {
        globalOptions[option] = preset[option];
      }
    }
  }
};

/**
    * Get the default options
    * @static
    * @param {boolean} [simple=true]
    * @returns {{}}
    */
showdown.getDefaultOptions = function (simple) {
  'use strict';
  return getDefaultOpts(simple);
};

/**
    * Get or set a subParser
    *
    * subParser(name)       - Get a registered subParser
    * subParser(name, func) - Register a subParser
    * @static
    * @param {string} name
    * @param {function} [func]
    * @returns {*}
    */
showdown.subParser = function (name, func) {
  'use strict';
  if (showdown.helper.isString(name)) {
    if (typeof func !== 'undefined') {
      parsers[name] = func;
    } else {
      if (parsers.hasOwnProperty(name)) {
        return parsers[name];
      } else {
        throw Error('SubParser named ' + name + ' not registered!');
      }
    }
  }
};

/**
    * Gets or registers an extension
    * @static
    * @param {string} name
    * @param {object|function=} ext
    * @returns {*}
    */
showdown.extension = function (name, ext) {
  'use strict';

  if (!showdown.helper.isString(name)) {
    throw Error('Extension \'name\' must be a string');
  }

  name = showdown.helper.stdExtName(name);

  // Getter
  if (showdown.helper.isUndefined(ext)) {
    if (!extensions.hasOwnProperty(name)) {
      throw Error('Extension named ' + name + ' is not registered!');
    }
    return extensions[name];

    // Setter
  } else {
    // Expand extension if it's wrapped in a function
    if (typeof ext === 'function') {
      ext = ext();
    }

    // Ensure extension is an array
    if (!showdown.helper.isArray(ext)) {
      ext = [ext];
    }

    var validExtension = validate(ext, name);

    if (validExtension.valid) {
      extensions[name] = ext;
    } else {
      throw Error(validExtension.error);
    }
  }
};

/**
    * Gets all extensions registered
    * @returns {{}}
    */
showdown.getAllExtensions = function () {
  'use strict';
  return extensions;
};

/**
    * Remove an extension
    * @param {string} name
    */
showdown.removeExtension = function (name) {
  'use strict';
  delete extensions[name];
};

/**
    * Removes all extensions
    */
showdown.resetExtensions = function () {
  'use strict';
  extensions = {};
};

/**
    * Validate extension
    * @param {array} extension
    * @param {string} name
    * @returns {{valid: boolean, error: string}}
    */
function validate(extension, name) {
  'use strict';

  var errMsg = name ? 'Error in ' + name + ' extension->' : 'Error in unnamed extension',
  ret = {
    valid: true,
    error: '' };


  if (!showdown.helper.isArray(extension)) {
    extension = [extension];
  }

  for (var i = 0; i < extension.length; ++i) {
    var baseMsg = errMsg + ' sub-extension ' + i + ': ',
    ext = extension[i];
    if (typeof ext !== 'object') {
      ret.valid = false;
      ret.error = baseMsg + 'must be an object, but ' + typeof ext + ' given';
      return ret;
    }

    if (!showdown.helper.isString(ext.type)) {
      ret.valid = false;
      ret.error = baseMsg + 'property "type" must be a string, but ' + typeof ext.type + ' given';
      return ret;
    }

    var type = ext.type = ext.type.toLowerCase();

    // normalize extension type
    if (type === 'language') {
      type = ext.type = 'lang';
    }

    if (type === 'html') {
      type = ext.type = 'output';
    }

    if (type !== 'lang' && type !== 'output' && type !== 'listener') {
      ret.valid = false;
      ret.error = baseMsg + 'type ' + type + ' is not recognized. Valid values: "lang/language", "output/html" or "listener"';
      return ret;
    }

    if (type === 'listener') {
      if (showdown.helper.isUndefined(ext.listeners)) {
        ret.valid = false;
        ret.error = baseMsg + '. Extensions of type "listener" must have a property called "listeners"';
        return ret;
      }
    } else {
      if (showdown.helper.isUndefined(ext.filter) && showdown.helper.isUndefined(ext.regex)) {
        ret.valid = false;
        ret.error = baseMsg + type + ' extensions must define either a "regex" property or a "filter" method';
        return ret;
      }
    }

    if (ext.listeners) {
      if (typeof ext.listeners !== 'object') {
        ret.valid = false;
        ret.error = baseMsg + '"listeners" property must be an object but ' + typeof ext.listeners + ' given';
        return ret;
      }
      for (var ln in ext.listeners) {
        if (ext.listeners.hasOwnProperty(ln)) {
          if (typeof ext.listeners[ln] !== 'function') {
            ret.valid = false;
            ret.error = baseMsg + '"listeners" property must be an hash of [event name]: [callback]. listeners.' + ln +
            ' must be a function but ' + typeof ext.listeners[ln] + ' given';
            return ret;
          }
        }
      }
    }

    if (ext.filter) {
      if (typeof ext.filter !== 'function') {
        ret.valid = false;
        ret.error = baseMsg + '"filter" must be a function, but ' + typeof ext.filter + ' given';
        return ret;
      }
    } else if (ext.regex) {
      if (showdown.helper.isString(ext.regex)) {
        ext.regex = new RegExp(ext.regex, 'g');
      }
      if (!ext.regex instanceof RegExp) {
        ret.valid = false;
        ret.error = baseMsg + '"regex" property must either be a string or a RegExp object, but ' + typeof ext.regex + ' given';
        return ret;
      }
      if (showdown.helper.isUndefined(ext.replace)) {
        ret.valid = false;
        ret.error = baseMsg + '"regex" extensions must implement a replace string or function';
        return ret;
      }
    }
  }
  return ret;
}

/**
   * Validate extension
   * @param {object} ext
   * @returns {boolean}
   */
showdown.validateExtension = function (ext) {
  'use strict';

  var validateExtension = validate(ext, null);
  if (!validateExtension.valid) {
    //console.warn(validateExtension.error);
    return false;
  }
  return true;
};

/**
    * showdownjs helper functions
    */

if (!showdown.hasOwnProperty('helper')) {
  showdown.helper = {};
}

/**
   * Check if var is string
   * @static
   * @param {string} a
   * @returns {boolean}
   */
showdown.helper.isString = function isString(a) {
  'use strict';
  return typeof a === 'string' || a instanceof String;
};

/**
    * Check if var is a function
    * @static
    * @param {string} a
    * @returns {boolean}
    */
showdown.helper.isFunction = function isFunction(a) {
  'use strict';
  var getType = {};
  return a && getType.toString.call(a) === '[object Function]';
};

/**
    * ForEach helper function
    * @static
    * @param {*} obj
    * @param {function} callback
    */
showdown.helper.forEach = function forEach(obj, callback) {
  'use strict';
  if (typeof obj.forEach === 'function') {
    obj.forEach(callback);
  } else {
    for (var i = 0; i < obj.length; i++) {
      callback(obj[i], i, obj);
    }
  }
};

/**
    * isArray helper function
    * @static
    * @param {*} a
    * @returns {boolean}
    */
showdown.helper.isArray = function isArray(a) {
  'use strict';
  return a.constructor === Array;
};

/**
    * Check if value is undefined
    * @static
    * @param {*} value The value to check.
    * @returns {boolean} Returns `true` if `value` is `undefined`, else `false`.
    */
showdown.helper.isUndefined = function isUndefined(value) {
  'use strict';
  return typeof value === 'undefined';
};

/**
    * Standardidize extension name
    * @static
    * @param {string} s extension name
    * @returns {string}
    */
showdown.helper.stdExtName = function (s) {
  'use strict';
  return s.replace(/[_-]||\s/g, '').toLowerCase();
};

function escapeCharactersCallback(wholeMatch, m1) {
  'use strict';
  var charCodeToEscape = m1.charCodeAt(0);
  return '~E' + charCodeToEscape + 'E';
}

/**
   * Callback used to escape characters when passing through String.replace
   * @static
   * @param {string} wholeMatch
   * @param {string} m1
   * @returns {string}
   */
showdown.helper.escapeCharactersCallback = escapeCharactersCallback;

/**
                                                                      * Escape characters in a string
                                                                      * @static
                                                                      * @param {string} text
                                                                      * @param {string} charsToEscape
                                                                      * @param {boolean} afterBackslash
                                                                      * @returns {XML|string|void|*}
                                                                      */
showdown.helper.escapeCharacters = function escapeCharacters(text, charsToEscape, afterBackslash) {
  'use strict';
  // First we have to escape the escape characters so that
  // we can build a character class out of them
  var regexString = '([' + charsToEscape.replace(/([\[\]\\])/g, '\\$1') + '])';

  if (afterBackslash) {
    regexString = '\\\\' + regexString;
  }

  var regex = new RegExp(regexString, 'g');
  text = text.replace(regex, escapeCharactersCallback);

  return text;
};

var rgxFindMatchPos = function rgxFindMatchPos(str, left, right, flags) {
  'use strict';
  var f = flags || '',
  g = f.indexOf('g') > -1,
  x = new RegExp(left + '|' + right, 'g' + f.replace(/g/g, '')),
  l = new RegExp(left, f.replace(/g/g, '')),
  pos = [],
  t,s,m,start,end;

  do {
    t = 0;
    while (m = x.exec(str)) {
      if (l.test(m[0])) {
        if (!t++) {
          s = x.lastIndex;
          start = s - m[0].length;
        }
      } else if (t) {
        if (! --t) {
          end = m.index + m[0].length;
          var obj = {
            left: { start: start, end: s },
            match: { start: s, end: m.index },
            right: { start: m.index, end: end },
            wholeMatch: { start: start, end: end } };

          pos.push(obj);
          if (!g) {
            return pos;
          }
        }
      }
    }
  } while (t && (x.lastIndex = s));

  return pos;
};

/**
    * matchRecursiveRegExp
    *
    * (c) 2007 Steven Levithan <stevenlevithan.com>
    * MIT License
    *
    * Accepts a string to search, a left and right format delimiter
    * as regex patterns, and optional regex flags. Returns an array
    * of matches, allowing nested instances of left/right delimiters.
    * Use the "g" flag to return all matches, otherwise only the
    * first is returned. Be careful to ensure that the left and
    * right format delimiters produce mutually exclusive matches.
    * Backreferences are not supported within the right delimiter
    * due to how it is internally combined with the left delimiter.
    * When matching strings whose format delimiters are unbalanced
    * to the left or right, the output is intentionally as a
    * conventional regex library with recursion support would
    * produce, e.g. "<<x>" and "<x>>" both produce ["x"] when using
    * "<" and ">" as the delimiters (both strings contain a single,
    * balanced instance of "<x>").
    *
    * examples:
    * matchRecursiveRegExp("test", "\\(", "\\)")
    * returns: []
    * matchRecursiveRegExp("<t<<e>><s>>t<>", "<", ">", "g")
    * returns: ["t<<e>><s>", ""]
    * matchRecursiveRegExp("<div id=\"x\">test</div>", "<div\\b[^>]*>", "</div>", "gi")
    * returns: ["test"]
    */
showdown.helper.matchRecursiveRegExp = function (str, left, right, flags) {
  'use strict';

  var matchPos = rgxFindMatchPos(str, left, right, flags),
  results = [];

  for (var i = 0; i < matchPos.length; ++i) {
    results.push([
    str.slice(matchPos[i].wholeMatch.start, matchPos[i].wholeMatch.end),
    str.slice(matchPos[i].match.start, matchPos[i].match.end),
    str.slice(matchPos[i].left.start, matchPos[i].left.end),
    str.slice(matchPos[i].right.start, matchPos[i].right.end)]);

  }
  return results;
};

/**
    *
    * @param {string} str
    * @param {string|function} replacement
    * @param {string} left
    * @param {string} right
    * @param {string} flags
    * @returns {string}
    */
showdown.helper.replaceRecursiveRegExp = function (str, replacement, left, right, flags) {
  'use strict';

  if (!showdown.helper.isFunction(replacement)) {
    var repStr = replacement;
    replacement = function replacement() {
      return repStr;
    };
  }

  var matchPos = rgxFindMatchPos(str, left, right, flags),
  finalStr = str,
  lng = matchPos.length;

  if (lng > 0) {
    var bits = [];
    if (matchPos[0].wholeMatch.start !== 0) {
      bits.push(str.slice(0, matchPos[0].wholeMatch.start));
    }
    for (var i = 0; i < lng; ++i) {
      bits.push(
      replacement(
      str.slice(matchPos[i].wholeMatch.start, matchPos[i].wholeMatch.end),
      str.slice(matchPos[i].match.start, matchPos[i].match.end),
      str.slice(matchPos[i].left.start, matchPos[i].left.end),
      str.slice(matchPos[i].right.start, matchPos[i].right.end)));


      if (i < lng - 1) {
        bits.push(str.slice(matchPos[i].wholeMatch.end, matchPos[i + 1].wholeMatch.start));
      }
    }
    if (matchPos[lng - 1].wholeMatch.end < str.length) {
      bits.push(str.slice(matchPos[lng - 1].wholeMatch.end));
    }
    finalStr = bits.join('');
  }
  return finalStr;
};

/**
    * POLYFILLS
    */
if (showdown.helper.isUndefined(console)) {
  console = {
    warn: function warn(msg) {
      'use strict';
      alert(msg);
    },
    log: function log(msg) {
      'use strict';
      alert(msg);
    },
    error: function error(msg) {
      'use strict';
      throw msg;
    } };

}

/**
   * Created by Estevao on 31-05-2015.
   */

/**
       * Showdown Converter class
       * @class
       * @param {object} [converterOptions]
       * @returns {Converter}
       */
showdown.Converter = function (converterOptions) {
  'use strict';

  var
  /**
       * Options used by this converter
       * @private
       * @type {{}}
       */
  options = {},

  /**
                 * Language extensions used by this converter
                 * @private
                 * @type {Array}
                 */
  langExtensions = [],

  /**
                        * Output modifiers extensions used by this converter
                        * @private
                        * @type {Array}
                        */
  outputModifiers = [],

  /**
                         * Event listeners
                         * @private
                         * @type {{}}
                         */
  listeners = {};

  _constructor();

  /**
                   * Converter constructor
                   * @private
                   */
  function _constructor() {
    converterOptions = converterOptions || {};

    for (var gOpt in globalOptions) {
      if (globalOptions.hasOwnProperty(gOpt)) {
        options[gOpt] = globalOptions[gOpt];
      }
    }

    // Merge options
    if (typeof converterOptions === 'object') {
      for (var opt in converterOptions) {
        if (converterOptions.hasOwnProperty(opt)) {
          options[opt] = converterOptions[opt];
        }
      }
    } else {
      throw Error('Converter expects the passed parameter to be an object, but ' + typeof converterOptions +
      ' was passed instead.');
    }

    if (options.extensions) {
      showdown.helper.forEach(options.extensions, _parseExtension);
    }
  }

  /**
     * Parse extension
     * @param {*} ext
     * @param {string} [name='']
     * @private
     */
  function _parseExtension(ext, name) {

    name = name || null;
    // If it's a string, the extension was previously loaded
    if (showdown.helper.isString(ext)) {
      ext = showdown.helper.stdExtName(ext);
      name = ext;

      // LEGACY_SUPPORT CODE
      if (showdown.extensions[ext]) {
        console.warn('DEPRECATION WARNING: ' + ext + ' is an old extension that uses a deprecated loading method.' +
        'Please inform the developer that the extension should be updated!');
        legacyExtensionLoading(showdown.extensions[ext], ext);
        return;
        // END LEGACY SUPPORT CODE

      } else if (!showdown.helper.isUndefined(extensions[ext])) {
        ext = extensions[ext];

      } else {
        throw Error('Extension "' + ext + '" could not be loaded. It was either not found or is not a valid extension.');
      }
    }

    if (typeof ext === 'function') {
      ext = ext();
    }

    if (!showdown.helper.isArray(ext)) {
      ext = [ext];
    }

    var validExt = validate(ext, name);
    if (!validExt.valid) {
      throw Error(validExt.error);
    }

    for (var i = 0; i < ext.length; ++i) {
      switch (ext[i].type) {

        case 'lang':
          langExtensions.push(ext[i]);
          break;

        case 'output':
          outputModifiers.push(ext[i]);
          break;}

      if (ext[i].hasOwnProperty(listeners)) {
        for (var ln in ext[i].listeners) {
          if (ext[i].listeners.hasOwnProperty(ln)) {
            listen(ln, ext[i].listeners[ln]);
          }
        }
      }
    }

  }

  /**
     * LEGACY_SUPPORT
     * @param {*} ext
     * @param {string} name
     */
  function legacyExtensionLoading(ext, name) {
    if (typeof ext === 'function') {
      ext = ext(new showdown.Converter());
    }
    if (!showdown.helper.isArray(ext)) {
      ext = [ext];
    }
    var valid = validate(ext, name);

    if (!valid.valid) {
      throw Error(valid.error);
    }

    for (var i = 0; i < ext.length; ++i) {
      switch (ext[i].type) {
        case 'lang':
          langExtensions.push(ext[i]);
          break;
        case 'output':
          outputModifiers.push(ext[i]);
          break;
        default: // should never reach here
          throw Error('Extension loader error: Type unrecognized!!!');}

    }
  }

  /**
     * Listen to an event
     * @param {string} name
     * @param {function} callback
     */
  function listen(name, callback) {
    if (!showdown.helper.isString(name)) {
      throw Error('Invalid argument in converter.listen() method: name must be a string, but ' + typeof name + ' given');
    }

    if (typeof callback !== 'function') {
      throw Error('Invalid argument in converter.listen() method: callback must be a function, but ' + typeof callback + ' given');
    }

    if (!listeners.hasOwnProperty(name)) {
      listeners[name] = [];
    }
    listeners[name].push(callback);
  }

  function rTrimInputText(text) {
    var rsp = text.match(/^\s*/)[0].length,
    rgx = new RegExp('^\\s{0,' + rsp + '}', 'gm');
    return text.replace(rgx, '');
  }

  /**
     * Dispatch an event
     * @private
     * @param {string} evtName Event name
     * @param {string} text Text
     * @param {{}} options Converter Options
     * @param {{}} globals
     * @returns {string}
     */
  this._dispatch = function dispatch(evtName, text, options, globals) {
    if (listeners.hasOwnProperty(evtName)) {
      for (var ei = 0; ei < listeners[evtName].length; ++ei) {
        var nText = listeners[evtName][ei](evtName, text, this, options, globals);
        if (nText && typeof nText !== 'undefined') {
          text = nText;
        }
      }
    }
    return text;
  };

  /**
      * Listen to an event
      * @param {string} name
      * @param {function} callback
      * @returns {showdown.Converter}
      */
  this.listen = function (name, callback) {
    listen(name, callback);
    return this;
  };

  /**
      * Converts a markdown string into HTML
      * @param {string} text
      * @returns {*}
      */
  this.makeHtml = function (text) {
    //check if text is not falsy
    if (!text) {
      return text;
    }

    var globals = {
      gHtmlBlocks: [],
      gHtmlMdBlocks: [],
      gHtmlSpans: [],
      gUrls: {},
      gTitles: {},
      gDimensions: {},
      gListLevel: 0,
      hashLinkCounts: {},
      langExtensions: langExtensions,
      outputModifiers: outputModifiers,
      converter: this,
      ghCodeBlocks: [] };


    // attacklab: Replace ~ with ~T
    // This lets us use tilde as an escape char to avoid md5 hashes
    // The choice of character is arbitrary; anything that isn't
    // magic in Markdown will work.
    text = text.replace(/~/g, '~T');

    // attacklab: Replace $ with ~D
    // RegExp interprets $ as a special character
    // when it's in a replacement string
    text = text.replace(/\$/g, '~D');

    // Standardize line endings
    text = text.replace(/\r\n/g, '\n'); // DOS to Unix
    text = text.replace(/\r/g, '\n'); // Mac to Unix

    if (options.smartIndentationFix) {
      text = rTrimInputText(text);
    }

    // Make sure text begins and ends with a couple of newlines:
    //text = '\n\n' + text + '\n\n';
    text = text;
    // detab
    text = showdown.subParser('detab')(text, options, globals);

    // stripBlankLines
    text = showdown.subParser('stripBlankLines')(text, options, globals);

    //run languageExtensions
    showdown.helper.forEach(langExtensions, function (ext) {
      text = showdown.subParser('runExtension')(ext, text, options, globals);
    });

    // run the sub parsers
    text = showdown.subParser('hashPreCodeTags')(text, options, globals);
    text = showdown.subParser('githubCodeBlocks')(text, options, globals);
    text = showdown.subParser('hashHTMLBlocks')(text, options, globals);
    text = showdown.subParser('hashHTMLSpans')(text, options, globals);
    text = showdown.subParser('stripLinkDefinitions')(text, options, globals);
    text = showdown.subParser('blockGamut')(text, options, globals);
    text = showdown.subParser('unhashHTMLSpans')(text, options, globals);
    text = showdown.subParser('unescapeSpecialChars')(text, options, globals);

    // attacklab: Restore dollar signs
    text = text.replace(/~D/g, '$$');

    // attacklab: Restore tildes
    text = text.replace(/~T/g, '~');

    // Run output modifiers
    showdown.helper.forEach(outputModifiers, function (ext) {
      text = showdown.subParser('runExtension')(ext, text, options, globals);
    });
    return text;
  };

  /**
      * Set an option of this Converter instance
      * @param {string} key
      * @param {*} value
      */
  this.setOption = function (key, value) {
    options[key] = value;
  };

  /**
      * Get the option of this Converter instance
      * @param {string} key
      * @returns {*}
      */
  this.getOption = function (key) {
    return options[key];
  };

  /**
      * Get the options of this Converter instance
      * @returns {{}}
      */
  this.getOptions = function () {
    return options;
  };

  /**
      * Add extension to THIS converter
      * @param {{}} extension
      * @param {string} [name=null]
      */
  this.addExtension = function (extension, name) {
    name = name || null;
    _parseExtension(extension, name);
  };

  /**
      * Use a global registered extension with THIS converter
      * @param {string} extensionName Name of the previously registered extension
      */
  this.useExtension = function (extensionName) {
    _parseExtension(extensionName);
  };

  /**
      * Set the flavor THIS converter should use
      * @param {string} name
      */
  this.setFlavor = function (name) {
    if (flavor.hasOwnProperty(name)) {
      var preset = flavor[name];
      for (var option in preset) {
        if (preset.hasOwnProperty(option)) {
          options[option] = preset[option];
        }
      }
    }
  };

  /**
      * Remove an extension from THIS converter.
      * Note: This is a costly operation. It's better to initialize a new converter
      * and specify the extensions you wish to use
      * @param {Array} extension
      */
  this.removeExtension = function (extension) {
    if (!showdown.helper.isArray(extension)) {
      extension = [extension];
    }
    for (var a = 0; a < extension.length; ++a) {
      var ext = extension[a];
      for (var i = 0; i < langExtensions.length; ++i) {
        if (langExtensions[i] === ext) {
          langExtensions[i].splice(i, 1);
        }
      }
      for (var ii = 0; ii < outputModifiers.length; ++i) {
        if (outputModifiers[ii] === ext) {
          outputModifiers[ii].splice(i, 1);
        }
      }
    }
  };

  /**
      * Get all extension of THIS converter
      * @returns {{language: Array, output: Array}}
      */
  this.getAllExtensions = function () {
    return {
      language: langExtensions,
      output: outputModifiers };

  };
};

/**
    * Turn Markdown link shortcuts into XHTML <a> tags.
    */
showdown.subParser('anchors', function (text, options, globals) {
  'use strict';

  text = globals.converter._dispatch('anchors.before', text, options, globals);

  var writeAnchorTag = function writeAnchorTag(wholeMatch, m1, m2, m3, m4, m5, m6, m7) {
    if (showdown.helper.isUndefined(m7)) {
      m7 = '';
    }
    wholeMatch = m1;
    var linkText = m2,
    linkId = m3.toLowerCase(),
    url = m4,
    title = m7;

    if (!url) {
      if (!linkId) {
        // lower-case and turn embedded newlines into spaces
        linkId = linkText.toLowerCase().replace(/ ?\n/g, ' ');
      }
      url = '#' + linkId;

      if (!showdown.helper.isUndefined(globals.gUrls[linkId])) {
        url = globals.gUrls[linkId];
        if (!showdown.helper.isUndefined(globals.gTitles[linkId])) {
          title = globals.gTitles[linkId];
        }
      } else {
        if (wholeMatch.search(/\(\s*\)$/m) > -1) {
          // Special case for explicit empty url
          url = '';
        } else {
          return wholeMatch;
        }
      }
    }

    url = showdown.helper.escapeCharacters(url, '*_', false);
    var result = '<a href="' + url + '"';

    if (title !== '' && title !== null) {
      title = title.replace(/"/g, '&quot;');
      title = showdown.helper.escapeCharacters(title, '*_', false);
      result += ' title="' + title + '"';
    }

    result += '>' + linkText + '</a>';

    return result;
  };

  // First, handle reference-style links: [link text] [id]
  /*
   text = text.replace(/
   (							// wrap whole match in $1
   \[
   (
   (?:
   \[[^\]]*\]		// allow brackets nested one level
   |
   [^\[]			// or anything else
   )*
   )
   \]
    [ ]?					// one optional space
   (?:\n[ ]*)?				// one optional newline followed by spaces
    \[
   (.*?)					// id = $3
   \]
   )()()()()					// pad remaining backreferences
   /g,_DoAnchors_callback);
   */


  text = text.replace(/(\[((?:\[[^\]]*]|[^\[\]])*)][ ]?(?:\n[ ]*)?\[(.*?)])()()()()/g, writeAnchorTag);

  //
  // Next, inline-style links: [link text](url "optional title")
  //

  /*
   text = text.replace(/
   (						// wrap whole match in $1
   \[
   (
   (?:
   \[[^\]]*\]	// allow brackets nested one level
   |
   [^\[\]]			// or anything else
   )
   )
   \]
   \(						// literal paren
   [ \t]*
   ()						// no id, so leave $3 empty
   <?(.*?)>?				// href = $4
   [ \t]*
   (						// $5
   (['"])				// quote char = $6
   (.*?)				// Title = $7
   \6					// matching quote
   [ \t]*				// ignore any spaces/tabs between closing quote and )
   )?						// title is optional
   \)
   )
   /g,writeAnchorTag);
   */
  text = text.replace(/(\[((?:\[[^\]]*]|[^\[\]])*)]\([ \t]*()<?(.*?(?:\(.*?\).*?)?)>?[ \t]*((['"])(.*?)\6[ \t]*)?\))/g,
  writeAnchorTag);

  //
  // Last, handle reference-style shortcuts: [link text]
  // These must come last in case you've also got [link test][1]
  // or [link test](/foo)
  //

  /*
   text = text.replace(/
   (                // wrap whole match in $1
   \[
   ([^\[\]]+)       // link text = $2; can't contain '[' or ']'
   \]
   )()()()()()      // pad rest of backreferences
   /g, writeAnchorTag);
   */
  text = text.replace(/(\[([^\[\]]+)])()()()()()/g, writeAnchorTag);

  text = globals.converter._dispatch('anchors.after', text, options, globals);
  return text;
});

showdown.subParser('autoLinks', function (text, options, globals) {
  'use strict';

  text = globals.converter._dispatch('autoLinks.before', text, options, globals);

  var simpleURLRegex = /\b(((https?|ftp|dict):\/\/|www\.)[^'">\s]+\.[^'">\s]+)(?=\s|$)(?!["<>])/gi,
  delimUrlRegex = /<(((https?|ftp|dict):\/\/|www\.)[^'">\s]+)>/gi,
  simpleMailRegex = /(?:^|[ \n\t])([A-Za-z0-9!#$%&'*+-/=?^_`\{|}~\.]+@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]+)(?:$|[ \n\t])/gi,
  delimMailRegex = /<(?:mailto:)?([-.\w]+@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]+)>/gi;

  text = text.replace(delimUrlRegex, replaceLink);
  text = text.replace(delimMailRegex, replaceMail);
  // simpleURLRegex  = /\b(((https?|ftp|dict):\/\/|www\.)[-.+~:?#@!$&'()*,;=[\]\w]+)\b/gi,
  // Email addresses: <address@domain.foo>

  if (options.simplifiedAutoLink) {
    text = text.replace(simpleURLRegex, replaceLink);
    text = text.replace(simpleMailRegex, replaceMail);
  }

  function replaceLink(wm, link) {
    var lnkTxt = link;
    if (/^www\./i.test(link)) {
      link = link.replace(/^www\./i, 'http://www.');
    }
    return '<a href="' + link + '">' + lnkTxt + '</a>';
  }

  function replaceMail(wholeMatch, m1) {
    var unescapedStr = showdown.subParser('unescapeSpecialChars')(m1);
    return showdown.subParser('encodeEmailAddress')(unescapedStr);
  }

  text = globals.converter._dispatch('autoLinks.after', text, options, globals);

  return text;
});

/**
     * These are all the transformations that form block-level
     * tags like paragraphs, headers, and list items.
     */
showdown.subParser('blockGamut', function (text, options, globals) {
  'use strict';

  text = globals.converter._dispatch('blockGamut.before', text, options, globals);

  // we parse blockquotes first so that we can have headings and hrs
  // inside blockquotes
  text = showdown.subParser('blockQuotes')(text, options, globals);
  text = showdown.subParser('headers')(text, options, globals);

  // Do Horizontal Rules:
  var key = showdown.subParser('hashBlock')('<hr />', options, globals);
  text = text.replace(/^[ ]{0,2}([ ]?\*[ ]?){3,}[ \t]*$/gm, key);
  text = text.replace(/^[ ]{0,2}([ ]?\-[ ]?){3,}[ \t]*$/gm, key);
  text = text.replace(/^[ ]{0,2}([ ]?_[ ]?){3,}[ \t]*$/gm, key);

  text = showdown.subParser('lists')(text, options, globals);
  text = showdown.subParser('codeBlocks')(text, options, globals);
  text = showdown.subParser('tables')(text, options, globals);

  // We already ran _HashHTMLBlocks() before, in Markdown(), but that
  // was to escape raw HTML in the original Markdown source. This time,
  // we're escaping the markup we've just created, so that we don't wrap
  // <p> tags around block-level tags.
  text = showdown.subParser('hashHTMLBlocks')(text, options, globals);
  text = showdown.subParser('paragraphs')(text, options, globals);

  text = globals.converter._dispatch('blockGamut.after', text, options, globals);

  return text;
});

showdown.subParser('blockQuotes', function (text, options, globals) {
  'use strict';

  text = globals.converter._dispatch('blockQuotes.before', text, options, globals);
  /*
                                                                                     text = text.replace(/
                                                                                     (								// Wrap whole match in $1
                                                                                     (
                                                                                     ^[ \t]*>[ \t]?			// '>' at the start of a line
                                                                                     .+\n					// rest of the first line
                                                                                     (.+\n)*					// subsequent consecutive lines
                                                                                     \n*						// blanks
                                                                                     )+
                                                                                     )
                                                                                     /gm, function(){...});
                                                                                     */

  text = text.replace(/((^[ \t]{0,3}>[ \t]?.+\n(.+\n)*\n*)+)/gm, function (wholeMatch, m1) {
    var bq = m1;

    // attacklab: hack around Konqueror 3.5.4 bug:
    // "----------bug".replace(/^-/g,"") == "bug"
    bq = bq.replace(/^[ \t]*>[ \t]?/gm, '~0'); // trim one level of quoting

    // attacklab: clean up hack
    bq = bq.replace(/~0/g, '');

    bq = bq.replace(/^[ \t]+$/gm, ''); // trim whitespace-only lines
    bq = showdown.subParser('githubCodeBlocks')(bq, options, globals);
    bq = showdown.subParser('blockGamut')(bq, options, globals); // recurse

    bq = bq.replace(/(^|\n)/g, '$1  ');
    // These leading spaces screw with <pre> content, so we need to fix that:
    bq = bq.replace(/(\s*<pre>[^\r]+?<\/pre>)/gm, function (wholeMatch, m1) {
      var pre = m1;
      // attacklab: hack around Konqueror 3.5.4 bug:
      pre = pre.replace(/^  /mg, '~0');
      pre = pre.replace(/~0/g, '');
      return pre;
    });

    return showdown.subParser('hashBlock')('<blockquote>\n' + bq + '\n</blockquote>', options, globals);
  });

  text = globals.converter._dispatch('blockQuotes.after', text, options, globals);
  return text;
});

/**
     * Process Markdown `<pre><code>` blocks.
     */
showdown.subParser('codeBlocks', function (text, options, globals) {
  'use strict';

  text = globals.converter._dispatch('codeBlocks.before', text, options, globals);
  /*
                                                                                    text = text.replace(text,
                                                                                    /(?:\n\n|^)
                                                                                    (								// $1 = the code block -- one or more lines, starting with a space/tab
                                                                                    (?:
                                                                                    (?:[ ]{4}|\t)			// Lines must start with a tab or a tab-width of spaces - attacklab: g_tab_width
                                                                                    .*\n+
                                                                                    )+
                                                                                    )
                                                                                    (\n*[ ]{0,3}[^ \t\n]|(?=~0))	// attacklab: g_tab_width
                                                                                    /g,function(){...});
                                                                                    */

  // attacklab: sentinel workarounds for lack of \A and \Z, safari\khtml bug
  text += '~0';

  var pattern = /(?:\n\n|^)((?:(?:[ ]{4}|\t).*\n+)+)(\n*[ ]{0,3}[^ \t\n]|(?=~0))/g;
  text = text.replace(pattern, function (wholeMatch, m1, m2) {
    var codeblock = m1,
    nextChar = m2,
    end = '\n';

    codeblock = showdown.subParser('outdent')(codeblock);
    codeblock = showdown.subParser('encodeCode')(codeblock);
    codeblock = showdown.subParser('detab')(codeblock);
    codeblock = codeblock.replace(/^\n+/g, ''); // trim leading newlines
    codeblock = codeblock.replace(/\n+$/g, ''); // trim trailing newlines

    if (options.omitExtraWLInCodeBlocks) {
      end = '';
    }

    codeblock = '<pre><code>' + codeblock + end + '</code></pre>';

    return showdown.subParser('hashBlock')(codeblock, options, globals) + nextChar;
  });

  // attacklab: strip sentinel
  text = text.replace(/~0/, '');

  text = globals.converter._dispatch('codeBlocks.after', text, options, globals);
  return text;
});

/**
     *
     *   *  Backtick quotes are used for <code></code> spans.
     *
     *   *  You can use multiple backticks as the delimiters if you want to
     *     include literal backticks in the code span. So, this input:
     *
     *         Just type ``foo `bar` baz`` at the prompt.
     *
     *       Will translate to:
     *
     *         <p>Just type <code>foo `bar` baz</code> at the prompt.</p>
     *
     *    There's no arbitrary limit to the number of backticks you
     *    can use as delimters. If you need three consecutive backticks
     *    in your code, use four for delimiters, etc.
     *
     *  *  You can use spaces to get literal backticks at the edges:
     *
     *         ... type `` `bar` `` ...
     *
     *       Turns to:
     *
     *         ... type <code>`bar`</code> ...
     */
showdown.subParser('codeSpans', function (text, options, globals) {
  'use strict';

  text = globals.converter._dispatch('codeSpans.before', text, options, globals);

  /*
                                                                                   text = text.replace(/
                                                                                   (^|[^\\])					// Character before opening ` can't be a backslash
                                                                                   (`+)						// $2 = Opening run of `
                                                                                   (							// $3 = The code block
                                                                                   [^\r]*?
                                                                                   [^`]					// attacklab: work around lack of lookbehind
                                                                                   )
                                                                                   \2							// Matching closer
                                                                                   (?!`)
                                                                                   /gm, function(){...});
                                                                                   */

  if (typeof text === 'undefined') {
    text = '';
  }
  text = text.replace(/(^|[^\\])(`+)([^\r]*?[^`])\2(?!`)/gm,
  function (wholeMatch, m1, m2, m3) {
    var c = m3;
    c = c.replace(/^([ \t]*)/g, ''); // leading whitespace
    c = c.replace(/[ \t]*$/g, ''); // trailing whitespace
    c = showdown.subParser('encodeCode')(c);
    return m1 + '<code>' + c + '</code>';
  });


  text = globals.converter._dispatch('codeSpans.after', text, options, globals);
  return text;
});

/**
     * Convert all tabs to spaces
     */
showdown.subParser('detab', function (text) {
  'use strict';

  // expand first n-1 tabs
  text = text.replace(/\t(?=\t)/g, '    '); // g_tab_width

  // replace the nth with two sentinels
  text = text.replace(/\t/g, '~A~B');

  // use the sentinel to anchor our regex so it doesn't explode
  text = text.replace(/~B(.+?)~A/g, function (wholeMatch, m1) {
    var leadingText = m1,
    numSpaces = 4 - leadingText.length % 4; // g_tab_width

    // there *must* be a better way to do this:
    for (var i = 0; i < numSpaces; i++) {
      leadingText += ' ';
    }

    return leadingText;
  });

  // clean up sentinels
  text = text.replace(/~A/g, '    '); // g_tab_width
  text = text.replace(/~B/g, '');

  return text;

});

/**
     * Smart processing for ampersands and angle brackets that need to be encoded.
     */
showdown.subParser('encodeAmpsAndAngles', function (text) {
  'use strict';
  // Ampersand-encoding based entirely on Nat Irons's Amputator MT plugin:
  // http://bumppo.net/projects/amputator/
  text = text.replace(/&(?!#?[xX]?(?:[0-9a-fA-F]+|\w+);)/g, '&amp;');

  // Encode naked <'s
  text = text.replace(/<(?![a-z\/?\$!])/gi, '&lt;');

  return text;
});

/**
     * Returns the string, with after processing the following backslash escape sequences.
     *
     * attacklab: The polite way to do this is with the new escapeCharacters() function:
     *
     *    text = escapeCharacters(text,"\\",true);
     *    text = escapeCharacters(text,"`*_{}[]()>#+-.!",true);
     *
     * ...but we're sidestepping its use of the (slow) RegExp constructor
     * as an optimization for Firefox.  This function gets called a LOT.
     */
showdown.subParser('encodeBackslashEscapes', function (text) {
  'use strict';
  text = text.replace(/\\(\\)/g, showdown.helper.escapeCharactersCallback);
  text = text.replace(/\\([`*_{}\[\]()>#+-.!])/g, showdown.helper.escapeCharactersCallback);
  return text;
});

/**
     * Encode/escape certain characters inside Markdown code runs.
     * The point is that in code, these characters are literals,
     * and lose their special Markdown meanings.
     */
showdown.subParser('encodeCode', function (text) {
  'use strict';

  // Encode all ampersands; HTML entities are not
  // entities within a Markdown code span.
  text = text.replace(/&/g, '&amp;');

  // Do the angle bracket song and dance:
  text = text.replace(/</g, '&lt;');
  text = text.replace(/>/g, '&gt;');

  // Now, escape characters that are magic in Markdown:
  text = showdown.helper.escapeCharacters(text, '*_{}[]\\', false);

  // jj the line above breaks this:
  //---
  //* Item
  //   1. Subitem
  //            special char: *
  // ---

  return text;
});

/**
     *  Input: an email address, e.g. "foo@example.com"
     *
     *  Output: the email address as a mailto link, with each character
     *    of the address encoded as either a decimal or hex entity, in
     *    the hopes of foiling most address harvesting spam bots. E.g.:
     *
     *    <a href="&#x6D;&#97;&#105;&#108;&#x74;&#111;:&#102;&#111;&#111;&#64;&#101;
     *       x&#x61;&#109;&#x70;&#108;&#x65;&#x2E;&#99;&#111;&#109;">&#102;&#111;&#111;
     *       &#64;&#101;x&#x61;&#109;&#x70;&#108;&#x65;&#x2E;&#99;&#111;&#109;</a>
     *
     *  Based on a filter by Matthew Wickline, posted to the BBEdit-Talk
     *  mailing list: <http://tinyurl.com/yu7ue>
     *
     */
showdown.subParser('encodeEmailAddress', function (addr) {
  'use strict';

  var encode = [
  function (ch) {
    return '&#' + ch.charCodeAt(0) + ';';
  },
  function (ch) {
    return '&#x' + ch.charCodeAt(0).toString(16) + ';';
  },
  function (ch) {
    return ch;
  }];


  addr = 'mailto:' + addr;

  addr = addr.replace(/./g, function (ch) {
    if (ch === '@') {
      // this *must* be encoded. I insist.
      ch = encode[Math.floor(Math.random() * 2)](ch);
    } else if (ch !== ':') {
      // leave ':' alone (to spot mailto: later)
      var r = Math.random();
      // roughly 10% raw, 45% hex, 45% dec
      ch =
      r > 0.9 ? encode[2](ch) : r > 0.45 ? encode[1](ch) : encode[0](ch);

    }
    return ch;
  });

  addr = '<a href="' + addr + '">' + addr + '</a>';
  addr = addr.replace(/">.+:/g, '">'); // strip the mailto: from the visible part

  return addr;
});

/**
     * Within tags -- meaning between < and > -- encode [\ ` * _] so they
     * don't conflict with their use in Markdown for code, italics and strong.
     */
showdown.subParser('escapeSpecialCharsWithinTagAttributes', function (text) {
  'use strict';

  // Build a regex to find HTML tags and comments.  See Friedl's
  // "Mastering Regular Expressions", 2nd Ed., pp. 200-201.
  var regex = /(<[a-z\/!$]("[^"]*"|'[^']*'|[^'">])*>|<!(--.*?--\s*)+>)/gi;

  text = text.replace(regex, function (wholeMatch) {
    var tag = wholeMatch.replace(/(.)<\/?code>(?=.)/g, '$1`');
    tag = showdown.helper.escapeCharacters(tag, '\\`*_', false);
    return tag;
  });

  return text;
});

/**
     * Handle github codeblocks prior to running HashHTML so that
     * HTML contained within the codeblock gets escaped properly
     * Example:
     * ```ruby
     *     def hello_world(x)
     *       puts "Hello, #{x}"
     *     end
     * ```
     */
showdown.subParser('githubCodeBlocks', function (text, options, globals) {
  'use strict';

  // early exit if option is not enabled
  if (!options.ghCodeBlocks) {
    return text;
  }

  text = globals.converter._dispatch('githubCodeBlocks.before', text, options, globals);

  text += '~0';

  text = text.replace(/(?:^|\n)```(.*)\n([\s\S]*?)\n```/g, function (wholeMatch, language, codeblock) {
    var end = options.omitExtraWLInCodeBlocks ? '' : '\n';

    // First parse the github code block
    codeblock = showdown.subParser('encodeCode')(codeblock);
    codeblock = showdown.subParser('detab')(codeblock);
    codeblock = codeblock.replace(/^\n+/g, ''); // trim leading newlines
    codeblock = codeblock.replace(/\n+$/g, ''); // trim trailing whitespace

    codeblock = '<pre><code' + (language ? ' class="' + language + ' language-' + language + '"' : '') + '>' + codeblock + end + '</code></pre>';

    codeblock = showdown.subParser('hashBlock')(codeblock, options, globals);

    // Since GHCodeblocks can be false positives, we need to
    // store the primitive text and the parsed text in a global var,
    // and then return a token
    return '\n\n~G' + (globals.ghCodeBlocks.push({ text: wholeMatch, codeblock: codeblock }) - 1) + 'G\n\n';
  });

  // attacklab: strip sentinel
  text = text.replace(/~0/, '');

  return globals.converter._dispatch('githubCodeBlocks.after', text, options, globals);
});

showdown.subParser('hashBlock', function (text, options, globals) {
  'use strict';
  text = text.replace(/(^\n+|\n+$)/g, '');
  return '\n\n~K' + (globals.gHtmlBlocks.push(text) - 1) + 'K\n\n';
});

showdown.subParser('hashElement', function (text, options, globals) {
  'use strict';

  return function (wholeMatch, m1) {
    var blockText = m1;

    // Undo double lines
    blockText = blockText.replace(/\n\n/g, '\n');
    blockText = blockText.replace(/^\n/, '');

    // strip trailing blank lines
    blockText = blockText.replace(/\n+$/g, '');

    // Replace the element text with a marker ("~KxK" where x is its key)
    blockText = '\n\n~K' + (globals.gHtmlBlocks.push(blockText) - 1) + 'K\n\n';

    return blockText;
  };
});

showdown.subParser('hashHTMLBlocks', function (text, options, globals) {
  'use strict';

  var blockTags = [
  'pre',
  'div',
  'h1',
  'h2',
  'h3',
  'h4',
  'h5',
  'h6',
  'blockquote',
  'table',
  'dl',
  'ol',
  'ul',
  'script',
  'noscript',
  'form',
  'fieldset',
  'iframe',
  'math',
  'style',
  'section',
  'header',
  'footer',
  'nav',
  'article',
  'aside',
  'address',
  'audio',
  'canvas',
  'figure',
  'hgroup',
  'output',
  'video',
  'p'],

  repFunc = function repFunc(wholeMatch, match, left, right) {
    var txt = wholeMatch;
    // check if this html element is marked as markdown
    // if so, it's contents should be parsed as markdown
    if (left.search(/\bmarkdown\b/) !== -1) {
      txt = left + globals.converter.makeHtml(match) + right;
    }
    return '\n\n~K' + (globals.gHtmlBlocks.push(txt) - 1) + 'K\n\n';
  };

  for (var i = 0; i < blockTags.length; ++i) {
    text = showdown.helper.replaceRecursiveRegExp(text, repFunc, '^(?: |\\t){0,3}<' + blockTags[i] + '\\b[^>]*>', '</' + blockTags[i] + '>', 'gim');
  }

  // HR SPECIAL CASE
  text = text.replace(/(\n[ ]{0,3}(<(hr)\b([^<>])*?\/?>)[ \t]*(?=\n{2,}))/g,
  showdown.subParser('hashElement')(text, options, globals));

  // Special case for standalone HTML comments:
  text = text.replace(/(<!--[\s\S]*?-->)/g,
  showdown.subParser('hashElement')(text, options, globals));

  // PHP and ASP-style processor instructions (<?...?> and <%...%>)
  text = text.replace(/(?:\n\n)([ ]{0,3}(?:<([?%])[^\r]*?\2>)[ \t]*(?=\n{2,}))/g,
  showdown.subParser('hashElement')(text, options, globals));
  return text;
});

/**
     * Hash span elements that should not be parsed as markdown
     */
showdown.subParser('hashHTMLSpans', function (text, config, globals) {
  'use strict';

  var matches = showdown.helper.matchRecursiveRegExp(text, '<code\\b[^>]*>', '</code>', 'gi');

  for (var i = 0; i < matches.length; ++i) {
    text = text.replace(matches[i][0], '~L' + (globals.gHtmlSpans.push(matches[i][0]) - 1) + 'L');
  }
  return text;
});

/**
     * Unhash HTML spans
     */
showdown.subParser('unhashHTMLSpans', function (text, config, globals) {
  'use strict';

  for (var i = 0; i < globals.gHtmlSpans.length; ++i) {
    text = text.replace('~L' + i + 'L', globals.gHtmlSpans[i]);
  }

  return text;
});

/**
     * Hash span elements that should not be parsed as markdown
     */
showdown.subParser('hashPreCodeTags', function (text, config, globals) {
  'use strict';

  var repFunc = function repFunc(wholeMatch, match, left, right) {
    // encode html entities
    var codeblock = left + showdown.subParser('encodeCode')(match) + right;
    return '\n\n~G' + (globals.ghCodeBlocks.push({ text: wholeMatch, codeblock: codeblock }) - 1) + 'G\n\n';
  };

  text = showdown.helper.replaceRecursiveRegExp(text, repFunc, '^(?: |\\t){0,3}<pre\\b[^>]*>\\s*<code\\b[^>]*>', '^(?: |\\t){0,3}</code>\\s*</pre>', 'gim');
  return text;
});

showdown.subParser('headers', function (text, options, globals) {
  'use strict';

  text = globals.converter._dispatch('headers.before', text, options, globals);

  var prefixHeader = options.prefixHeaderId,
  headerLevelStart = isNaN(parseInt(options.headerLevelStart)) ? 1 : parseInt(options.headerLevelStart),

  // Set text-style headers:
  //	Header 1
  //	========
  //
  //	Header 2
  //	--------
  //
  setextRegexH1 = options.smoothLivePreview ? /^(.+)[ \t]*\n={2,}[ \t]*\n+/gm : /^(.+)[ \t]*\n=+[ \t]*\n+/gm,
  setextRegexH2 = options.smoothLivePreview ? /^(.+)[ \t]*\n-{2,}[ \t]*\n+/gm : /^(.+)[ \t]*\n-+[ \t]*\n+/gm;

  text = text.replace(setextRegexH1, function (wholeMatch, m1) {

    var spanGamut = showdown.subParser('spanGamut')(m1, options, globals),
    hID = options.noHeaderId ? '' : ' id="' + headerId(m1) + '"',
    hLevel = headerLevelStart,
    hashBlock = '<h' + hLevel + hID + '>' + spanGamut + '</h' + hLevel + '>';
    return showdown.subParser('hashBlock')(hashBlock, options, globals);
  });

  text = text.replace(setextRegexH2, function (matchFound, m1) {
    var spanGamut = showdown.subParser('spanGamut')(m1, options, globals),
    hID = options.noHeaderId ? '' : ' id="' + headerId(m1) + '"',
    hLevel = headerLevelStart + 1,
    hashBlock = '<h' + hLevel + hID + '>' + spanGamut + '</h' + hLevel + '>';
    return showdown.subParser('hashBlock')(hashBlock, options, globals);
  });

  // atx-style headers:
  //  # Header 1
  //  ## Header 2
  //  ## Header 2 with closing hashes ##
  //  ...
  //  ###### Header 6
  //
  text = text.replace(/^(#{1,6})[ \t]*(.+?)[ \t]*#*\n+/gm, function (wholeMatch, m1, m2) {
    var span = showdown.subParser('spanGamut')(m2, options, globals),
    hID = options.noHeaderId ? '' : ' id="' + headerId(m2) + '"',
    hLevel = headerLevelStart - 1 + m1.length,
    header = '<h' + hLevel + hID + '>' + span + '</h' + hLevel + '>';

    return showdown.subParser('hashBlock')(header, options, globals);
  });

  function headerId(m) {
    var title,escapedId = m.replace(/[^\w]/g, '').toLowerCase();

    if (globals.hashLinkCounts[escapedId]) {
      title = escapedId + '-' + globals.hashLinkCounts[escapedId]++;
    } else {
      title = escapedId;
      globals.hashLinkCounts[escapedId] = 1;
    }

    // Prefix id to prevent causing inadvertent pre-existing style matches.
    if (prefixHeader === true) {
      prefixHeader = 'section';
    }

    if (showdown.helper.isString(prefixHeader)) {
      return prefixHeader + title;
    }
    return title;
  }

  text = globals.converter._dispatch('headers.after', text, options, globals);
  return text;
});

/**
     * Turn Markdown image shortcuts into <img> tags.
     */
showdown.subParser('images', function (text, options, globals) {
  'use strict';

  text = globals.converter._dispatch('images.before', text, options, globals);

  var inlineRegExp = /!\[(.*?)]\s?\([ \t]*()<?(\S+?)>?(?: =([*\d]+[A-Za-z%]{0,4})x([*\d]+[A-Za-z%]{0,4}))?[ \t]*(?:(['"])(.*?)\6[ \t]*)?\)/g,
  referenceRegExp = /!\[([^\]]*?)] ?(?:\n *)?\[(.*?)]()()()()()/g;

  function writeImageTag(wholeMatch, altText, linkId, url, width, height, m5, title) {

    var gUrls = globals.gUrls,
    gTitles = globals.gTitles,
    gDims = globals.gDimensions;

    linkId = linkId.toLowerCase();

    if (!title) {
      title = '';
    }

    if (url === '' || url === null) {
      if (linkId === '' || linkId === null) {
        // lower-case and turn embedded newlines into spaces
        linkId = altText.toLowerCase().replace(/ ?\n/g, ' ');
      }
      url = '#' + linkId;

      if (!showdown.helper.isUndefined(gUrls[linkId])) {
        url = gUrls[linkId];
        if (!showdown.helper.isUndefined(gTitles[linkId])) {
          title = gTitles[linkId];
        }
        if (!showdown.helper.isUndefined(gDims[linkId])) {
          width = gDims[linkId].width;
          height = gDims[linkId].height;
        }
      } else {
        return wholeMatch;
      }
    }

    altText = altText.replace(/"/g, '&quot;');
    altText = showdown.helper.escapeCharacters(altText, '*_', false);
    url = showdown.helper.escapeCharacters(url, '*_', false);
    var result = '<img src="' + url + '" alt="' + altText + '"';

    if (title) {
      title = title.replace(/"/g, '&quot;');
      title = showdown.helper.escapeCharacters(title, '*_', false);
      result += ' title="' + title + '"';
    }

    if (width && height) {
      width = width === '*' ? 'auto' : width;
      height = height === '*' ? 'auto' : height;

      result += ' width="' + width + '"';
      result += ' height="' + height + '"';
    }

    result += ' />';
    return result;
  }

  // First, handle reference-style labeled images: ![alt text][id]
  text = text.replace(referenceRegExp, writeImageTag);

  // Next, handle inline images:  ![alt text](url =<width>x<height> "optional title")
  text = text.replace(inlineRegExp, writeImageTag);

  text = globals.converter._dispatch('images.after', text, options, globals);
  return text;
});

showdown.subParser('italicsAndBold', function (text, options, globals) {
  'use strict';

  text = globals.converter._dispatch('italicsAndBold.before', text, options, globals);

  if (options.literalMidWordUnderscores) {
    //underscores
    // Since we are consuming a \s character, we need to add it
    text = text.replace(/(^|\s|>|\b)__(?=\S)([\s\S]+?)__(?=\b|<|\s|$)/gm, '$1<strong>$2</strong>');
    text = text.replace(/(^|\s|>|\b)_(?=\S)([\s\S]+?)_(?=\b|<|\s|$)/gm, '$1<em>$2</em>');
    //asterisks
    text = text.replace(/(\*\*)(?=\S)([^\r]*?\S[*]*)\1/g, '<strong>$2</strong>');
    text = text.replace(/(\*)(?=\S)([^\r]*?\S)\1/g, '<em>$2</em>');

  } else {
    // <strong> must go first:
    text = text.replace(/(\*\*|__)(?=\S)([^\r]*?\S[*_]*)\1/g, '<strong>$2</strong>');
    text = text.replace(/(\*|_)(?=\S)([^\r]*?\S)\1/g, '<em>$2</em>');
  }

  text = globals.converter._dispatch('italicsAndBold.after', text, options, globals);
  return text;
});

/**
     * Form HTML ordered (numbered) and unordered (bulleted) lists.
     */
showdown.subParser('lists', function (text, options, globals) {
  'use strict';

  text = globals.converter._dispatch('lists.before', text, options, globals);
  /**
                                                                               * Process the contents of a single ordered or unordered list, splitting it
                                                                               * into individual list items.
                                                                               * @param {string} listStr
                                                                               * @param {boolean} trimTrailing
                                                                               * @returns {string}
                                                                               */
  function processListItems(listStr, trimTrailing) {
    // The $g_list_level global keeps track of when we're inside a list.
    // Each time we enter a list, we increment it; when we leave a list,
    // we decrement. If it's zero, we're not in a list anymore.
    //
    // We do this because when we're not inside a list, we want to treat
    // something like this:
    //
    //    I recommend upgrading to version
    //    8. Oops, now this line is treated
    //    as a sub-list.
    //
    // As a single paragraph, despite the fact that the second line starts
    // with a digit-period-space sequence.
    //
    // Whereas when we're inside a list (or sub-list), that line will be
    // treated as the start of a sub-list. What a kludge, huh? This is
    // an aspect of Markdown's syntax that's hard to parse perfectly
    // without resorting to mind-reading. Perhaps the solution is to
    // change the syntax rules such that sub-lists must start with a
    // starting cardinal number; e.g. "1." or "a.".
    globals.gListLevel++;

    // trim trailing blank lines:
    listStr = listStr.replace(/\n{2,}$/, '\n');

    // attacklab: add sentinel to emulate \z
    listStr += '~0';

    var rgx = /(\n)?(^[ \t]*)([*+-]|\d+[.])[ \t]+((\[(x|X| )?])?[ \t]*[^\r]+?(\n{1,2}))(?=\n*(~0|\2([*+-]|\d+[.])[ \t]+))/gm,
    isParagraphed = /\n[ \t]*\n(?!~0)/.test(listStr);

    listStr = listStr.replace(rgx, function (wholeMatch, m1, m2, m3, m4, taskbtn, checked) {
      checked = checked && checked.trim() !== '';
      var item = showdown.subParser('outdent')(m4, options, globals),
      bulletStyle = '';

      // Support for github tasklists
      if (taskbtn && options.tasklists) {
        bulletStyle = ' class="task-list-item" style="list-style-type: none;"';
        item = item.replace(/^[ \t]*\[(x|X| )?]/m, function () {
          var otp = '<input type="checkbox" disabled style="margin: 0px 0.35em 0.25em -1.6em; vertical-align: middle;"';
          if (checked) {
            otp += ' checked';
          }
          otp += '>';
          return otp;
        });
      }
      // m1 - Leading line or
      // Has a double return (multi paragraph) or
      // Has sublist
      if (m1 || item.search(/\n{2,}/) > -1) {
        item = showdown.subParser('githubCodeBlocks')(item, options, globals);
        item = showdown.subParser('blockGamut')(item, options, globals);
      } else {
        // Recursion for sub-lists:
        item = showdown.subParser('lists')(item, options, globals);
        item = item.replace(/\n$/, ''); // chomp(item)
        if (isParagraphed) {
          item = showdown.subParser('paragraphs')(item, options, globals);
        } else {
          item = showdown.subParser('spanGamut')(item, options, globals);
        }
      }
      item = '\n<li' + bulletStyle + '>' + item + '</li>\n';
      return item;
    });

    // attacklab: strip sentinel
    listStr = listStr.replace(/~0/g, '');

    globals.gListLevel--;

    if (trimTrailing) {
      listStr = listStr.replace(/\s+$/, '');
    }

    return listStr;
  }

  /**
     * Check and parse consecutive lists (better fix for issue #142)
     * @param {string} list
     * @param {string} listType
     * @param {boolean} trimTrailing
     * @returns {string}
     */
  function parseConsecutiveLists(list, listType, trimTrailing) {
    // check if we caught 2 or more consecutive lists by mistake
    // we use the counterRgx, meaning if listType is UL we look for UL and vice versa
    var counterRxg = listType === 'ul' ? /^ {0,2}\d+\.[ \t]/gm : /^ {0,2}[*+-][ \t]/gm,
    subLists = [],
    result = '';

    if (list.search(counterRxg) !== -1) {
      (function parseCL(txt) {
        var pos = txt.search(counterRxg);
        if (pos !== -1) {
          // slice
          result += '\n\n<' + listType + '>' + processListItems(txt.slice(0, pos), !!trimTrailing) + '</' + listType + '>\n\n';

          // invert counterType and listType
          listType = listType === 'ul' ? 'ol' : 'ul';
          counterRxg = listType === 'ul' ? /^ {0,2}\d+\.[ \t]/gm : /^ {0,2}[*+-][ \t]/gm;

          //recurse
          parseCL(txt.slice(pos));
        } else {
          result += '\n\n<' + listType + '>' + processListItems(txt, !!trimTrailing) + '</' + listType + '>\n\n';
        }
      })(list);
      for (var i = 0; i < subLists.length; ++i) {

      }
    } else {
      result = '\n\n<' + listType + '>' + processListItems(list, !!trimTrailing) + '</' + listType + '>\n\n';
    }

    return result;
  }

  // attacklab: add sentinel to hack around khtml/safari bug:
  // http://bugs.webkit.org/show_bug.cgi?id=11231
  text += '~0';

  // Re-usable pattern to match any entire ul or ol list:
  var wholeList = /^(([ ]{0,3}([*+-]|\d+[.])[ \t]+)[^\r]+?(~0|\n{2,}(?=\S)(?![ \t]*(?:[*+-]|\d+[.])[ \t]+)))/gm;

  if (globals.gListLevel) {
    text = text.replace(wholeList, function (wholeMatch, list, m2) {
      var listType = m2.search(/[*+-]/g) > -1 ? 'ul' : 'ol';
      return parseConsecutiveLists(list, listType, true);
    });
  } else {
    wholeList = /(\n\n|^\n?)(([ ]{0,3}([*+-]|\d+[.])[ \t]+)[^\r]+?(~0|\n{2,}(?=\S)(?![ \t]*(?:[*+-]|\d+[.])[ \t]+)))/gm;
    //wholeList = /(\n\n|^\n?)( {0,3}([*+-]|\d+\.)[ \t]+[\s\S]+?)(?=(~0)|(\n\n(?!\t| {2,}| {0,3}([*+-]|\d+\.)[ \t])))/g;
    text = text.replace(wholeList, function (wholeMatch, m1, list, m3) {

      var listType = m3.search(/[*+-]/g) > -1 ? 'ul' : 'ol';
      return parseConsecutiveLists(list, listType);
    });
  }

  // attacklab: strip sentinel
  text = text.replace(/~0/, '');

  text = globals.converter._dispatch('lists.after', text, options, globals);
  return text;
});

/**
     * Remove one level of line-leading tabs or spaces
     */
showdown.subParser('outdent', function (text) {
  'use strict';

  // attacklab: hack around Konqueror 3.5.4 bug:
  // "----------bug".replace(/^-/g,"") == "bug"
  text = text.replace(/^(\t|[ ]{1,4})/gm, '~0'); // attacklab: g_tab_width

  // attacklab: clean up hack
  text = text.replace(/~0/g, '');

  return text;
});

/**
     *
     */
showdown.subParser('paragraphs', function (text, options, globals) {
  'use strict';

  text = globals.converter._dispatch('paragraphs.before', text, options, globals);
  // Strip leading and trailing lines:
  text = text.replace(/^\n+/g, '');
  text = text.replace(/\n+$/g, '');

  var grafs = text.split(/\n{2,}/g),
  grafsOut = [],
  end = grafs.length; // Wrap <p> tags

  for (var i = 0; i < end; i++) {
    var str = grafs[i];
    // if this is an HTML marker, copy it
    if (str.search(/~(K|G)(\d+)\1/g) >= 0) {
      grafsOut.push(str);
    } else {
      str = showdown.subParser('spanGamut')(str, options, globals);
      str = str.replace(/^([ \t]*)/g, '<p>');
      str += '</p>';
      grafsOut.push(str);
    }
  }

  /** Unhashify HTML blocks */
  end = grafsOut.length;
  for (i = 0; i < end; i++) {
    var blockText = '',
    grafsOutIt = grafsOut[i],
    codeFlag = false;
    // if this is a marker for an html block...
    while (grafsOutIt.search(/~(K|G)(\d+)\1/) >= 0) {
      var delim = RegExp.$1,
      num = RegExp.$2;

      if (delim === 'K') {
        blockText = globals.gHtmlBlocks[num];
      } else {
        // we need to check if ghBlock is a false positive
        if (codeFlag) {
          // use encoded version of all text
          blockText = showdown.subParser('encodeCode')(globals.ghCodeBlocks[num].text);
        } else {
          blockText = globals.ghCodeBlocks[num].codeblock;
        }
      }
      blockText = blockText.replace(/\$/g, '$$$$'); // Escape any dollar signs

      grafsOutIt = grafsOutIt.replace(/(\n\n)?~(K|G)\d+\2(\n\n)?/, blockText);
      // Check if grafsOutIt is a pre->code
      if (/^<pre\b[^>]*>\s*<code\b[^>]*>/.test(grafsOutIt)) {
        codeFlag = true;
      }
    }
    grafsOut[i] = grafsOutIt;
  }
  text = grafsOut.join('\n\n');
  // Strip leading and trailing lines:
  text = text.replace(/^\n+/g, '');
  text = text.replace(/\n+$/g, '');
  return globals.converter._dispatch('paragraphs.after', text, options, globals);
});

/**
     * Run extension
     */
showdown.subParser('runExtension', function (ext, text, options, globals) {
  'use strict';

  if (ext.filter) {
    text = ext.filter(text, globals.converter, options);

  } else if (ext.regex) {
    // TODO remove this when old extension loading mechanism is deprecated
    var re = ext.regex;
    if (!re instanceof RegExp) {
      re = new RegExp(re, 'g');
    }
    text = text.replace(re, ext.replace);
  }

  return text;
});

/**
     * These are all the transformations that occur *within* block-level
     * tags like paragraphs, headers, and list items.
     */
showdown.subParser('spanGamut', function (text, options, globals) {
  'use strict';

  text = globals.converter._dispatch('spanGamut.before', text, options, globals);
  text = showdown.subParser('codeSpans')(text, options, globals);
  text = showdown.subParser('escapeSpecialCharsWithinTagAttributes')(text, options, globals);
  text = showdown.subParser('encodeBackslashEscapes')(text, options, globals);

  // Process anchor and image tags. Images must come first,
  // because ![foo][f] looks like an anchor.
  text = showdown.subParser('images')(text, options, globals);
  text = showdown.subParser('anchors')(text, options, globals);

  // Make links out of things like `<http://example.com/>`
  // Must come after _DoAnchors(), because you can use < and >
  // delimiters in inline links like [this](<url>).
  text = showdown.subParser('autoLinks')(text, options, globals);
  text = showdown.subParser('encodeAmpsAndAngles')(text, options, globals);
  text = showdown.subParser('italicsAndBold')(text, options, globals);
  text = showdown.subParser('strikethrough')(text, options, globals);

  // Do hard breaks:
  text = text.replace(/  +\n/g, ' <br />\n');

  text = globals.converter._dispatch('spanGamut.after', text, options, globals);
  return text;
});

showdown.subParser('strikethrough', function (text, options, globals) {
  'use strict';

  if (options.strikethrough) {
    text = globals.converter._dispatch('strikethrough.before', text, options, globals);
    text = text.replace(/(?:~T){2}([\s\S]+?)(?:~T){2}/g, '<del>$1</del>');
    text = globals.converter._dispatch('strikethrough.after', text, options, globals);
  }

  return text;
});

/**
     * Strip any lines consisting only of spaces and tabs.
     * This makes subsequent regexs easier to write, because we can
     * match consecutive blank lines with /\n+/ instead of something
     * contorted like /[ \t]*\n+/
     */
showdown.subParser('stripBlankLines', function (text) {
  'use strict';
  return text.replace(/^[ \t]+$/mg, '');
});

/**
     * Strips link definitions from text, stores the URLs and titles in
     * hash references.
     * Link defs are in the form: ^[id]: url "optional title"
     *
     * ^[ ]{0,3}\[(.+)\]: // id = $1  attacklab: g_tab_width - 1
     * [ \t]*
     * \n?                  // maybe *one* newline
     * [ \t]*
     * <?(\S+?)>?          // url = $2
     * [ \t]*
     * \n?                // maybe one newline
     * [ \t]*
     * (?:
     * (\n*)              // any lines skipped = $3 attacklab: lookbehind removed
     * ["(]
     * (.+?)              // title = $4
     * [")]
     * [ \t]*
     * )?                 // title is optional
     * (?:\n+|$)
     * /gm,
     * function(){...});
     *
     */
showdown.subParser('stripLinkDefinitions', function (text, options, globals) {
  'use strict';

  var regex = /^ {0,3}\[(.+)]:[ \t]*\n?[ \t]*<?(\S+?)>?(?: =([*\d]+[A-Za-z%]{0,4})x([*\d]+[A-Za-z%]{0,4}))?[ \t]*\n?[ \t]*(?:(\n*)["|'(](.+?)["|')][ \t]*)?(?:\n+|(?=~0))/gm;

  // attacklab: sentinel workarounds for lack of \A and \Z, safari\khtml bug
  text += '~0';

  text = text.replace(regex, function (wholeMatch, linkId, url, width, height, blankLines, title) {
    linkId = linkId.toLowerCase();
    globals.gUrls[linkId] = showdown.subParser('encodeAmpsAndAngles')(url); // Link IDs are case-insensitive

    if (blankLines) {
      // Oops, found blank lines, so it's not a title.
      // Put back the parenthetical statement we stole.
      return blankLines + title;

    } else {
      if (title) {
        globals.gTitles[linkId] = title.replace(/"|'/g, '&quot;');
      }
      if (options.parseImgDimensions && width && height) {
        globals.gDimensions[linkId] = {
          width: width,
          height: height };

      }
    }
    // Completely remove the definition from the text
    return '';
  });

  // attacklab: strip sentinel
  text = text.replace(/~0/, '');

  return text;
});

showdown.subParser('tables', function (text, options, globals) {
  'use strict';

  if (!options.tables) {
    return text;
  }

  var tableRgx = /^[ \t]{0,3}\|?.+\|.+\n[ \t]{0,3}\|?[ \t]*:?[ \t]*(?:-|=){2,}[ \t]*:?[ \t]*\|[ \t]*:?[ \t]*(?:-|=){2,}[\s\S]+?(?:\n\n|~0)/gm;

  function parseStyles(sLine) {
    if (/^:[ \t]*--*$/.test(sLine)) {
      return ' style="text-align:left;"';
    } else if (/^--*[ \t]*:[ \t]*$/.test(sLine)) {
      return ' style="text-align:right;"';
    } else if (/^:[ \t]*--*[ \t]*:$/.test(sLine)) {
      return ' style="text-align:center;"';
    } else {
      return '';
    }
  }

  function parseHeaders(header, style) {
    var id = '';
    header = header.trim();
    if (options.tableHeaderId) {
      id = ' id="' + header.replace(/ /g, '_').toLowerCase() + '"';
    }
    header = showdown.subParser('spanGamut')(header, options, globals);

    return '<th' + id + style + '>' + header + '</th>\n';
  }

  function parseCells(cell, style) {
    var subText = showdown.subParser('spanGamut')(cell, options, globals);
    return '<td' + style + '>' + subText + '</td>\n';
  }

  function buildTable(headers, cells) {
    var tb = '<table>\n<thead>\n<tr>\n',
    tblLgn = headers.length;

    for (var i = 0; i < tblLgn; ++i) {
      tb += headers[i];
    }
    tb += '</tr>\n</thead>\n<tbody>\n';

    for (i = 0; i < cells.length; ++i) {
      tb += '<tr>\n';
      for (var ii = 0; ii < tblLgn; ++ii) {
        tb += cells[i][ii];
      }
      tb += '</tr>\n';
    }
    tb += '</tbody>\n</table>\n';
    return tb;
  }

  text = globals.converter._dispatch('tables.before', text, options, globals);

  text = text.replace(tableRgx, function (rawTable) {

    var i,tableLines = rawTable.split('\n');

    // strip wrong first and last column if wrapped tables are used
    for (i = 0; i < tableLines.length; ++i) {
      if (/^[ \t]{0,3}\|/.test(tableLines[i])) {
        tableLines[i] = tableLines[i].replace(/^[ \t]{0,3}\|/, '');
      }
      if (/\|[ \t]*$/.test(tableLines[i])) {
        tableLines[i] = tableLines[i].replace(/\|[ \t]*$/, '');
      }
    }

    var rawHeaders = tableLines[0].split('|').map(function (s) {return s.trim();}),
    rawStyles = tableLines[1].split('|').map(function (s) {return s.trim();}),
    rawCells = [],
    headers = [],
    styles = [],
    cells = [];

    tableLines.shift();
    tableLines.shift();

    for (i = 0; i < tableLines.length; ++i) {
      if (tableLines[i].trim() === '') {
        continue;
      }
      rawCells.push(
      tableLines[i].
      split('|').
      map(function (s) {
        return s.trim();
      }));

    }

    if (rawHeaders.length < rawStyles.length) {
      return rawTable;
    }

    for (i = 0; i < rawStyles.length; ++i) {
      styles.push(parseStyles(rawStyles[i]));
    }

    for (i = 0; i < rawHeaders.length; ++i) {
      if (showdown.helper.isUndefined(styles[i])) {
        styles[i] = '';
      }
      headers.push(parseHeaders(rawHeaders[i], styles[i]));
    }

    for (i = 0; i < rawCells.length; ++i) {
      var row = [];
      for (var ii = 0; ii < headers.length; ++ii) {
        if (showdown.helper.isUndefined(rawCells[i][ii])) {

        }
        row.push(parseCells(rawCells[i][ii], styles[ii]));
      }
      cells.push(row);
    }

    return buildTable(headers, cells);
  });

  text = globals.converter._dispatch('tables.after', text, options, globals);

  return text;
});

/**
     * Swap back in all the special characters we've hidden.
     */
showdown.subParser('unescapeSpecialChars', function (text) {
  'use strict';

  text = text.replace(/~E(\d+)E/g, function (wholeMatch, m1) {
    var charCodeToReplace = parseInt(m1);
    return String.fromCharCode(charCodeToReplace);
  });
  return text;
});
module.exports = showdown;

/***/ }),

/***/ 218:
/*!*******************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/wxcomponents/wxParse/html2json.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * html2Json 改造来自: https://github.com/Jxck/html2json
 * 
 * 
 * author: Di (微信小程序开发工程师)
 * organization: WeAppDev(微信小程序开发论坛)(http://weappdev.com)
 *               垂直微信小程序开发交流社区
 * 
 * github地址: https://github.com/icindy/wxParse
 * 
 * for: 微信小程序富文本解析
 * detail : http://weappdev.com/t/wxparse-alpha0-1-html-markdown/184
 */

var __placeImgeUrlHttps = "https";
var __emojisReg = '';
var __emojisBaseSrc = '';
var __emojis = {};
var wxDiscode = __webpack_require__(/*! ./wxDiscode.js */ 219);
var HTMLParser = __webpack_require__(/*! ./htmlparser.js */ 220);
// Empty Elements - HTML 5
var empty = makeMap("area,base,basefont,br,col,frame,hr,img,input,link,meta,param,embed,command,keygen,source,track,wbr");
// Block Elements - HTML 5
var block = makeMap("br,a,code,address,article,applet,aside,audio,blockquote,button,canvas,center,dd,del,dir,div,dl,dt,fieldset,figcaption,figure,footer,form,frameset,h1,h2,h3,h4,h5,h6,header,hgroup,hr,iframe,ins,isindex,li,map,menu,noframes,noscript,object,ol,output,p,pre,section,script,table,tbody,td,tfoot,th,thead,tr,ul,video");

// Inline Elements - HTML 5
var inline = makeMap("abbr,acronym,applet,b,basefont,bdo,big,button,cite,del,dfn,em,font,i,iframe,img,input,ins,kbd,label,map,object,q,s,samp,script,select,small,span,strike,strong,sub,sup,textarea,tt,u,var");

// Elements that you can, intentionally, leave open
// (and which close themselves)
var closeSelf = makeMap("colgroup,dd,dt,li,options,p,td,tfoot,th,thead,tr");

// Attributes that have their values filled in disabled="disabled"
var fillAttrs = makeMap("checked,compact,declare,defer,disabled,ismap,multiple,nohref,noresize,noshade,nowrap,readonly,selected");

// Special Elements (can contain anything)
var special = makeMap("wxxxcode-style,script,style,view,scroll-view,block");
function makeMap(str) {
  var obj = {},items = str.split(",");
  for (var i = 0; i < items.length; i++) {
    obj[items[i]] = true;}
  return obj;
}

function q(v) {
  return '"' + v + '"';
}

function removeDOCTYPE(html) {
  return html.
  replace(/<\?xml.*\?>\n/, '').
  replace(/<.*!doctype.*\>\n/, '').
  replace(/<.*!DOCTYPE.*\>\n/, '');
}


function html2json(html, bindName) {
  //处理字符串
  html = removeDOCTYPE(html);
  html = wxDiscode.strDiscode(html);
  //生成node节点
  var bufArray = [];
  var results = {
    node: bindName,
    nodes: [],
    images: [],
    imageUrls: [] };

  HTMLParser(html, {
    start: function start(tag, attrs, unary) {
      //debug(tag, attrs, unary);
      // node for this element
      var node = {
        node: 'element',
        tag: tag };


      if (block[tag]) {
        node.tagType = "block";
      } else if (inline[tag]) {
        node.tagType = "inline";
      } else if (closeSelf[tag]) {
        node.tagType = "closeSelf";
      }

      if (attrs.length !== 0) {
        node.attr = attrs.reduce(function (pre, attr) {
          var name = attr.name;
          var value = attr.value;
          if (name == 'class') {
            // console.dir(value);
            //  value = value.join("")
            node.classStr = value;
          }
          // has multi attibutes
          // make it array of attribute
          if (name == 'style') {
            // console.dir(value);
            //  value = value.join("")
            node.styleStr = value;
          }
          if (value.match(/ /)) {
            value = value.split(' ');
          }


          // if attr already exists
          // merge it
          if (pre[name]) {
            if (Array.isArray(pre[name])) {
              // already array, push to last
              pre[name].push(value);
            } else {
              // single value, make it array
              pre[name] = [pre[name], value];
            }
          } else {
            // not exist, put it
            pre[name] = value;
          }

          return pre;
        }, {});
      }

      //对img添加额外数据
      if (node.tag === 'img') {
        node.imgIndex = results.images.length;
        var imgUrl = node.attr.src;
        imgUrl = wxDiscode.urlToHttpUrl(imgUrl, __placeImgeUrlHttps);
        node.attr.src = imgUrl;
        node.from = bindName;
        results.images.push(node);
        results.imageUrls.push(imgUrl);
      }

      if (unary) {
        // if this tag dosen't have end tag
        // like <img src="hoge.png"/>
        // add to parents
        var parent = bufArray[0] || results;
        if (parent.nodes === undefined) {
          parent.nodes = [];
        }
        parent.nodes.push(node);
      } else {
        bufArray.unshift(node);
      }
    },
    end: function end(tag) {
      //debug(tag);
      // merge into parent tag
      var node = bufArray.shift();
      if (node.tag !== tag) console.error('invalid state: mismatch end tag');

      if (bufArray.length === 0) {
        results.nodes.push(node);
      } else {
        var parent = bufArray[0];
        if (parent.nodes === undefined) {
          parent.nodes = [];
        }
        parent.nodes.push(node);
      }
    },
    chars: function chars(text) {
      //debug(text);
      var node = {
        node: 'text',
        text: text,
        textArray: transEmojiStr(text) };


      if (bufArray.length === 0) {
        results.nodes.push(node);
      } else {
        var parent = bufArray[0];
        if (parent.nodes === undefined) {
          parent.nodes = [];
        }
        parent.nodes.push(node);
      }
    },
    comment: function comment(text) {
      //debug(text);
      // var node = {
      //     node: 'comment',
      //     text: text,
      // };
      // var parent = bufArray[0];
      // if (parent.nodes === undefined) {
      //     parent.nodes = [];
      // }
      // parent.nodes.push(node);
    } });

  return results;
};

function transEmojiStr(str) {
  // var eReg = new RegExp("["+__reg+' '+"]");
  //   str = str.replace(/\[([^\[\]]+)\]/g,':$1:')

  var emojiObjs = [];
  //如果正则表达式为空
  if (__emojisReg.length == 0 || !__emojis) {
    var emojiObj = {};
    emojiObj.node = "text";
    emojiObj.text = str;
    array = [emojiObj];
    return array;
  }
  //这个地方需要调整
  str = str.replace(/\[([^\[\]]+)\]/g, ':$1:');
  var eReg = new RegExp("[:]");
  var array = str.split(eReg);
  for (var i = 0; i < array.length; i++) {
    var ele = array[i];
    var emojiObj = {};
    if (__emojis[ele]) {
      emojiObj.node = "element";
      emojiObj.tag = "emoji";
      emojiObj.text = __emojis[ele];
      emojiObj.baseSrc = __emojisBaseSrc;
    } else {
      emojiObj.node = "text";
      emojiObj.text = ele;
    }
    emojiObjs.push(emojiObj);
  }

  return emojiObjs;
}

function emojisInit() {var reg = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';var baseSrc = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "/wxParse/emojis/";var emojis = arguments.length > 2 ? arguments[2] : undefined;
  __emojisReg = reg;
  __emojisBaseSrc = baseSrc;
  __emojis = emojis;
}

module.exports = {
  html2json: html2json,
  emojisInit: emojisInit };

/***/ }),

/***/ 219:
/*!*******************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/wxcomponents/wxParse/wxDiscode.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// HTML 支持的数学符号
function strNumDiscode(str) {
  str = str.replace(/&forall;/g, '∀');
  str = str.replace(/&part;/g, '∂');
  str = str.replace(/&exists;/g, '∃');
  str = str.replace(/&empty;/g, '∅');
  str = str.replace(/&nabla;/g, '∇');
  str = str.replace(/&isin;/g, '∈');
  str = str.replace(/&notin;/g, '∉');
  str = str.replace(/&ni;/g, '∋');
  str = str.replace(/&prod;/g, '∏');
  str = str.replace(/&sum;/g, '∑');
  str = str.replace(/&minus;/g, '−');
  str = str.replace(/&lowast;/g, '∗');
  str = str.replace(/&radic;/g, '√');
  str = str.replace(/&prop;/g, '∝');
  str = str.replace(/&infin;/g, '∞');
  str = str.replace(/&ang;/g, '∠');
  str = str.replace(/&and;/g, '∧');
  str = str.replace(/&or;/g, '∨');
  str = str.replace(/&cap;/g, '∩');
  str = str.replace(/&cap;/g, '∪');
  str = str.replace(/&int;/g, '∫');
  str = str.replace(/&there4;/g, '∴');
  str = str.replace(/&sim;/g, '∼');
  str = str.replace(/&cong;/g, '≅');
  str = str.replace(/&asymp;/g, '≈');
  str = str.replace(/&ne;/g, '≠');
  str = str.replace(/&le;/g, '≤');
  str = str.replace(/&ge;/g, '≥');
  str = str.replace(/&sub;/g, '⊂');
  str = str.replace(/&sup;/g, '⊃');
  str = str.replace(/&nsub;/g, '⊄');
  str = str.replace(/&sube;/g, '⊆');
  str = str.replace(/&supe;/g, '⊇');
  str = str.replace(/&oplus;/g, '⊕');
  str = str.replace(/&otimes;/g, '⊗');
  str = str.replace(/&perp;/g, '⊥');
  str = str.replace(/&sdot;/g, '⋅');
  return str;
}

//HTML 支持的希腊字母
function strGreeceDiscode(str) {
  str = str.replace(/&Alpha;/g, 'Α');
  str = str.replace(/&Beta;/g, 'Β');
  str = str.replace(/&Gamma;/g, 'Γ');
  str = str.replace(/&Delta;/g, 'Δ');
  str = str.replace(/&Epsilon;/g, 'Ε');
  str = str.replace(/&Zeta;/g, 'Ζ');
  str = str.replace(/&Eta;/g, 'Η');
  str = str.replace(/&Theta;/g, 'Θ');
  str = str.replace(/&Iota;/g, 'Ι');
  str = str.replace(/&Kappa;/g, 'Κ');
  str = str.replace(/&Lambda;/g, 'Λ');
  str = str.replace(/&Mu;/g, 'Μ');
  str = str.replace(/&Nu;/g, 'Ν');
  str = str.replace(/&Xi;/g, 'Ν');
  str = str.replace(/&Omicron;/g, 'Ο');
  str = str.replace(/&Pi;/g, 'Π');
  str = str.replace(/&Rho;/g, 'Ρ');
  str = str.replace(/&Sigma;/g, 'Σ');
  str = str.replace(/&Tau;/g, 'Τ');
  str = str.replace(/&Upsilon;/g, 'Υ');
  str = str.replace(/&Phi;/g, 'Φ');
  str = str.replace(/&Chi;/g, 'Χ');
  str = str.replace(/&Psi;/g, 'Ψ');
  str = str.replace(/&Omega;/g, 'Ω');

  str = str.replace(/&alpha;/g, 'α');
  str = str.replace(/&beta;/g, 'β');
  str = str.replace(/&gamma;/g, 'γ');
  str = str.replace(/&delta;/g, 'δ');
  str = str.replace(/&epsilon;/g, 'ε');
  str = str.replace(/&zeta;/g, 'ζ');
  str = str.replace(/&eta;/g, 'η');
  str = str.replace(/&theta;/g, 'θ');
  str = str.replace(/&iota;/g, 'ι');
  str = str.replace(/&kappa;/g, 'κ');
  str = str.replace(/&lambda;/g, 'λ');
  str = str.replace(/&mu;/g, 'μ');
  str = str.replace(/&nu;/g, 'ν');
  str = str.replace(/&xi;/g, 'ξ');
  str = str.replace(/&omicron;/g, 'ο');
  str = str.replace(/&pi;/g, 'π');
  str = str.replace(/&rho;/g, 'ρ');
  str = str.replace(/&sigmaf;/g, 'ς');
  str = str.replace(/&sigma;/g, 'σ');
  str = str.replace(/&tau;/g, 'τ');
  str = str.replace(/&upsilon;/g, 'υ');
  str = str.replace(/&phi;/g, 'φ');
  str = str.replace(/&chi;/g, 'χ');
  str = str.replace(/&psi;/g, 'ψ');
  str = str.replace(/&omega;/g, 'ω');
  str = str.replace(/&thetasym;/g, 'ϑ');
  str = str.replace(/&upsih;/g, 'ϒ');
  str = str.replace(/&piv;/g, 'ϖ');
  str = str.replace(/&middot;/g, '·');
  return str;
}

// 

function strcharacterDiscode(str) {
  // 加入常用解析
  str = str.replace(/&nbsp;/g, ' ');
  str = str.replace(/&quot;/g, "'");
  str = str.replace(/&amp;/g, '&');
  // str = str.replace(/&lt;/g, '‹');
  // str = str.replace(/&gt;/g, '›');

  str = str.replace(/&lt;/g, '<');
  str = str.replace(/&gt;/g, '>');

  return str;
}

// HTML 支持的其他实体
function strOtherDiscode(str) {
  str = str.replace(/&OElig;/g, 'Œ');
  str = str.replace(/&oelig;/g, 'œ');
  str = str.replace(/&Scaron;/g, 'Š');
  str = str.replace(/&scaron;/g, 'š');
  str = str.replace(/&Yuml;/g, 'Ÿ');
  str = str.replace(/&fnof;/g, 'ƒ');
  str = str.replace(/&circ;/g, 'ˆ');
  str = str.replace(/&tilde;/g, '˜');
  str = str.replace(/&ensp;/g, '');
  str = str.replace(/&emsp;/g, '');
  str = str.replace(/&thinsp;/g, '');
  str = str.replace(/&zwnj;/g, '');
  str = str.replace(/&zwj;/g, '');
  str = str.replace(/&lrm;/g, '');
  str = str.replace(/&rlm;/g, '');
  str = str.replace(/&ndash;/g, '–');
  str = str.replace(/&mdash;/g, '—');
  str = str.replace(/&lsquo;/g, '‘');
  str = str.replace(/&rsquo;/g, '’');
  str = str.replace(/&sbquo;/g, '‚');
  str = str.replace(/&ldquo;/g, '“');
  str = str.replace(/&rdquo;/g, '”');
  str = str.replace(/&bdquo;/g, '„');
  str = str.replace(/&dagger;/g, '†');
  str = str.replace(/&Dagger;/g, '‡');
  str = str.replace(/&bull;/g, '•');
  str = str.replace(/&hellip;/g, '…');
  str = str.replace(/&permil;/g, '‰');
  str = str.replace(/&prime;/g, '′');
  str = str.replace(/&Prime;/g, '″');
  str = str.replace(/&lsaquo;/g, '‹');
  str = str.replace(/&rsaquo;/g, '›');
  str = str.replace(/&oline;/g, '‾');
  str = str.replace(/&euro;/g, '€');
  str = str.replace(/&trade;/g, '™');

  str = str.replace(/&larr;/g, '←');
  str = str.replace(/&uarr;/g, '↑');
  str = str.replace(/&rarr;/g, '→');
  str = str.replace(/&darr;/g, '↓');
  str = str.replace(/&harr;/g, '↔');
  str = str.replace(/&crarr;/g, '↵');
  str = str.replace(/&lceil;/g, '⌈');
  str = str.replace(/&rceil;/g, '⌉');

  str = str.replace(/&lfloor;/g, '⌊');
  str = str.replace(/&rfloor;/g, '⌋');
  str = str.replace(/&loz;/g, '◊');
  str = str.replace(/&spades;/g, '♠');
  str = str.replace(/&clubs;/g, '♣');
  str = str.replace(/&hearts;/g, '♥');

  str = str.replace(/&diams;/g, '♦');

  return str;
}

function strMoreDiscode(str) {
  str = str.replace(/\r\n/g, "");
  str = str.replace(/\n/g, "");

  str = str.replace(/code/g, "wxxxcode-style");
  return str;
}

function strDiscode(str) {
  str = strNumDiscode(str);
  str = strGreeceDiscode(str);
  str = strcharacterDiscode(str);
  str = strOtherDiscode(str);
  str = strMoreDiscode(str);
  return str;
}
function urlToHttpUrl(url, rep) {

  var patt1 = new RegExp("^//");
  var result = patt1.test(url);
  if (result) {
    url = rep + ":" + url;
  }
  return url;
}

module.exports = {
  strDiscode: strDiscode,
  urlToHttpUrl: urlToHttpUrl };

/***/ }),

/***/ 220:
/*!********************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/wxcomponents/wxParse/htmlparser.js ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * 
 * htmlParser改造自: https://github.com/blowsie/Pure-JavaScript-HTML5-Parser
 * 
 * author: Di (微信小程序开发工程师)
 * organization: WeAppDev(微信小程序开发论坛)(http://weappdev.com)
 *               垂直微信小程序开发交流社区
 * 
 * github地址: https://github.com/icindy/wxParse
 * 
 * for: 微信小程序富文本解析
 * detail : http://weappdev.com/t/wxparse-alpha0-1-html-markdown/184
 */
// Regular Expressions for parsing tags and attributes
var startTag = /^<([-A-Za-z0-9_]+)((?:\s+[a-zA-Z_:][-a-zA-Z0-9_:.]*(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)>/,
endTag = /^<\/([-A-Za-z0-9_]+)[^>]*>/,
attr = /([a-zA-Z_:][-a-zA-Z0-9_:.]*)(?:\s*=\s*(?:(?:"((?:\\.|[^"])*)")|(?:'((?:\\.|[^'])*)')|([^>\s]+)))?/g;

// Empty Elements - HTML 5
var empty = makeMap("area,base,basefont,br,col,frame,hr,img,input,link,meta,param,embed,command,keygen,source,track,wbr");

// Block Elements - HTML 5
var block = makeMap("a,address,code,article,applet,aside,audio,blockquote,button,canvas,center,dd,del,dir,div,dl,dt,fieldset,figcaption,figure,footer,form,frameset,h1,h2,h3,h4,h5,h6,header,hgroup,hr,iframe,ins,isindex,li,map,menu,noframes,noscript,object,ol,output,p,pre,section,script,table,tbody,td,tfoot,th,thead,tr,ul,video");

// Inline Elements - HTML 5
var inline = makeMap("abbr,acronym,applet,b,basefont,bdo,big,br,button,cite,del,dfn,em,font,i,iframe,img,input,ins,kbd,label,map,object,q,s,samp,script,select,small,span,strike,strong,sub,sup,textarea,tt,u,var");

// Elements that you can, intentionally, leave open
// (and which close themselves)
var closeSelf = makeMap("colgroup,dd,dt,li,options,p,td,tfoot,th,thead,tr");

// Attributes that have their values filled in disabled="disabled"
var fillAttrs = makeMap("checked,compact,declare,defer,disabled,ismap,multiple,nohref,noresize,noshade,nowrap,readonly,selected");

// Special Elements (can contain anything)
var special = makeMap("wxxxcode-style,script,style,view,scroll-view,block");

function HTMLParser(html, handler) {
  var index,chars,match,stack = [],last = html;
  stack.last = function () {
    return this[this.length - 1];
  };

  while (html) {
    chars = true;

    // Make sure we're not in a script or style element
    if (!stack.last() || !special[stack.last()]) {

      // Comment
      if (html.indexOf("<!--") == 0) {
        index = html.indexOf("-->");

        if (index >= 0) {
          if (handler.comment)
          handler.comment(html.substring(4, index));
          html = html.substring(index + 3);
          chars = false;
        }

        // end tag
      } else if (html.indexOf("</") == 0) {
        match = html.match(endTag);

        if (match) {
          html = html.substring(match[0].length);
          match[0].replace(endTag, parseEndTag);
          chars = false;
        }

        // start tag
      } else if (html.indexOf("<") == 0) {
        match = html.match(startTag);

        if (match) {
          html = html.substring(match[0].length);
          match[0].replace(startTag, parseStartTag);
          chars = false;
        }
      }

      if (chars) {
        index = html.indexOf("<");
        var text = '';
        while (index === 0) {
          text += "<";
          html = html.substring(1);
          index = html.indexOf("<");
        }
        text += index < 0 ? html : html.substring(0, index);
        html = index < 0 ? "" : html.substring(index);

        if (handler.chars)
        handler.chars(text);
      }

    } else {

      html = html.replace(new RegExp("([\\s\\S]*?)<\/" + stack.last() + "[^>]*>"), function (all, text) {
        text = text.replace(/<!--([\s\S]*?)-->|<!\[CDATA\[([\s\S]*?)]]>/g, "$1$2");
        if (handler.chars)
        handler.chars(text);

        return "";
      });


      parseEndTag("", stack.last());
    }

    if (html == last)
    throw "Parse Error: " + html;
    last = html;
  }

  // Clean up any remaining tags
  parseEndTag();

  function parseStartTag(tag, tagName, rest, unary) {
    tagName = tagName.toLowerCase();

    if (block[tagName]) {
      while (stack.last() && inline[stack.last()]) {
        parseEndTag("", stack.last());
      }
    }

    if (closeSelf[tagName] && stack.last() == tagName) {
      parseEndTag("", tagName);
    }

    unary = empty[tagName] || !!unary;

    if (!unary)
    stack.push(tagName);

    if (handler.start) {
      var attrs = [];

      rest.replace(attr, function (match, name) {
        var value = arguments[2] ? arguments[2] :
        arguments[3] ? arguments[3] :
        arguments[4] ? arguments[4] :
        fillAttrs[name] ? name : "";

        attrs.push({
          name: name,
          value: value,
          escaped: value.replace(/(^|[^\\])"/g, '$1\\\"') //"
        });
      });

      if (handler.start) {
        handler.start(tagName, attrs, unary);
      }

    }
  }

  function parseEndTag(tag, tagName) {
    // If no tag name is provided, clean shop
    if (!tagName)
    var pos = 0;

    // Find the closest opened tag of the same type
    else {
        tagName = tagName.toLowerCase();
        for (var pos = stack.length - 1; pos >= 0; pos--) {
          if (stack[pos] == tagName)
          break;}
      }
    if (pos >= 0) {
      // Close all the open elements, up the stack
      for (var i = stack.length - 1; i >= pos; i--) {
        if (handler.end)
        handler.end(stack[i]);}

      // Remove the open elements from the stack
      stack.length = pos;
    }
  }
};


function makeMap(str) {
  var obj = {},items = str.split(",");
  for (var i = 0; i < items.length; i++) {
    obj[items[i]] = true;}
  return obj;
}

module.exports = HTMLParser;

/***/ }),

/***/ 3:
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ 4:
/*!********************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/pages.json ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ 8:
/*!*******************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/wxcomponents/public/requestUrl.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.dellUrl = dellUrl;exports.dateTimePicker = dateTimePicker;exports.getMonthDay = getMonthDay;var _clientInterface = _interopRequireDefault(__webpack_require__(/*! ./clientInterface.js */ 9));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}function _toConsumableArray(arr) {return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();}function _nonIterableSpread() {throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _unsupportedIterableToArray(o, minLen) {if (!o) return;if (typeof o === "string") return _arrayLikeToArray(o, minLen);var n = Object.prototype.toString.call(o).slice(8, -1);if (n === "Object" && o.constructor) n = o.constructor.name;if (n === "Map" || n === "Set") return Array.from(o);if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);}function _iterableToArray(iter) {if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter);}function _arrayWithoutHoles(arr) {if (Array.isArray(arr)) return _arrayLikeToArray(arr);}function _arrayLikeToArray(arr, len) {if (len == null || len > arr.length) len = arr.length;for (var i = 0, arr2 = new Array(len); i < len; i++) {arr2[i] = arr[i];}return arr2;}

function jsonToStr(json) {

  var returnParam = "";
  var str = ["jsonOnly=1"];
  for (var p in json) {
    str.push(p + "=" + json[p]);
  }
  returnParam += str.join("&");
  return returnParam;
}

function dellUrl(url, params, method, random, loginToken) {
  console.log("url", url);
  var post = {
    url: "",
    method: "",
    params: {} };

  if (!params) {
    params = {};
  }
  if (!loginToken) {
    loginToken = '';
  }

  params.loginToken = loginToken;
  if (!random || method == 'post') {
    params.__ajax_random__ = Math.random();
  }

  params.mini = 1;
  params.mini = 1;
  if (!method) {
    method = "get";
  }
  if (method == "all") {
    method = post;
  }

  console.log(params);
  if (url.substring(0, 6) == "Client") {
    post.url = _clientInterface.default[url].url;
  } else {
    post.url = url;
  }
  if (!method || method == 'get') {

    post.params = '?' + jsonToStr(params) + "&apiversion=2";
    post.url = post.url + post.params;
  } else {
    post.params = params;
    post.params.apiversion = 2;
    post.params.jsonOnly = 1;
  }
  post.method = method;

  console.log(post);
  return post;
}
// 时间日期方法
function withData(param) {
  return param < 10 ? '0' + param : '' + param;
}
function getLoopArray(start, end, type) {
  console.log("===getLoopArray==", start, end, type);
  var start = start || 0;
  var end = end || 1;
  var array = [];
  var unit = '年';
  if (type == "month") {
    unit = "月";
  } else if (type == "data") {
    unit = "日";
  } else if (type == "hours") {
    unit = "时";
  } else if (type == "minutes") {
    unit = "分";
  } else if (type == "seconds") {
    unit = "秒";
  }
  for (var i = start; i <= end; i++) {
    array.push(withData(i) + unit);
  }
  console.log("===getLoopArray==", array);
  return array;
}
function getMonthDay(year, month) {
  console.log("getMonthDay", month, year);
  var flag = year % 400 == 0 || year % 4 == 0 && year % 100 != 0,array = null;
  console.log("flag", flag);
  switch (month) {
    case '01':
    case '03':
    case '05':
    case '07':
    case '08':
    case '10':
    case '12':
      array = getLoopArray(1, 31, 'data');
      break;
    case '04':
    case '06':
    case '09':
    case '11':
      array = getLoopArray(1, 30, 'data');
      break;
    case '02':
      array = flag ? getLoopArray(1, 29, 'data') : getLoopArray(1, 28, 'data');
      break;
    default:
      array = '月份格式不正确，请重新输入！';}

  return array;
}
function getNewDateArry() {
  // 当前时间的处理
  var newDate = new Date();
  var year = withData(newDate.getFullYear()) + "年",
  mont = withData(newDate.getMonth() + 1) + "月",
  date = withData(newDate.getDate()) + "日",
  hour = withData(newDate.getHours()) + "时",
  minu = withData(newDate.getMinutes()) + "分",
  seco = withData(newDate.getSeconds()) + "秒";

  return [year, mont, date, hour, minu, seco];
}
function dateTimePicker(startYear, endYear, date) {
  // 返回默认显示的数组和联动数组的声明
  console.log("==dateTimePicker==", date);
  var dateTime = [],dateTimeArray = [[], [], [], [], [], []];
  var start = startYear || 1978;
  var end = endYear || 2100;
  // 默认开始显示数据
  var defaultDate = date ? [].concat(_toConsumableArray(date.split(' ')[0].split('-')), _toConsumableArray(date.split(' ')[1].split(':'))) : getNewDateArry();
  console.log("==defaultDate==", defaultDate);
  // 处理联动列表数据
  /*年月日 时分秒*/
  var year = defaultDate[0].slice(0, -1);
  var month = defaultDate[1].slice(0, -1);
  console.log("==defaultDate==", year, month);
  dateTimeArray[0] = getLoopArray(start, end, 'year');
  dateTimeArray[1] = getLoopArray(1, 12, 'month');
  dateTimeArray[2] = getMonthDay(year, month, 'data');
  dateTimeArray[3] = getLoopArray(0, 23, 'hours');
  dateTimeArray[4] = getLoopArray(0, 59, 'minutes');
  dateTimeArray[5] = getLoopArray(0, 59, 'seconds');

  dateTimeArray.forEach(function (current, index) {
    dateTime.push(current.indexOf(defaultDate[index]));
  });

  return {
    dateTimeArray: dateTimeArray,
    dateTime: dateTime };

}

/***/ }),

/***/ 818:
/*!*******************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcode.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni) {Object.defineProperty(exports, "__esModule", { value: true });exports.default = void 0;function _toConsumableArray(arr) {return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();}function _nonIterableSpread() {throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _unsupportedIterableToArray(o, minLen) {if (!o) return;if (typeof o === "string") return _arrayLikeToArray(o, minLen);var n = Object.prototype.toString.call(o).slice(8, -1);if (n === "Object" && o.constructor) n = o.constructor.name;if (n === "Map" || n === "Set") return Array.from(o);if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);}function _iterableToArray(iter) {if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter);}function _arrayWithoutHoles(arr) {if (Array.isArray(arr)) return _arrayLikeToArray(arr);}function _arrayLikeToArray(arr, len) {if (len == null || len > arr.length) len = arr.length;for (var i = 0, arr2 = new Array(len); i < len; i++) {arr2[i] = arr[i];}return arr2;}var barcodes = __webpack_require__(/*! ./barcodes/index.js */ 819)['default'];
var _barcode = {};
(function () {
  // 初始化
  _barcode = function barcode(cont, ctxid, options, ctxsize, result) {
    var ops = {},newOptions,encodings,globaContext,ctx,globaCtxid,cbCanvasSize,cbResult;
    globaCtxid = ctxid;
    cbCanvasSize = ctxsize;
    cbResult = result;
    newOptions = Object.assign(ops, options);
    // 修成margin
    fixMargin(newOptions);
    // 处理options 数据
    if (newOptions.text == '' || cont == '') {
      return false;
    }
    // 获取ctx
    globaContext = cont;
    ctx = uni.createCanvasContext(globaCtxid, globaContext);
    // 获取编码数据
    encodings = new barcodes[newOptions.format.toUpperCase()](newOptions.text, newOptions).encode();
    var fixencodings = fixEncodings(encodings, newOptions);
    // 返回canvas实际大小
    cbCanvasSize({ width: fixencodings.width, height: fixencodings.height });
    // 绘制canvas
    setTimeout(function () {
      drawCanvas.render(newOptions, fixencodings);
    }, 50);
    // 绘制canvas
    var drawCanvas = {
      render: function render(options, encoding) {var _this = this;
        this.prepare(options, encoding);
        encoding.encodings.forEach(function (v, i) {
          _this.barcode(options, v);
          _this.text(options, v);
          _this.move(v);
        });
        this.draw(options, encoding);
      },
      barcode: function barcode(options, encoding) {
        var binary = encoding.data;
        var yFrom;
        if (options.textPosition == "top") {
          yFrom = options.marginTop + options.fontSize + options.textMargin;
        } else {
          yFrom = options.marginTop;
        }
        // 绘制条码
        ctx.fillStyle = options.lineColor;
        for (var b = 0; b < binary.length; b++) {
          var x = b * options.width + encoding.barcodePadding;
          var height = options.height;
          if (encoding.options) {
            if (encoding.options.height != undefined) {
              height = encoding.options.height;
            }
          }
          if (binary[b] === "1") {
            ctx.fillRect(x, yFrom, options.width, height);
          } else if (binary[b]) {
            ctx.fillRect(x, yFrom, options.width, height * binary[b]);
          }
        }
      },
      text: function text(options, encoding) {
        if (options.displayValue) {
          var x, y, align, size;
          if (options.textPosition == "top") {
            y = options.marginTop + options.fontSize;
          } else {
            y = options.height + options.textMargin + options.marginTop + options.fontSize;
          }
          if (encoding.options) {
            if (encoding.options.textAlign != undefined) {
              align = encoding.options.textAlign;
            }
            if (encoding.options.fontSize != undefined) {
              size = encoding.options.fontSize;
            }
          } else {
            align = options.textAlign;
            size = options.fontSize;
          }
          ctx.setFontSize(size);
          if (align == "left" || encoding.barcodePadding > 0) {
            x = 0;
            ctx.setTextAlign('left');
          } else if (align == "right") {
            x = encoding.width - 1;
            ctx.setTextAlign('right');
          } else
          {
            x = encoding.width / 2;
            ctx.setTextAlign('center');
          }
          ctx.fillStyle = options.fontColor;
          if (encoding.text != undefined) {
            ctx.fillText(encoding.text, x, y);
          }
        }
      },
      move: function move(encoding) {
        ctx.translate(encoding.width, 0);
      },
      prepare: function prepare(options, encoding) {
        // 绘制背景
        if (options.background) {
          ctx.fillStyle = options.background;
          ctx.fillRect(0, 0, encoding.width, encoding.height);
        }
        ctx.translate(options.marginLeft, 0);
      },
      draw: function draw(options, encoding) {var _this2 = this;
        ctx.draw(false, function () {
          _this2.toImgs(options, encoding);
        });
      },
      toImgs: function toImgs(options, encoding) {
        setTimeout(function () {
          uni.canvasToTempFilePath({
            width: encoding.width,
            height: encoding.height,
            destWidth: encoding.width,
            destHeight: encoding.height,
            canvasId: globaCtxid,
            fileType: 'png',
            success: function success(res) {
              cbResult(res.tempFilePath);
            },
            fail: function fail(res) {
              cbResult(res);
            },
            complete: function complete() {
              uni.hideLoading();
            } },
          globaContext);
        }, options.text.length + 100);
      } };

    // 混入canvas数据
    function fixEncodings(encoding, options) {
      var encodingArr = [],width = options.marginLeft + options.marginRight,height;
      if (!Array.isArray(encoding)) {
        encodingArr[0] = JSON.parse(JSON.stringify(encoding));
      } else {
        encodingArr = _toConsumableArray(encoding);
      }
      encodingArr.forEach(function (v, i) {
        // 获取文本宽度
        var textWidth = ctx.measureText(encodingArr[i].text ? encodingArr[i].text : '').width;
        // 获取条形码宽度
        var barcodeWidth = encodingArr[i].data.length * options.width;
        // 获取内边距
        var barcodePadding = 0;
        if (options.displayValue && barcodeWidth < textWidth) {
          if (options.textAlign == "center") {
            barcodePadding = Math.floor((textWidth - barcodeWidth) / 2);
          } else if (options.textAlign == "left") {
            barcodePadding = 0;
          } else if (options.textAlign == "right") {
            barcodePadding = Math.floor(textWidth - barcodeWidth);
          }
        }
        // 混入encodingArr[i]
        encodingArr[i].barcodePadding = barcodePadding;
        encodingArr[i].width = Math.ceil(Math.max(textWidth, barcodeWidth));
        width += encodingArr[i].width;
        if (encodingArr[i].options) {
          if (encodingArr[i].options.height != undefined) {
            encodingArr[i].height = encodingArr[i].options.height + (options.displayValue && (encodingArr[i].text ? encodingArr[i].text : '').length > 0 ? options.fontSize + options.textMargin : 0) + options.marginTop + options.marginBottom;
          } else {
            encodingArr[i].height = height = options.height + (options.displayValue && (encodingArr[i].text ? encodingArr[i].text : '').length > 0 ? options.fontSize + options.textMargin : 0) + options.marginTop + options.marginBottom;
          }
        } else {
          encodingArr[i].height = height = options.height + (options.displayValue && (encodingArr[i].text ? encodingArr[i].text : '').length > 0 ? options.fontSize + options.textMargin : 0) + options.marginTop + options.marginBottom;
        }
      });
      return { encodings: encodingArr, width: width, height: height };
    }
    // 修正Margin
    function fixMargin(options) {
      options.marginTop = options.marginTop == undefined ? options.margin : options.marginTop;
      options.marginBottom = options.marginBottom == undefined ? options.margin : options.marginBottom;
      options.marginRight = options.marginRight == undefined ? options.margin : options.marginRight;
      options.marginLeft = options.marginLeft == undefined ? options.margin : options.marginLeft;
    }
  };
})();var _default =

_barcode;exports.default = _default;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 1)["default"]))

/***/ }),

/***/ 819:
/*!**************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/index.js ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _CODE = __webpack_require__(/*! ./CODE39/ */ 820);

var _CODE2 = __webpack_require__(/*! ./CODE128/ */ 822);

var _EAN_UPC = __webpack_require__(/*! ./EAN_UPC/ */ 830);

var _ITF = __webpack_require__(/*! ./ITF/ */ 840);

var _MSI = __webpack_require__(/*! ./MSI/ */ 844);

var _pharmacode = __webpack_require__(/*! ./pharmacode/ */ 851);

var _codabar = __webpack_require__(/*! ./codabar */ 852);

var _GenericBarcode = __webpack_require__(/*! ./GenericBarcode/ */ 853);

exports.default = {
  CODE39: _CODE.CODE39,
  CODE128: _CODE2.CODE128, CODE128A: _CODE2.CODE128A, CODE128B: _CODE2.CODE128B, CODE128C: _CODE2.CODE128C,
  EAN13: _EAN_UPC.EAN13, EAN8: _EAN_UPC.EAN8, EAN5: _EAN_UPC.EAN5, EAN2: _EAN_UPC.EAN2,
  UPC: _EAN_UPC.UPC, UPCE: _EAN_UPC.UPCE,
  ITF14: _ITF.ITF14,
  ITF: _ITF.ITF,
  MSI: _MSI.MSI, MSI10: _MSI.MSI10, MSI11: _MSI.MSI11, MSI1010: _MSI.MSI1010, MSI1110: _MSI.MSI1110,
  PHARMACODE: _pharmacode.pharmacode,
  CODABAR: _codabar.codabar,
  GENERICBARCODE: _GenericBarcode.GenericBarcode };

/***/ }),

/***/ 820:
/*!*********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/CODE39/index.js ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });

exports.CODE39 = undefined;

var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ 821);

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;} // Encoding documentation:
// https://en.wikipedia.org/wiki/Code_39#Encoding

var CODE39 = function (_Barcode) {
  _inherits(CODE39, _Barcode);

  function CODE39(data, options) {
    _classCallCheck(this, CODE39);

    data = data.toUpperCase();

    // Calculate mod43 checksum if enabled
    if (options.mod43) {
      data += getCharacter(mod43checksum(data));
    }

    return _possibleConstructorReturn(this, (CODE39.__proto__ || Object.getPrototypeOf(CODE39)).call(this, data, options));
  }

  _createClass(CODE39, [{
    key: "encode",
    value: function encode() {
      // First character is always a *
      var result = getEncoding("*");

      // Take every character and add the binary representation to the result
      for (var i = 0; i < this.data.length; i++) {
        result += getEncoding(this.data[i]) + "0";
      }

      // Last character is always a *
      result += getEncoding("*");
      return {
        data: result,
        text: this.text };

    } },
  {
    key: "valid",
    value: function valid() {
      return this.data.search(/^[0-9A-Z\-\.\ \$\/\+\%]+$/) !== -1;
    } }]);


  return CODE39;
}(_Barcode3.default);

// All characters. The position in the array is the (checksum) value


var characters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "-", ".", " ", "$", "/", "+", "%", "*"];

// The decimal representation of the characters, is converted to the
// corresponding binary with the getEncoding function
var encodings = [20957, 29783, 23639, 30485, 20951, 29813, 23669, 20855, 29789, 23645, 29975, 23831, 30533, 22295, 30149, 24005, 21623, 29981, 23837, 22301, 30023, 23879, 30545, 22343, 30161, 24017, 21959, 30065, 23921, 22385, 29015, 18263, 29141, 17879, 29045, 18293, 17783, 29021, 18269, 17477, 17489, 17681, 20753, 35770];

// Get the binary representation of a character by converting the encodings
// from decimal to binary
function getEncoding(character) {
  return getBinary(characterValue(character));
}

function getBinary(characterValue) {
  return encodings[characterValue].toString(2);
}

function getCharacter(characterValue) {
  return characters[characterValue];
}

function characterValue(character) {
  return characters.indexOf(character);
}

function mod43checksum(data) {
  var checksum = 0;
  for (var i = 0; i < data.length; i++) {
    checksum += characterValue(data[i]);
  }

  checksum = checksum % 43;
  return checksum;
}

exports.CODE39 = CODE39;

/***/ }),

/***/ 821:
/*!****************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/Barcode.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

var Barcode = function Barcode(data, options) {
  _classCallCheck(this, Barcode);

  this.data = data;
  this.text = options.text || data;
  this.options = options;
};

exports.default = Barcode;

/***/ }),

/***/ 822:
/*!**********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/CODE128/index.js ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });

exports.CODE128C = exports.CODE128B = exports.CODE128A = exports.CODE128 = undefined;

var _CODE128_AUTO = __webpack_require__(/*! ./CODE128_AUTO.js */ 823);

var _CODE128_AUTO2 = _interopRequireDefault(_CODE128_AUTO);

var _CODE128A = __webpack_require__(/*! ./CODE128A.js */ 827);

var _CODE128A2 = _interopRequireDefault(_CODE128A);

var _CODE128B = __webpack_require__(/*! ./CODE128B.js */ 828);

var _CODE128B2 = _interopRequireDefault(_CODE128B);

var _CODE128C = __webpack_require__(/*! ./CODE128C.js */ 829);

var _CODE128C2 = _interopRequireDefault(_CODE128C);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

exports.CODE128 = _CODE128_AUTO2.default;
exports.CODE128A = _CODE128A2.default;
exports.CODE128B = _CODE128B2.default;
exports.CODE128C = _CODE128C2.default;

/***/ }),

/***/ 823:
/*!*****************************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/CODE128/CODE128_AUTO.js ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _CODE2 = __webpack_require__(/*! ./CODE128 */ 824);

var _CODE3 = _interopRequireDefault(_CODE2);

var _auto = __webpack_require__(/*! ./auto */ 826);

var _auto2 = _interopRequireDefault(_auto);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;}

var CODE128AUTO = function (_CODE) {
  _inherits(CODE128AUTO, _CODE);

  function CODE128AUTO(data, options) {
    _classCallCheck(this, CODE128AUTO);

    // ASCII value ranges 0-127, 200-211
    if (/^[\x00-\x7F\xC8-\xD3]+$/.test(data)) {
      var _this = _possibleConstructorReturn(this, (CODE128AUTO.__proto__ || Object.getPrototypeOf(CODE128AUTO)).call(this, (0, _auto2.default)(data), options));
    } else {
      var _this = _possibleConstructorReturn(this, (CODE128AUTO.__proto__ || Object.getPrototypeOf(CODE128AUTO)).call(this, data, options));
    }
    return _possibleConstructorReturn(_this);
  }

  return CODE128AUTO;
}(_CODE3.default);

exports.default = CODE128AUTO;

/***/ }),

/***/ 824:
/*!************************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/CODE128/CODE128.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ 821);

var _Barcode3 = _interopRequireDefault(_Barcode2);

var _constants = __webpack_require__(/*! ./constants */ 825);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;}

// This is the master class,
// it does require the start code to be included in the string
var CODE128 = function (_Barcode) {
  _inherits(CODE128, _Barcode);

  function CODE128(data, options) {
    _classCallCheck(this, CODE128);

    // Get array of ascii codes from data
    var _this = _possibleConstructorReturn(this, (CODE128.__proto__ || Object.getPrototypeOf(CODE128)).call(this, data.substring(1), options));

    _this.bytes = data.split('').map(function (_char) {
      return _char.charCodeAt(0);
    });
    return _this;
  }

  _createClass(CODE128, [{
    key: 'valid',
    value: function valid() {
      // ASCII value ranges 0-127, 200-211
      return /^[\x00-\x7F\xC8-\xD3]+$/.test(this.data);

    }

    // The public encoding function
  },
  {
    key: 'encode',
    value: function encode() {
      var bytes = this.bytes;
      // Remove the start code from the bytes and set its index
      var startIndex = bytes.shift() - 105;
      // Get start set by index
      var startSet = _constants.SET_BY_CODE[startIndex];

      if (startSet === undefined) {
        throw new RangeError('The encoding does not start with a start character.');
      }

      if (this.shouldEncodeAsEan128() === true) {
        bytes.unshift(_constants.FNC1);
      }

      // Start encode with the right type
      var encodingResult = CODE128.next(bytes, 1, startSet);

      return {
        text: this.text === this.data ? this.text.replace(/[^\x20-\x7E]/g, '') : this.text,
        data:
        // Add the start bits
        CODE128.getBar(startIndex) +
        // Add the encoded bits
        encodingResult.result +
        // Add the checksum
        CODE128.getBar((encodingResult.checksum + startIndex) % _constants.MODULO) +
        // Add the end bits
        CODE128.getBar(_constants.STOP) };

    }

    // GS1-128/EAN-128
  },
  {
    key: 'shouldEncodeAsEan128',
    value: function shouldEncodeAsEan128() {
      var isEAN128 = this.options.ean128 || false;
      if (typeof isEAN128 === 'string') {
        isEAN128 = isEAN128.toLowerCase() === 'true';
      }
      return isEAN128;
    }

    // Get a bar symbol by index
  }],
  [{
    key: 'getBar',
    value: function getBar(index) {
      return _constants.BARS[index] ? _constants.BARS[index].toString() : '';
    }

    // Correct an index by a set and shift it from the bytes array
  },
  {
    key: 'correctIndex',
    value: function correctIndex(bytes, set) {
      if (set === _constants.SET_A) {
        var charCode = bytes.shift();
        return charCode < 32 ? charCode + 64 : charCode - 32;
      } else if (set === _constants.SET_B) {
        return bytes.shift() - 32;
      } else {
        return (bytes.shift() - 48) * 10 + bytes.shift() - 48;
      }
    } },
  {
    key: 'next',
    value: function next(bytes, pos, set) {
      if (!bytes.length) {
        return { result: '', checksum: 0 };
      }

      var nextCode = void 0,
      index = void 0;

      // Special characters
      if (bytes[0] >= 200) {
        index = bytes.shift() - 105;
        var nextSet = _constants.SWAP[index];

        // Swap to other set
        if (nextSet !== undefined) {
          nextCode = CODE128.next(bytes, pos + 1, nextSet);
        }
        // Continue on current set but encode a special character
        else {
            // Shift
            if ((set === _constants.SET_A || set === _constants.SET_B) && index === _constants.SHIFT) {
              // Convert the next character so that is encoded correctly
              bytes[0] = set === _constants.SET_A ? bytes[0] > 95 ? bytes[0] - 96 : bytes[0] : bytes[0] < 32 ? bytes[0] + 96 : bytes[0];
            }
            nextCode = CODE128.next(bytes, pos + 1, set);
          }
      }
      // Continue encoding
      else {
          index = CODE128.correctIndex(bytes, set);
          nextCode = CODE128.next(bytes, pos + 1, set);
        }

      // Get the correct binary encoding and calculate the weight
      var enc = CODE128.getBar(index);
      var weight = index * pos;

      return {
        result: enc + nextCode.result,
        checksum: weight + nextCode.checksum };

    } }]);


  return CODE128;
}(_Barcode3.default);

exports.default = CODE128;

/***/ }),

/***/ 825:
/*!**************************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/CODE128/constants.js ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _SET_BY_CODE;

function _defineProperty(obj, key, value) {if (key in obj) {Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });} else {obj[key] = value;}return obj;}

// constants for internal usage
var SET_A = exports.SET_A = 0;
var SET_B = exports.SET_B = 1;
var SET_C = exports.SET_C = 2;

// Special characters
var SHIFT = exports.SHIFT = 98;
var START_A = exports.START_A = 103;
var START_B = exports.START_B = 104;
var START_C = exports.START_C = 105;
var MODULO = exports.MODULO = 103;
var STOP = exports.STOP = 106;
var FNC1 = exports.FNC1 = 207;

// Get set by start code
var SET_BY_CODE = exports.SET_BY_CODE = (_SET_BY_CODE = {}, _defineProperty(_SET_BY_CODE, START_A, SET_A), _defineProperty(_SET_BY_CODE, START_B, SET_B), _defineProperty(_SET_BY_CODE, START_C, SET_C), _SET_BY_CODE);

// Get next set by code
var SWAP = exports.SWAP = {
  101: SET_A,
  100: SET_B,
  99: SET_C };


var A_START_CHAR = exports.A_START_CHAR = String.fromCharCode(208); // START_A + 105
var B_START_CHAR = exports.B_START_CHAR = String.fromCharCode(209); // START_B + 105
var C_START_CHAR = exports.C_START_CHAR = String.fromCharCode(210); // START_C + 105

// 128A (Code Set A)
// ASCII characters 00 to 95 (0–9, A–Z and control codes), special characters, and FNC 1–4
var A_CHARS = exports.A_CHARS = "[\x00-\x5F\xC8-\xCF]";

// 128B (Code Set B)
// ASCII characters 32 to 127 (0–9, A–Z, a–z), special characters, and FNC 1–4
var B_CHARS = exports.B_CHARS = "[\x20-\x7F\xC8-\xCF]";

// 128C (Code Set C)
// 00–99 (encodes two digits with a single code point) and FNC1
var C_CHARS = exports.C_CHARS = "(\xCF*[0-9]{2}\xCF*)";

// CODE128 includes 107 symbols:
// 103 data symbols, 3 start symbols (A, B and C), and 1 stop symbol (the last one)
// Each symbol consist of three black bars (1) and three white spaces (0).
var BARS = exports.BARS = [11011001100, 11001101100, 11001100110, 10010011000, 10010001100, 10001001100, 10011001000, 10011000100, 10001100100, 11001001000, 11001000100, 11000100100, 10110011100, 10011011100, 10011001110, 10111001100, 10011101100, 10011100110, 11001110010, 11001011100, 11001001110, 11011100100, 11001110100, 11101101110, 11101001100, 11100101100, 11100100110, 11101100100, 11100110100, 11100110010, 11011011000, 11011000110, 11000110110, 10100011000, 10001011000, 10001000110, 10110001000, 10001101000, 10001100010, 11010001000, 11000101000, 11000100010, 10110111000, 10110001110, 10001101110, 10111011000, 10111000110, 10001110110, 11101110110, 11010001110, 11000101110, 11011101000, 11011100010, 11011101110, 11101011000, 11101000110, 11100010110, 11101101000, 11101100010, 11100011010, 11101111010, 11001000010, 11110001010, 10100110000, 10100001100, 10010110000, 10010000110, 10000101100, 10000100110, 10110010000, 10110000100, 10011010000, 10011000010, 10000110100, 10000110010, 11000010010, 11001010000, 11110111010, 11000010100, 10001111010, 10100111100, 10010111100, 10010011110, 10111100100, 10011110100, 10011110010, 11110100100, 11110010100, 11110010010, 11011011110, 11011110110, 11110110110, 10101111000, 10100011110, 10001011110, 10111101000, 10111100010, 11110101000, 11110100010, 10111011110, 10111101110, 11101011110, 11110101110, 11010000100, 11010010000, 11010011100, 1100011101011];

/***/ }),

/***/ 826:
/*!*********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/CODE128/auto.js ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _constants = __webpack_require__(/*! ./constants */ 825);

// Match Set functions
var matchSetALength = function matchSetALength(string) {
  return string.match(new RegExp('^' + _constants.A_CHARS + '*'))[0].length;
};
var matchSetBLength = function matchSetBLength(string) {
  return string.match(new RegExp('^' + _constants.B_CHARS + '*'))[0].length;
};
var matchSetC = function matchSetC(string) {
  return string.match(new RegExp('^' + _constants.C_CHARS + '*'))[0];
};

// CODE128A or CODE128B
function autoSelectFromAB(string, isA) {
  var ranges = isA ? _constants.A_CHARS : _constants.B_CHARS;
  var untilC = string.match(new RegExp('^(' + ranges + '+?)(([0-9]{2}){2,})([^0-9]|$)'));

  if (untilC) {
    return untilC[1] + String.fromCharCode(204) + autoSelectFromC(string.substring(untilC[1].length));
  }

  var chars = string.match(new RegExp('^' + ranges + '+'))[0];

  if (chars.length === string.length) {
    return string;
  }

  return chars + String.fromCharCode(isA ? 205 : 206) + autoSelectFromAB(string.substring(chars.length), !isA);
}

// CODE128C
function autoSelectFromC(string) {
  var cMatch = matchSetC(string);
  var length = cMatch.length;

  if (length === string.length) {
    return string;
  }

  string = string.substring(length);

  // Select A/B depending on the longest match
  var isA = matchSetALength(string) >= matchSetBLength(string);
  return cMatch + String.fromCharCode(isA ? 206 : 205) + autoSelectFromAB(string, isA);
}

// Detect Code Set (A, B or C) and format the string

exports.default = function (string) {
  var newString = void 0;
  var cLength = matchSetC(string).length;

  // Select 128C if the string start with enough digits
  if (cLength >= 2) {
    newString = _constants.C_START_CHAR + autoSelectFromC(string);
  } else {
    // Select A/B depending on the longest match
    var isA = matchSetALength(string) > matchSetBLength(string);
    newString = (isA ? _constants.A_START_CHAR : _constants.B_START_CHAR) + autoSelectFromAB(string, isA);
  }

  return newString.replace(/[\xCD\xCE]([^])[\xCD\xCE]/, // Any sequence between 205 and 206 characters
  function (match, _char) {
    return String.fromCharCode(203) + _char;
  });
};

/***/ }),

/***/ 827:
/*!*************************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/CODE128/CODE128A.js ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _CODE2 = __webpack_require__(/*! ./CODE128.js */ 824);

var _CODE3 = _interopRequireDefault(_CODE2);

var _constants = __webpack_require__(/*! ./constants */ 825);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;}

var CODE128A = function (_CODE) {
  _inherits(CODE128A, _CODE);

  function CODE128A(string, options) {
    _classCallCheck(this, CODE128A);

    return _possibleConstructorReturn(this, (CODE128A.__proto__ || Object.getPrototypeOf(CODE128A)).call(this, _constants.A_START_CHAR + string, options));
  }

  _createClass(CODE128A, [{
    key: 'valid',
    value: function valid() {
      return new RegExp('^' + _constants.A_CHARS + '+$').test(this.data);
    } }]);


  return CODE128A;
}(_CODE3.default);

exports.default = CODE128A;

/***/ }),

/***/ 828:
/*!*************************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/CODE128/CODE128B.js ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _CODE2 = __webpack_require__(/*! ./CODE128.js */ 824);

var _CODE3 = _interopRequireDefault(_CODE2);

var _constants = __webpack_require__(/*! ./constants */ 825);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;}

var CODE128B = function (_CODE) {
  _inherits(CODE128B, _CODE);

  function CODE128B(string, options) {
    _classCallCheck(this, CODE128B);

    return _possibleConstructorReturn(this, (CODE128B.__proto__ || Object.getPrototypeOf(CODE128B)).call(this, _constants.B_START_CHAR + string, options));
  }

  _createClass(CODE128B, [{
    key: 'valid',
    value: function valid() {
      return new RegExp('^' + _constants.B_CHARS + '+$').test(this.data);
    } }]);


  return CODE128B;
}(_CODE3.default);

exports.default = CODE128B;

/***/ }),

/***/ 829:
/*!*************************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/CODE128/CODE128C.js ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _CODE2 = __webpack_require__(/*! ./CODE128.js */ 824);

var _CODE3 = _interopRequireDefault(_CODE2);

var _constants = __webpack_require__(/*! ./constants */ 825);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;}

var CODE128C = function (_CODE) {
  _inherits(CODE128C, _CODE);

  function CODE128C(string, options) {
    _classCallCheck(this, CODE128C);

    return _possibleConstructorReturn(this, (CODE128C.__proto__ || Object.getPrototypeOf(CODE128C)).call(this, _constants.C_START_CHAR + string, options));
  }

  _createClass(CODE128C, [{
    key: 'valid',
    value: function valid() {
      return new RegExp('^' + _constants.C_CHARS + '+$').test(this.data);
    } }]);


  return CODE128C;
}(_CODE3.default);

exports.default = CODE128C;

/***/ }),

/***/ 830:
/*!**********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/EAN_UPC/index.js ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });

exports.UPCE = exports.UPC = exports.EAN2 = exports.EAN5 = exports.EAN8 = exports.EAN13 = undefined;

var _EAN = __webpack_require__(/*! ./EAN13.js */ 831);

var _EAN2 = _interopRequireDefault(_EAN);

var _EAN3 = __webpack_require__(/*! ./EAN8.js */ 835);

var _EAN4 = _interopRequireDefault(_EAN3);

var _EAN5 = __webpack_require__(/*! ./EAN5.js */ 836);

var _EAN6 = _interopRequireDefault(_EAN5);

var _EAN7 = __webpack_require__(/*! ./EAN2.js */ 837);

var _EAN8 = _interopRequireDefault(_EAN7);

var _UPC = __webpack_require__(/*! ./UPC.js */ 838);

var _UPC2 = _interopRequireDefault(_UPC);

var _UPCE = __webpack_require__(/*! ./UPCE.js */ 839);

var _UPCE2 = _interopRequireDefault(_UPCE);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

exports.EAN13 = _EAN2.default;
exports.EAN8 = _EAN4.default;
exports.EAN5 = _EAN6.default;
exports.EAN2 = _EAN8.default;
exports.UPC = _UPC2.default;
exports.UPCE = _UPCE2.default;

/***/ }),

/***/ 831:
/*!**********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/EAN_UPC/EAN13.js ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _get = function get(object, property, receiver) {if (object === null) object = Function.prototype;var desc = Object.getOwnPropertyDescriptor(object, property);if (desc === undefined) {var parent = Object.getPrototypeOf(object);if (parent === null) {return undefined;} else {return get(parent, property, receiver);}} else if ("value" in desc) {return desc.value;} else {var getter = desc.get;if (getter === undefined) {return undefined;}return getter.call(receiver);}};

var _constants = __webpack_require__(/*! ./constants */ 832);

var _EAN2 = __webpack_require__(/*! ./EAN */ 833);

var _EAN3 = _interopRequireDefault(_EAN2);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;} // Encoding documentation:
// https://en.wikipedia.org/wiki/International_Article_Number_(EAN)#Binary_encoding_of_data_digits_into_EAN-13_barcode

// Calculate the checksum digit
// https://en.wikipedia.org/wiki/International_Article_Number_(EAN)#Calculation_of_checksum_digit
var checksum = function checksum(number) {
  var res = number.substr(0, 12).split('').map(function (n) {
    return +n;
  }).reduce(function (sum, a, idx) {
    return idx % 2 ? sum + a * 3 : sum + a;
  }, 0);

  return (10 - res % 10) % 10;
};

var EAN13 = function (_EAN) {
  _inherits(EAN13, _EAN);

  function EAN13(data, options) {
    _classCallCheck(this, EAN13);

    // Add checksum if it does not exist
    if (data.search(/^[0-9]{12}$/) !== -1) {
      data += checksum(data);
    }

    // Adds a last character to the end of the barcode
    var _this = _possibleConstructorReturn(this, (EAN13.__proto__ || Object.getPrototypeOf(EAN13)).call(this, data, options));

    _this.lastChar = options.lastChar;
    return _this;
  }

  _createClass(EAN13, [{
    key: 'valid',
    value: function valid() {
      return this.data.search(/^[0-9]{13}$/) !== -1 && +this.data[12] === checksum(this.data);
    } },
  {
    key: 'leftText',
    value: function leftText() {
      return _get(EAN13.prototype.__proto__ || Object.getPrototypeOf(EAN13.prototype), 'leftText', this).call(this, 1, 6);
    } },
  {
    key: 'leftEncode',
    value: function leftEncode() {
      var data = this.data.substr(1, 6);
      var structure = _constants.EAN13_STRUCTURE[this.data[0]];
      return _get(EAN13.prototype.__proto__ || Object.getPrototypeOf(EAN13.prototype), 'leftEncode', this).call(this, data, structure);
    } },
  {
    key: 'rightText',
    value: function rightText() {
      return _get(EAN13.prototype.__proto__ || Object.getPrototypeOf(EAN13.prototype), 'rightText', this).call(this, 7, 6);
    } },
  {
    key: 'rightEncode',
    value: function rightEncode() {
      var data = this.data.substr(7, 6);
      return _get(EAN13.prototype.__proto__ || Object.getPrototypeOf(EAN13.prototype), 'rightEncode', this).call(this, data, 'RRRRRR');
    }

    // The "standard" way of printing EAN13 barcodes with guard bars
  },
  {
    key: 'encodeGuarded',
    value: function encodeGuarded() {
      var data = _get(EAN13.prototype.__proto__ || Object.getPrototypeOf(EAN13.prototype), 'encodeGuarded', this).call(this);

      // Extend data with left digit & last character
      if (this.options.displayValue) {
        data.unshift({
          data: '000000000000',
          text: this.text.substr(0, 1),
          options: { textAlign: 'left', fontSize: this.fontSize } });


        if (this.options.lastChar) {
          data.push({
            data: '00' });

          data.push({
            data: '00000',
            text: this.options.lastChar,
            options: { fontSize: this.fontSize } });

        }
      }

      return data;
    } }]);


  return EAN13;
}(_EAN3.default);

exports.default = EAN13;

/***/ }),

/***/ 832:
/*!**************************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/EAN_UPC/constants.js ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });

// Standard start end and middle bits
var SIDE_BIN = exports.SIDE_BIN = '101';
var MIDDLE_BIN = exports.MIDDLE_BIN = '01010';

var BINARIES = exports.BINARIES = {
  'L': [// The L (left) type of encoding
  '0001101', '0011001', '0010011', '0111101', '0100011', '0110001', '0101111', '0111011', '0110111', '0001011'],
  'G': [// The G type of encoding
  '0100111', '0110011', '0011011', '0100001', '0011101', '0111001', '0000101', '0010001', '0001001', '0010111'],
  'R': [// The R (right) type of encoding
  '1110010', '1100110', '1101100', '1000010', '1011100', '1001110', '1010000', '1000100', '1001000', '1110100'],
  'O': [// The O (odd) encoding for UPC-E
  '0001101', '0011001', '0010011', '0111101', '0100011', '0110001', '0101111', '0111011', '0110111', '0001011'],
  'E': [// The E (even) encoding for UPC-E
  '0100111', '0110011', '0011011', '0100001', '0011101', '0111001', '0000101', '0010001', '0001001', '0010111'] };


// Define the EAN-2 structure
var EAN2_STRUCTURE = exports.EAN2_STRUCTURE = ['LL', 'LG', 'GL', 'GG'];

// Define the EAN-5 structure
var EAN5_STRUCTURE = exports.EAN5_STRUCTURE = ['GGLLL', 'GLGLL', 'GLLGL', 'GLLLG', 'LGGLL', 'LLGGL', 'LLLGG', 'LGLGL', 'LGLLG', 'LLGLG'];

// Define the EAN-13 structure
var EAN13_STRUCTURE = exports.EAN13_STRUCTURE = ['LLLLLL', 'LLGLGG', 'LLGGLG', 'LLGGGL', 'LGLLGG', 'LGGLLG', 'LGGGLL', 'LGLGLG', 'LGLGGL', 'LGGLGL'];

/***/ }),

/***/ 833:
/*!********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/EAN_UPC/EAN.js ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _constants = __webpack_require__(/*! ./constants */ 832);

var _encoder = __webpack_require__(/*! ./encoder */ 834);

var _encoder2 = _interopRequireDefault(_encoder);

var _Barcode2 = __webpack_require__(/*! ../Barcode */ 821);

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;}

// Base class for EAN8 & EAN13
var EAN = function (_Barcode) {
  _inherits(EAN, _Barcode);

  function EAN(data, options) {
    _classCallCheck(this, EAN);

    // Make sure the font is not bigger than the space between the guard bars
    var _this = _possibleConstructorReturn(this, (EAN.__proto__ || Object.getPrototypeOf(EAN)).call(this, data, options));

    _this.fontSize = !options.flat && options.fontSize > options.width * 10 ? options.width * 10 : options.fontSize;

    // Make the guard bars go down half the way of the text
    _this.guardHeight = options.height + _this.fontSize / 2 + options.textMargin;
    return _this;
  }

  _createClass(EAN, [{
    key: 'encode',
    value: function encode() {
      return this.options.flat ? this.encodeFlat() : this.encodeGuarded();
    } },
  {
    key: 'leftText',
    value: function leftText(from, to) {
      return this.text.substr(from, to);
    } },
  {
    key: 'leftEncode',
    value: function leftEncode(data, structure) {
      return (0, _encoder2.default)(data, structure);
    } },
  {
    key: 'rightText',
    value: function rightText(from, to) {
      return this.text.substr(from, to);
    } },
  {
    key: 'rightEncode',
    value: function rightEncode(data, structure) {
      return (0, _encoder2.default)(data, structure);
    } },
  {
    key: 'encodeGuarded',
    value: function encodeGuarded() {
      var textOptions = { fontSize: this.fontSize };
      var guardOptions = { height: this.guardHeight };

      return [{ data: _constants.SIDE_BIN, options: guardOptions }, { data: this.leftEncode(), text: this.leftText(), options: textOptions }, { data: _constants.MIDDLE_BIN, options: guardOptions }, { data: this.rightEncode(), text: this.rightText(), options: textOptions }, { data: _constants.SIDE_BIN, options: guardOptions }];
    } },
  {
    key: 'encodeFlat',
    value: function encodeFlat() {
      var data = [_constants.SIDE_BIN, this.leftEncode(), _constants.MIDDLE_BIN, this.rightEncode(), _constants.SIDE_BIN];

      return {
        data: data.join(''),
        text: this.text };

    } }]);


  return EAN;
}(_Barcode3.default);

exports.default = EAN;

/***/ }),

/***/ 834:
/*!************************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/EAN_UPC/encoder.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _constants = __webpack_require__(/*! ./constants */ 832);

// Encode data string
var encode = function encode(data, structure, separator) {
  var encoded = data.split('').map(function (val, idx) {
    return _constants.BINARIES[structure[idx]];
  }).map(function (val, idx) {
    return val ? val[data[idx]] : '';
  });

  if (separator) {
    var last = data.length - 1;
    encoded = encoded.map(function (val, idx) {
      return idx < last ? val + separator : val;
    });
  }

  return encoded.join('');
};

exports.default = encode;

/***/ }),

/***/ 835:
/*!*********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/EAN_UPC/EAN8.js ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _get = function get(object, property, receiver) {if (object === null) object = Function.prototype;var desc = Object.getOwnPropertyDescriptor(object, property);if (desc === undefined) {var parent = Object.getPrototypeOf(object);if (parent === null) {return undefined;} else {return get(parent, property, receiver);}} else if ("value" in desc) {return desc.value;} else {var getter = desc.get;if (getter === undefined) {return undefined;}return getter.call(receiver);}};

var _EAN2 = __webpack_require__(/*! ./EAN */ 833);

var _EAN3 = _interopRequireDefault(_EAN2);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;} // Encoding documentation:
// http://www.barcodeisland.com/ean8.phtml

// Calculate the checksum digit
var checksum = function checksum(number) {
  var res = number.substr(0, 7).split('').map(function (n) {
    return +n;
  }).reduce(function (sum, a, idx) {
    return idx % 2 ? sum + a : sum + a * 3;
  }, 0);

  return (10 - res % 10) % 10;
};

var EAN8 = function (_EAN) {
  _inherits(EAN8, _EAN);

  function EAN8(data, options) {
    _classCallCheck(this, EAN8);

    // Add checksum if it does not exist
    if (data.search(/^[0-9]{7}$/) !== -1) {
      data += checksum(data);
    }

    return _possibleConstructorReturn(this, (EAN8.__proto__ || Object.getPrototypeOf(EAN8)).call(this, data, options));
  }

  _createClass(EAN8, [{
    key: 'valid',
    value: function valid() {
      return this.data.search(/^[0-9]{8}$/) !== -1 && +this.data[7] === checksum(this.data);
    } },
  {
    key: 'leftText',
    value: function leftText() {
      return _get(EAN8.prototype.__proto__ || Object.getPrototypeOf(EAN8.prototype), 'leftText', this).call(this, 0, 4);
    } },
  {
    key: 'leftEncode',
    value: function leftEncode() {
      var data = this.data.substr(0, 4);
      return _get(EAN8.prototype.__proto__ || Object.getPrototypeOf(EAN8.prototype), 'leftEncode', this).call(this, data, 'LLLL');
    } },
  {
    key: 'rightText',
    value: function rightText() {
      return _get(EAN8.prototype.__proto__ || Object.getPrototypeOf(EAN8.prototype), 'rightText', this).call(this, 4, 4);
    } },
  {
    key: 'rightEncode',
    value: function rightEncode() {
      var data = this.data.substr(4, 4);
      return _get(EAN8.prototype.__proto__ || Object.getPrototypeOf(EAN8.prototype), 'rightEncode', this).call(this, data, 'RRRR');
    } }]);


  return EAN8;
}(_EAN3.default);

exports.default = EAN8;

/***/ }),

/***/ 836:
/*!*********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/EAN_UPC/EAN5.js ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _constants = __webpack_require__(/*! ./constants */ 832);

var _encoder = __webpack_require__(/*! ./encoder */ 834);

var _encoder2 = _interopRequireDefault(_encoder);

var _Barcode2 = __webpack_require__(/*! ../Barcode */ 821);

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;} // Encoding documentation:
// https://en.wikipedia.org/wiki/EAN_5#Encoding

var checksum = function checksum(data) {
  var result = data.split('').map(function (n) {
    return +n;
  }).reduce(function (sum, a, idx) {
    return idx % 2 ? sum + a * 9 : sum + a * 3;
  }, 0);
  return result % 10;
};

var EAN5 = function (_Barcode) {
  _inherits(EAN5, _Barcode);

  function EAN5(data, options) {
    _classCallCheck(this, EAN5);

    return _possibleConstructorReturn(this, (EAN5.__proto__ || Object.getPrototypeOf(EAN5)).call(this, data, options));
  }

  _createClass(EAN5, [{
    key: 'valid',
    value: function valid() {
      return this.data.search(/^[0-9]{5}$/) !== -1;
    } },
  {
    key: 'encode',
    value: function encode() {
      var structure = _constants.EAN5_STRUCTURE[checksum(this.data)];
      return {
        data: '1011' + (0, _encoder2.default)(this.data, structure, '01'),
        text: this.text };

    } }]);


  return EAN5;
}(_Barcode3.default);

exports.default = EAN5;

/***/ }),

/***/ 837:
/*!*********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/EAN_UPC/EAN2.js ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _constants = __webpack_require__(/*! ./constants */ 832);

var _encoder = __webpack_require__(/*! ./encoder */ 834);

var _encoder2 = _interopRequireDefault(_encoder);

var _Barcode2 = __webpack_require__(/*! ../Barcode */ 821);

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;} // Encoding documentation:
// https://en.wikipedia.org/wiki/EAN_2#Encoding

var EAN2 = function (_Barcode) {
  _inherits(EAN2, _Barcode);

  function EAN2(data, options) {
    _classCallCheck(this, EAN2);

    return _possibleConstructorReturn(this, (EAN2.__proto__ || Object.getPrototypeOf(EAN2)).call(this, data, options));
  }

  _createClass(EAN2, [{
    key: 'valid',
    value: function valid() {
      return this.data.search(/^[0-9]{2}$/) !== -1;
    } },
  {
    key: 'encode',
    value: function encode() {
      // Choose the structure based on the number mod 4
      var structure = _constants.EAN2_STRUCTURE[parseInt(this.data) % 4];
      return {
        // Start bits + Encode the two digits with 01 in between
        data: '1011' + (0, _encoder2.default)(this.data, structure, '01'),
        text: this.text };

    } }]);


  return EAN2;
}(_Barcode3.default);

exports.default = EAN2;

/***/ }),

/***/ 838:
/*!********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/EAN_UPC/UPC.js ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

exports.checksum = checksum;

var _encoder = __webpack_require__(/*! ./encoder */ 834);

var _encoder2 = _interopRequireDefault(_encoder);

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ 821);

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;} // Encoding documentation:
// https://en.wikipedia.org/wiki/Universal_Product_Code#Encoding

var UPC = function (_Barcode) {
  _inherits(UPC, _Barcode);

  function UPC(data, options) {
    _classCallCheck(this, UPC);

    // Add checksum if it does not exist
    if (data.search(/^[0-9]{11}$/) !== -1) {
      data += checksum(data);
    }

    var _this = _possibleConstructorReturn(this, (UPC.__proto__ || Object.getPrototypeOf(UPC)).call(this, data, options));

    _this.displayValue = options.displayValue;

    // Make sure the font is not bigger than the space between the guard bars
    if (options.fontSize > options.width * 10) {
      _this.fontSize = options.width * 10;
    } else {
      _this.fontSize = options.fontSize;
    }

    // Make the guard bars go down half the way of the text
    _this.guardHeight = options.height + _this.fontSize / 2 + options.textMargin;
    return _this;
  }

  _createClass(UPC, [{
    key: "valid",
    value: function valid() {
      return this.data.search(/^[0-9]{12}$/) !== -1 && this.data[11] == checksum(this.data);
    } },
  {
    key: "encode",
    value: function encode() {
      if (this.options.flat) {
        return this.flatEncoding();
      } else {
        return this.guardedEncoding();
      }
    } },
  {
    key: "flatEncoding",
    value: function flatEncoding() {
      var result = "";

      result += "101";
      result += (0, _encoder2.default)(this.data.substr(0, 6), "LLLLLL");
      result += "01010";
      result += (0, _encoder2.default)(this.data.substr(6, 6), "RRRRRR");
      result += "101";

      return {
        data: result,
        text: this.text };

    } },
  {
    key: "guardedEncoding",
    value: function guardedEncoding() {
      var result = [];

      // Add the first digit
      if (this.displayValue) {
        result.push({
          data: "00000000",
          text: this.text.substr(0, 1),
          options: { textAlign: "left", fontSize: this.fontSize } });

      }

      // Add the guard bars
      result.push({
        data: "101" + (0, _encoder2.default)(this.data[0], "L"),
        options: { height: this.guardHeight } });


      // Add the left side
      result.push({
        data: (0, _encoder2.default)(this.data.substr(1, 5), "LLLLL"),
        text: this.text.substr(1, 5),
        options: { fontSize: this.fontSize } });


      // Add the middle bits
      result.push({
        data: "01010",
        options: { height: this.guardHeight } });


      // Add the right side
      result.push({
        data: (0, _encoder2.default)(this.data.substr(6, 5), "RRRRR"),
        text: this.text.substr(6, 5),
        options: { fontSize: this.fontSize } });


      // Add the end bits
      result.push({
        data: (0, _encoder2.default)(this.data[11], "R") + "101",
        options: { height: this.guardHeight } });


      // Add the last digit
      if (this.displayValue) {
        result.push({
          data: "00000000",
          text: this.text.substr(11, 1),
          options: { textAlign: "right", fontSize: this.fontSize } });

      }

      return result;
    } }]);


  return UPC;
}(_Barcode3.default);

// Calulate the checksum digit
// https://en.wikipedia.org/wiki/International_Article_Number_(EAN)#Calculation_of_checksum_digit


function checksum(number) {
  var result = 0;

  var i;
  for (i = 1; i < 11; i += 2) {
    result += parseInt(number[i]);
  }
  for (i = 0; i < 11; i += 2) {
    result += parseInt(number[i]) * 3;
  }

  return (10 - result % 10) % 10;
}

exports.default = UPC;

/***/ }),

/***/ 839:
/*!*********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/EAN_UPC/UPCE.js ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _encoder = __webpack_require__(/*! ./encoder */ 834);

var _encoder2 = _interopRequireDefault(_encoder);

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ 821);

var _Barcode3 = _interopRequireDefault(_Barcode2);

var _UPC = __webpack_require__(/*! ./UPC.js */ 838);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;} // Encoding documentation:
// https://en.wikipedia.org/wiki/Universal_Product_Code#Encoding
//
// UPC-E documentation:
// https://en.wikipedia.org/wiki/Universal_Product_Code#UPC-E

var EXPANSIONS = ["XX00000XXX", "XX10000XXX", "XX20000XXX", "XXX00000XX", "XXXX00000X", "XXXXX00005", "XXXXX00006", "XXXXX00007", "XXXXX00008", "XXXXX00009"];

var PARITIES = [["EEEOOO", "OOOEEE"], ["EEOEOO", "OOEOEE"], ["EEOOEO", "OOEEOE"], ["EEOOOE", "OOEEEO"], ["EOEEOO", "OEOOEE"], ["EOOEEO", "OEEOOE"], ["EOOOEE", "OEEEOO"], ["EOEOEO", "OEOEOE"], ["EOEOOE", "OEOEEO"], ["EOOEOE", "OEEOEO"]];

var UPCE = function (_Barcode) {
  _inherits(UPCE, _Barcode);

  function UPCE(data, options) {
    _classCallCheck(this, UPCE);

    var _this = _possibleConstructorReturn(this, (UPCE.__proto__ || Object.getPrototypeOf(UPCE)).call(this, data, options));
    // Code may be 6 or 8 digits;
    // A 7 digit code is ambiguous as to whether the extra digit
    // is a UPC-A check or number system digit.


    _this.isValid = false;
    if (data.search(/^[0-9]{6}$/) !== -1) {
      _this.middleDigits = data;
      _this.upcA = expandToUPCA(data, "0");
      _this.text = options.text || '' + _this.upcA[0] + data + _this.upcA[_this.upcA.length - 1];
      _this.isValid = true;
    } else if (data.search(/^[01][0-9]{7}$/) !== -1) {
      _this.middleDigits = data.substring(1, data.length - 1);
      _this.upcA = expandToUPCA(_this.middleDigits, data[0]);

      if (_this.upcA[_this.upcA.length - 1] === data[data.length - 1]) {
        _this.isValid = true;
      } else {
        // checksum mismatch
        return _possibleConstructorReturn(_this);
      }
    } else {
      return _possibleConstructorReturn(_this);
    }

    _this.displayValue = options.displayValue;

    // Make sure the font is not bigger than the space between the guard bars
    if (options.fontSize > options.width * 10) {
      _this.fontSize = options.width * 10;
    } else {
      _this.fontSize = options.fontSize;
    }

    // Make the guard bars go down half the way of the text
    _this.guardHeight = options.height + _this.fontSize / 2 + options.textMargin;
    return _this;
  }

  _createClass(UPCE, [{
    key: 'valid',
    value: function valid() {
      return this.isValid;
    } },
  {
    key: 'encode',
    value: function encode() {
      if (this.options.flat) {
        return this.flatEncoding();
      } else {
        return this.guardedEncoding();
      }
    } },
  {
    key: 'flatEncoding',
    value: function flatEncoding() {
      var result = "";

      result += "101";
      result += this.encodeMiddleDigits();
      result += "010101";

      return {
        data: result,
        text: this.text };

    } },
  {
    key: 'guardedEncoding',
    value: function guardedEncoding() {
      var result = [];

      // Add the UPC-A number system digit beneath the quiet zone
      if (this.displayValue) {
        result.push({
          data: "00000000",
          text: this.text[0],
          options: { textAlign: "left", fontSize: this.fontSize } });

      }

      // Add the guard bars
      result.push({
        data: "101",
        options: { height: this.guardHeight } });


      // Add the 6 UPC-E digits
      result.push({
        data: this.encodeMiddleDigits(),
        text: this.text.substring(1, 7),
        options: { fontSize: this.fontSize } });


      // Add the end bits
      result.push({
        data: "010101",
        options: { height: this.guardHeight } });


      // Add the UPC-A check digit beneath the quiet zone
      if (this.displayValue) {
        result.push({
          data: "00000000",
          text: this.text[7],
          options: { textAlign: "right", fontSize: this.fontSize } });

      }

      return result;
    } },
  {
    key: 'encodeMiddleDigits',
    value: function encodeMiddleDigits() {
      var numberSystem = this.upcA[0];
      var checkDigit = this.upcA[this.upcA.length - 1];
      var parity = PARITIES[parseInt(checkDigit)][parseInt(numberSystem)];
      return (0, _encoder2.default)(this.middleDigits, parity);
    } }]);


  return UPCE;
}(_Barcode3.default);

function expandToUPCA(middleDigits, numberSystem) {
  var lastUpcE = parseInt(middleDigits[middleDigits.length - 1]);
  var expansion = EXPANSIONS[lastUpcE];

  var result = "";
  var digitIndex = 0;
  for (var i = 0; i < expansion.length; i++) {
    var c = expansion[i];
    if (c === 'X') {
      result += middleDigits[digitIndex++];
    } else {
      result += c;
    }
  }

  result = '' + numberSystem + result;
  return '' + result + (0, _UPC.checksum)(result);
}

exports.default = UPCE;

/***/ }),

/***/ 840:
/*!******************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/ITF/index.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });

exports.ITF14 = exports.ITF = undefined;

var _ITF = __webpack_require__(/*! ./ITF */ 841);

var _ITF2 = _interopRequireDefault(_ITF);

var _ITF3 = __webpack_require__(/*! ./ITF14 */ 843);

var _ITF4 = _interopRequireDefault(_ITF3);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

exports.ITF = _ITF2.default;
exports.ITF14 = _ITF4.default;

/***/ }),

/***/ 841:
/*!****************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/ITF/ITF.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _constants = __webpack_require__(/*! ./constants */ 842);

var _Barcode2 = __webpack_require__(/*! ../Barcode */ 821);

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;}

var ITF = function (_Barcode) {
  _inherits(ITF, _Barcode);

  function ITF() {
    _classCallCheck(this, ITF);

    return _possibleConstructorReturn(this, (ITF.__proto__ || Object.getPrototypeOf(ITF)).apply(this, arguments));
  }

  _createClass(ITF, [{
    key: 'valid',
    value: function valid() {
      return this.data.search(/^([0-9]{2})+$/) !== -1;
    } },
  {
    key: 'encode',
    value: function encode() {
      var _this2 = this;

      // Calculate all the digit pairs
      var encoded = this.data.match(/.{2}/g).map(function (pair) {
        return _this2.encodePair(pair);
      }).join('');

      return {
        data: _constants.START_BIN + encoded + _constants.END_BIN,
        text: this.text };

    }

    // Calculate the data of a number pair
  },
  {
    key: 'encodePair',
    value: function encodePair(pair) {
      var second = _constants.BINARIES[pair[1]];

      return _constants.BINARIES[pair[0]].split('').map(function (first, idx) {
        return (first === '1' ? '111' : '1') + (second[idx] === '1' ? '000' : '0');
      }).join('');
    } }]);


  return ITF;
}(_Barcode3.default);

exports.default = ITF;

/***/ }),

/***/ 842:
/*!**********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/ITF/constants.js ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });

var START_BIN = exports.START_BIN = '1010';
var END_BIN = exports.END_BIN = '11101';

var BINARIES = exports.BINARIES = ['00110', '10001', '01001', '11000', '00101', '10100', '01100', '00011', '10010', '01010'];

/***/ }),

/***/ 843:
/*!******************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/ITF/ITF14.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _ITF2 = __webpack_require__(/*! ./ITF */ 841);

var _ITF3 = _interopRequireDefault(_ITF2);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;}

// Calculate the checksum digit
var checksum = function checksum(data) {
  var res = data.substr(0, 13).split('').map(function (num) {
    return parseInt(num, 10);
  }).reduce(function (sum, n, idx) {
    return sum + n * (3 - idx % 2 * 2);
  }, 0);

  return Math.ceil(res / 10) * 10 - res;
};

var ITF14 = function (_ITF) {
  _inherits(ITF14, _ITF);

  function ITF14(data, options) {
    _classCallCheck(this, ITF14);

    // Add checksum if it does not exist
    if (data.search(/^[0-9]{13}$/) !== -1) {
      data += checksum(data);
    }
    return _possibleConstructorReturn(this, (ITF14.__proto__ || Object.getPrototypeOf(ITF14)).call(this, data, options));
  }

  _createClass(ITF14, [{
    key: 'valid',
    value: function valid() {
      return this.data.search(/^[0-9]{14}$/) !== -1 && +this.data[13] === checksum(this.data);
    } }]);


  return ITF14;
}(_ITF3.default);

exports.default = ITF14;

/***/ }),

/***/ 844:
/*!******************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/MSI/index.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });

exports.MSI1110 = exports.MSI1010 = exports.MSI11 = exports.MSI10 = exports.MSI = undefined;

var _MSI = __webpack_require__(/*! ./MSI.js */ 845);

var _MSI2 = _interopRequireDefault(_MSI);

var _MSI3 = __webpack_require__(/*! ./MSI10.js */ 846);

var _MSI4 = _interopRequireDefault(_MSI3);

var _MSI5 = __webpack_require__(/*! ./MSI11.js */ 848);

var _MSI6 = _interopRequireDefault(_MSI5);

var _MSI7 = __webpack_require__(/*! ./MSI1010.js */ 849);

var _MSI8 = _interopRequireDefault(_MSI7);

var _MSI9 = __webpack_require__(/*! ./MSI1110.js */ 850);

var _MSI10 = _interopRequireDefault(_MSI9);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

exports.MSI = _MSI2.default;
exports.MSI10 = _MSI4.default;
exports.MSI11 = _MSI6.default;
exports.MSI1010 = _MSI8.default;
exports.MSI1110 = _MSI10.default;

/***/ }),

/***/ 845:
/*!****************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/MSI/MSI.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ 821);

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;} // Encoding documentation
// https://en.wikipedia.org/wiki/MSI_Barcode#Character_set_and_binary_lookup

var MSI = function (_Barcode) {
  _inherits(MSI, _Barcode);

  function MSI(data, options) {
    _classCallCheck(this, MSI);

    return _possibleConstructorReturn(this, (MSI.__proto__ || Object.getPrototypeOf(MSI)).call(this, data, options));
  }

  _createClass(MSI, [{
    key: "encode",
    value: function encode() {
      // Start bits
      var ret = "110";

      for (var i = 0; i < this.data.length; i++) {
        // Convert the character to binary (always 4 binary digits)
        var digit = parseInt(this.data[i]);
        var bin = digit.toString(2);
        bin = addZeroes(bin, 4 - bin.length);

        // Add 100 for every zero and 110 for every 1
        for (var b = 0; b < bin.length; b++) {
          ret += bin[b] == "0" ? "100" : "110";
        }
      }

      // End bits
      ret += "1001";

      return {
        data: ret,
        text: this.text };

    } },
  {
    key: "valid",
    value: function valid() {
      return this.data.search(/^[0-9]+$/) !== -1;
    } }]);


  return MSI;
}(_Barcode3.default);

function addZeroes(number, n) {
  for (var i = 0; i < n; i++) {
    number = "0" + number;
  }
  return number;
}

exports.default = MSI;

/***/ }),

/***/ 846:
/*!******************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/MSI/MSI10.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _MSI2 = __webpack_require__(/*! ./MSI.js */ 845);

var _MSI3 = _interopRequireDefault(_MSI2);

var _checksums = __webpack_require__(/*! ./checksums.js */ 847);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;}

var MSI10 = function (_MSI) {
  _inherits(MSI10, _MSI);

  function MSI10(data, options) {
    _classCallCheck(this, MSI10);

    return _possibleConstructorReturn(this, (MSI10.__proto__ || Object.getPrototypeOf(MSI10)).call(this, data + (0, _checksums.mod10)(data), options));
  }

  return MSI10;
}(_MSI3.default);

exports.default = MSI10;

/***/ }),

/***/ 847:
/*!**********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/MSI/checksums.js ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });

exports.mod10 = mod10;
exports.mod11 = mod11;
function mod10(number) {
  var sum = 0;
  for (var i = 0; i < number.length; i++) {
    var n = parseInt(number[i]);
    if ((i + number.length) % 2 === 0) {
      sum += n;
    } else {
      sum += n * 2 % 10 + Math.floor(n * 2 / 10);
    }
  }
  return (10 - sum % 10) % 10;
}

function mod11(number) {
  var sum = 0;
  var weights = [2, 3, 4, 5, 6, 7];
  for (var i = 0; i < number.length; i++) {
    var n = parseInt(number[number.length - 1 - i]);
    sum += weights[i % weights.length] * n;
  }
  return (11 - sum % 11) % 11;
}

/***/ }),

/***/ 848:
/*!******************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/MSI/MSI11.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _MSI2 = __webpack_require__(/*! ./MSI.js */ 845);

var _MSI3 = _interopRequireDefault(_MSI2);

var _checksums = __webpack_require__(/*! ./checksums.js */ 847);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;}

var MSI11 = function (_MSI) {
  _inherits(MSI11, _MSI);

  function MSI11(data, options) {
    _classCallCheck(this, MSI11);

    return _possibleConstructorReturn(this, (MSI11.__proto__ || Object.getPrototypeOf(MSI11)).call(this, data + (0, _checksums.mod11)(data), options));
  }

  return MSI11;
}(_MSI3.default);

exports.default = MSI11;

/***/ }),

/***/ 849:
/*!********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/MSI/MSI1010.js ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _MSI2 = __webpack_require__(/*! ./MSI.js */ 845);

var _MSI3 = _interopRequireDefault(_MSI2);

var _checksums = __webpack_require__(/*! ./checksums.js */ 847);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;}

var MSI1010 = function (_MSI) {
  _inherits(MSI1010, _MSI);

  function MSI1010(data, options) {
    _classCallCheck(this, MSI1010);

    data += (0, _checksums.mod10)(data);
    data += (0, _checksums.mod10)(data);
    return _possibleConstructorReturn(this, (MSI1010.__proto__ || Object.getPrototypeOf(MSI1010)).call(this, data, options));
  }

  return MSI1010;
}(_MSI3.default);

exports.default = MSI1010;

/***/ }),

/***/ 850:
/*!********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/MSI/MSI1110.js ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });


var _MSI2 = __webpack_require__(/*! ./MSI.js */ 845);

var _MSI3 = _interopRequireDefault(_MSI2);

var _checksums = __webpack_require__(/*! ./checksums.js */ 847);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;}

var MSI1110 = function (_MSI) {
  _inherits(MSI1110, _MSI);

  function MSI1110(data, options) {
    _classCallCheck(this, MSI1110);

    data += (0, _checksums.mod11)(data);
    data += (0, _checksums.mod10)(data);
    return _possibleConstructorReturn(this, (MSI1110.__proto__ || Object.getPrototypeOf(MSI1110)).call(this, data, options));
  }

  return MSI1110;
}(_MSI3.default);

exports.default = MSI1110;

/***/ }),

/***/ 851:
/*!*************************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/pharmacode/index.js ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });

exports.pharmacode = undefined;

var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ 821);

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;} // Encoding documentation
// http://www.gomaro.ch/ftproot/Laetus_PHARMA-CODE.pdf

var pharmacode = function (_Barcode) {
  _inherits(pharmacode, _Barcode);

  function pharmacode(data, options) {
    _classCallCheck(this, pharmacode);

    var _this = _possibleConstructorReturn(this, (pharmacode.__proto__ || Object.getPrototypeOf(pharmacode)).call(this, data, options));

    _this.number = parseInt(data, 10);
    return _this;
  }

  _createClass(pharmacode, [{
    key: "encode",
    value: function encode() {
      var z = this.number;
      var result = "";

      // http://i.imgur.com/RMm4UDJ.png
      // (source: http://www.gomaro.ch/ftproot/Laetus_PHARMA-CODE.pdf, page: 34)
      while (!isNaN(z) && z != 0) {
        if (z % 2 === 0) {
          // Even
          result = "11100" + result;
          z = (z - 2) / 2;
        } else {
          // Odd
          result = "100" + result;
          z = (z - 1) / 2;
        }
      }

      // Remove the two last zeroes
      result = result.slice(0, -2);

      return {
        data: result,
        text: this.text };

    } },
  {
    key: "valid",
    value: function valid() {
      return this.number >= 3 && this.number <= 131070;
    } }]);


  return pharmacode;
}(_Barcode3.default);

exports.pharmacode = pharmacode;

/***/ }),

/***/ 852:
/*!**********************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/codabar/index.js ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });

exports.codabar = undefined;

var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ 821);

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;} // Encoding specification:
// http://www.barcodeisland.com/codabar.phtml

var codabar = function (_Barcode) {
  _inherits(codabar, _Barcode);

  function codabar(data, options) {
    _classCallCheck(this, codabar);

    if (data.search(/^[0-9\-\$\:\.\+\/]+$/) === 0) {
      data = "A" + data + "A";
    }

    var _this = _possibleConstructorReturn(this, (codabar.__proto__ || Object.getPrototypeOf(codabar)).call(this, data.toUpperCase(), options));

    _this.text = _this.options.text || _this.text.replace(/[A-D]/g, '');
    return _this;
  }

  _createClass(codabar, [{
    key: "valid",
    value: function valid() {
      return this.data.search(/^[A-D][0-9\-\$\:\.\+\/]+[A-D]$/) !== -1;
    } },
  {
    key: "encode",
    value: function encode() {
      var result = [];
      var encodings = this.getEncodings();
      for (var i = 0; i < this.data.length; i++) {
        result.push(encodings[this.data.charAt(i)]);
        // for all characters except the last, append a narrow-space ("0")
        if (i !== this.data.length - 1) {
          result.push("0");
        }
      }
      return {
        text: this.text,
        data: result.join('') };

    } },
  {
    key: "getEncodings",
    value: function getEncodings() {
      return {
        "0": "101010011",
        "1": "101011001",
        "2": "101001011",
        "3": "110010101",
        "4": "101101001",
        "5": "110101001",
        "6": "100101011",
        "7": "100101101",
        "8": "100110101",
        "9": "110100101",
        "-": "101001101",
        "$": "101100101",
        ":": "1101011011",
        "/": "1101101011",
        ".": "1101101101",
        "+": "101100110011",
        "A": "1011001001",
        "B": "1001001011",
        "C": "1010010011",
        "D": "1010011001" };

    } }]);


  return codabar;
}(_Barcode3.default);

exports.codabar = codabar;

/***/ }),

/***/ 853:
/*!*****************************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/components/tki-barcode/barcodes/GenericBarcode/index.js ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true });

exports.GenericBarcode = undefined;

var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();

var _Barcode2 = __webpack_require__(/*! ../Barcode.js */ 821);

var _Barcode3 = _interopRequireDefault(_Barcode2);

function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}

function _possibleConstructorReturn(self, call) {if (!self) {throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call && (typeof call === "object" || typeof call === "function") ? call : self;}

function _inherits(subClass, superClass) {if (typeof superClass !== "function" && superClass !== null) {throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;}

var GenericBarcode = function (_Barcode) {
  _inherits(GenericBarcode, _Barcode);

  function GenericBarcode(data, options) {
    _classCallCheck(this, GenericBarcode);

    return _possibleConstructorReturn(this, (GenericBarcode.__proto__ || Object.getPrototypeOf(GenericBarcode)).call(this, data, options)); // Sets this.data and this.text
  }

  // Return the corresponding binary numbers for the data provided


  _createClass(GenericBarcode, [{
    key: "encode",
    value: function encode() {
      return {
        data: "10101010101010101010101010101010101010101",
        text: this.text };

    }

    // Resturn true/false if the string provided is valid for this encoder
  },
  {
    key: "valid",
    value: function valid() {
      return true;
    } }]);


  return GenericBarcode;
}(_Barcode3.default);

exports.GenericBarcode = GenericBarcode;

/***/ }),

/***/ 9:
/*!************************************************************************************!*\
  !*** D:/sansanWorkSpace/uni_appletCode_uni/wxcomponents/public/clientInterface.js ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = void 0;var _Client;function _defineProperty(obj, key, value) {if (key in obj) {Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });} else {obj[key] = value;}return obj;}var Client = (_Client = {
  "title": "三三云服客户端数据接口集合定义",
  "version": "1.0.0",
  "engine": "mockjs",
  "rulebase": "./interfaceRules/",
  "status": "online",
  "client_url": "https://mini.sansancloud.com/chainalliance/shuiguo",
  "project_no": "jianzhan",

  "Client.SuperShopManager.MiniStore.code": {
    "name": "门店支付二维码",
    "id": "Client.SuperShopManager.MiniApp.GetWxAuthPageLink",
    "args": [],


    "url": "/super_shop_manager_get_mini_code.html" },

  "Client.Set.SessionLocation": {
    "name": "设置会话用户的地址信息",
    "id": "Set.SessionLocation",
    "method": "all",
    "args": [{
      "name": "longitude",
      "type": "String",
      "remark": "经度" },

    {
      "name": "latitude",
      "type": "String",
      "remark": "维度" },

    {
      "name": "province",
      "type": "String",
      "remark": "省" },

    {
      "name": "city",
      "type": "String",
      "remark": "市" },

    {
      "name": "district",
      "type": "String",
      "remark": "区" },

    {
      "name": "street",
      "type": "String",
      "remark": "街道" },

    {
      "name": "streetNumber",
      "type": "String",
      "remark": "门牌号" }],



    "url": "/setLocation.html" },


  "Client.Area.SetLocateArea": {
    "name": "设置区域",
    "id": "Client.Area.SetLocateArea",
    "method": "post",
    "args": [],



    "url": "/setLocateArea.html" },


  "Client.Area.ListAllArea": {
    "name": "获取所有区域",
    "id": "Client.Area.ListAllArea",
    "method": "get",
    "args": [],



    "url": "/list_all_area.html" },


  "Client.Config.SetDeviceWidth": {
    "name": "设置设备宽度",
    "id": "Client.Config.SetDeviceWidth",
    "method": "post",
    "args": [{
      "name": "deviceWidth",
      "type": "double",
      "remark": "设备宽度" }],



    "url": "/set_device_width.html" },


  "Client.GetPlatformSetting": {
    "name": "获得平台设置",
    "id": "Client.GetPlatformSetting",
    "method": "get",
    "args": [{}],




    "url": "/get_platform_setting.html" },


  "Client.Get.ProductShopViewListMore": {
    "name": "获得产品列表（店铺视角）",
    "id": "Client.Get.ProductShopViewListMore",
    "method": "get",
    "args": [{
      "name": "categoryId",
      "type": "int",
      "remark": "产品类型ID" },

    {
      "name": "page",
      "type": "String",
      "remark": "页数" },

    {
      "name": "belongShop",
      "type": "int",
      "remark": "归属店铺ID" }],



    "url": "/more_product_shop_view_list.html" },



  "Client.Get.ProductListMore": {
    "name": "获得产品列表",
    "id": "Client.Get.ProductListMore",
    "method": "get",
    "args": [{
      "name": "categoryId",
      "type": "int",
      "remark": "产品类型ID" },

    {
      "name": "page",
      "type": "String",
      "remark": "页数" },
    {
      "name": "saleTypeId",
      "type": "String",
      "remark": "销售类型ID" },

    {
      "name": "belongShop",
      "type": "int",
      "remark": "归属店铺ID" }],



    "url": "/more_product_list.html" },



  "Client.Get.SaleProductListMore": {
    "name": "获得产品列表",
    "id": "Client.Get.SaleProductListMore",
    "method": "get",
    "args": [{
      "name": "saleTypeId",
      "type": "int",
      "remark": "销售类型ID" },

    {
      "name": "page",
      "type": "String",
      "remark": "页数" }],


    "url": "/more_sale_product_list.html" },


  "Client.Get.GetDeepSaleTypeItems": {
    "name": "深度遍历获得销售类型子项",
    "id": "Client.Get.GetDeepSaleTypeItems",
    "method": "get",
    "args": [{
      "name": "saleTypeId",
      "type": "int",
      "remark": "销售类型ID" }],



    "url": "/get_deep_sale_type_items.html" },


  "Client.Search.GetShopProduct": {
    "name": "查看店铺分类",
    "id": "Search.ShopTypes",
    "method": "get",
    "args": [{
      "name": "page",
      "type": "int",
      "remark": "页数" },

    {
      "name": "productName",
      "type": "String",
      "remark": "产品名" },

    {
      "name": "shopProductType",
      "type": "int",
      "remark": "店铺分类" },

    {
      "name": "shopId",
      "type": "int",
      "remark": "店铺ID" }],


    "url": "/search_shop_items_list.html" },



  "Client.Lease.CreateLeaseRentOrder": {
    "name": "创建租赁费用订单",
    "id": "Client.Lease.CreateLeaseRentOrder",
    "method": "post",
    "args": [{
      "name": "leaseRecordId",
      "type": "int",
      "remark": "平台用户ID" }],

    "url": "/create_lease_rent_order.html" },



  "Client.Lease.GetUserLeaseRecord": {
    "name": "用户租赁记录",
    "id": "Client.Lease.UserLeaseRecords",
    "method": "get",
    "args": [{
      "name": "leaseRecordId",
      "type": "int",
      "remark": "平台用户ID" }],

    "url": "/get_lease_record_detail.html" },


  "Client.User.GetVisitItems": {
    "name": "获取用户访问记录",
    "id": "Client.User.GetVisitItems",
    "method": "get",
    "args": [

    {
      "name": "page",
      "type": "int",
      "remark": "page" },
    {
      "name": "visitType",
      "type": "int",
      "remark": "访问类型 1 产品 2 店铺 3 新闻 4 用户" }],



    "url": "/get_user_visit_items.html" },


  "Client.User.DiaryCenterDetail": {
    "name": "日志中心详情",
    "id": "Client.User.DiaryCenterDetail",
    "method": "get",
    "args": [

    {
      "name": "platformUserId",
      "type": "int",
      "remark": "平台用户ID" }],


    "url": "/get_diary_center_detail.html" },


  "Client.User.NearUsers": {
    "name": "根据地址位置获得最近用户列表",
    "id": "Client.User.NearUsers",
    "method": "get",
    "args": [

    {
      "name": "page",
      "type": "int",
      "remark": "分页页码" }],


    "url": "/get_near_users.html" },


  "Client.Get.NearShopsMore": {
    "name": "获得附近店铺",
    "id": "Client.Get.NearShopsMore",
    "method": "get",
    "args": [{
      "name": "longitude",
      "type": "String",
      "remark": "精度" },

    {
      "name": "page",
      "type": "String",
      "remark": "页数" },

    {
      "name": "latitude",
      "type": "String",
      "remark": "维度" },

    {
      "name": "shopName",
      "type": "String",
      "remark": "店铺名" }],



    "url": "/more_near_shops.html" },





  "Client.Get.NewsList": {
    "name": "获得新闻列表",
    "id": "Client.Get.NewsList",
    "method": "get",
    "args": [

    {
      "name": "page",
      "type": "String",
      "remark": "页数" },


    {
      "name": "newsTypeId",
      "type": "int",
      "remark": "新闻类型ID" }],


    "url": "/more_news_list.html" },





  "Client.Get.NewsBbsList": {
    "name": "获得BBS帖子列表",
    "id": "Client.Get.NewsBbsList",
    "method": "get",
    "args": [

    {
      "name": "page",
      "type": "String",
      "remark": "页数" },


    {
      "name": "newsTypeId",
      "type": "int",
      "remark": "新闻类型ID" }],


    "url": "/more_news_bbs_list.html" },




  "Client.Get.NewsType": {
    "name": "获取新闻类型",
    "id": "Client.Get.NewsType",
    "method": "get",
    "args": [


    {
      "name": "newsTypeId",
      "type": "int",
      "remark": "新闻类型ID" }],


    "url": "/get_news_type.html" },


  "Client.Get.UserNewsBbsList": {
    "name": "获得用户BBS帖子列表",
    "id": "Client.Get.UserNewsBbsList",
    "method": "get",
    "args": [

    {
      "name": "page",
      "type": "String",
      "remark": "页数" },


    {
      "name": "newsTypeId",
      "type": "int",
      "remark": "新闻类型ID" }],


    "url": "/user_news_bbs_list.html" },


  "Client.Bbs.NewsDetail": {
    "name": "获得帖子详情",
    "id": "Client.Bbs.NewsDetail",
    "method": "get",
    "args": [


    {
      "name": "newsId",
      "type": "int",
      "remark": "新闻ID" }],


    "url": "/get_news_bbs_detail.html" },



  "Client.Bbs.NewsBbsComments": {
    "name": "获得新闻评论",
    "id": "Client.Bbs.NewsBbsComments",
    "method": "get",
    "args": [

    {
      "name": "page",
      "type": "int",
      "remark": "页数" },

    {
      "name": "additionTopic",
      "type": "int",
      "remark": "补充主题" },

    {
      "name": "newsId",
      "type": "int",
      "remark": "新闻ID" }],


    "url": "/get_news_bbs_comments.html" },




  "Client.Bbs.NewsBbsRecomments": {
    "name": "获得评论的再评论",
    "id": "Client.Bbs.NewsBbsRecomments",
    "method": "get",
    "args": [

    {
      "name": "page",
      "type": "int",
      "remark": "页数" },

    {
      "name": "commentId",
      "type": "int",
      "remark": "回复Id" }],



    "url": "/get_news_bbs_recomments.html" },



  "Client.Bbs.AddBbsRecomment": {
    "name": "新增回复的评论",
    "id": "Client.Bbs.AddBbsRecomment",
    "method": "post",
    "args": [

    {
      "name": "commentId",
      "type": "int",
      "remark": "评论ID" },

    {
      "name": "comment",
      "type": "String",
      "remark": "评论内容" }],


    "url": "/add_bbs_recomments.html" },


  "Client.Bbs.AddBbsComment": {
    "name": "新增新闻评论",
    "id": "Client.Bbs.AddBbsComment",
    "method": "post",
    "args": [

    {
      "name": "newsId",
      "type": "int",
      "remark": "新闻ID" },

    {
      "name": "comment",
      "type": "String",
      "remark": "评论内容" }],


    "url": "/add_bbs_comments.html" },



  "Client.AfterSale.SetAfterSaleUserFeedback": {
    "name": "用户设置反馈信息",
    "id": "Client.AfterSale.SetAfterSaleUserFeedback",
    "method": "post",
    "args": [{
      "name": "orderNo",
      "type": "String",
      "remark": "售后工单号" },
    {
      "name": "afterSaleCaseId",
      "type": "int",
      "remark": "caseId" },
    {
      "name": "afterSalePhaseId",
      "type": "int",
      "remark": "步骤id" },
    {
      "name": "userPhotos",
      "type": "String",
      "remark": "图像json" },
    {
      "name": "userInput",
      "type": "String",
      "remark": "用户输入描述" },
    {
      "name": "userResult",
      "type": "int",
      "remark": "用户选择答案" }],

    "url": "/set_after_sale_user_feedback.html" },


  "Client.AfterSale.GetAfterSaleBackExpress": {
    "name": "获得售后工单物流",
    "id": "Client.AfterSale.GetAfterSaleBackExpress",
    "method": "get",
    "args": [{
      "name": "orderNo",
      "type": "String",
      "remark": "售后工单号" }],

    "url": "/get_after_sale_back_express_detail.html" },



  "Client.Bbs.DeleteBbsComment": {
    "name": "新增新闻评论",
    "id": "Client.Bbs.AddBbsComment",
    "method": "post",
    "args": [

    {
      "name": "commentId",
      "type": "int",
      "remark": "新闻评论ID" }],


    "url": "/delete_bbs_comments.html" },



  "Client.Bbs.DeleteBbsNews": {
    "name": "新增新闻评论",
    "id": "Client.Bbs.DeleteBbsNews",
    "method": "post",
    "args": [

    {
      "name": "newsId",
      "type": "int",
      "remark": "新闻ID" }],


    "url": "/delete_bbs_news.html" },



  "Client.Bbs.CreateBbsNews": {
    "name": "新增新闻",
    "id": "Client.Bbs.CreateBbsNews",
    "method": "post",
    "args": [

    {
      "name": "title",
      "type": "String",
      "remark": "标题" },

    {
      "name": "content",
      "type": "String",
      "remark": "内容" },

    {
      "name": "imagePath",
      "type": "String",
      "remark": "封面" },

    {
      "name": "description",
      "type": "String",
      "remark": "描述" },

    {
      "name": "newsType",
      "type": "int",
      "remark": "新闻类型" }],


    "url": "/add_bbs_news.html" },



  "Client.Bbs.ChangeBbsNewsName": {
    "name": "新增新闻",
    "id": "Client.Bbs.ChangeBbsNewsName",
    "method": "post",
    "args": [

    {
      "name": "title",
      "type": "String",
      "remark": "标题" }],



    "url": "/change_bbs_news_name.html" },


  "Client.GetVerifyCode": {
    "name": "获取验证码",
    "id": "Client.GetVerifyCode",
    "method": "post",
    "args": [{
      "name": "telno",
      "type": "String",
      "remark": "手机号码" },

    {
      "name": "type",
      "type": "int",
      "remark": "不传输注册时使用 1 找回密码时使用   2绑定手机号码时使用  3 手机验证码登陆时 " }],


    "url": "/get_verify_code.html" },


  "Client.BindTelno": {
    "name": "绑定电话号码",
    "id": "Client.BindTelno",
    "method": "post",
    "args": [{
      "name": "telno",
      "type": "String",
      "remark": "手机号码" },

    {
      "name": "verifyCode",
      "type": "String",
      "remark": "验证码" }],


    "url": "/bind_telno.html" },



  "Client.User.BindWxOpenId": {
    "name": "绑定微信openid",
    "id": "Client.User.BindWxOpenId",
    "method": "post",
    "args": [{
      "name": "unionId",
      "type": "String",
      "remark": "微信unionid" },
    {
      "name": "openid",
      "type": "String",
      "remark": "微信openid" },
    {
      "name": "wxAppLogin",
      "type": "int",
      "remark": "0 微信 1 app  2 pc" }],

    "url": "/bind_wx_openid.html" },


  "Client.UnBindTelno": {
    "name": "解除绑定电话号码",
    "id": "Client.UnBindTelno",
    "method": "post",
    "args": [],


    "url": "/un_bind_telno.html" },


  "Client.User.BindErp": {
    "name": "绑定Erp",
    "id": "Client.User.BindErp",
    "method": "post",
    "args": [{
      "name": "username",
      "type": "String",
      "remark": "erp用户名" },

    {
      "name": "password",
      "type": "String",
      "remark": "erp密码" }],


    "url": "/bind_erp.html" },


  "Client.User.Regist": {
    "name": "用户注册",
    "id": "Client.User.Regist",
    "method": "all",
    "args": [{
      "name": "username",
      "type": "String",
      "remark": "用户名" },

    {
      "name": "password",
      "type": "String",
      "remark": "密码" },

    {
      "name": "password2",
      "type": "String",
      "remark": "密码2" },

    {
      "name": "verifyCode",
      "type": "String",
      "remark": "验证码" }],


    "url": "/regist.html" },




  "Client.User.Regist2": {
    "name": "用户注册",
    "id": "Client.User.Regist2",
    "method": "all",
    "args": [{
      "name": "username",
      "type": "String",
      "remark": "用户名" },

    {
      "name": "password",
      "type": "String",
      "remark": "密码" },

    {
      "name": "password2",
      "type": "String",
      "remark": "密码2" },

    {
      "name": "nickname",
      "type": "String",
      "remark": "昵称" }],


    "url": "/regist2.html" },


  "Client.User.ChangePassword": {
    "name": "密码变更",
    "id": "Client.User.ChangePassword",
    "method": "post",
    "args": [{
      "name": "username",
      "type": "String",
      "remark": "用户名" },

    {
      "name": "password",
      "type": "String",
      "remark": "密码" },

    {
      "name": "password2",
      "type": "String",
      "remark": "密码2" },

    {
      "name": "verifyCode",
      "type": "String",
      "remark": "验证码" }],


    "url": "/change_password.html" },



  "Client.User.ChangePassword2": {
    "name": "密码变更",
    "id": "Client.User.ChangePassword2",
    "method": "post",
    "args": [{
      "name": "oldPassword",
      "type": "String",
      "remark": "旧密码" },

    {
      "name": "newPassword",
      "type": "String",
      "remark": "新密码" },

    {
      "name": "newPassword2",
      "type": "String",
      "remark": "新密码2" }],


    "url": "/change_password2.html" },



  "Client.User.ChangeUserInfo": {
    "name": "用户资料变更",
    "id": "Client.User.ChangeUserInfo",
    "method": "post",
    "args": [{
      "name": "headimg",
      "type": "String",
      "remark": "头像" },

    {
      "name": "nickname",
      "type": "String",
      "remark": "昵称" },

    {
      "name": "telno",
      "type": "String",
      "remark": "电话" },

    {
      "name": "userTip",
      "type": "String",
      "remark": "个性签名" },

    {
      "name": "sex",
      "type": "int",
      "remark": "性别" }],


    "url": "/change_user_info.html" },


  "Client.User.Login": {
    "name": "登陆",
    "id": "Client.User.Login",
    "method": "all",
    "args": [

    {
      "name": "loginType",
      "type": "int",
      "remark": "登录类型0 普通  1 微信  2 短信验证码" },

    {
      "name": "telno",
      "type": "String",
      "remark": "绑定手机号码" },
    {
      "name": "verifyCode",
      "type": "String",
      "remark": "验证码" },

    {
      "name": "username",
      "type": "String",
      "remark": "用户名" },


    {
      "name": "password",
      "type": "String",
      "remark": "密码" }],


    "url": "/login.html" },



  "Client.User.WxPcCodeLogin": {
    "name": "PC授权码登陆",
    "id": "Client.User.WxPcCodeLogin",
    "method": "post",
    "args": [{
      "name": "code",
      "type": "String",
      "remark": "授权码" }],

    "url": "/wx_pc_code_login.html" },


  "Client.User.WxAppCodeLogin": {
    "name": "app授权码登陆",
    "id": "Client.User.WxAppCodeLogin",
    "method": "post",
    "args": [

    {
      "name": "code",
      "type": "String",
      "remark": "授权码" },

    {
      "name": "refreshToken",
      "type": "String",
      "remark": "刷新授权码" }],


    "url": "/wx_app_code_login.html" },


  "Client.User.AddressList": {
    "name": "获得地址列表",
    "id": "Client.User.AddressList",
    "method": "get",
    "args": [],



    "url": "/get_login_user_address_list.html" },


  "Client.User.SelectAddressList": {
    "name": "获得地址列表",
    "id": "Client.User.SelectAddressList",
    "method": "get",
    "args": [],



    "url": "/select_login_user_address_list.html" },


  "Client.User.AddressAdd": {
    "name": "新增地址",
    "id": "Client.User.AddressAdd",
    "method": "all",
    "args": [{
      "name": "contanctName",
      "type": "String",
      "remark": "姓名" },

    {
      "name": "telno",
      "type": "String",
      "remark": "电话号码" },
    {
      "name": "province",
      "type": "String",
      "remark": "省份" },
    {
      "name": "city",
      "type": "String",
      "remark": "市" },
    {
      "name": "district",
      "type": "String",
      "remark": "区（县）" },
    {
      "name": "detail",
      "type": "String",
      "remark": "详细地址）" },
    {
      "name": "longitude",
      "type": "String",
      "remark": "经度）" },
    {
      "name": "latitude",
      "type": "String",
      "remark": "维度）" },
    {
      "name": "defaultAddress",
      "type": "int",
      "remark": "默认地址）" }],



    "url": "/add_address.html" },



  "Client.User.AddressModify": {
    "name": "修改地址",
    "id": "Client.User.AddressModify",
    "method": "all",
    "args": [{
      "name": "contanctName",
      "type": "String",
      "remark": "姓名" },

    {
      "name": "telno",
      "type": "String",
      "remark": "电话号码" },
    {
      "name": "province",
      "type": "String",
      "remark": "省份" },
    {
      "name": "city",
      "type": "String",
      "remark": "市" },
    {
      "name": "district",
      "type": "String",
      "remark": "区（县）" },
    {
      "name": "detail",
      "type": "String",
      "remark": "详细地址）" },
    {
      "name": "longitude",
      "type": "String",
      "remark": "经度）" },
    {
      "name": "latitude",
      "type": "String",
      "remark": "维度）" },
    {
      "name": "defaultAddress",
      "type": "int",
      "remark": "默认地址）" },
    {
      "name": "addressId",
      "type": "int",
      "remark": "地址ID" }],



    "url": "/edit_address.html" },



  "Client.User.AddressDelete": {
    "name": "删除地址",
    "id": "Client.User.AddressDelete",
    "method": "post",
    "args": [{
      "name": "addressId",
      "type": "int",
      "remark": "地址ID" }],



    "url": "/delete_address.html" },


  "Client.User.AddressSetDefault": {
    "name": "修改地址",
    "id": "Client.User.AddressSetDefault",
    "method": "post",
    "args": [{
      "name": "addressId",
      "type": "int",
      "remark": "地址ID" }],



    "url": "/set_default_address.html" },




  "Client.User.ChangeCarItemCount": {
    "name": "修改购物车项目数量",
    "id": "Client.User.ChangeCarItemCount",
    "method": "post",
    "args": [{
      "name": "productId",
      "type": "int",
      "remark": "商品ID" },

    {
      "name": "shopId",
      "type": "int",
      "remark": "店铺ID" },


    {
      "name": "count",
      "type": "int",
      "remark": "数量" },

    {
      "name": "cartesionId",
      "type": "int",
      "remark": "规格集ID" },

    {
      "name": "type",
      "type": "String",
      "remark": "类型(add dec change)" }],



    "url": "/change_shopping_car_item.html" },




  "Client.User.CarItemList": {
    "name": "获得购物车列表",
    "id": "Client.User.CarItemList",
    "method": "get",
    "args": [],



    "url": "/get_shopping_car_list_item.html" },





  "Client.User.CarItemdDelete": {
    "name": "从购物车中删除商品",
    "id": "Client.User.CarItemdDelete",
    "method": "post",
    "args": [{
      "name": "shopId",
      "type": "String",
      "remark": "店铺ID" },

    {
      "name": "type",
      "type": "String",
      "remark": "类型(all selected shopall)" },
    {
      "name": "selectedIds",
      "type": "String",
      "remark": "选中ID 以逗号分隔开" }],




    "url": "/delete_shopping_car_list_item.html" },



  "Client.User.CarItemsCreateOrder": {
    "name": "选中的商品生成订单",
    "id": "Client.User.CarItemsCreateOrder",
    "method": "post",
    "args": [{
      "name": "shopId",
      "type": "int",
      "remark": "店铺ID" },

    {
      "name": "selectedIds",
      "type": "String",
      "remark": "选中ID 以逗号分隔开" },
    {
      "name": "promotionId",
      "type": "String",
      "remark": "活动id" }],




    "url": "/shopping_car_list_item_create_order.html" },


  "Client.User.ListPromotionsByCarItems": {
    "name": "根据选中的商品获取活动列表",
    "id": "Client.User.ListPromotionsByCarItems",
    "method": "post",
    "args": [{
      "name": "shopId",
      "type": "int",
      "remark": "店铺ID" },

    {
      "name": "selectedIds",
      "type": "String",
      "remark": "选中ID 以逗号分隔开" }],



    "url": "/list_promotions_by_car_items.html" },




  "Client.User.BuyNow": {
    "name": "立即购买商品",
    "id": "Client.User.BuyNow",
    "method": "post",
    "args": [{
      "name": "shopId",
      "type": "int",
      "remark": "店铺ID" },

    {
      "name": "productId",
      "type": "int",
      "remark": "产品ID" },
    {
      "name": "orderType",
      "type": "int",
      "remark": "订单类型 0 普通订单 1 服务类订单 3 预售  4充值  5租赁 6租赁消费支付" },

    {
      "name": "itemCount",
      "type": "int",
      "remark": "产品数量" }],


    "url": "/buy_now.html" },



  "Client.User.CreateRechargeOrder": {
    "name": "创建充值订单",
    "id": "Client.User.CreateRechargeOrder",
    "method": "post",
    "args": [

    {
      "name": "payType",
      "type": "int",
      "remark": "支付方式" },

    {
      "name": "rechargeAmount",
      "type": "BigDecimal",
      "remark": "充值金额" }],


    "url": "/create_recharge_order.html" },



  "Client.User.CommitPlatformUserQiyeInfo": {
    "name": "提交平台用户企业信息",
    "id": "Client.User.CommitPlatformUserQiyeInfo",
    "method": "post",
    "args": [],


    "url": "/commit_platform_user_qiye_info.html" },




  "Client.User.FollowUsers": {
    "name": "用户关注的用户",
    "id": "Client.User.FollowUsers",
    "method": "get",
    "args": [

    {
      "name": "platformUserId",
      "type": "int",
      "remark": "平台用户ID" },


    {
      "name": "page",
      "type": "int",
      "remark": "页数" }],


    "url": "/get_follow_users.html" },




  "Client.User.UnFollowUser": {
    "name": "取消关注用户",
    "id": "Client.User.UnFollowUser",
    "method": "post",
    "args": [

    {
      "name": "followPlatformUserId",
      "type": "int",
      "remark": "关注用户ID" }],


    "url": "/un_follow_user.html" },


  "Client.User.FollowUser": {
    "name": "关注用户",
    "id": "Client.User.FollowUser",
    "method": "post",
    "args": [

    {
      "name": "followPlatformUserId",
      "type": "int",
      "remark": "关注用户ID" }],


    "url": "/follow_user.html" },


  "Client.User.FensiUsers": {
    "name": "用户粉丝",
    "id": "Client.User.FensiUsers",
    "method": "get",
    "args": [

    {
      "name": "platformUserId",
      "type": "int",
      "remark": "平台用户ID" },


    {
      "name": "page",
      "type": "int",
      "remark": "页数" }],


    "url": "/get_fensi_users.html" },



  "Client.Brand.GetBrandDetail": {
    "name": "获取品牌详情",
    "id": "Client.Brand.GetBrandDetail",
    "method": "get",
    "args": [

    {
      "name": "brandId",
      "type": "int",
      "remark": "品牌id" }],


    "url": "/get_brand_detail.html" },



  "Client.Brand.GetBrandList": {
    "name": "获取品牌",
    "id": "Client.Brand.GetBrandList",
    "method": "get",
    "args": [

    {
      "name": "page",
      "type": "int",
      "remark": "页数" },
    {
      "name": "brandName",
      "type": "String",
      "remark": "品牌名" }],


    "url": "/get_brand_list.html" },



  "Client.User.Messages": {
    "name": "用户消息",
    "id": "Client.User.Messages",
    "method": "get",
    "args": [

    {
      "name": "page",
      "type": "int",
      "remark": "页数" }],


    "url": "/get_user_messages.html" },




  "Client.User.MessageCounters": {
    "name": "用户消息统计记录",
    "id": "Client.User.MessageCounters",
    "method": "get",
    "args": [

    {
      "name": "page",
      "type": "int",
      "remark": "页数" },
    {
      "name": "targetType",
      "type": "int",
      "remark": "目标类型" }],


    "url": "/get_user_message_counters.html" },


  "Client.User.MessageTypes": {
    "name": "用户消息类型",
    "id": "Client.User.MessageTypes",
    "method": "get",
    "args": [],



    "url": "/get_user_message_types.html" },


  "Client.Order.GetEditOrderDetail": {
    "name": "获得编辑订单页详细",
    "id": "Client.Order.GetEditOrderDetail",
    "method": "get",
    "args": [{
      "name": "orderNo",
      "type": "String",
      "remark": "订单号" },
    {
      "name": "gotCouponListId",
      "type": "int",
      "remark": "用户领取优惠券id" },

    {
      "name": "expressNo",
      "type": "String",
      "remark": "快递策略号 -1 系统自动选择    0 不要策略到店自取" }],





    "url": "/get_edit_order_detail.html" },


  "Client.Order.ChangeOrderItemCount": {
    "name": "修改订单项数目",
    "id": "Client.Order.ChangeOrderItemCount",
    "method": "post",
    "args": [{
      "name": "count",
      "type": "int",
      "remark": "数目" },

    {
      "name": "itemId",
      "type": "int",
      "remark": "订单项ID" }],



    "url": "/change_order_item_count.html" },


  "Client.Order.CommentOrder": {
    "name": "订单评论（含店铺评论及产品评论）",
    "id": "Client.Order.CommentOrder",
    "method": "post",
    "args": [

    {
      "name": "orderNo",
      "type": "String",
      "remark": "订单号" },
    {
      "name": "shopId",
      "type": "int",
      "remark": "店铺ID  " },
    {
      "name": "productId",
      "type": "int",
      "remark": "产品ID  " },
    {
      "name": "commentContent",
      "type": "String",
      "remark": "评论" },
    {
      "name": "commentImages",
      "type": "String",
      "remark": "评论图片  " },
    {
      "name": "niming",
      "type": "int",
      "remark": "匿名  " },
    {
      "name": "pingfen",
      "type": "int",
      "remark": "评分  " },
    {
      "name": "shangpinfuhedu",
      "type": "int",
      "remark": "商品符合度  " },
    {
      "name": "dianjiafuwutaidu",
      "type": "int",
      "remark": "点击服务态度  " },
    {
      "name": "wuliufahuosudu",
      "type": "int",
      "remark": "物流发货速度  " },
    {
      "name": "peisongyuanfuwutaidu",
      "type": "int",
      "remark": "配送服务态度" }],


    "url": "/comment_order.html" },

  "Client.User.ReadMessage": {
    "name": "阅读用户消息",
    "id": "Client.User.ReadMessage",
    "method": "post",
    "args": [{
      "name": "messageId",
      "type": "int",
      "remark": "消息ID" }],

    "url": "/read_user_message.html" },


  "Client.User.SendUserMessage": {
    "name": "用户向用户发送消息",
    "id": "Client.User.SendUserMessage",
    "method": "post",
    "args": [{
      "name": "content",
      "type": "String",
      "remark": "消息内容" },
    {
      "name": "targetPlatformUserId",
      "type": "int",
      "remark": "目标用户平台ID" }],

    "url": "/send_user_message.html" },



  "Client.User.DaidingProduct": {
    "name": "待定产品",
    "id": "Client.User.DaidingProduct",
    "method": "post",
    "args": [{
      "name": "productId",
      "type": "int",
      "remark": "产品ID" }],

    "url": "/daiding_product.html" },



  "Client.User.CancelDaidingProduct": {
    "name": "取消待定产品",
    "id": "Client.User.CancelDaidingProduct",
    "method": "post",
    "args": [{
      "name": "productId",
      "type": "int",
      "remark": "产品ID" }],

    "url": "/cancel_daiding_product.html" },



  "Client.User.DaidingProductList": {
    "name": "待定产品列表",
    "id": "Client.User.DaidingProductList",
    "method": "get",
    "args": [{
      "name": "page",
      "type": "int",
      "remark": "页数" }],

    "url": "/daiding_product_list.html" },


  "Client.ShopOrder.OrderShopCommentPagePartial": {
    "name": "获得店铺评论页面",
    "id": "Client.ShopOrder.OrderShopCommentPagePartial",
    "method": "get",
    "args": [{
      "name": "orderNo",
      "type": "String",
      "remark": "订单号" }],




    "url": "/get_order_shop_comment_page_partial.html" },


  "Client.ShopOrder.OrderProductCommentPagePartial": {
    "name": "获得产品评论页面",
    "id": "Client.ShopOrder.OrderProductCommentPagePartial",
    "method": "get",
    "args": [{
      "name": "orderItemId",
      "type": "int",
      "remark": "订单产品ID" }],




    "url": "/get_order_product_comment_page_partial.html" },


  "Client.Shop.AccountEarnList": {
    "name": "获得店铺账户收入列表",
    "id": "Client.Shop.AccountEarnList",
    "method": "get",
    "args": [{
      "name": "earnStatus",
      "type": "int",
      "remark": "收入状态（0待实现 1 已实现）" },
    {
      "name": "page",
      "type": "int",
      "remark": "页数" }],



    "url": "/get_shop_account_earn_list.html" },

  "Client.Shop.AccountEventList": {
    "name": "获得店铺账户事件记录",
    "id": "Client.Shop.AccountEventList",
    "method": "get",
    "args": [{
      "name": "page",
      "type": "int",
      "remark": "页数" }],




    "url": "/get_shop_account_event_list.html" },


  "Client.Shop.AccountInfo": {
    "name": "获得店铺账户信息",
    "id": "Client.Shop.AccountInfo",
    "method": "get",
    "args": [],




    "url": "/get_shop_account_info.html" } }, _defineProperty(_Client, "Client.Order.GetEditOrderDetail",

{
  "name": "获得编辑订单页详细",
  "id": "Client.Order.GetEditOrderDetail",
  "method": "get",
  "args": [{
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" }],




  "url": "/get_edit_order_detail.html" }), _defineProperty(_Client,


"Client.Order.GetOrderDetail", {
  "name": "获得编辑订单页详细",
  "id": "Client.Order.GetOrderDetail",
  "method": "get",
  "args": [{
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" },
  {
    "name": "gotCouponListId",
    "type": "int",
    "remark": "领取优惠券id" }],




  "url": "/get_order_detail.html" }), _defineProperty(_Client,



"Client.Order.SubmitOrder", {
  "name": "提交订单",
  "id": "Client.Order.SubmitOrder",
  "method": "post",
  "args": [{
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" },

  {
    "name": "addressId",
    "type": "int",
    "remark": "地址ID" },

  {
    "name": "buyerScript",
    "type": "String",
    "remark": "留言" },

  {
    "name": "payType",
    "type": "int",
    "remark": "支付类型" },

  {
    "name": "jifenDikou",
    "type": "int",
    "remark": "积分抵扣" }],





  "url": "/submit_order.html" }), _defineProperty(_Client,





"Client.Order.OrderUnshow", {
  "name": "订单删除",
  "id": "Client.Order.OrderUnshow",
  "method": "post",
  "args": [{
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" }],

  "url": "/order_unshow.html" }), _defineProperty(_Client,


"Client.Order.OrderReceived", {
  "name": "订单到货",
  "id": "Client.Order.OrderReceived",
  "method": "post",
  "args": [{
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" }],

  "url": "/order_received.html" }), _defineProperty(_Client,


"Client.Order.SetHuikuanImage", {
  "name": "设置汇款截图",
  "id": "Client.Order.SetHuikuanImage",
  "method": "post",
  "args": [{
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" },
  {
    "name": "huikuanImage",
    "type": "String",
    "remark": "汇款截图" }],

  "url": "/set_huikuan_image.html" }), _defineProperty(_Client,


"Client.Order.FinishedOrder", {
  "name": "订单结束",
  "id": "Client.Order.FinishedOrder",
  "method": "post",
  "args": [{
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" }],

  "url": "/order_finished.html" }), _defineProperty(_Client,


"Client.Order.CancelOrder", {
  "name": "取消订单",
  "id": "Client.Order.CancelOrder",
  "method": "post",
  "args": [{
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" }],

  "url": "/cancel_order.html" }), _defineProperty(_Client,

"Client.Order.PressOrder", {
  "name": "催促订单",
  "id": "Client.Order.PressOrder",
  "method": "post",
  "args": [{
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" }],

  "url": "/press_order.html" }), _defineProperty(_Client,

"Client.Order.AccountPay", {
  "name": "余额支付",
  "id": "Client.Order.AccountPay",
  "method": "post",
  "args": [{
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" }],




  "url": "/order_account_pay.html" }), _defineProperty(_Client,



"Client.Order.JifenPay", {
  "name": "积分支付",
  "id": "Client.Order.JifenPay",
  "method": "post",
  "args": [{
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" }],




  "url": "/order_jifen_pay.html" }), _defineProperty(_Client,




"Client.Order.OrderList", {
  "name": "订单列表",
  "id": "Client.Order.OrderList",
  "method": "get",
  "args": [{
    "name": "page",
    "type": "int",
    "remark": "页数" },

  {
    "name": "easyStatus",
    "type": "String",
    "remark": "订单状态" },

  {
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" }],




  "url": "/get_order_list.html" }), _defineProperty(_Client,



"Client.ShopOrder.OrderList", {
  "name": "订单列表",
  "id": "Client.ShopOrder.OrderList",
  "method": "get",
  "args": [{
    "name": "page",
    "type": "int",
    "remark": "页数" },

  {
    "name": "easyStatus",
    "type": "String",
    "remark": "订单状态" },

  {
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" }],




  "url": "/get_shop_order_list.html" }), _defineProperty(_Client,


"Client.Order.WaitCommentProductList", {
  "name": "待评价订单产品",
  "id": "Client.Order.WaitCommentProductList",
  "method": "get",
  "args": [{
    "name": "page",
    "type": "int",
    "remark": "页数" }],

  "url": "/get_wait_comment_product_list.html" }), _defineProperty(_Client,



"Client.Order.CommentList", {
  "name": "订单评论列表",
  "id": "Client.Order.CommentList",
  "method": "get",
  "args": [{
    "name": "productId",
    "type": "int",
    "remark": "产品ID" },
  {
    "name": "shopId",
    "type": "int",
    "remark": "店铺ID" }],

  "url": "/get_product_comment_list.html" }), _defineProperty(_Client, "Client.Order.OrderReceived",

{
  "name": "订单到货",
  "id": "Client.Order.OrderReceived",
  "method": "post",
  "args": [{
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" }],

  "url": "/order_received.html" }), _defineProperty(_Client,

"Client.User.AddToFavorite", {
  "name": "加入收藏",
  "id": "Client.User.AddToFavorite",
  "method": "post",
  "args": [{
    "name": "favoriteType",
    "type": "int",
    "remark": "收藏类型" },

  {
    "name": "itemId",
    "type": "int",
    "remark": "收藏对象Id" }],



  "url": "/add_to_favorite.html" }), _defineProperty(_Client,







"Client.User.RemoveFavorite", {
  "name": "取消收藏",
  "id": "Client.User.RemoveFavorite",
  "method": "post",
  "args": [{
    "name": "favoriteType",
    "type": "int",
    "remark": "收藏类型" },

  {
    "name": "itemId",
    "type": "int",
    "remark": "取消收藏对象Id" }],



  "url": "/remove_favorite.html" }), _defineProperty(_Client,



"Client.User.ListAccountEvent", {
  "name": "获得余额变更记录",
  "id": "Client.User.ListAccountEvent",
  "method": "get",
  "args": [{
    "name": "page",
    "type": "int",
    "remark": "页数" }],



  "url": "/get_user_account_events.html" }), _defineProperty(_Client,




"Client.User.ListJifenEvent", {
  "name": "获得积分变更记录",
  "id": "Client.User.ListJifenEvent",
  "method": "get",
  "args": [{
    "name": "page",
    "type": "int",
    "remark": "页数" }],



  "url": "/get_user_jifen_events.html" }), _defineProperty(_Client,


"Client.Get.Favorite", {
  "name": "获得收藏",
  "id": "Client.Get.Favorite",
  "method": "get",
  "args": [{
    "name": "favoriteType",
    "type": "int",
    "remark": "收藏类型" },

  {
    "name": "page",
    "type": "int",
    "remark": "页数" }],



  "url": "/get_favorite.html" }), _defineProperty(_Client,






"Client.Yyg.BuyNow", {
  "name": "立即购买",
  "id": "Client.Yyg.BuyNow",
  "method": "all",
  "args": [{
    "name": "count",
    "type": "int",
    "remark": "数量" },

  {
    "name": " Id",
    "type": "int",
    "remark": "一元购ID" }],



  "url": "/yyg_buy_now.html" }), _defineProperty(_Client,





"Client.Yyg.GetAttendYiyuangouList", {
  "name": "获得参与本期一元购记录",
  "id": "Client.Yyg.GetAttendYiyuangouList",
  "method": "all",
  "args": [{
    "name": "yygId",
    "type": "int",
    "remark": "一元购Id  " },
  {
    "name": "loginUser",
    "type": "int",
    "remark": "记录限制为登陆用户 0 否 1 是  " }],



  "url": "/get_attend_yiyuangou_list.html" }), _defineProperty(_Client,



"Client.Yyg.SetAddress", {
  "name": "设置地址",
  "id": "Yyg.SetAddress",
  "method": "post",
  "args": [{
    "name": "addressId",
    "type": "int",
    "remark": "地址ID" },

  {
    "name": " yiyuangouId",
    "type": "int",
    "remark": "一元购ID" }],



  "url": "/set_yyg_address.html" }), _defineProperty(_Client,



"Client.Yyg.ListMore", {
  "name": "一元购列表",
  "id": "Client.Yyg.ListMore",
  "method": "get",
  "args": [{
    "name": "productId",
    "type": "int",
    "remark": "产品ID" },

  {
    "name": "searchPrice",
    "type": "Decimal",
    "remark": "价格" },

  {
    "name": "status",
    "type": "int",
    "remark": "状态" },

  {
    "name": "yiyuangouType",
    "type": "String",
    "remark": "一元购类型" },

  {
    "name": "page",
    "type": "int",
    "remark": "页数" }],



  "url": "/yyg_list_more.html" }), _defineProperty(_Client,





"Client.Yyg.MyListMore", {
  "name": "我的一元购列表",
  "id": "Client.Yyg.MyListMore",
  "method": "get",
  "args": [],


  "url": "/my_yyg_list_more.html" }), _defineProperty(_Client,



"Client.Fx.GetFxUsers", {
  "name": "分销用户列表",
  "id": "Client.Fx.GetFxUsers",
  "method": "get",
  "args": [{
    "name": "page",
    "type": "int",
    "remark": "页数" },

  {
    "name": "fxLevel",
    "type": "int",
    "remark": "分销级数" }],


  "url": "/get_fx_tg_users.html" }), _defineProperty(_Client,



"Client.Fx.GetFxYongjinList", {
  "name": "分销佣金列表",
  "id": "Client.Fx.GetFxYongjinList",
  "method": "get",
  "args": [{
    "name": "page",
    "type": "int",
    "remark": "页数" }],

  "url": "/get_fx_yongjin_list.html" }), _defineProperty(_Client,




"Client.Fx.GetTixianList", {
  "name": "获取提现记录",
  "id": "Client.Fx.GetTixianList",
  "method": "get",
  "args": [{
    "name": "page",
    "type": "int",
    "remark": "页数" }],

  "url": "/get_tixian_list.html" }), _defineProperty(_Client,



"Client.Fx.ReqTixian", {
  "name": "请求提现",
  "id": "Client.Fx.ReqTixian",
  "method": "post",
  "args": [{
    "name": "amount",
    "type": "BigDecimal",
    "remark": "请求提现金额" }],

  "url": "/req_tixian.html" }), _defineProperty(_Client,




"Client.Zhongchou.GetZhongchouMission", {
  "name": "获取众筹任务",
  "id": "Client.Zhongchou.GetZhongchouMission",
  "method": "get",
  "args": [{
    "name": "zcTopicId",
    "type": "int",
    "remark": "众筹主题ID" },

  {
    "name": "missionUserId",
    "type": "int",
    "remark": "参与任务用户ID" }],


  "url": "/get_user_zc_mission.html" }), _defineProperty(_Client,



"Client.Zhongchou.PublishMission", {
  "name": "发布众筹任务",
  "id": "Client.Zhongchou.PublishMission",
  "method": "post",
  "args": [

  {
    "name": "missionId ",
    "type": "int",
    "remark": "任务ID" },
  {
    "name": "kouhao",
    "type": "String",
    "remark": "口号" },
  {
    "name": "headimage",
    "type": "String",
    "remark": "头像" },
  {
    "name": "telno",
    "type": "String",
    "remark": "电话" },
  {
    "name": "ganyan",
    "type": "String",
    "remark": "感言" }],


  "url": "/publish_mission.html" }), _defineProperty(_Client,



"Client.Zhongchou.HelpMission", {
  "name": "发布众筹任务",
  "id": "Client.Zhongchou.HelpMission",
  "method": "post",
  "args": [

  {
    "name": "missionId ",
    "type": "int",
    "remark": "任务ID" }],


  "url": "/help_mission.html" }), _defineProperty(_Client,


"Client.UploadImage", {
  "name": "上传文件",
  "id": "Client.UploadImage",
  "method": "post",
  "args": [{
    "name": "imgName",
    "type": "String",
    "remark": "图片名" },

  {
    "name": "imgStr",
    "type": "String",
    "remark": "Base64格式图片" }],


  "url": "/upload_file.html" }), _defineProperty(_Client,



"Client.Product.GetMeasureCartesion", {
  "name": "获取产品规格集信息",
  "id": "Client.Product.GetMeasureCartesion",
  "method": "get",
  "args": [{
    "name": "productId",
    "type": "int",
    "remark": "产品ID" },

  {
    "name": "measureIds",
    "type": "String",
    "remark": "规格集ID" }],


  "url": "/get_measure_cartesion.html" }), _defineProperty(_Client,




"Client.Product.ProductTabView", {
  "name": "获取产品类型tab视图",
  "id": "Client.Product.ProductTabView",
  "method": "get",
  "args": [{
    "name": "productTypeId",
    "type": "int",
    "remark": "产品类型ID" }],

  "url": "/get_product_tab_view.html" }), _defineProperty(_Client,



"Client.Product.ProductTreeView", {
  "name": "获取产品类型tree视图",
  "id": "Client.Product.ProductTreeView",
  "method": "get",
  "args": [{
    "name": "productTypeId",
    "type": "int",
    "remark": "产品类型ID" }],

  "url": "/get_product_tree_list_view.html" }), _defineProperty(_Client,


"Client.DefinedItemOrder.List", {
  "name": "获得用户定义订单",
  "id": "Client.DefinedItemOrder.List",
  "method": "get",
  "args": [{
    "name": "page",
    "type": "int",
    "remark": "页数" }],



  "url": "/get_user_defined_item_order.html" }), _defineProperty(_Client,


"Client.DefinedItemOrder.Detail", {
  "name": "获得用户定义订单",
  "id": "Client.DefinedItemOrder.Detail",
  "method": "get",
  "args": [{
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" }],



  "url": "/get_user_defined_item_order_detail.html" }), _defineProperty(_Client,

"Client.DefinedItemOrder.Req", {
  "name": "用户请求定义订单",
  "id": "Client.DefinedItemOrder.Req",
  "method": "post",
  "args": [{
    "name": "itemName",
    "type": "String",
    "remark": "请求项目名" },

  {
    "name": "itemDescription",
    "type": "String",
    "remark": "请求项目描述" },

  {
    "name": "itemIcon",
    "type": "String",
    "remark": "请求项目图片" },

  {
    "name": "payType",
    "type": "int",
    "remark": "请求平台支付方式" },

  {
    "name": "itemPrice",
    "type": "BigDecimal",
    "remark": "请求平台支付金额" },

  {
    "name": "relateProductId",
    "type": "int",
    "remark": "请求项目关联产品" },

  {
    "name": "relateShopId",
    "type": "int",
    "remark": "请求项目关联店铺" }],



  "url": "/req_user_defined_item_order.html" }), _defineProperty(_Client,

"Client.DefinedItemOrder.ChangeOrderPrice", {
  "name": "修改用户定义订单价格",
  "id": "Client.DefinedItemOrder.ChangeOrderPrice",
  "method": "post",
  "args": [{
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" },

  {
    "name": "itemPrice",
    "type": "BigDecimal",
    "remark": "订单价格" }],




  "url": "/change_user_defined_order_price.html" }), _defineProperty(_Client,


"Client.TargetLike.Like", {
  "name": "点赞",
  "id": "Client.TargetLike.Like",
  "method": "post",
  "args": [{
    "name": "targetType",
    "type": "int",
    "remark": "目标类型" },

  {
    "name": "targetId",
    "type": "int",
    "remark": "目标ID" }],



  "url": "/target_like.html" }), _defineProperty(_Client,



"Client.TargetLike.Dislike", {
  "name": "取消点赞",
  "id": "Client.TargetLike.Dislike",
  "method": "post",
  "args": [{
    "name": "targetType",
    "type": "int",
    "remark": "目标类型" },

  {
    "name": "targetId",
    "type": "int",
    "remark": "目标ID" }],


  "url": "/target_dislike.html" }), _defineProperty(_Client,



"Client.ProductDetail.GetProductDetailShopView", {
  "name": "获取产品详情（店铺视图)",
  "id": "Client.ProductDetail.GetProductDetailShopView",
  "method": "get",
  "args": [{
    "name": "productId",
    "type": "int",
    "remark": "产品ID" }],

  "url": "/get_product_detail_shop_view.html" }), _defineProperty(_Client,




"Client.ChatRoom.UserChatRooms", {
  "name": "获取用户聊天室",
  "id": "Client.ChatRoom.UserChatRooms",
  "method": "get",
  "args": [

  {
    "name": "page",
    "type": "int",
    "remark": "页面" }],


  "url": "/get_user_chat_rooms.html" }), _defineProperty(_Client,


"Client.ChatRoom.CreateOrGetDialogChatRoom", {
  "name": "获取用户对话聊天室",
  "id": "Client.ChatRoom.CreateOrGetDialogChatRoom",
  "method": "post",
  "args": [

  {
    "name": "platformUserIdOne",
    "type": "int",
    "remark": "对话用户ID1" },

  {
    "name": "platformUserIdTwo",
    "type": "int",
    "remark": "对话用户ID2" }],


  "url": "/create_or_get_dialog_chat_room.html" }), _defineProperty(_Client,


"Client.ChatRoom.ChatRoomMessageRecords", {
  "name": "获取聊天室对话信息",
  "id": "Client.ChatRoom.ChatRoomMessageRecords",
  "method": "get",
  "args": [{
    "name": "roomId",
    "type": "int",
    "remark": "聊天室ID" },

  {
    "name": "page",
    "type": "int",
    "remark": "页面" }],


  "url": "/get_chat_room_message_records.html" }), _defineProperty(_Client,

"Client.ChatRoom.SendChatRoomMessage", {
  "name": "用户向聊天室发送消息",
  "id": "Client.ChatRoom.SendChatRoomMessage",
  "method": "all",
  "args": [{
    "name": "content",
    "type": "String",
    "remark": "消息内容" },
  {
    "name": "roomId",
    "type": "int",
    "remark": "房间ID" }],

  "url": "/send_chat_room_message.html" }), _defineProperty(_Client,


"Client.User.BackItemList", {
  "name": "获得用户可退货列表",
  "id": "Client.User.BackItemList",
  "method": "get",
  "args": [

  {
    "name": "page",
    "type": "int",
    "remark": "页数" },

  {
    "name": "platformNo",
    "type": "String",
    "remark": "平台号" },
  {
    "name": "secretCode",
    "type": "String",
    "remark": "口令" }],



  "url": "/get_back_item_list.html" }), _defineProperty(_Client,

"Client.User.BackOrderItemPage", {
  "name": "退货页",
  "id": "Client.User.BackOrderItemPage",
  "method": "get",
  "args": [

  {
    "name": "orderItemId",
    "type": "int",
    "remark": "订单项ID" },

  {
    "name": "platformNo",
    "type": "String",
    "remark": "平台号" },
  {
    "name": "secretCode",
    "type": "String",
    "remark": "口令" }],



  "url": "/get_back_order_item_page.html" }), _defineProperty(_Client,

"Client.User.SendBackOrderItemReq", {
  "name": "发送退款请求",
  "id": "Client.User.SendBackOrderItemReq",
  "method": "post",
  "args": [

  {
    "name": "orderItemId",
    "type": "int",
    "remark": "订单项ID" },
  {
    "name": "backReason",
    "type": "String",
    "remark": "退款理由" }],




  "url": "/send_back_order_item_req.html" }), _defineProperty(_Client,

"Client.ShopManager.AcceptOrRejectBackOrderItemAmount", {
  "name": "同意或拒绝退款",
  "id": "Client.ShopManager.AcceptOrRejectBackOrderItemAmount",
  "method": "post",
  "args": [{
    "name": "acceptStatus",
    "type": "int",
    "remark": "1 同意  2 拒绝" },

  {
    "name": "shopBackAmountId",
    "type": "int",
    "remark": "退款ID" },
  {
    "name": "rejectReason",
    "type": "String",
    "remark": "拒绝理由" },

  {
    "name": "platformNo",
    "type": "String",
    "remark": "平台号" },
  {
    "name": "secretCode",
    "type": "String",
    "remark": "口令" }],



  "url": "/accept_or_reject_back_order_item_amount.html" }), _defineProperty(_Client,

"Client.User.ShopBackAmountList", {
  "name": "获得用户管理店铺退货请求",
  "id": "Client.User.ShopBackAmountList",
  "method": "get",
  "args": [

  {
    "name": "page",
    "type": "int",
    "remark": "页数" }],



  "url": "/get_shop_back_amount_list.html" }), _defineProperty(_Client,



"Client.Coupon.AvailableCoupons", {
  "name": "获取可领取的优惠券列表",
  "id": "Client.Coupon.AvailableCoupons",
  "method": "get",
  "args": [

  {
    "name": "page",
    "type": "int",
    "remark": "页数" }],



  "url": "/get_available_coupons.html" }), _defineProperty(_Client,



"Client.Coupon.GainCoupon", {
  "name": "领取优惠券",
  "id": "Client.Coupon.GainCoupon",
  "method": "post",
  "args": [

  {
    "name": "couponId",
    "type": "int",
    "remark": "优惠券ID" },
  {
    "name": "couponSecretCode",
    "type": "String",
    "remark": "优惠券码" },
  {
    "name": "couponSecretPassword",
    "type": "String",
    "remark": "优惠券密码" }],



  "url": "/gain_coupon.html" }), _defineProperty(_Client,


"Client.Coupon.MyCoupons", {
  "name": "我的优惠券",
  "id": "Client.Coupon.MyCoupons",
  "method": "get",
  "args": [

  {
    "name": "page",
    "type": "int",
    "remark": "页数" },

  {
    "name": "couponState",
    "type": "int",
    "remark": "优惠券状态" }],



  "url": "/get_my_coupons_list.html" }), _defineProperty(_Client,

"Client.ChatRoom.DownloadWxAudio", {
  "name": "下载微信语音",
  "id": "Client.ChatRoom.DownloadWxAudio",
  "method": "post",
  "args": [

  {
    "name": "resWxServerId",
    "type": "int",
    "remark": "资源ID" }],



  "url": "/download_wx_res.html" }), _defineProperty(_Client,

"Client.Qiandao.Qiandao", {
  "name": "签到",
  "id": "Client.Qiandao.Qiandao",
  "method": "post",
  "args": [

  {
    "name": "qiandaoType",
    "type": "String",
    "remark": "签到类型" }],



  "url": "/qiandao.html" }), _defineProperty(_Client,


"Client.Qiandao.YuQiandao", {
  "name": "预签到",
  "id": "Client.Qiandao.YuQiandao",
  "method": "all",
  "args": [

  {
    "name": "qiandaoType",
    "type": "String",
    "remark": "签到类型" },

  {
    "name": "qiandaoCellDateStr",
    "type": "String",
    "remark": "被签到日期" }],



  "url": "/yu_qiandao.html" }), _defineProperty(_Client,


"Client.Qiandao.CancelYuQiandao", {
  "name": "取消预签到",
  "id": "Client.Qiandao.CancelYuQiandao",
  "method": "all",
  "args": [

  {
    "name": "qiandaoType",
    "type": "String",
    "remark": "签到类型" },

  {
    "name": "qiandaoCellDateStr",
    "type": "String",
    "remark": "被签到日期" }],



  "url": "/cancel_yu_qiandao.html" }), _defineProperty(_Client,

"Client.Qiandao.MonthQiandaoData", {
  "name": "获取月份签到数据",
  "id": "Client.Qiandao.MonthQiandaoData",
  "method": "get",
  "args": [

  {
    "name": "qiandaoType",
    "type": "String",
    "remark": "签到类型" },

  {
    "name": "year",
    "type": "int",
    "remark": "年份" },
  {
    "name": "month",
    "type": "int",
    "remark": "月份" }],



  "url": "/month_qiandao_data.html" }), _defineProperty(_Client,

"Client.Qiandao.GetQiandaoRecordsList", {
  "name": "获取用户签到数据",
  "id": "Client.Qiandao.GetQiandaoRecordsList",
  "method": "get",
  "args": [

  {
    "name": "qiandaoType",
    "type": "String",
    "remark": "签到类型" },

  {
    "name": "platformUserId",
    "type": "int",
    "remark": "平台用户ID" },

  {
    "name": "year",
    "type": "int",
    "remark": "年份" },
  {
    "name": "month",
    "type": "int",
    "remark": "月份" },
  {
    "name": "day",
    "type": "int",
    "remark": "日" },
  {
    "name": "week",
    "type": "int",
    "remark": "周" },
  {
    "name": "recordType",
    "type": "int",
    "remark": "查询记录类型 0年 1月 2 周 3 日" }],



  "url": "/get_qiandao_records_list.html" }), _defineProperty(_Client,

"Client.Qiandao.QiandaoComment", {
  "name": "签到评论",
  "id": "Client.Qiandao.QiandaoComment",
  "method": "post",
  "args": [{
    "name": "qiandaoItemId",
    "type": "int",
    "remark": "签到项ID" },
  {
    "name": "comment",
    "type": "String",
    "remark": "评价" },
  {
    "name": "pingfen",
    "type": "int",
    "remark": "评分" },
  {
    "name": "niming",
    "type": "int",
    "remark": "匿名" }],

  "url": "/comment_qiandao_item.html" }), _defineProperty(_Client,

"Client.Qiandao.GetQiandaoCommentPage", {
  "name": "获取签到评论页",
  "id": "Client.Qiandao.GetQiandaoCommentPage",
  "method": "get",
  "args": [{
    "name": "year",
    "type": "int",
    "remark": "年份" },
  {
    "name": "month",
    "type": "int",
    "remark": "月" },
  {
    "name": "day",
    "type": "int",
    "remark": "日" }],



  "url": "/get_qiandao_comment_page.html" }), _defineProperty(_Client,


"Client.Qiandao.GetQiandaoMonthPage", {
  "name": "获取每月签到页面",
  "id": "Client.Qiandao.GetQiandaoMonthPage",
  "method": "get",
  "args": [{
    "name": "year",
    "type": "int",
    "remark": "年份" },
  {
    "name": "month",
    "type": "int",
    "remark": "月" }],



  "url": "/get_qiandao_month_page.html" }), _defineProperty(_Client,

"Client.Qiandao.GetWeekQiandaoPage", {
  "name": "获取每周签到页面",
  "id": "Client.Qiandao.GetWeekQiandaoPage",
  "method": "get",
  "args": [{
    "name": "year",
    "type": "int",
    "remark": "年份" },
  {
    "name": "week",
    "type": "int",
    "remark": "周" }],



  "url": "/get_week_qiandao_page.html" }), _defineProperty(_Client,

"Client.Qiandao.GetWeekQiandaoTongjiPage", {
  "name": "获取每周签到统计页面",
  "id": "Client.Qiandao.GetWeekQiandaoTongjiPage",
  "method": "get",
  "args": [{
    "name": "year",
    "type": "int",
    "remark": "年份" },
  {
    "name": "week",
    "type": "int",
    "remark": "周" }],



  "url": "/get_week_tongji_page.html" }), _defineProperty(_Client,

"Client.User.GuestBook", {
  "name": "用户登记",
  "id": "Client.User.GuestBook",
  "method": "post",
  "args": [

  {
    "name": "name",
    "type": "String",
    "remark": "名字" },

  {
    "name": "bookAddress",
    "type": "String",
    "remark": "地址" },
  {
    "name": "content",
    "type": "String",
    "remark": "内容" },
  {
    "name": "telno",
    "type": "String",
    "remark": "电话" },
  {
    "name": "tags",
    "type": "String",
    "remark": "标签" },
  {
    "name": "bookType",
    "type": "String",
    "remark": "登记类型" }],



  "url": "/user_guest_book.html" }), _defineProperty(_Client,


"Client.Weixin.UnifinedOrder", {
  "name": "统一下单",
  "id": "Client.Weixin.UnifinedOrder",
  "method": "post",
  "args": [

  {
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" },


  {
    "name": "openid",
    "type": "String",
    "remark": "微信用户openid(app不需要 公众号存放session中的也不需要）" },


  {
    "name": "app",
    "type": "int",
    "remark": "是否app 0 否  1 是）" }],





  "url": "/unifined_order.html" }), _defineProperty(_Client,


"Client.Weixin.GetQrcode", {
  "name": "获取二维码",
  "id": "Client.Weixin.GetQrcode",
  "method": "all",
  "args": [],



  "url": "/get_qrcode.html" }), _defineProperty(_Client,


"Client.Weixin.GetWxConfig", {
  "name": "获得微信jsapi配置",
  "id": "Client.Weixin.GetWxConfig",
  "method": "all",
  "args": [

  {
    "name": "configUrl",
    "type": "String",
    "remark": "配置URL" }],





  "url": "/get_wx_config.html" }), _defineProperty(_Client,


"Client.Alipay.AlipayApp", {
  "name": "支付宝app支付",
  "id": "Client.Alipay.AlipayApp",
  "method": "post",
  "args": [

  {
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" }],





  "url": "/alipay_app.html" }), _defineProperty(_Client,


"Client.Alipay.AlipayPage", {
  "name": "支付宝Page支付",
  "id": "Client.Alipay.AlipayPage",
  "method": "post",
  "args": [

  {
    "name": "orderNo",
    "type": "String",
    "remark": "订单号" }],





  "url": "/alipay_page.html" }), _defineProperty(_Client,

"Client.AfterSale.DeleteAfterSaleOrder", {
  "name": "删除售后case",
  "id": "Client.AfterSale.DeleteAfterSaleOrder",
  "method": "post",
  "args": [{
    "name": "afterSaleOrderId",
    "type": "int",
    "remark": "售后工单id" }],

  "url": "/delete_after_sale_order.html" }), _defineProperty(_Client,


"Client.AfterSale.CreateAfterSaleOrder", {
  "name": "创建售后工单",
  "id": "Client.AfterSale.CreateAfterSaleOrder",
  "method": "all",
  "args": [

  {
    "name": "afterSaleCaseId",
    "type": "int",
    "remark": "售后case id" },


  {
    "name": "productCount",
    "type": "int",
    "remark": "产品数量" },


  {
    "name": "contactName",
    "type": "String",
    "remark": "联系人" },


  {
    "name": "contactProvince",
    "type": "String",
    "remark": "省" },


  {
    "name": "contactCity",
    "type": "String",
    "remark": "市" },


  {
    "name": "contactArea",
    "type": "String",
    "remark": "县" },


  {
    "name": "contactTelno",
    "type": "String",
    "remark": "电话" },


  {
    "name": "contactAddress",
    "type": "String",
    "remark": "详细地址" }],





  "url": "/create_after_sale_order.html" }), _defineProperty(_Client,


"Client.AfterSale.GetAfterSaleFeedbackPhase", {
  "name": "获取分步编辑信息",
  "id": "Client.AfterSale.GetAfterSaleFeedbackPhase",
  "method": "get",
  "args": [

  {
    "name": "orderNo",
    "type": "String",
    "remark": "售后工单号" },

  {
    "name": "afterSaleCaseId",
    "type": "int",
    "remark": "售后caseid" },

  {
    "name": "afterSalePhaseId",
    "type": "int",
    "remark": "售后步骤id" }],



  "url": "/get_after_sale_feedback_phase.html" }), _defineProperty(_Client,


"Client.AfterSale.SetAfterSaleOrderExpress", {
  "name": "提交售后单物流信息",
  "id": "Client.AfterSale.SetAfterSaleOrderExpress",
  "method": "post",
  "args": [

  {
    "name": "orderNo",
    "type": "String",
    "remark": "工单号" },

  {
    "name": "expressCompanyName",
    "type": "String",
    "remark": "物流公司" },

  {
    "name": "expressNo",
    "type": "String",
    "remark": "物流单号" }],



  "url": "/set_after_sale_order_express.html" }), _defineProperty(_Client,


"Client.AfterSale.SubmitAfterSaleOrder", {
  "name": "提交售后工单",
  "id": "Client.AfterSale.SubmitAfterSaleOrder",
  "method": "post",
  "args": [

  {
    "name": "afterSaleOrderId",
    "type": "int",
    "remark": "工单id" }],


  "url": "/submit_after_sale_order.html" }), _defineProperty(_Client,



"Client.AfterSale.GetUserAfterSaleOrders", {
  "name": "用户售后工单",
  "id": "Client.AfterSale.GetUserAfterSaleOrders",
  "method": "get",
  "args": [

  {
    "name": "orderStatus",
    "type": "int",
    "remark": "工单状态" },
  {
    "name": "page",
    "type": "int",
    "remark": "page" }],



  "url": "/get_user_sale_orders.html" }), _defineProperty(_Client,



"Client.AfterSale.GetAfterSaleProductList", {
  "name": "获得售后产品列表",
  "id": "Client.AfterSale.GetAfterSaleProductList",
  "method": "get",
  "args": [{
    "name": "categoryId",
    "type": "int",
    "remark": "产品类型ID" },

  {
    "name": "page",
    "type": "String",
    "remark": "页数" },

  {
    "name": "belongShop",
    "type": "int",
    "remark": "归属店铺ID" }],



  "url": "/get_after_sale_product_list.html" }), _Client);var _default =





Client;exports.default = _default;

/***/ })

}]);
//# sourceMappingURL=../../.sourcemap/mp-weixin/common/vendor.js.map