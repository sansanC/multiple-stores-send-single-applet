const app = getApp();
var WxParse = require('../../wxParse/wxParse.js');
Component({
  properties: {
    receiveData: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {},
    content:'',
  },
  ready:function(){
    var that = this
    var oldData=that.data.receiveData;
    let content=oldData.jsonData?oldData.jsonData.content:oldData;
    WxParse.wxParse('article', 'html', content, that, 10);
    console.log("article",that.data.article,content)
    app.globalData.consoleFun("=====gridist组件-richText=====",[oldData])
    content = content.replace(/<img/gi, '<img style="max-width:100%;height:auto;float:left;display:block" ')
         								 	.replace(/<section/g, '<div')
         									.replace(/\/section>/g, '\div>');
    console.log("content",content);
    that.setData({content:content})
  },
  methods: {
    tolinkUrl: function (event) {
      app.globalData.linkEvent(event.currentTarget.dataset.link);
    }
  },
})